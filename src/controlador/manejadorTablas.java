/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import boston.conexionDB;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import modelo.Boleta;
import modelo.Cierre_Caja;
import modelo.Detalle_boleta;
import vista.AgregarPago.ModeloTablaPagos;
import vista.CierreCaja;
//import vista.CierreCaja.ModeloTablaFacturacion;
import vista.CierreCajaResumido.ModeloTablaFacturacion;
//import vista.CierreCaja.ModeloTablaGastos;
import vista.CierreCajaResumido.ModeloTablaGastos;
import vista.CierreCaja.ModeloTablaVentaBebida;
import vista.CierreCaja.ModeloTablaVentaComida;
import vista.CierreCaja.ModeloTablaPromociones;
import vista.CierreCaja.ModeloTablaDescuentos;
import vista.CuentasCorrientes.ModeloTablaBoletasAdeudadas;
import vista.CuentasCorrientes.ModeloTablaBoletasCanceladas;
//import vista.CierreCaja.ModeloTablaVentasTipo;
import vista.CierreCajaResumido.ModeloTablaVentasTipo;
import vista.Gestion_Costos.ModeloTablaCostos;
import vista.Inicio;
import vista.Inicio.ModeloTablaInicio;
import vista.Inicio.ModeloTablaMesasOcupadas;
import vista.NuevaMesa;
import vista.nuevaVtaRapida.ModeloTablaVtaRapida;
import vista.NuevaMesa.ModeloNuevoMesa;
import vista.AgregarPromocion.ModeloPromocion;
import vista.Clientes;
import vista.DatosRelevantes.ModeloTablaVentasMozos;
import vista.BusquedaCierres.ModeloTablaBusquedaCierres;
import vista.BusquedaCierres.ModeloTablaUltimoCierre;
import vista.BusquedaCierres.ModeloTablaTotalesCierres;
import vista.CuentasCorrientes.ModeloTablaPagosRealizados;
import vista.Inicio.ModeloTablaDelivery;
import vista.ResumenCierre.ModeloTablaBoletas;
import vista.ResumenCierre.ModeloTablaResumen;
import vista.MesasTrabadas.ModeloTablaMesasTrabadas;

/**
 *
 * @author Rodri Mayer
 */
public class manejadorTablas {

    manejadorFacturacion mf = new manejadorFacturacion();
    manejadorCondVta mcv = new manejadorCondVta();
    manejadorFechas mfe = new manejadorFechas();

    public void traerArticulos(JTable tablaArticulos, DefaultTableModel modelo, int tipo) {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
        String sql = "";

        tablaArticulos.setModel(modelo);
        modelo.addColumn("CODIGO");
        modelo.addColumn("ARTICULO");
        modelo.addColumn("PRECIO");

        tablaArticulos.getColumnModel().getColumn(0).setMaxWidth(100);
        tablaArticulos.getColumnModel().getColumn(0).setMinWidth(100);
        tablaArticulos.getColumnModel().getColumn(0).setPreferredWidth(100);
        tablaArticulos.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);

        tablaArticulos.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);

        tablaArticulos.getColumnModel().getColumn(2).setMaxWidth(75);
        tablaArticulos.getColumnModel().getColumn(2).setMinWidth(75);
        tablaArticulos.getColumnModel().getColumn(2).setPreferredWidth(75);
        tablaArticulos.getColumnModel().getColumn(2).setCellRenderer(rightRenderer);

        Object[] articulo = new Object[3];

        if (tipo == 1) {//traer articulos habilitados
            sql = "SELECT codigo, desc_articulo, precio_vta from Articulo WHERE baja = 0";
        } else {//traer articulos dados de baja
            sql = "SELECT codigo, desc_articulo, precio_vta from Articulo WHERE baja = 1";
        }

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                articulo[0] = rs1.getString(1);
                articulo[1] = rs1.getString(2);
                articulo[2] = rs1.getFloat(3);

                modelo.addRow(articulo);
            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO ARTICULOS: " + e);
        }
    }

    public void busquedaArticulos(DefaultTableModel modelo, String busqueda, int tipo) {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
        String sql = "";

        Object[] articulo = new Object[3];

        if (tipo == 1) {
            sql = "SELECT codigo, desc_articulo, precio_vta from Articulo "
                    + "WHERE desc_articulo LIKE '%" + busqueda + "%' and baja = 0";
        } else {
            sql = "SELECT codigo, desc_articulo, precio_vta from Articulo "
                    + "WHERE desc_articulo LIKE '%" + busqueda + "%' and baja = 1";
        }

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                articulo[0] = rs1.getString(1);
                articulo[1] = rs1.getString(2);
                articulo[2] = rs1.getFloat(3);

                modelo.addRow(articulo);
            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO ARTICULOS: " + e);
        }

    }

    public void busquedaClientes(Clientes.MiModelo modelo, String busqueda) {
        Object[] articulo = new Object[3];

        String sql = "SELECT id_cliente, nombre_cliente, apellido_cliente, telefono from Cliente "
                + "WHERE nombre_cliente LIKE '%" + busqueda + "%' or apellido_cliente LIKE '%" + busqueda + "%'";

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                articulo[0] = rs1.getString(1);
                articulo[1] = rs1.getString(2) + " " + rs1.getString(3);
                articulo[2] = rs1.getString(4);

                modelo.addRow(articulo);
            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO CLIENTES: " + e);
        }

    }

    public void modeloNvaVtaRapida(JTable tablaVtaRapida, ModeloTablaVtaRapida tm) {

        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

        tablaVtaRapida.setModel(tm);

        tm.addColumn("CODIGO");
        tm.addColumn("ARTICULO");
        tm.addColumn("CANTIDAD");
        tm.addColumn("PRECIO UNI");
        tm.addColumn("PRECIO TOTAL");

        tablaVtaRapida.getColumnModel().getColumn(0).setMaxWidth(0);
        tablaVtaRapida.getColumnModel().getColumn(0).setMinWidth(0);
        tablaVtaRapida.getColumnModel().getColumn(0).setPreferredWidth(0);

        tablaVtaRapida.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tablaVtaRapida.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        tablaVtaRapida.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
        tablaVtaRapida.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
        tablaVtaRapida.getColumnModel().getColumn(4).setCellRenderer(centerRenderer);

    }

    public void modeloNuevaMesa(JTable tablaVtaRapida, ModeloNuevoMesa tm) {

        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

        tablaVtaRapida.setModel(tm);

        tm.addColumn("ID_DETALLEMESA");
        tm.addColumn("CODIGO");
        tm.addColumn("ARTICULO");
        tm.addColumn("CANTIDAD");
        tm.addColumn("PRECIO UNIDAD");
        tm.addColumn("PRECIO TOTAL");

        tablaVtaRapida.getColumnModel().getColumn(0).setMaxWidth(0);
        tablaVtaRapida.getColumnModel().getColumn(0).setMinWidth(0);
        tablaVtaRapida.getColumnModel().getColumn(0).setPreferredWidth(0);

        tablaVtaRapida.getColumnModel().getColumn(1).setMaxWidth(0);
        tablaVtaRapida.getColumnModel().getColumn(1).setMinWidth(0);
        tablaVtaRapida.getColumnModel().getColumn(1).setPreferredWidth(0);

        tablaVtaRapida.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tablaVtaRapida.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        tablaVtaRapida.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
        tablaVtaRapida.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
        tablaVtaRapida.getColumnModel().getColumn(4).setCellRenderer(centerRenderer);
        tablaVtaRapida.getColumnModel().getColumn(5).setCellRenderer(centerRenderer);

    }

    public void modeloTablaMesasOcupadas(JTable tablaInicio, ModeloTablaMesasOcupadas mti) {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

        tablaInicio.setModel(mti);

        mti.addColumn("BOLETA N°");
        mti.addColumn("MESA");
        //mti.addColumn("CLIENTE");
        mti.addColumn("MOZO");
        mti.addColumn("TOTAL");

        tablaInicio.getColumnModel().getColumn(0).setMaxWidth(0);
        tablaInicio.getColumnModel().getColumn(0).setMinWidth(0);
        tablaInicio.getColumnModel().getColumn(0).setPreferredWidth(0);

        tablaInicio.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tablaInicio.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        tablaInicio.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
        tablaInicio.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
        //tablaInicio.getColumnModel().getColumn(4).setCellRenderer(centerRenderer);

    }

    public void traerMesasOcupadas(ModeloTablaMesasOcupadas mmo) {
        Object[] a = new Object[4];

        System.out.println("CANTIDAD DE FILAS A ELIMINAR: " + mmo.getRowCount());

        int cant = mmo.getRowCount();

        for (int i = 0; i < cant; i++) {
            mmo.removeRow(0);
        }

        String sql = "SELECT b.id_boleta, b.id_mozo, u.nombre_user, u.apellido_user, b.total, b.mesa "
                + "FROM boleta as b "
                + "JOIN User as u "
                + "ON b.id_mozo = u.dni "
                + "WHERE b.cerrada = 0 and mesa <> 0 and pagada = 0 and b.id_tipoVta = 1 "
                + "ORDER BY b.mesa asc";

        System.out.println(sql);

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                a[0] = rs1.getInt(1);
                a[1] = rs1.getInt(6);
                a[2] = rs1.getString(3) + " " + rs1.getString(4);
                a[3] = rs1.getFloat(5);

                mmo.addRow(a);
            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO ARTICULOS: " + e);
        }

    }

    public void modeloTablaDelivery(JTable tablaDelivery, ModeloTablaDelivery md) {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

        tablaDelivery.setModel(md);

        md.addColumn("BOLETA N°");
        md.addColumn("DELIVERY");
        md.addColumn("CLIENTE");
        md.addColumn("DIRECCION");
        md.addColumn("TOTAL");

        //tablaDelivery.getColumnModel().getColumn(0).setMaxWidth(0);
        //tablaDelivery.getColumnModel().getColumn(0).setMinWidth(0);
        //tablaDelivery.getColumnModel().getColumn(0).setPreferredWidth(0);
        tablaDelivery.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tablaDelivery.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        tablaDelivery.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
        tablaDelivery.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
        tablaDelivery.getColumnModel().getColumn(4).setCellRenderer(centerRenderer);

    }

    public void traerDeliverys(ModeloTablaDelivery mtd) {
        Object[] a = new Object[5];

        System.out.println("CANTIDAD DE FILAS A ELIMINAR: " + mtd.getRowCount());

        int cant = mtd.getRowCount();

        for (int i = 0; i < cant; i++) {
            mtd.removeRow(0);
        }

        String sql = "SELECT b.id_boleta, b.id_mozo, u.nombre_user, u.apellido_user, b.id_cliente, c.nombre_cliente, c.apellido_cliente, b.total, b.mesa, b.direccion_delivery "
                + "FROM boleta as b "
                + "JOIN User as u "
                + "ON b.id_mozo = u.dni "
                + "JOIN Cliente as c "
                + "ON b.id_cliente = c.id_cliente "
                + "WHERE b.cerrada = 0 and pagada = 0 and b.id_tipoVta = 2 "
                + "ORDER BY concat(fecha_cierre,hora_cierre) desc";

        System.out.println(sql);

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                a[0] = rs1.getInt(1); //id boleta
                a[1] = rs1.getString(3) + " " + rs1.getString(4); //delivery
                a[2] = rs1.getString(6) + " " + rs1.getString(7); //cliente
                a[3] = rs1.getString(10); //domicilio
                a[4] = rs1.getFloat(8); //total

                mtd.addRow(a);
            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO ARTICULOS: " + e);
        }

    }

    public void modeloTablaVentaInicio(JTable tablaInicio, ModeloTablaInicio mti) {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

        tablaInicio.setModel(mti);

        mti.addColumn("BOLETA N°");
        mti.addColumn("MESA / TIPO");
        mti.addColumn("MOZO / DELIVERY");
        mti.addColumn("TOTAL");

        //tablaInicio.getColumnModel().getColumn(0).setMaxWidth(500);
        //tablaInicio.getColumnModel().getColumn(0).setMinWidth(500);
        //tablaInicio.getColumnModel().getColumn(0).setPreferredWidth(500);
        tablaInicio.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tablaInicio.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        tablaInicio.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
        tablaInicio.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);

    }

    public void modeloTablaMesasTrabadas(JTable tablaInicio, ModeloTablaMesasTrabadas mmt) {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

        tablaInicio.setModel(mmt);

        mmt.addColumn("BOLETA N°");
        mmt.addColumn("MESA");
        mmt.addColumn("FECHA");
        mmt.addColumn("TOTAL");

        //tablaInicio.getColumnModel().getColumn(0).setMaxWidth(500);
        //tablaInicio.getColumnModel().getColumn(0).setMinWidth(500);
        //tablaInicio.getColumnModel().getColumn(0).setPreferredWidth(500);
        tablaInicio.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tablaInicio.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        tablaInicio.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
        tablaInicio.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);

    }

    public void traerMesasTrabadas(ModeloTablaMesasTrabadas mmt) {
        Object[] a = new Object[4];

        System.out.println("CANTIDAD DE FILAS A ELIMINAR: " + mmt.getRowCount());

        int cant = mmt.getRowCount();

        for (int i = 0; i < cant; i++) {
            mmt.removeRow(0);
        }

        String sql = "SELECT id_boleta, fecha_boleta, total, mesa "
                + "FROM boleta "
                + "WHERE cerrada = 0 and pagada = 0 ";
                //+ "ORDER BY b.mesa asc";

        System.out.println(sql);

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                a[0] = rs1.getInt(1);
                a[1] = rs1.getInt(4);
                a[2] = rs1.getString(2);
                a[3] = rs1.getFloat(3);

                mmt.addRow(a);
            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO ARTICULOS: " + e);
        }

    }

    public ModeloTablaPagos diseñoTablaPagos(JTable tablaFacturacion, ModeloTablaPagos modelo) {
        JTable tA = tablaFacturacion;
        tablaFacturacion.setModel(modelo);
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

        tA.setModel(modelo);
        modelo.addColumn("ID_PAGO");
        modelo.addColumn("DETALLE");
        modelo.addColumn("MONTO");

        tA.getColumnModel().getColumn(0).setMaxWidth(0);
        tA.getColumnModel().getColumn(0).setMinWidth(0);
        tA.getColumnModel().getColumn(0).setPreferredWidth(0);

        tA.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tA.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        tA.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);

        return modelo;
    }

    public void agregarVentaTabla(ModeloTablaInicio mti, Boleta b) {
        Object[] a = new Object[4];
        a[0] = b.getId_boleta();
        a[1] = 0;
        a[2] = b.getVendedor();
        a[3] = b.getTotal();

        mti.addRow(a);

    }

    public void traerVentasSinCerrar(ModeloTablaInicio mti) {
        Object[] a = new Object[4];

        System.out.println("CANTIDAD DE FILAS A ELIMINAR: " + mti.getRowCount());

        int cant = mti.getRowCount();

        for (int i = 0; i < cant; i++) {
            mti.removeRow(0);
        }

        String sql = "SELECT b.id_boleta, b.id_mozo, u.nombre_user, u.apellido_user,  b.total, b.mesa, b.id_tipoVta "
                + "FROM boleta as b "
                + "JOIN User as u "
                + "ON b.id_mozo = u.dni "
                + "WHERE b.cerrada = 0 and pagada = 1 "
                + "ORDER BY concat(fecha_cierre,hora_cierre) desc";

        System.out.println(sql);

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                a[0] = rs1.getInt(1);
                if (rs1.getInt(7) == 2) {
                    a[1] = "Delivery";
                } else if (rs1.getInt(6) != 0) {
                    a[1] = rs1.getInt(6);
                } else {
                    a[1] = "Vta Rapida";
                }
                a[2] = rs1.getString(3) + " " + rs1.getString(4);
                a[3] = rs1.getFloat(5);

                mti.addRow(a);
            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO ARTICULOS: " + e);
        }

    }

    public void agregarVentaRapida(ModeloTablaInicio mti) {
        Object[] a = new Object[4];

        String sql = "SELECT b.id_boleta, b.id_mozo, u.nombre_user, u.apellido_user,  b.total "
                + "FROM boleta as b "
                + "JOIN User as u "
                + "ON b.id_mozo = u.dni "
                + "WHERE b.cerrada = 0 "
                + "ORDER BY b.id_boleta desc "
                + "LIMIT 1";

        System.out.println(sql);

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                a[0] = rs1.getInt(1);
                a[1] = 0;
                a[2] = rs1.getString(3) + " " + rs1.getString(4);
                a[3] = rs1.getFloat(5);

                mti.addRow(a);
            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO ARTICULOS: " + e);
        }

    }

    public ModeloNuevoMesa cargarDetalles(ArrayList<Detalle_boleta> adb, ModeloNuevoMesa mnm) {
        manejadorArticulos ma = new manejadorArticulos();

        for (int i = 0; i < adb.size(); i++) {
            System.out.println("CODIGO A INSERTAR: " + adb.get(i).getCodigo());
            Object[] ar = new Object[6];
            ar[0] = adb.get(i).getId_detalleboleta();
            ar[1] = adb.get(i).getCodigo();
            ar[2] = ma.traerArticuloPorCodigo(adb.get(i).getCodigo()).getDesc_articulo();
            ar[3] = adb.get(i).getCantidad();
            ar[4] = adb.get(i).getPrecio();
            ar[5] = Integer.parseInt(ar[3].toString()) * Float.parseFloat(ar[4].toString());

            mnm.addRow(ar);
        }

        return mnm;
    }

    public void modeloTablaCostos(ModeloTablaCostos mtc, JTable tablaCostos) {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

        tablaCostos.setModel(mtc);

        mtc.addColumn("ID_COSTO");
        mtc.addColumn("ID_TIPOCOSTO");
        mtc.addColumn("FECHA");
        mtc.addColumn("DESCRIPCION");
        mtc.addColumn("TIPO COSTO");
        mtc.addColumn("MONTO");

        tablaCostos.getColumnModel().getColumn(0).setMaxWidth(0);
        tablaCostos.getColumnModel().getColumn(0).setMinWidth(0);
        tablaCostos.getColumnModel().getColumn(0).setPreferredWidth(0);

        tablaCostos.getColumnModel().getColumn(1).setMaxWidth(0);
        tablaCostos.getColumnModel().getColumn(1).setMinWidth(0);
        tablaCostos.getColumnModel().getColumn(1).setPreferredWidth(0);

        tablaCostos.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tablaCostos.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        tablaCostos.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
        tablaCostos.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
        tablaCostos.getColumnModel().getColumn(4).setCellRenderer(centerRenderer);
        tablaCostos.getColumnModel().getColumn(5).setCellRenderer(rightRenderer);
    }

    public void traerCostosDelDia(ModeloTablaCostos mcc) {
        Object[] a = new Object[6];
        //float acumulado = 0;

        System.out.println("CANTIDAD DE FILAS A ELIMINAR: " + mcc.getRowCount());

        int cant = mcc.getRowCount();

        for (int i = 0; i < cant; i++) {
            mcc.removeRow(0);
        }

        String sql = "SELECT c.id_costo, c.id_tipoCosto, d.desc_tipocosto, c.desc_costo, c.valor, c.fecha_costo "
                + "FROM Costo as c "
                + "JOIN Tipo_costo as d "
                + "ON c.id_tipocosto = d.id_tipocosto "
                + "WHERE cerrado = 0";

        System.out.println(sql);

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                a[0] = rs1.getInt(1);
                a[1] = rs1.getInt(2);
                a[2] = rs1.getString(6);
                a[3] = rs1.getString(4);
                a[4] = rs1.getString(3);
                a[5] = rs1.getFloat(5);
                //acumulado = acumulado + rs1.getFloat(5);
                mcc.addRow(a);
            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO COSTOS: " + e);
        }

        //return acumulado;
    }

    public void modeloTablaVentasTipo(JTable tablaVentasTipo, vista.CierreCajaResumido.ModeloTablaVentasTipo modeloTablaVentasTipo) {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

        tablaVentasTipo.setModel(modeloTablaVentasTipo);

        modeloTablaVentasTipo.addColumn("TIPO");
        modeloTablaVentasTipo.addColumn("TOTAL");

        tablaVentasTipo.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tablaVentasTipo.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);

        Object[] tipo = new Object[2];

        String sql = "SELECT t1.id_tipoProd1, t1.desc_tipoProd1, SUM( d.cantidad * d.precio ) "
                + "FROM Detalle_mesa AS d "
                + "JOIN articulo AS a ON a.codigo = d.codigo "
                + "JOIN Tipo_Prod AS t1 ON a.id_tipoProd = t1.id_tipoProd1 "
                + "JOIN Boleta AS b ON b.id_boleta = d.id_boleta "
                + "WHERE b.cerrada =0 "
                + "AND b.pagada =1 "
                + "GROUP BY t1.id_tipoProd1, t1.desc_tipoProd1";

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                tipo[0] = rs1.getString(2);
                tipo[1] = "$" + rs1.getFloat(3);

                modeloTablaVentasTipo.addRow(tipo);
            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO CIERRE POR TIPO: " + e);
        }

    }

    public void modeloTablaVentasCategoria(JTable tablaVentasTipo, vista.CierreCajaResumido.ModeloTablaVentasCategoria modeloTablaVentasTipo) {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

        tablaVentasTipo.setModel(modeloTablaVentasTipo);

        modeloTablaVentasTipo.addColumn("TIPO");
        modeloTablaVentasTipo.addColumn("TOTAL");

        tablaVentasTipo.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tablaVentasTipo.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);

        Object[] tipo = new Object[2];

        String sql = "SELECT t1.id_tipoVta, t1.desc_tipoVta, SUM( d.cantidad * d.precio ) "
                + "FROM Detalle_mesa AS d "
                + "JOIN Boleta AS b "
                + "ON b.id_boleta = d.id_boleta "
                + "JOIN Tipo_vta AS t1 "
                + "ON b.id_tipoVta = t1.id_tipoVta "
                + "WHERE b.cerrada =0 AND b.pagada =1 "
                + "GROUP BY t1.id_tipoVta, t1.desc_tipoVta";
        
        System.out.println("BAR/DELIVERY SQL: "+sql);

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                tipo[0] = rs1.getString(2);
                tipo[1] = "$" + rs1.getFloat(3);

                modeloTablaVentasTipo.addRow(tipo);
            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO CIERRE POR TIPO: " + e);
        }

    }

    public void modeloTablaVentaComida(JTable tablaVentaComida, ModeloTablaVentaComida modeloTablaVentaComida) {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

        tablaVentaComida.setModel(modeloTablaVentaComida);

        modeloTablaVentaComida.addColumn("TIPO");
        modeloTablaVentaComida.addColumn("TOTAL");

        tablaVentaComida.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tablaVentaComida.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);

        Object[] tipo = new Object[2];

        String sql = "SELECT t2.id_tipoProd2, t2.desc_tipoProd2, SUM( d.cantidad * d.precio ) "
                + "FROM Detalle_mesa AS d "
                + "JOIN articulo AS a ON a.codigo = d.codigo "
                + "JOIN Tipo_Prod2 AS t2 ON a.id_tipoProd2 = t2.id_tipoProd2 "
                + "JOIN Boleta AS b ON b.id_boleta = d.id_boleta "
                + "WHERE b.cerrada =0 "
                + "AND b.pagada =1 "
                + "AND a.id_tipoProd = 1 "
                + "GROUP BY t2.id_tipoProd2, t2.desc_tipoProd2 ";

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                tipo[0] = rs1.getString(2);
                tipo[1] = "$" + rs1.getFloat(3);

                modeloTablaVentaComida.addRow(tipo);
            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO CIERRE POR COMIDA: " + e);
        }

    }

    public void modeloTablaVentaBebida(JTable tablaVentaBebida, ModeloTablaVentaBebida modeloTablaVentaBebida) {

        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

        tablaVentaBebida.setModel(modeloTablaVentaBebida);

        modeloTablaVentaBebida.addColumn("TIPO");
        modeloTablaVentaBebida.addColumn("TOTAL");

        tablaVentaBebida.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tablaVentaBebida.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);

        Object[] tipo = new Object[2];

        String sql = "SELECT t2.id_tipoProd2, t2.desc_tipoProd2, SUM( d.cantidad * d.precio ) "
                + "FROM Detalle_mesa AS d "
                + "JOIN articulo AS a ON a.codigo = d.codigo "
                + "JOIN Tipo_Prod2 AS t2 ON a.id_tipoProd2 = t2.id_tipoProd2 "
                + "JOIN Boleta AS b ON b.id_boleta = d.id_boleta "
                + "WHERE b.cerrada =0 "
                + "AND b.pagada =1 "
                + "AND a.id_tipoProd = 2 "
                + "GROUP BY t2.id_tipoProd2, t2.desc_tipoProd2 ";

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                tipo[0] = rs1.getString(2);
                tipo[1] = "$" + rs1.getFloat(3);

                modeloTablaVentaBebida.addRow(tipo);
            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO CIERRE POR BEBIDA: " + e);
        }

    }

    public void modeloTablaVentaPromociones(JTable tablaVentaPromociones, ModeloTablaPromociones modeloTablaVentaPromociones) {

        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

        tablaVentaPromociones.setModel(modeloTablaVentaPromociones);

        modeloTablaVentaPromociones.addColumn("TIPO");
        modeloTablaVentaPromociones.addColumn("TOTAL");

        tablaVentaPromociones.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tablaVentaPromociones.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);

        Object[] tipo = new Object[2];

        String sql = "SELECT t2.id_tipoProd2, t2.desc_tipoProd2, SUM( d.cantidad * d.precio ) "
                + "FROM Detalle_mesa AS d "
                + "JOIN articulo AS a ON a.codigo = d.codigo "
                + "JOIN Tipo_Prod2 AS t2 ON a.id_tipoProd2 = t2.id_tipoProd2 "
                + "JOIN Boleta AS b ON b.id_boleta = d.id_boleta "
                + "WHERE b.cerrada =0 "
                + "AND b.pagada =1 "
                + "AND a.id_tipoProd = 3 "
                + "GROUP BY t2.id_tipoProd2, t2.desc_tipoProd2";

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                tipo[0] = rs1.getString(2);
                tipo[1] = "$" + rs1.getFloat(3);

                modeloTablaVentaPromociones.addRow(tipo);
            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO CIERRE POR PROMOCIONES: " + e);
        }

    }

    public void modeloTablaVentaDescuentos(JTable tablaVentaDescuentos, ModeloTablaDescuentos modeloTablaVentaDescuentos) {

        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

        tablaVentaDescuentos.setModel(modeloTablaVentaDescuentos);

        modeloTablaVentaDescuentos.addColumn("TIPO");
        modeloTablaVentaDescuentos.addColumn("TOTAL");

        tablaVentaDescuentos.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tablaVentaDescuentos.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);

        Object[] tipo = new Object[2];

        String sql = "SELECT a.desc_articulo, SUM( d.cantidad * d.precio ) "
                + "FROM Detalle_mesa AS d "
                + "JOIN articulo AS a ON a.codigo = d.codigo "
                + "JOIN Tipo_Prod AS t1 ON a.id_tipoProd = t1.id_tipoProd1 "
                + "JOIN Boleta AS b ON b.id_boleta = d.id_boleta "
                + "WHERE b.cerrada =0 "
                + "AND b.pagada =1 "
                + "AND t1.id_tipoProd1 = 4 "
                + "GROUP BY a.desc_articulo ";

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                tipo[0] = rs1.getString(1);
                tipo[1] = "$" + rs1.getFloat(2);

                modeloTablaVentaDescuentos.addRow(tipo);
            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO CIERRE POR DESCUENTOS: " + e);
        }

    }

    public void modeloTablaGastos(JTable tablaGastos, ModeloTablaGastos modeloTablaGastos) {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

        tablaGastos.setModel(modeloTablaGastos);

        modeloTablaGastos.addColumn("DESCRIPCION");
        modeloTablaGastos.addColumn("TOTAL");

        tablaGastos.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tablaGastos.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);

        Object[] tipo = new Object[2];

        String sql = "SELECT t.id_tipoCosto, t.desc_tipoCosto, c.desc_costo, SUM( c.valor ) "
                + "FROM Costo AS c "
                + "JOIN Tipo_Costo AS t ON t.id_tipoCosto = c.id_tipoCosto "
                + "WHERE c.cerrado = 0 "
                + "GROUP BY t.id_tipoCosto, t.desc_tipoCosto, c.desc_costo";

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        System.out.println(sql);

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                tipo[0] = rs1.getString(3);
                tipo[1] = "$" + rs1.getFloat(4);

                modeloTablaGastos.addRow(tipo);
            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO CIERRE POR COSTOS: " + e);
        }

    }

    public float modeloTablaFacturacion(JTable tablaFacturacion, ModeloTablaFacturacion modeloTablaFacturacion) {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

        tablaFacturacion.setModel(modeloTablaFacturacion);

        modeloTablaFacturacion.addColumn("TIPO");
        modeloTablaFacturacion.addColumn("VENTAS");
        modeloTablaFacturacion.addColumn("GASTOS");
        modeloTablaFacturacion.addColumn("TOTAL");

        tablaFacturacion.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tablaFacturacion.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        tablaFacturacion.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
        tablaFacturacion.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);

        ArrayList<Object[]> ventas = new ArrayList<>();
        ArrayList<Object[]> costos = new ArrayList<>();

        float totalVentas = 0;
        float totalCostos = 0;
        float ganancia;

        String sql = "SELECT fb.id_condVta, c.desc_condVta, SUM(fb.monto) "
                + "FROM Facturacion_boleta as fb "
                + "JOIN Cond_Vta as c "
                + "on fb.id_condVta = c.id_condVta "
                + "JOIN boleta as b "
                + "on fb.id_boleta = b.id_boleta "
                + "WHERE b.cerrada = 0 "
                + "group by fb.id_condVta, c.desc_condVta";

        System.out.println(sql);

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                Object[] tipo = new Object[4];
                tipo[0] = rs1.getString(2);
                tipo[1] = rs1.getFloat(3);
                tipo[2] = "-";
                //tipo[3] = "-";
                totalVentas = totalVentas + rs1.getFloat(3);
                ventas.add(tipo);

                //modeloTablaFacturacion.addRow(tipo);
            }

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO CIERRE FACTURACION VTAS: " + e);
        }

        sql = "SELECT fc.id_condVta, c.desc_condVta, SUM(fc.monto) "
                + "FROM Facturacion_costo as fc "
                + "JOIN Cond_Vta as c "
                + "on fc.id_condVta = c.id_condVta "
                + "JOIN Costo as cos "
                + "on fc.id_costo = cos.id_costo "
                + "WHERE cos.cerrado = 0 "
                + "group by fc.id_condVta, c.desc_condVta";

        System.out.println(sql);

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                Object[] tipo2 = new Object[3];
                tipo2[0] = rs1.getString(2);
                tipo2[2] = rs1.getFloat(3);
                totalCostos = totalCostos + rs1.getFloat(3);
                costos.add(tipo2);

            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO CIERRE FACTURACION COSTOS: " + e);
        }

        int tamañoCostos = costos.size();
        int tamañoVentas = ventas.size();

        if (tamañoVentas >= tamañoCostos) {
            for (int i = 0; i < tamañoVentas; i++) {
                modeloTablaFacturacion.addRow(ventas.get(i));
            }

            for (int i = 0; i < tablaFacturacion.getRowCount(); i++) {
                String busqueda = tablaFacturacion.getValueAt(i, 0).toString();
                float totalV = Float.parseFloat(ventas.get(i)[1].toString());
                tablaFacturacion.setValueAt("$" + ventas.get(i)[1], i, 1);
                float totalC = 0;

                for (int j = 0; j < costos.size(); j++) {
                    if (busqueda.equals(costos.get(j)[0].toString())) {
                        tablaFacturacion.setValueAt("-$" + costos.get(j)[2], i, 2);
                        totalC = Float.parseFloat(costos.get(j)[2].toString());
                    }
                }

                float total = totalV - totalC;

                tablaFacturacion.setValueAt("$" + total, i, 3);
            }

        } else {
            for (int i = 0; i < tamañoCostos; i++) {
                modeloTablaFacturacion.addRow(costos.get(i));
            }

            for (int i = 0; i < tablaFacturacion.getRowCount(); i++) {
                String busqueda = tablaFacturacion.getValueAt(i, 0).toString();
                tablaFacturacion.setValueAt("-$" + costos.get(i)[2], i, 2);
                float totalC = Float.parseFloat(costos.get(i)[2].toString());
                float totalV = 0;

                for (int j = 0; j < ventas.size(); j++) {
                    if (busqueda.equals(ventas.get(j)[0].toString())) {
                        tablaFacturacion.setValueAt("$" + ventas.get(j)[1], i, 1);
                        totalV = Float.parseFloat(ventas.get(j)[1].toString());
                    }
                }

                float total = totalV - totalC;

                tablaFacturacion.setValueAt("$" + total, i, 3);

            }
        }

        ganancia = totalVentas - totalCostos;

        return ganancia;

    }

    public void modeloTablaPromocion(JTable tablaPromocion, ModeloPromocion tm) {

        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

        tablaPromocion.setModel(tm);

        tm.addColumn("CODIGO");
        tm.addColumn("ARTICULO");
        tm.addColumn("CANTIDAD");
        tm.addColumn("PRECIO UNI");
        tm.addColumn("PRECIO TOTAL");

        /*
        tablaPromocion.getColumnModel().getColumn(0).setMaxWidth(0);
        tablaPromocion.getColumnModel().getColumn(0).setMinWidth(0);
        tablaPromocion.getColumnModel().getColumn(0).setPreferredWidth(0);

        tablaPromocion.getColumnModel().getColumn(1).setMaxWidth(0);
        tablaPromocion.getColumnModel().getColumn(1).setMinWidth(0);
        tablaPromocion.getColumnModel().getColumn(1).setPreferredWidth(0);*/
        tablaPromocion.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tablaPromocion.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        tablaPromocion.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
        tablaPromocion.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
        tablaPromocion.getColumnModel().getColumn(4).setCellRenderer(centerRenderer);

    }

    public void modeloTablaBoletasAdeudadas(JTable tablaBoletas, ModeloTablaBoletasAdeudadas mcc) {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

        tablaBoletas.setModel(mcc);

        mcc.addColumn("ID_DETALLE");
        mcc.addColumn("BOLETA N°");
        mcc.addColumn("TOTAL BOLETA");
        mcc.addColumn("DEBE");
        mcc.addColumn("FECHA");

        tablaBoletas.getColumnModel().getColumn(0).setMaxWidth(0);
        tablaBoletas.getColumnModel().getColumn(0).setMinWidth(0);
        tablaBoletas.getColumnModel().getColumn(0).setPreferredWidth(0);

        tablaBoletas.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tablaBoletas.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        tablaBoletas.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
        tablaBoletas.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
        tablaBoletas.getColumnModel().getColumn(4).setCellRenderer(centerRenderer);

    }

    public void modeloTablaPagosRealizados(JTable tablaBoletas, ModeloTablaPagosRealizados mcc) {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

        tablaBoletas.setModel(mcc);

        mcc.addColumn("FECHA");
        mcc.addColumn("TOTAL");
        mcc.addColumn("CONDICION");

        tablaBoletas.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tablaBoletas.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        tablaBoletas.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);

    }

    public void modeloTablaBoletasCanceladas(JTable tablaBoletas, ModeloTablaBoletasCanceladas mcc) {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

        tablaBoletas.setModel(mcc);

        mcc.addColumn("ID_DET");
        mcc.addColumn("BOLETA");
        mcc.addColumn("TOTAL");

        tablaBoletas.getColumnModel().getColumn(0).setMaxWidth(0);
        tablaBoletas.getColumnModel().getColumn(0).setMinWidth(0);
        tablaBoletas.getColumnModel().getColumn(0).setPreferredWidth(0);

        tablaBoletas.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        tablaBoletas.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);

    }

    public void traerArticulosPromocion(ModeloPromocion mp, String codigo) {
        Object[] a = new Object[5];

        /*System.out.println("CANTIDAD DE FILAS A ELIMINAR: " + mp.getRowCount());

        int cant = mmo.getRowCount();

        for (int i = 0; i < cant; i++) {
            mmo.removeRow(0);
        }*/
        String sql = "SELECT e.codigo_articulo, a.desc_articulo, e.cantidad_articulo, a.precio_vta "
                + "FROM Equiv_Prom as e "
                + "JOIN Articulo as a "
                + "ON e.codigo_articulo = a.codigo "
                + "WHERE e.codigo_promocion = '" + codigo + "'";

        System.out.println(sql);

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                a[0] = rs1.getString(1);
                a[1] = rs1.getString(2);
                a[2] = rs1.getInt(3);
                a[3] = rs1.getFloat(4);
                a[4] = rs1.getInt(3) * rs1.getFloat(4);

                mp.addRow(a);
            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO ARTICULOS: " + e);
        }

    }

    public void traerVentasMozo(JTable tablaVentasMozas, ModeloTablaVentasMozos mtv) {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

        tablaVentasMozas.setModel(mtv);
        mtv.addColumn("MOZO");
        mtv.addColumn("VENTAS");

        tablaVentasMozas.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tablaVentasMozas.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);

        Object[] a = new Object[2];

        String sql = "SELECT U.DNI, u.nombre_user, u.apellido_user, SUM(b.total) "
                + "FROM user as u "
                + "JOIN boleta AS b "
                + "ON B.id_mozo = U.dni "
                + "WHERE B.cerrada = 0 "
                + "GROUP BY U.dni, U.nombre_user, U.apellido_user "
                + "ORDER BY SUM(B.TOTAL) DESC";

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                a[0] = rs1.getString(2) + " " + rs1.getString(3);
                a[1] = rs1.getFloat(4);

                mtv.addRow(a);
            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO VENTAS DE MOZOS: " + e);
        }

    }

    public void traerUltimoCierre(ModeloTablaUltimoCierre modeloTablaUltimoCierre, JTable tablaUltimoCierre, Cierre_Caja cj) {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

        tablaUltimoCierre.setModel(modeloTablaUltimoCierre);

        modeloTablaUltimoCierre.addColumn("ID");
        modeloTablaUltimoCierre.addColumn("FECHA");
        modeloTablaUltimoCierre.addColumn("HORA");
        modeloTablaUltimoCierre.addColumn("VENTAS");
        modeloTablaUltimoCierre.addColumn("COSTOS");
        modeloTablaUltimoCierre.addColumn("GANANCIA");

        tablaUltimoCierre.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tablaUltimoCierre.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        tablaUltimoCierre.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
        tablaUltimoCierre.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
        tablaUltimoCierre.getColumnModel().getColumn(4).setCellRenderer(centerRenderer);
        tablaUltimoCierre.getColumnModel().getColumn(5).setCellRenderer(centerRenderer);

        tablaUltimoCierre.getColumnModel().getColumn(0).setMaxWidth(0);
        tablaUltimoCierre.getColumnModel().getColumn(0).setMinWidth(0);
        tablaUltimoCierre.getColumnModel().getColumn(0).setPreferredWidth(0);

        Object[] a = new Object[6];

        a[0] = cj.getId_cierreCaja();
        a[1] = cj.getFecha_cierre();
        a[2] = cj.getHora_cierre();
        a[3] = cj.getVenta_contado() + cj.getVenta_credito() + cj.getVenta_debito();
        a[4] = cj.getCosto_contado() + cj.getCosto_credito() + cj.getCosto_debito();
        a[5] = Float.parseFloat(a[3].toString()) - Float.parseFloat(a[4].toString());

        modeloTablaUltimoCierre.addRow(a);

    }

    public void modeloTablaBusquedaCierres(ModeloTablaBusquedaCierres mtb, JTable tablaBusqueda) {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

        tablaBusqueda.setModel(mtb);

        mtb.addColumn("ID");
        mtb.addColumn("FECHA");
        mtb.addColumn("HORA");
        mtb.addColumn("VENTAS");
        mtb.addColumn("COSTOS");
        mtb.addColumn("GANANCIA");

        tablaBusqueda.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tablaBusqueda.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        tablaBusqueda.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
        tablaBusqueda.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
        tablaBusqueda.getColumnModel().getColumn(4).setCellRenderer(centerRenderer);
        tablaBusqueda.getColumnModel().getColumn(5).setCellRenderer(centerRenderer);

        /*
        tablaBusqueda.getColumnModel().getColumn(0).setMaxWidth(0);
        tablaBusqueda.getColumnModel().getColumn(0).setMinWidth(0);
        tablaBusqueda.getColumnModel().getColumn(0).setPreferredWidth(0);
         */
    }

    public void buscarCierresFechas(ModeloTablaBusquedaCierres mtb, String fechaDesde, String fechaHasta, ModeloTablaTotalesCierres mt) {

        int cant = mtb.getRowCount();
        float ventas = 0;
        float costos = 0;
        float ganancia = 0;

        for (int j = 0; j < cant; j++) {
            mtb.removeRow(0);
        }

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        ArrayList<Cierre_Caja> cj = new ArrayList<>();

        cj = mf.traerCierreCajaFechas(fechaDesde, fechaHasta);

        for (int i = 0; i < cj.size(); i++) {
            Object[] a = new Object[6];

            a[0] = cj.get(i).getId_cierreCaja();
            a[1] = cj.get(i).getFecha_cierre();
            a[2] = cj.get(i).getHora_cierre();
            a[3] = cj.get(i).getVenta_contado() + cj.get(i).getVenta_credito() + cj.get(i).getVenta_debito();
            a[4] = cj.get(i).getCosto_contado() + cj.get(i).getCosto_credito() + cj.get(i).getCosto_debito();
            a[5] = Float.parseFloat(a[3].toString()) - Float.parseFloat(a[4].toString());

            mtb.addRow(a);

        }

        Object[] b = new Object[4];

        int cant2 = mt.getRowCount();

        if (mt.getRowCount() > 0) {
            for (int i = 0; i < cant2; i++) {
                mt.removeRow(0);
            }
        }

        b[0] = mtb.getRowCount();

        for (int i = 0; i < mtb.getRowCount(); i++) {
            ventas = ventas + Float.parseFloat(mtb.getValueAt(i, 3).toString());
            costos = costos + Float.parseFloat(mtb.getValueAt(i, 4).toString());
            ganancia = ganancia + Float.parseFloat(mtb.getValueAt(i, 5).toString());
        }

        b[1] = ventas;
        b[2] = costos;
        b[3] = ganancia;

        mt.addRow(b);

    }

    public void buscarCierreNumero(ModeloTablaBusquedaCierres mtb, int id, ModeloTablaTotalesCierres mt) {

        int cant = mtb.getRowCount();
        float ventas = 0;
        float costos = 0;
        float ganancia = 0;

        for (int j = 0; j < cant; j++) {
            mtb.removeRow(0);
        }

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        Cierre_Caja cj = new Cierre_Caja();

        cj = mf.traerCierreCaja(id);

        Object[] a = new Object[6];

        a[0] = cj.getId_cierreCaja();
        a[1] = cj.getFecha_cierre();
        a[2] = cj.getHora_cierre();
        a[3] = cj.getVenta_contado() + cj.getVenta_credito() + cj.getVenta_debito();
        a[4] = cj.getCosto_contado() + cj.getCosto_credito() + cj.getCosto_debito();
        a[5] = Float.parseFloat(a[3].toString()) - Float.parseFloat(a[4].toString());

        mtb.addRow(a);

        Object[] b = new Object[4];

        int cant2 = mtb.getRowCount();
        b[0] = cant2;

        if (mt.getRowCount() > 0) {
            for (int i = 0; i < cant2; i++) {
                mt.removeRow(0);
            }
        }

        for (int i = 0; i < mtb.getRowCount(); i++) {
            ventas = ventas + Float.parseFloat(mtb.getValueAt(i, 3).toString());
            costos = costos + Float.parseFloat(mtb.getValueAt(i, 4).toString());
            ganancia = ganancia + Float.parseFloat(mtb.getValueAt(i, 5).toString());
        }

        b[1] = ventas;
        b[2] = costos;
        b[3] = ganancia;

        mt.addRow(b);

    }

    public void modeloTablaTotalesCierres(ModeloTablaTotalesCierres mt, JTable tabla) {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

        tabla.setModel(mt);

        mt.addColumn("CANT BOLETAS");
        mt.addColumn("VENTAS");
        mt.addColumn("COSTOS");
        mt.addColumn("GANANCIA");

        tabla.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tabla.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        tabla.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
        tabla.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);

    }

    public void traerBoletasCierre(ModeloTablaBoletas mti, JTable tabla, int id) {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

        tabla.setModel(mti);

        mti.addColumn("BOLETA N°");
        mti.addColumn("MESA");
        mti.addColumn("MOZO");
        mti.addColumn("TOTAL");

        tabla.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tabla.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        tabla.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
        tabla.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);

        Object[] a = new Object[4];

        System.out.println("CANTIDAD DE FILAS A ELIMINAR: " + mti.getRowCount());

        int cant = mti.getRowCount();

        for (int i = 0; i < cant; i++) {
            mti.removeRow(0);
        }

        String sql = "SELECT b.id_boleta, b.id_mozo, u.nombre_user, u.apellido_user,  b.total, b.mesa "
                + "FROM boleta as b "
                + "JOIN User as u "
                + "ON b.id_mozo = u.dni "
                + "WHERE b.cerrada = " + id + " "
                + "ORDER BY b.id_boleta asc";

        System.out.println(sql);

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                a[0] = rs1.getInt(1);
                a[1] = rs1.getInt(6);
                a[2] = rs1.getString(3) + " " + rs1.getString(4);
                a[3] = rs1.getFloat(5);

                mti.addRow(a);
            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO ARTICULOS: " + e);
        }

    }

    public void traerBoletasCierreLimites(ModeloTablaBoletas mti, JTable tabla, int id_desde, int id_hasta) {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

        tabla.setModel(mti);

        mti.addColumn("BOLETA N°");
        mti.addColumn("MESA");
        mti.addColumn("MOZO");
        mti.addColumn("TOTAL");

        tabla.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tabla.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        tabla.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
        tabla.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);

        Object[] a = new Object[4];

        System.out.println("CANTIDAD DE FILAS A ELIMINAR: " + mti.getRowCount());

        int cant = mti.getRowCount();

        for (int i = 0; i < cant; i++) {
            mti.removeRow(0);
        }

        String sql = "SELECT b.id_boleta, b.id_mozo, u.nombre_user, u.apellido_user,  b.total, b.mesa "
                + "FROM boleta as b "
                + "JOIN User as u "
                + "ON b.id_mozo = u.dni "
                + "WHERE b.cerrada >= " + id_desde + " and b.cerrada < " + id_hasta + " "
                + "ORDER BY b.id_boleta asc";

        System.out.println(sql);

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                a[0] = rs1.getInt(1);
                a[1] = rs1.getInt(6);
                a[2] = rs1.getString(3) + " " + rs1.getString(4);
                a[3] = rs1.getFloat(5);

                mti.addRow(a);
            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO ARTICULOS: " + e);
        }

    }

    public void traerResumenCierre(ModeloTablaResumen mti, JTable tabla, int id) {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

        tabla.setModel(mti);

        mti.addColumn("TIPO");
        mti.addColumn("ARTICULO");
        mti.addColumn("CANTIDAD");
        mti.addColumn("TOTAL");

        tabla.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tabla.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        tabla.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
        tabla.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);

        Object[] a = new Object[4];

        System.out.println("CANTIDAD DE FILAS A ELIMINAR: " + mti.getRowCount());

        int cant = mti.getRowCount();

        for (int i = 0; i < cant; i++) {
            mti.removeRow(0);
        }

        String sql = "SELECT t1.desc_tipoProd1, a.desc_articulo, SUM( dm.cantidad ) as cantidad , SUM( dm.precio * dm.cantidad ) as total "
                + "FROM Articulo AS a "
                + "JOIN tipo_prod AS t1 ON a.id_tipoProd = t1.id_tipoProd1 "
                + "JOIN detalle_mesa AS dm ON a.codigo = dm.codigo "
                + "JOIN boleta AS b ON b.id_boleta = dm.id_boleta "
                + "WHERE b.cerrada = " + id + " "
                + "GROUP BY t1.desc_tipoProd1, a.desc_articulo "
                + "ORDER BY t1.desc_tipoProd1 asc, cantidad desc";

        System.out.println(sql);

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                a[0] = rs1.getString(1);
                a[1] = rs1.getString(2);
                a[2] = rs1.getInt(3);
                a[3] = rs1.getFloat(4);

                mti.addRow(a);
            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO ARTICULOS: " + e);
        }

    }

    public void traerResumenCierreLimites(ModeloTablaResumen mti, JTable tabla, int id_desde, int id_hasta) {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

        tabla.setModel(mti);

        mti.addColumn("TIPO");
        mti.addColumn("ARTICULO");
        mti.addColumn("CANTIDAD");
        mti.addColumn("TOTAL");

        tabla.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tabla.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        tabla.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
        tabla.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);

        Object[] a = new Object[4];

        System.out.println("CANTIDAD DE FILAS A ELIMINAR: " + mti.getRowCount());

        int cant = mti.getRowCount();

        for (int i = 0; i < cant; i++) {
            mti.removeRow(0);
        }

        String sql = "SELECT t1.desc_tipoProd1, a.desc_articulo, SUM( dm.cantidad ) as cantidad , SUM( dm.precio * dm.cantidad ) as total "
                + "FROM Articulo AS a "
                + "JOIN tipo_prod AS t1 ON a.id_tipoProd = t1.id_tipoProd1 "
                + "JOIN detalle_mesa AS dm ON a.codigo = dm.codigo "
                + "JOIN boleta AS b ON b.id_boleta = dm.id_boleta "
                + "WHERE b.cerrada >= " + id_desde + " and b.cerrada < " + id_hasta + " "
                + "GROUP BY t1.desc_tipoProd1, a.desc_articulo "
                + "ORDER BY t1.desc_tipoProd1 asc, cantidad desc";

        System.out.println(sql);

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                a[0] = rs1.getString(1);
                a[1] = rs1.getString(2);
                a[2] = rs1.getInt(3);
                a[3] = rs1.getFloat(4);

                mti.addRow(a);
            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO ARTICULOS: " + e);
        }
    }

    public void traerPagos(ModeloTablaPagos mtp, int id_boleta) {
        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        String sql = "SELECT f.id_condVta, c.desc_condVta, f.monto "
                + "FROM facturacion_boleta as f "
                + "JOIN cond_vta as c "
                + "ON f.id_condVta = c.id_condVta "
                + "WHERE f.id_boleta = " + id_boleta;

        System.out.println(sql);

        Object[] a = new Object[3];

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                a[0] = rs1.getInt(1);
                a[1] = rs1.getString(2);
                a[2] = rs1.getFloat(3);

                mtp.addRow(a);
            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO FORMAS DE PAGO: " + e);
        }
    }

    public void traerBoletasAdeudadas(ModeloTablaBoletasAdeudadas mcc, int id_cta_cte) {
        Object[] a = new Object[5];

        String sql = "SELECT d.id_detalle, d.id_cta_cte, d.id_boleta, d.saldo, b.fecha_boleta, b.total "
                + "FROM DETALLE_CTA_CTE as d "
                + "JOIN BOLETA as b "
                + "ON d.id_boleta = b.id_boleta "
                + "WHERE d.id_cta_cte = " + id_cta_cte + " and d.saldo > 0";

        System.out.println(sql);

        int cant = mcc.getRowCount();

        for (int i = 0; i < cant; i++) {
            mcc.removeRow(0);
        }

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                a[0] = rs1.getInt(1);
                a[1] = rs1.getInt(3);
                a[2] = rs1.getFloat(6);
                a[3] = rs1.getFloat(4);
                a[4] = mfe.convertirFecha2(rs1.getString(5));

                mcc.addRow(a);
            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO ARTICULOS: " + e);
        }

    }

    public void traerBoletasCtaCtePagadas(ModeloTablaBoletasCanceladas mcc, int id_cta_cte) {
        Object[] a = new Object[3];

        String sql = "SELECT d.id_detalle, d.id_cta_cte, d.id_boleta, d.saldo, b.fecha_boleta, b.total, fb.monto "
                + "FROM DETALLE_CTA_CTE as d "
                + "JOIN BOLETA as b "
                + "ON d.id_boleta = b.id_boleta "
                + "JOIN FACTURACION_BOLETA as fb "
                + "ON b.id_boleta = fb.id_boleta "
                + "WHERE d.id_cta_cte = " + id_cta_cte + " and d.saldo = 0 and fb.id_condVta = 4";

        System.out.println(sql);

        int cant = mcc.getRowCount();

        for (int i = 0; i < cant; i++) {
            mcc.removeRow(0);
        }

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                a[0] = rs1.getInt(1);
                a[1] = rs1.getInt(3);
                a[2] = rs1.getFloat(7);

                mcc.addRow(a);
            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO BOLETAS: " + e);
        }

    }

    public void traerPagosCtaCte(ModeloTablaPagosRealizados mcc, int id_cta_cte) {
        Object[] a = new Object[3];

        String sql = "SELECT DATE_FORMAT(fcc.fecha, '%d/%m/%Y'), fcc.monto, cv.desc_condVta "
                + "FROM FACTURACION_CTA_CTE as fcc "
                + "JOIN COND_VTA as cv "
                + "ON fcc.id_condVta = cv.id_condVta "
                + "WHERE fcc.id_cta_cte = " + id_cta_cte;

        System.out.println(sql);

        int cant = mcc.getRowCount();

        for (int i = 0; i < cant; i++) {
            mcc.removeRow(0);
        }

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                a[0] = rs1.getString(1);
                a[1] = rs1.getFloat(2);
                a[2] = rs1.getString(3);

                mcc.addRow(a);
            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO PAGOS CTA CTE: " + e);
        }

    }
}
