/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import boston.conexionDB;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author rmmayer
 */
public class manejadorFechas {

    public String obtenerFechaActual() {

        Calendar fecha = Calendar.getInstance();
        String dia, mes, annio;
        String fechaSql = null;

        Date date = new Date();
        //Caso 1: obtener la hora y salida por pantalla con formato:
        DateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
        System.out.println("Hora: " + hourFormat.format(date));
        System.out.println("HORA: " + Integer.parseInt(hourFormat.format(date).toString().substring(0, 2)));

        if ((fecha.get(Calendar.DATE)) < 10) {
            dia = "0" + Integer.toString(fecha.get(Calendar.DATE));
        } else {
            dia = Integer.toString(fecha.get(Calendar.DATE));

        }

        if (fecha.get(Calendar.MONTH) + 1 < 10) {
            mes = "0" + Integer.toString(fecha.get(Calendar.MONTH) + 1);

        } else {
            mes = Integer.toString(fecha.get(Calendar.MONTH) + 1);

        }

        annio = Integer.toString(fecha.get(Calendar.YEAR));

        fechaSql = annio + mes + dia;
        System.out.println("FECHA SQL: " + fechaSql);

        return fechaSql;

    }

    public String obtenerFechasVtoCtaCte(String fechaActual, int nroCuota) {
        String fechaVto = "";
        Calendar fecha = Calendar.getInstance();
        String dia = this.obtenerDiaVto();
        String mes = "";
        fecha.add(Calendar.MONTH, nroCuota);
        String annio = String.valueOf(fecha.get(Calendar.YEAR));

        if ((fecha.get(Calendar.MONTH) + 1) < 10) {
            mes = "0" + String.valueOf(fecha.get(Calendar.MONTH) + 1);
        } else {
            mes = String.valueOf(fecha.get(Calendar.MONTH) + 1);
        }

        //mes = fechaActual.substring(2, 4);
        System.out.println("mes de la cuota: " + mes);
        System.out.println("año de la cuota: " + annio);
        fechaVto = annio + mes + dia;
        System.out.println("fecha vto: " + fechaVto);

        return fechaVto;

    }

    public String obtenerDiaVto() {
        String dia = "";
        conexionDB con = new conexionDB();
        Connection reg = con.conectar();

        String sql = "SELECT valor "
                + "FROM configuracion "
                + "WHERE desc_config = 'dia_vto_cuotas'";

        System.out.println("sql para traer dia vto: " + sql);

        try {
            Statement st = reg.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                dia = String.valueOf(rs.getInt(1));
            }

            reg.close();

        } catch (SQLException e) {
            System.out.println("ERROR AL TRAER DIA DE VTO EN CONFIGURACIONES");
        }

        return dia;
    }

    public String convertirFecha(String fecha) {
        String fechaConv = fecha.substring(6, 8) + "/" + fecha.substring(4, 6) + "/" + fecha.substring(0, 4);
        System.out.println("fecha convertida: " + fechaConv);
        return fechaConv;
    }
    
    public String convertirFecha2(String fecha) {
        String fechaConv = fecha.substring(8, 10) + "/" + fecha.substring(5, 7) + "/" + fecha.substring(0, 4);
        System.out.println("fecha convertida: " + fechaConv);
        return fechaConv;
    }
    

    public String obtenerHoraActual() {
        String hora = "";
        Calendar calendario = new GregorianCalendar();
        int horas, minutos, segundos;
        String horasS="", minutosS="", segundosS="";

        horas = calendario.get(Calendar.HOUR_OF_DAY);
        if(horas<10){
            horasS="0"+horas;
        }else{
            horasS = String.valueOf(horas);
        }
        minutos = calendario.get(Calendar.MINUTE);
        if(minutos<10){
            minutosS="0"+minutos;
        }else{
            minutosS = String.valueOf(minutos);
        }
        segundos = calendario.get(Calendar.SECOND);
        if(segundos<10){
            segundosS="0"+segundos;
        }else{
            segundosS = String.valueOf(segundos);
        }

        hora = horasS + ":" + minutosS + ":" + segundosS;

        return hora;
    }

    public String convertirFechaInversa(String fecha) {
        //String fechaConv = fecha.substring(0,2)+fecha.substring(2, 4)+fecha.substring(4,8);

        //String fechaConv = fecha.substring(6,10)+fecha.substring(3, 5)+fecha.substring(0,2);
        String fechaConv = fecha.substring(0, 4) + fecha.substring(5, 7) + fecha.substring(8, 10);

        //System.out.println("fecha convertida: "+fechaConv);
        return fechaConv;
    }

}
