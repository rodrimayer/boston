/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import boston.conexionDB;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import modelo.Articulo;
import modelo.Barril;
import modelo.Detalle_boleta;
import modelo.Equiv_Prom;
import modelo.Tipo_Prod1;
import modelo.Tipo_Prod2;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author Rodri Mayer
 */
public class manejadorArticulos {

    public class MiModelo extends DefaultTableModel {

        public boolean isCellEditable(int row, int column) {
            return false;
        }
    }

    public void traerTipoProd(JComboBox comboTipos) {
        comboTipos.removeAllItems();
        conexionDB c = new conexionDB();
        Connection con = c.conectar();
        Tipo_Prod1 tp1 = new Tipo_Prod1();

        String sql = "SELECT id_tipoProd1, desc_tipoProd1 from tipo_prod";
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                tp1.setId_tipoProd1(rs1.getInt(1));
                tp1.setDesc_tipoProd1(rs1.getString(2));
                comboTipos.addItem(tp1.getDesc_tipoProd1());
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }

        //comboTipos.setSelectedIndex(-1);
    }

    public void traerTipoProd2(JComboBox comboTipos, int id) {
        comboTipos.removeAllItems();
        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        String sql = "SELECT desc_tipoProd2 from tipo_prod2 where id_tipoProd1 = " + id;
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                comboTipos.addItem(rs1.getString(1));
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }

        comboTipos.setSelectedIndex(-1);
    }

    public boolean existeID(String id) {
        boolean b = false;
        conexionDB c = new conexionDB();
        Connection con = c.conectar();
        String sql = "select * from articulo where codigo = '" + id + "'";
        System.out.println(sql);
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                b = true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("BANDERA: " + b);
        return b;
    }

    public int insertarArticulo(Articulo a) {
        int n = 0;
        conexionDB con = new conexionDB();
        Connection reg = con.conectar();
        String sql = "INSERT INTO articulo (codigo, desc_articulo, id_tipoProd, id_tipoProd2, precio_costo, precio_vta) VALUES (?,?,?,?,?,?)";

        try {

            PreparedStatement ps = reg.prepareStatement(sql);
            ps.setString(1, a.getCodigo());
            ps.setString(2, a.getDesc_articulo());
            ps.setInt(3, a.getId_tipoProd1());
            ps.setInt(4, a.getId_tipoProd2());
            ps.setFloat(5, a.getPrecio_costo());
            ps.setFloat(6, a.getPrecio_venta());

            System.out.println(sql);

            n = ps.executeUpdate();
            reg.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }

        return n;
    }

    public int encontrarIDTipoProd1(String tipo) {
        int id_tipo = 0;

        String sql = "SELECT id_tipoProd1 from TIPO_PROD where desc_tipoProd1 = '" + tipo + "'";
        conexionDB con = new conexionDB();
        Connection reg = con.conectar();

        try {
            Statement st1 = reg.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                id_tipo = rs1.getInt(1);
            }

            reg.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO EL ID_TIPO: " + e);
        }

        return id_tipo;
    }

    public int encontrarIDTipoProd2(String tipo) {
        int id_tipo = 0;

        String sql = "SELECT id_tipoProd2 from TIPO_PROD2 where desc_tipoProd2 = '" + tipo + "'";
        conexionDB con = new conexionDB();
        Connection reg = con.conectar();

        try {
            Statement st1 = reg.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                id_tipo = rs1.getInt(1);
            }

            reg.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO EL ID_TIPO: " + e);
        }

        return id_tipo;
    }

    public String encontrarTipoProd2(int id) {
        String tipo = " ";

        String sql = "SELECT desc_tipoProd2 from TIPO_PROD2 where id_tipoProd2 = " + id;
        conexionDB con = new conexionDB();
        Connection reg = con.conectar();

        try {
            Statement st1 = reg.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                tipo = rs1.getString(1);
            }

            reg.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO LA DESCRIPCION DEL ID TIPO 2: " + e);
        }

        return tipo;
    }

    public void traerArticulosComboBox(JComboBox comboBoxArticulos) {

        String sql = "SELECT desc_articulo from Articulo";

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                comboBoxArticulos.addItem(rs1.getString(1));
            }

            comboBoxArticulos.setSelectedIndex(-1);

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO ARTICULOS: " + e);
        }
    }

    public Articulo traerArticuloPorCodigo(String codigo) {
        Articulo a = new Articulo();
        String sql = "SELECT codigo, desc_articulo, id_tipoProd, id_tipoProd2, precio_costo, precio_vta from Articulo where codigo = '" + codigo + "'";
        System.out.println(sql);

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                a.setCodigo(codigo);
                a.setDesc_articulo(rs1.getString(2));
                a.setId_tipoProd1(rs1.getInt(3));
                a.setId_tipoProd2(rs1.getInt(4));
                a.setPrecio_costo(rs1.getFloat(5));
                a.setPrecio_venta(rs1.getFloat(6));

            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO ARTICULOS: " + e);
        }

        return a;

    }

    public Articulo traerArticuloPorString(String bus) {

        Articulo a = new Articulo();
        String sql = "SELECT codigo, desc_articulo, id_tipoProd, id_tipoProd2, precio_costo, precio_vta from Articulo where desc_articulo = '" + bus + "'";

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                a.setCodigo(rs1.getString(1));
                a.setDesc_articulo(rs1.getString(2));
                a.setId_tipoProd1(rs1.getInt(3));
                a.setId_tipoProd2(rs1.getInt(4));
                a.setPrecio_costo(rs1.getFloat(5));
                a.setPrecio_venta(rs1.getFloat(6));

            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO ARTICULOS: " + e);
        }

        return a;

    }

    public Articulo traerCervezaArtesanalComboBox(JComboBox cervezas) {
        Articulo a = new Articulo();

        String sql = "SELECT codigo, desc_articulo, id_tipoProd, id_tipoProd2, precio_costo, precio_vta "
                + "FROM Articulo "
                + "WHERE id_tipoProd2 = 5";

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                cervezas.addItem(rs1.getString(2));
                a.setCodigo(rs1.getString(1));
                a.setDesc_articulo(rs1.getString(2));
                a.setId_tipoProd1(rs1.getInt(3));
                a.setId_tipoProd2(rs1.getInt(4));
                a.setPrecio_costo(rs1.getFloat(5));
                a.setPrecio_venta(rs1.getFloat(6));
            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO ARTICULOS: " + e);
        }

        return a;
    }

    public int insertarBarril(Barril b) {
        int n = 0;
        conexionDB con = new conexionDB();
        Connection reg = con.conectar();
        String sql = "INSERT INTO barril (desc_barril, cantidad_total, cantidad_consumida, posicion) VALUES (?,?,?,?)";

        try {

            PreparedStatement ps = reg.prepareStatement(sql);
            ps.setString(1, b.getDesc_barril());
            ps.setFloat(2, b.getCantidad_total());
            ps.setFloat(3, b.getCantidad_consumida());
            ps.setInt(4, b.getPosicion());

            System.out.println(sql);

            n = ps.executeUpdate();
            reg.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }

        return n;

    }

    public void traerBarrilesActivos(DefaultCategoryDataset datosBarriles, int posicion) {
        conexionDB con = new conexionDB();
        Connection c = con.conectar();

        String sql = "SELECT id_barril, desc_barril, cantidad_total, cantidad_consumida, posicion "
                + "FROM Barril "
                + "WHERE (cantidad_total <> cantidad_consumida) and posicion = " + posicion;
        System.out.println(sql);

        try {
            Statement st1 = c.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                datosBarriles.addValue(rs1.getFloat(3) - rs1.getFloat(4), rs1.getString(2), String.valueOf(rs1.getInt(5)));
                //datosBarriles.setValue(rs1.getFloat(3) - rs1.getFloat(4), rs1.getInt(5)+" "+rs1.getString(2), "Total");
                //datosBarriles.addValue(rs1.getFloat(3) - rs1.getFloat(4), rs1.getInt(5) + " " + rs1.getString(2), "Total");
            }

            rs1.close();
            c.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO BARRILES: " + e);
        }

    }

    public void traerBarrilesActivosComboBox(JComboBox barrilesActivos) {
        conexionDB con = new conexionDB();
        Connection c = con.conectar();

        String sql = "SELECT id_barril, desc_barril, cantidad_total, cantidad_consumida, posicion "
                + "FROM Barril "
                + "WHERE (cantidad_total <> cantidad_consumida) "
                + "ORDER BY posicion asc";
        System.out.println(sql);

        try {
            Statement st1 = c.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                barrilesActivos.addItem(rs1.getString(2) + " - " + String.valueOf(rs1.getFloat(3) - rs1.getFloat(4)) + " litros");
                //datosBarriles.setValue(rs1.getFloat(3) - rs1.getFloat(4), rs1.getInt(5)+" "+rs1.getString(2), "Total");
                //datosBarriles.addValue(rs1.getFloat(3) - rs1.getFloat(4), rs1.getInt(5) + " " + rs1.getString(2), "Total");
            }

            rs1.close();
            c.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO BARRILES: " + e);
        }

    }

    public Barril comprobarBarril(int posicion) {
        Barril b = new Barril();
        Articulo agos = new Articulo();

        conexionDB con = new conexionDB();
        Connection c = con.conectar();

        String sql = "SELECT id_barril, desc_barril, cantidad_total, cantidad_consumida, posicion "
                + "FROM Barril "
                + "WHERE posicion = " + posicion + " and cantidad_consumida <> cantidad_total";

        System.out.println(sql);

        try {
            Statement st1 = c.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                b.setId_barril(rs1.getInt(1));
                b.setDesc_barril(rs1.getString(2));
                b.setCantidad_total(rs1.getFloat(3));
                b.setCantidad_consumida(rs1.getFloat(4));
                b.setPosicion(rs1.getInt(5));
            }

            c.close();

        } catch (Exception e) {
            System.err.println("ERROR COMPROBANDO BARRIL: " + e);
        }

        return b;
    }

    public void actualizarConsumoBarril(Barril b) {
        conexionDB con = new conexionDB();
        Connection c = con.conectar();

        String sql = "UPDATE BARRIL "
                + "SET cantidad_consumida = cantidad_total "
                + "WHERE id_barril = " + b.getId_barril();

        Statement st1;
        try {
            st1 = c.createStatement();
            st1.executeUpdate(sql);

            c.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void descontarPintaBarril(float descuento, int posicion, String tipo_cerveza, DefaultCategoryDataset datosBarriles) {
        conexionDB con = new conexionDB();
        Connection c = con.conectar();

        String sql = "UPDATE BARRIL "
                + "SET cantidad_consumida = cantidad_consumida + " + descuento + " "
                + "WHERE posicion = " + posicion + " and (cantidad_consumida <> cantidad_total)";

        System.out.println(sql);

        Statement st1;
        try {
            st1 = c.createStatement();
            st1.executeUpdate(sql);

            c.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }

        datosBarriles.incrementValue(-descuento, tipo_cerveza, String.valueOf(posicion));
    }

    public int obtenerPosicionBarril(String tipo_cerveza) {
        int posicion = 0;

        String sql = "SELECT posicion from BARRIL where desc_barril = '" + tipo_cerveza + "' "
                + "and cantidad_total <> cantidad_consumida";

        System.out.println(sql);

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        Statement st1;
        ResultSet rs1;

        try {
            st1 = con.createStatement();
            rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                posicion = rs1.getInt(1);
            }

            con.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }

        return posicion;
    }

    public float traerPrecioHH() {
        float precioHH = 0;

        String sql = "SELECT valor_configuracion "
                + "FROM Configuracion "
                + "WHERE desc_configuracion = 'Happy Hour'";

        System.out.println(sql);

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        Statement st1;
        ResultSet rs1;

        try {
            st1 = con.createStatement();
            rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                precioHH = rs1.getFloat(1);
            }

            con.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }

        return precioHH;
    }

    public void actualizarStock(String codigo) {

        conexionDB con = new conexionDB();
        Connection c = con.conectar();

        String sql = "UPDATE ARTICULO "
                + "SET stock = stock - 1 "
                + "WHERE codigo = '" + codigo + "'";

        Statement st1;
        try {
            st1 = c.createStatement();
            st1.executeUpdate(sql);

            c.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public boolean verificarStock(String codigo) {
        boolean b = true;

        String sql = "SELECT stock "
                + "FROM Articulo "
                + "WHERE codigo = '" + codigo + "'";

        System.out.println(sql);

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        Statement st1;
        ResultSet rs1;

        try {
            st1 = con.createStatement();
            rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                if (rs1.getInt(1) == 0) {
                    b = false;
                }
            }

            con.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }

        return b;

    }

    public void actualizarBarriles(ArrayList<Barril> array, DefaultCategoryDataset datosBarriles) {

        for (int i = 0; i < array.size(); i++) {
            descontarPintaBarril(array.get(i).getCantidad_consumida(), array.get(i).getPosicion(), array.get(i).getDesc_barril(), datosBarriles);
        }
    }

    public void actualizarArticulo(Articulo a, String codigo) {
        conexionDB con = new conexionDB();
        Connection c = con.conectar();

        String sql = "UPDATE ARTICULO "
                + "SET codigo =  '" + a.getCodigo() + "', "
                + "desc_articulo = '" + a.getDesc_articulo() + "', "
                + "id_tipoProd = " + a.getId_tipoProd1() + ", "
                + "id_tipoProd2 = " + a.getId_tipoProd2() + ", "
                + "precio_costo = " + a.getPrecio_costo() + ", "
                + "precio_vta = " + a.getPrecio_venta() + " "
                + "WHERE codigo = '" + codigo + "'";

        System.out.println(sql);

        Statement st1;
        try {
            st1 = c.createStatement();
            st1.executeUpdate(sql);

            c.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int insertarPromocion(Equiv_Prom ep) {
        int n = 0;
        conexionDB con = new conexionDB();
        Connection reg = con.conectar();
        String sql = "INSERT INTO Equiv_Prom (codigo_promocion, codigo_articulo, cantidad_articulo) VALUES (?,?,?)";

        try {

            PreparedStatement ps = reg.prepareStatement(sql);
            ps.setString(1, ep.getCodigo_promocion());
            ps.setString(2, ep.getCodigo_articulo());
            ps.setInt(3, ep.getCantidad_articulo());

            System.out.println(sql);

            n = ps.executeUpdate();
            reg.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }

        return n;
    }

    public void insertarPromocionArray(ArrayList<Equiv_Prom> eps) {
        int n = 0;
        conexionDB con = new conexionDB();
        Connection reg = con.conectar();
        String sql = "INSERT INTO Equiv_Prom (codigo_promocion, codigo_articulo, cantidad_articulo) VALUES (?,?,?)";

        for (int i = 0; i < eps.size(); i++) {
            try {

                PreparedStatement ps = reg.prepareStatement(sql);
                ps.setString(1, eps.get(i).getCodigo_promocion());
                ps.setString(2, eps.get(i).getCodigo_articulo());
                ps.setInt(3, eps.get(i).getCantidad_articulo());

                System.out.println(sql);

                ps.executeUpdate();

            } catch (SQLException ex) {
                Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        try {
            reg.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void eliminarPromocion(String codigo) {
        conexionDB con = new conexionDB();
        Connection reg = con.conectar();
        String sql = "DELETE FROM Equiv_Prom "
                + "WHERE Codigo_promocion = '" + codigo + "'";

        try {

            Statement st1 = reg.createStatement();
            st1.executeUpdate(sql);

            System.out.println(sql);

            reg.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public ArrayList<Object[]> traerArticulosPromocion(String codigo) {
        ArrayList<Object[]> promocion = new ArrayList<>();

        String sql = "SELECT e.codigo_articulo, a.desc_articulo, e.cantidad_articulo, a.precio_vta "
                + "FROM Equiv_Prom as e "
                + "JOIN Articulo as a "
                + "ON e.codigo_articulo = a.codigo "
                + "WHERE e.codigo_promocion = '" + codigo + "'";

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                Object[] fc = new Object[4];
                fc[0] = rs1.getString(1);
                fc[1] = rs1.getString(2);
                fc[2] = rs1.getInt(3);
                fc[3] = rs1.getFloat(4);

                promocion.add(fc);

                //modeloTablaFacturacion.addRow(tipo);
            }

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO FACTURACION COSTOS: " + e);
        }

        for (int i = 0; i < promocion.size(); i++) {
            System.out.println(promocion.get(i)[0] + " " + promocion.get(i)[2]);
        }

        return promocion;
    }

    public int insertarTipoProd2(Tipo_Prod2 tp2) {
        int n = 0;
        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        String sql = "INSERT into Tipo_Prod2 "
                + "(desc_tipoProd2, id_tipoProd1) "
                + "VALUES ('" + tp2.getDesc_tipoProd2() + "', " + tp2.getId_tipoProd1() + ")";

        try {
            Statement st1 = con.createStatement();
            n = st1.executeUpdate(sql);
        } catch (Exception e) {
            System.err.println("ERROR INSERTANDO TIPO PROD 2: " + e);
        }

        return n;
    }

    public int actualizarTipo2(Tipo_Prod2 tp2) {
        int n = 0;
        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        String sql = "UPDATE Tipo_Prod2 "
                + "set desc_tipoProd2 = '" + tp2.getDesc_tipoProd2() + "', "
                + "id_tipoProd1 = " + tp2.getId_tipoProd1() + " "
                + "WHERE id_tipoProd2 = " + tp2.getId_tipoProd2();

        System.out.println(sql);

        try {
            Statement st1 = con.createStatement();
            n = st1.executeUpdate(sql);
        } catch (Exception e) {
            System.err.println("ERROR ACTUALIZANDO TIPO PROD 2: " + e);
        }

        return n;
    }

    public boolean comprobarBarril(String desc) {
        boolean b = false;

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        String sql = "SELECT desc_barril "
                + "FROM barril "
                + "WHERE cantidad_consumida <> cantidad_total and desc_barril = '" + desc + "'";

        System.out.println(sql);

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                if (rs1.getString(1).equals(desc)) {
                    b = true;
                    System.out.println("hay barril de " + desc);
                }
            }

        } catch (Exception e) {
            System.err.println("ERROR COMPROBANDO BARRILES: " + e);
        }

        return b;
    }

    public void actualizarIDs(String id_nuevo, String id_anterior) {
        conexionDB con = new conexionDB();
        Connection c = con.conectar();

        String sql1 = "UPDATE DETALLE_MESA "
                + "SET codigo =  '" + id_nuevo + "' "
                + "WHERE codigo = '" + id_anterior + "'";

        String sql2 = "UPDATE EQUIV_PROM "
                + "SET codigo_articulo =  '" + id_nuevo + "' "
                + "WHERE codigo_articulo = '" + id_anterior + "'";

        System.out.println(sql1);
        System.out.println(sql2);

        Statement st1;
        Statement st2;
        try {
            st1 = c.createStatement();
            st1.executeUpdate(sql1);
            st2 = c.createStatement();
            st2.executeUpdate(sql2);

            c.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void inhabilitarArticulo(String codigo) {
        conexionDB con = new conexionDB();
        Connection c = con.conectar();

        String sql1 = "UPDATE ARTICULO "
                + "SET baja =  1 "
                + "WHERE codigo = '" + codigo+ "'";

        System.out.println(sql1);

        Statement st1;

        try {
            st1 = c.createStatement();
            st1.executeUpdate(sql1);

            c.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void habilitarArticulo(String codigo) {
        conexionDB con = new conexionDB();
        Connection c = con.conectar();

        String sql1 = "UPDATE ARTICULO "
                + "SET baja =  0 "
                + "WHERE codigo = '" + codigo+ "'";

        System.out.println(sql1);

        Statement st1;

        try {
            st1 = c.createStatement();
            st1.executeUpdate(sql1);

            c.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
