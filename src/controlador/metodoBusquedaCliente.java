/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import boston.conexionDB;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import vista.CuentasCorrientes;
import vista.NuevaMesa;
import vista.nuevaVtaRapida;

/**
 *
 * @author rmmayer
 */
public class metodoBusquedaCliente {

    public NuevaMesa c;
    public CuentasCorrientes cc;
    //public JFrame cpd;

    public metodoBusquedaCliente(JFrame c, int tipo) {
        if (tipo == 1) {
            this.c = (NuevaMesa) c;
        } else {
            this.cc = (CuentasCorrientes) c;
        }
    }

    public boolean comparar(String cadena, int tipo) {
        int xx = 0;
        boolean encontrado = false;
        if (tipo == 1) {
            Object[] lista = this.c.getcomboBoxCliente().getSelectedObjects();//       traer todos los items mejor.
            xx = this.c.getcomboBoxCliente().getItemCount();
            for (int i = 0; i < xx; i++) {
                //  System.out.println("elemento seleccionado - "+lista[i]);

                if (cadena.equals(this.c.getcomboBoxCliente().getItemAt(i))) {
                    //  nn=(String)boxNombre.getItemAt(i).toString(); 
                    encontrado = true;
                    break;
                }

            }
        } else {
            Object[] lista = this.cc.getcomboBoxCliente().getSelectedObjects();
            xx = this.cc.getcomboBoxCliente().getItemCount();
            for (int i = 0; i < xx; i++) {
                //  System.out.println("elemento seleccionado - "+lista[i]);

                if (cadena.equals(this.cc.getcomboBoxCliente().getItemAt(i))) {
                    //  nn=(String)boxNombre.getItemAt(i).toString(); 
                    encontrado = true;
                    break;
                }

            }
        }

        return encontrado;

    }

    public DefaultComboBoxModel getLista(String cadenaEscrita, int tipo) {
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();

        try {
            conexionDB con = new conexionDB();
            Connection conex = con.conectar();
            Statement st1 = conex.createStatement();
            String sql = "";
            if (tipo == 1) {
                sql = "select nombre_cliente, apellido_cliente "
                        + "from CLIENTE "
                        + "WHERE nombre_cliente LIKE '%" + cadenaEscrita + "%' or apellido_cliente LIKE '%" + cadenaEscrita + "%'";
            }else{
                sql = "select c.nombre_cliente, c.apellido_cliente, cc.habilitada "
                        + "from CLIENTE as c "
                        + "JOIN CTA_CTE as cc "
                        + "ON c.id_cliente = cc.id_cliente "
                        + "WHERE c.nombre_cliente LIKE '%" + cadenaEscrita + "%' or c.apellido_cliente LIKE '%" + cadenaEscrita + "%' and cc.habilitada = 1";
            }

            //String sql2 = "select desc_articulo from articulo where desc_articulo LIKE '%"+cadenaEscrita+"%'";
            System.out.println(sql);
            ResultSet res = st1.executeQuery(sql);

            while (res.next()) {
                modelo.addElement(res.getString(1) + " " + res.getString(2));
            }
            res.close();
            conex.close();
        } catch (SQLException ex) {

        }
        return modelo;
    }

}
