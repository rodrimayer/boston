/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import boston.conexionDB;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import modelo.Privilegio;
import modelo.Usuario;
import modelo.Configuracion;
import modelo.Tipo_Prod2;
import modelo.Tipo_costo;

/**
 *
 * @author rmmayer
 */
public class manejadorConfiguraciones {

    public ArrayList<Configuracion> traerVariables(JComboBox variables) {
        conexionDB c = new conexionDB();
        Connection con = c.conectar();
        ArrayList<Configuracion> array = new ArrayList<>();
        
        variables.removeAllItems();

        String sql = "SELECT desc_configuracion, id_configuracion, valor_configuracion "
                + "FROM Configuracion ";

        System.out.println(sql);

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                variables.addItem(rs1.getString(1));
                Configuracion conf = new Configuracion();
                conf.setDesc_configuracion(rs1.getString(1));
                conf.setId_configuracion(rs1.getInt(2));
                conf.setValor_configuracion(rs1.getFloat(3));
                
                array.add(conf);

            }

            variables.setSelectedIndex(-1);
            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO LAS VARIABLES: " + e);
        }
        
        return array;
    }

    public ArrayList<Usuario> traerUsuarios(JComboBox usuarios) {
        conexionDB c = new conexionDB();
        Connection con = c.conectar();
        ArrayList<Usuario> array = new ArrayList<>();
        
        usuarios.removeAllItems();

        String sql = "SELECT u.dni, u.cuenta_user, u.pass_user, u.nombre_user, u.apellido_user, u.direccion_user, u.telefono, p.id_privilegio, p.desc_privilegio, u.habilitado "
                + "FROM User as u "
                + "JOIN Privilegio as p "
                + "ON u.id_privilegio = p.id_privilegio";

        System.out.println(sql);

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                Usuario u = new Usuario();
                Privilegio p = new Privilegio();
                
                p.setId_privilegio(rs1.getInt(8));
                p.setDesc_privilegio(rs1.getString(9));
                
                u.setId_usuario(rs1.getInt(1));
                u.setCuenta_usuario(rs1.getString(2));
                u.setPass_usuario(rs1.getString(3));
                u.setNombre_usuario(rs1.getString(4));
                u.setApellido_usuario(rs1.getString(5));
                u.setDireccion_usuario(rs1.getString(6));
                u.setTelefono(rs1.getString(7));
                u.setPrivilegio(p);
                u.setHabilitado(rs1.getInt(10));
                
                array.add(u);
                
                usuarios.addItem(u.getNombre_usuario()+" "+u.getApellido_usuario());
                

            }

            usuarios.setSelectedIndex(-1);
            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO LOS USUARIOS: " + e);
        }

        return array;
    }
    
    public void traerPrivilegios (JComboBox privilegios){
        conexionDB c = new conexionDB();
        Connection con = c.conectar();
        
        privilegios.removeAllItems();
        
        String sql = "SELECT id_privilegio, desc_privilegio "
                + "FROM Privilegio";
        
        System.out.println(sql);
        
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                Usuario u = new Usuario();
                Privilegio p = new Privilegio();
                
                p.setId_privilegio(rs1.getInt(1));
                p.setDesc_privilegio(rs1.getString(2));                
                
                privilegios.addItem(p.getDesc_privilegio());

            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO LOS PRIVILEGIOS: " + e);
        }
        
    }
    
    public int insertarVariable (Configuracion conf, JComboBox variables){
        String sqlAgregarCliente = "INSERT INTO Configuracion (desc_configuracion, valor_configuracion) VALUES (?,?)";
        conexionDB c = new conexionDB();
        Connection con = c.conectar();
        int n = 0;
        
        try {
            PreparedStatement ps1 = con.prepareStatement(sqlAgregarCliente);
            ps1.setString(1, conf.getDesc_configuracion());
            ps1.setFloat(2, conf.getValor_configuracion());            
            n = ps1.executeUpdate();

            if (n > 0) {
                //JOptionPane.showMessageDialog(null, "Registro guardado con exito");
                con.close();
                //variables.removeAllItems();
                //this.traerVariables(variables);
            }

        } catch (SQLException ex) {
            Logger.getLogger(manejadorCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return n;
    }
    
    public void insertarPrivilegio (Privilegio p, JComboBox privilegios){
        String sqlAgregarCliente = "INSERT INTO Privilegio (desc_privilegio) VALUES (?)";
        conexionDB c = new conexionDB();
        Connection con = c.conectar();
        try {
            PreparedStatement ps1 = con.prepareStatement(sqlAgregarCliente);
            ps1.setString(1, p.getDesc_privilegio());                       
            int n = ps1.executeUpdate();

            if (n > 0) {
                JOptionPane.showMessageDialog(null, "Registro guardado con exito");
                con.close();
                privilegios.removeAllItems();
                this.traerPrivilegios(privilegios);
            }

        } catch (SQLException ex) {
            Logger.getLogger(manejadorCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void modificarVariable (Configuracion conf){
        String sql = "UPDATE Configuracion "
                + "SET valor_configuracion = "+conf.getValor_configuracion()+" "
                + "WHERE id_configuracion = "+conf.getId_configuracion();
        
        System.out.println(sql);
        
        conexionDB c = new conexionDB();
        Connection con = c.conectar();
        
        try{
            Statement st1 = con.createStatement();
            st1.executeUpdate(sql);
            
            con.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(manejadorConfiguraciones.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public Configuracion valorVariableString (String variable){
        Configuracion conf = new Configuracion();
        
        conexionDB c = new conexionDB();
        Connection con = c.conectar();
        
        String sql = "SELECT id_configuracion, valor_configuracion "
                + "FROM Configuracion "
                + "WHERE desc_configuracion = '"+variable+"'";
        
        System.out.println(sql);
        
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                conf.setId_configuracion(rs1.getInt(1));
                conf.setDesc_configuracion(variable);
                conf.setValor_configuracion(rs1.getFloat(2));
            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO LOS PRIVILEGIOS: " + e);
        }
        
        return conf;
    }
    
    public ArrayList<Tipo_costo> traerTipoCosto(JComboBox tipoCosto) {
        conexionDB c = new conexionDB();
        Connection con = c.conectar();
        ArrayList<Tipo_costo> array = new ArrayList<>();
        
        tipoCosto.removeAllItems();

        String sql = "SELECT id_tipoCosto, desc_tipoCosto "
                + "FROM tipo_costo ";

        System.out.println(sql);

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                tipoCosto.addItem(rs1.getString(2));
                Tipo_costo tc = new Tipo_costo();
                tc.setId_tipoCosto(rs1.getInt(1));
                tc.setDesc_tipoCosto(rs1.getString(2));
                
                array.add(tc);

            }

            tipoCosto.setSelectedIndex(-1);
            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO LOS TIPOS DE COSTO: " + e);
        }
        
        return array;
    }
    
    public ArrayList<Tipo_Prod2> traerTipoProd2 (JComboBox tipoprod2){
        ArrayList<Tipo_Prod2> tipos = new ArrayList<>();
        
        conexionDB c = new conexionDB();
        Connection con = c.conectar();
        ArrayList<Tipo_costo> array = new ArrayList<>();
        
        tipoprod2.removeAllItems();

        String sql = "SELECT id_tipoProd2, desc_tipoProd2, id_tipoProd1 "
                + "FROM tipo_prod2 ";

        System.out.println(sql);

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                tipoprod2.addItem(rs1.getString(2));
                Tipo_Prod2 tp2 = new Tipo_Prod2();
                tp2.setId_tipoProd2(rs1.getInt(1));
                tp2.setDesc_tipoProd2(rs1.getString(2));
                tp2.setId_tipoProd1(rs1.getInt(3));
                
                tipos.add(tp2);

            }

            tipoprod2.setSelectedIndex(-1);
            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO LOS TIPOS ARTICULOS: " + e);
        }
        
        
        return tipos;
    }
    

}
