/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import boston.conexionDB;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modelo.Cta_Cte;
import modelo.Detalle_Cta_Cte;
import modelo.Facturacion_Cta_Cte;

/**
 *
 * @author Rodri Mayer
 */
public class manejadorCtaCte {

    public void crearCtaCte(int id_cliente) {
        conexionDB con = new conexionDB();
        Connection c = con.conectar();

        String sql = "INSERT INTO CTA_CTE (id_cliente, habilitada) VALUES (?,?)";

        System.out.println(sql);

        try {

            PreparedStatement ps = c.prepareStatement(sql);
            ps.setInt(1, id_cliente);
            ps.setInt(2, 1);

            ps.executeUpdate();
            c.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorCtaCte.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Cta_Cte obtenerCtaCte(int dni) {
        conexionDB con = new conexionDB();
        Connection c = con.conectar();

        Cta_Cte cc = new Cta_Cte();

        /*
        String sql = "SELECT cc.id_cta_cte, cc.id_cliente, SUM(dcc.saldo), cc.habilitada "
                + "FROM CTA_CTE as cc "
                + "JOIN DETALLE_CTA_CTE as dcc "
                + "ON cc.id_cta_cte = dcc.id_cta_cte "
                + "WHERE cc.id_cliente = " + dni;*/
        String sql = "SELECT cc.id_cta_cte, cc.id_cliente, cc.habilitada "
                + "FROM CTA_CTE as cc "
                + "WHERE cc.id_cliente = " + dni;

        System.out.println(sql);

        try {
            Statement st1 = c.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                cc.setId_cta_cte(rs1.getInt(1));
                cc.setId_cliente(dni);
                cc.setHabilitada(rs1.getInt(3));
            }

            c.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO CUENTA CORRIENTE: " + e);
        }

        return cc;
    }

    public void insertarDetalleCtaCte(Detalle_Cta_Cte dcc) {
        conexionDB con = new conexionDB();
        Connection c = con.conectar();

        String sql = "INSERT INTO DETALLE_CTA_CTE (id_cta_cte, id_boleta, saldo) VALUES (?, ?, ?)";

        System.out.println(sql);

        try {

            PreparedStatement ps = c.prepareStatement(sql);
            ps.setInt(1, dcc.getId_cta_cte());
            ps.setInt(2, dcc.getId_boleta());
            ps.setFloat(3, dcc.getSaldo());

            ps.executeUpdate();
            c.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorCtaCte.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void actualizarSaldoCtaCte(float total, int id) {
        conexionDB con = new conexionDB();
        Connection c = con.conectar();

        String sql = "UPDATE CTA_CTE "
                + "SET total = total + " + total + " "
                + "WHERE id_cta_cte = " + id;

        System.out.println(sql);
        Statement st1;
        try {

            st1 = c.createStatement();
            st1.executeUpdate(sql);

            c.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorCtaCte.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void bajaCtaCte(int id) {
        conexionDB con = new conexionDB();
        Connection c = con.conectar();

        String sql = "UPDATE CTA_CTE "
                + "SET habilitada = 0 "
                + "WHERE id_cta_cte = " + id;

        System.out.println(sql);
        Statement st1;
        try {

            st1 = c.createStatement();
            st1.executeUpdate(sql);

            c.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorCtaCte.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void altaCtaCte(int id) {
        conexionDB con = new conexionDB();
        Connection c = con.conectar();

        String sql = "UPDATE CTA_CTE "
                + "SET habilitada = 1 "
                + "WHERE id_cta_cte = " + id;

        System.out.println(sql);
        Statement st1;
        try {

            st1 = c.createStatement();
            st1.executeUpdate(sql);

            c.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorCtaCte.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Detalle_Cta_Cte obtenerDetalleCtaCte(int id_boleta) {
        Detalle_Cta_Cte dcc = new Detalle_Cta_Cte();

        conexionDB con = new conexionDB();
        Connection c = con.conectar();

        Cta_Cte cc = new Cta_Cte();

        String sql = "SELECT id_detalle, id_cta_cte, id_boleta, saldo "
                + "FROM DETALLE_CTA_CTE "
                + "WHERE id_boleta = " + id_boleta;

        System.out.println(sql);

        try {
            Statement st1 = c.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                dcc.setId_detalle(rs1.getInt(1));
                dcc.setId_cta_cte(rs1.getInt(2));
                dcc.setId_boleta(rs1.getInt(3));
                dcc.setSaldo(rs1.getFloat(4));
            }

            c.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO DETALLE CUENTA CORRIENTE: " + e);
        }

        return dcc;

    }

    public float obtenerDeudaCtaCte(int id) {
        conexionDB con = new conexionDB();
        Connection c = con.conectar();

        float total = 0;

        String sql = "SELECT SUM(saldo) "
                + "FROM DETALLE_CTA_CTE "
                + "WHERE id_cta_cte = " + id;

        System.out.println(sql);

        try {
            Statement st1 = c.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                total = rs1.getFloat(1);
            }

            c.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO CUENTA CORRIENTE: " + e);
        }

        return total;
    }

    public void pagoTotalCtaCte(int id) {
        conexionDB con = new conexionDB();
        Connection c = con.conectar();

        String sql = "UPDATE DETALLE_CTA_CTE "
                + "SET saldo = 0 "
                + "WHERE id_cta_cte = " + id;

        System.out.println(sql);
        Statement st1;
        try {

            st1 = c.createStatement();
            st1.executeUpdate(sql);
            JOptionPane.showMessageDialog(null, "SE CANCELO LA TOTALIDAD DE LA CTA_CTE CON EXITO");

            c.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorCtaCte.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void insertarFacturacionCtaCte(Facturacion_Cta_Cte fcc) {
        conexionDB con = new conexionDB();
        Connection c = con.conectar();

        String sql = "INSERT INTO FACTURACION_CTA_CTE (id_cta_cte, id_condVta, monto, fecha) VALUES (?, ?, ?, ?)";

        System.out.println(sql);

        try {

            PreparedStatement ps = c.prepareStatement(sql);
            ps.setInt(1, fcc.getId_cta_cte());
            ps.setInt(2, fcc.getId_condVta());
            ps.setFloat(3, fcc.getMonto());
            ps.setString(4, fcc.getFechasS());

            ps.executeUpdate();
            c.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorCtaCte.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void pagoBoletasParcial(int id_boleta, float monto) {
        conexionDB con = new conexionDB();
        Connection c = con.conectar();

        String sql = "UPDATE DETALLE_CTA_CTE "
                + "SET saldo = saldo - " + monto + " "
                + "WHERE id_boleta = " + id_boleta;

        System.out.println(sql);
        Statement st1;
        try {

            st1 = c.createStatement();
            st1.executeUpdate(sql);
            //JOptionPane.showMessageDialog(null,"PAGO PARCIAL REALIZADO CON EXITO");

            c.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorCtaCte.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void pagarBoletaCtaCte(int id_boleta) {
        conexionDB con = new conexionDB();
        Connection c = con.conectar();

        String sql = "UPDATE DETALLE_CTA_CTE "
                + "SET saldo = 0 "
                + "WHERE id_boleta = " + id_boleta;

        System.out.println(sql);
        Statement st1;
        try {

            st1 = c.createStatement();
            st1.executeUpdate(sql);
            //JOptionPane.showMessageDialog(null,"PAGO PARCIAL REALIZADO CON EXITO");

            c.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorCtaCte.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
