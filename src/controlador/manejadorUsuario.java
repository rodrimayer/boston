/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import boston.conexionDB;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modelo.Privilegio;
import modelo.Usuario;

/**
 *
 * @author rmmayer
 */
public class manejadorUsuario {

    public Usuario traerUsuario(String cuenta, String contraseña) {
        Usuario u = new Usuario();
        Privilegio p = new Privilegio();
        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        String sql = "SELECT u.dni, u.cuenta_user, u.pass_user, u.nombre_user, u.apellido_user, p.id_privilegio, p.desc_privilegio, u.habilitado, u.root "
                + "FROM user u "
                + "JOIN privilegio p "
                + "ON u.id_privilegio = p.id_privilegio "
                + "WHERE u.cuenta_user = '" + cuenta + "' and u.pass_user = '" + contraseña + "'";

        Statement st1;
        try {
            st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                p.setId_privilegio(rs1.getInt(6));
                p.setDesc_privilegio(rs1.getString(7));
                u.setId_usuario(rs1.getInt(1));
                u.setCuenta_usuario(rs1.getString(2));
                u.setPass_usuario(rs1.getString(3));
                u.setNombre_usuario(rs1.getString(4));
                u.setApellido_usuario(rs1.getString(5));
                u.setPrivilegio(p);
                u.setHabilitado(rs1.getInt(8));
                u.setRoot(rs1.getInt(9));

                System.out.println("nombre usuario: " + u.getNombre_usuario());
                System.out.println("apellido usuario: " + u.getApellido_usuario());
            }

        } catch (SQLException ex) {
            Logger.getLogger(manejadorUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }

        return u;
    }

    public Usuario traerMozo(String busqueda) {
        Usuario u = new Usuario();
        Privilegio p = new Privilegio();
        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        String sql = "SELECT u.dni, u.cuenta_user, u.pass_user, u.nombre_user, u.apellido_user, p.id_privilegio, p.desc_privilegio, u.habilitado "
                + "FROM user u "
                + "JOIN privilegio p "
                + "ON u.id_privilegio = p.id_privilegio "
                + "where CONCAT(u.nombre_user, ' ' , u.apellido_user) LIKE '%" + busqueda + "%'";

        System.out.println(sql);

        Statement st1;
        try {
            st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                p.setId_privilegio(rs1.getInt(6));
                p.setDesc_privilegio(rs1.getString(7));
                u.setId_usuario(rs1.getInt(1));
                u.setCuenta_usuario(rs1.getString(2));
                u.setPass_usuario(rs1.getString(3));
                u.setNombre_usuario(rs1.getString(4));
                u.setApellido_usuario(rs1.getString(5));
                u.setPrivilegio(p);
                u.setHabilitado(rs1.getInt(8));

                System.out.println("nombre usuario: " + u.getNombre_usuario());
                System.out.println("apellido usuario: " + u.getApellido_usuario());
            }

        } catch (SQLException ex) {
            Logger.getLogger(manejadorUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }

        return u;
    }
    
    public Usuario traerMozoID(int id) {
        Usuario u = new Usuario();
        Privilegio p = new Privilegio();
        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        String sql = "SELECT u.dni, u.cuenta_user, u.pass_user, u.nombre_user, u.apellido_user, p.id_privilegio, p.desc_privilegio, u.habilitado "
                + "FROM user u "
                + "JOIN privilegio p "
                + "ON u.id_privilegio = p.id_privilegio "
                + "where u.dni = "+id;

        System.out.println(sql);

        Statement st1;
        try {
            st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                p.setId_privilegio(rs1.getInt(6));
                p.setDesc_privilegio(rs1.getString(7));
                u.setId_usuario(rs1.getInt(1));
                u.setCuenta_usuario(rs1.getString(2));
                u.setPass_usuario(rs1.getString(3));
                u.setNombre_usuario(rs1.getString(4));
                u.setApellido_usuario(rs1.getString(5));
                u.setPrivilegio(p);
                u.setHabilitado(rs1.getInt(8));

                System.out.println("nombre usuario: " + u.getNombre_usuario());
                System.out.println("apellido usuario: " + u.getApellido_usuario());
            }

        } catch (SQLException ex) {
            Logger.getLogger(manejadorUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }

        return u;
    }

    public boolean existeUsuario(String cuenta, String contraseña) {
        boolean b = false;
        conexionDB c = new conexionDB();
        Connection con = c.conectar();
        String sql = "SELECT u.dni, u.cuenta_user, u.pass_user, u.nombre_user, u.apellido_user, p.id_privilegio, p.desc_privilegio, u.habilitado "
                + "FROM user u "
                + "JOIN privilegio p "
                + "ON u.id_privilegio = p.id_privilegio "
                + "WHERE u.cuenta_user = '" + cuenta + "' and u.pass_user = '" + contraseña + "' and u.habilitado = 1";

        Statement st1;
        try {
            st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                b = true;
            }

        } catch (SQLException ex) {
            Logger.getLogger(manejadorUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }

        return b;
    }

    public Usuario traerUsuarioString(String cadena) {
        Usuario u = new Usuario();
        Privilegio p = new Privilegio();
        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        String sql = "SELECT u.dni, u.cuenta_user, u.pass_user, u.nombre_user, u.apellido_user, p.id_privilegio, p.desc_privilegio, u.habilitado "
                + "FROM user u "
                + "JOIN privilegio p "
                + "ON u.id_privilegio = p.id_privilegio "
                + "where CONCAT(u.nombre_user, ' ' , u.apellido_user) LIKE '%" + cadena + "%'";

        System.out.println(sql);

        Statement st1;
        try {
            st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                p.setId_privilegio(rs1.getInt(6));
                p.setDesc_privilegio(rs1.getString(7));
                u.setId_usuario(rs1.getInt(1));
                u.setCuenta_usuario(rs1.getString(2));
                u.setPass_usuario(rs1.getString(3));
                u.setNombre_usuario(rs1.getString(4));
                u.setApellido_usuario(rs1.getString(5));
                u.setPrivilegio(p);
                u.setHabilitado(rs1.getInt(8));

                System.out.println("nombre usuario: " + u.getNombre_usuario());
                System.out.println("apellido usuario: " + u.getApellido_usuario());
            }

        } catch (SQLException ex) {
            Logger.getLogger(manejadorUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }

        return u;

    }

    public int insertarUsuario(Usuario u) {
        int n = 0;
        conexionDB con = new conexionDB();
        Connection reg = con.conectar();
        String sql = "INSERT INTO user (dni, cuenta_user, pass_user, nombre_user, apellido_user, direccion_user, telefono, id_privilegio, habilitado) VALUES (?,?,?,?,?,?,?,?,?)";

        try {

            PreparedStatement ps = reg.prepareStatement(sql);
            ps.setInt(1, u.getId_usuario());
            ps.setString(2, u.getCuenta_usuario());
            ps.setString(3, u.getPass_usuario());
            ps.setString(4, u.getNombre_usuario());
            ps.setString(5, u.getApellido_usuario());
            ps.setString(6, u.getDireccion_usuario());
            ps.setString(7, u.getTelefono());
            ps.setInt(8, u.getPrivilegio().getId_privilegio());
            ps.setInt(9, u.getHabilitado());

            System.out.println(sql);

            n = ps.executeUpdate();
            reg.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }

        return n;
    }

    public int actualizarUsuario(Usuario u) {
        int n = 0;
        conexionDB con = new conexionDB();
        Connection c = con.conectar();

        String sql = "UPDATE user "
                + "SET dni =  '" + u.getId_usuario() + "', "
                + "cuenta_user = '" + u.getCuenta_usuario() + "', "
                + "pass_user = '" + u.getPass_usuario() + "', "
                + "nombre_user = '" + u.getNombre_usuario() + "', "
                + "apellido_user = '" + u.getApellido_usuario() + "', "
                + "direccion_user = '" + u.getDireccion_usuario() + "', "
                + "telefono = '" + u.getTelefono() + "', "
                + "id_privilegio = " + u.getPrivilegio().getId_privilegio() + ", "
                + "habilitado = " + u.getHabilitado() + " "
                + "WHERE dni = " + u.getId_usuario();

        System.out.println(sql);

        Statement st1;
        try {
            st1 = c.createStatement();
            n = st1.executeUpdate(sql);

            c.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }

        return n;
    }

    public boolean isRoot(int id) {
        boolean b = false;

        conexionDB con = new conexionDB();
        Connection c = con.conectar();

        String sql = "SELECT root "
                + "FROM User "
                + "WHERE dni = " + id;

        System.out.println(sql);

        Statement st1;
        try {
            st1 = c.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);
            
            while(rs1.next()){
                if(rs1.getInt(1)==1){
                    b = true;
                }
            }
            
            if(b){
                System.out.println("ES USUARIO ROOT");
            }else{
                System.out.println("NO ES USUARIO ROOT");
            }
            
            c.close();

        } catch (Exception ex) {
            System.out.println("ERROR AL TRAER EL PRIVILEGIO ROOT: "+ex);
        }
        
        return b;
    }
}
    