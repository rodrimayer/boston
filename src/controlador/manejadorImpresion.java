/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import br.com.adilson.util.Extenso;
import br.com.adilson.util.PrinterMatrix;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import modelo.Boleta;
import modelo.Cierre_Caja;
import modelo.Cliente;
import modelo.Detalle_boleta;
import modelo.Usuario;

/**
 *
 * @author Rodri Mayer
 */
public class manejadorImpresion {

    manejadorFechas mf = new manejadorFechas();
    manejadorArticulos ma = new manejadorArticulos();
    manejadorFacturacion mfac = new manejadorFacturacion();

    public void imprimirFactura2(Boleta b, Cliente c, ArrayList<Detalle_boleta> aDB) {

        Calendar calendario = new GregorianCalendar();
        int horas, minutos, segundos;

        horas = calendario.get(Calendar.HOUR_OF_DAY);
        minutos = calendario.get(Calendar.MINUTE);
        segundos = calendario.get(Calendar.SECOND);

        PrinterMatrix printer = new PrinterMatrix();

        Extenso e = new Extenso();

        String ceros = "";

        if (b.getId_boleta() < 10) {
            ceros = "00000000000000";
        } else if (b.getId_boleta() < 100) {
            ceros = "0000000000000";
        } else if (b.getId_boleta() < 1000) {
            ceros = "000000000000";
        } else if (b.getId_boleta() < 10000) {
            ceros = "00000000000";
        }

        e.setNumber(70.85);

        //Definir el tamanho del papel para la impresion  aca 25 lineas y 80 columnas
        printer.setOutSize(10 + aDB.size(), 58);
        //Imprimir * de la 2da linea a 25 en la columna 1;
        // printer.printCharAtLin(2, 25, 1, "*");
        //Imprimir * 1ra linea de la columa de 1 a 80
        printer.printCharAtCol(1, 1, 32, "=");
        //Imprimir Encabezado nombre del La EMpresa
        printer.printTextWrap(1, 1, 1, 58, "BOSTON BEER & CO.");
        //printer.printTextWrap(linI, linE, colI, colE, null);
        printer.printTextWrap(2, 2, 1, 58, "Num. Boleta: " + ceros + b.getId_boleta());
        //printer.printTextWrap(2, 2, 33, 80, "Fecha de Emision: " + mf.convertirFecha(b.getFecha_boleta()) + "    Mesa: " + b.getMesa());
        //printer.printTextWrap(2, 2, 33, 80, "Fecha de Emision: " + mf.convertirFecha(b.getFecha_boleta()) + "    Hora: "+ horas+":"+minutos+":"+segundos);
        //printer.printTextWrap(2, 2, 33, 80, "Fecha: " + mf.convertirFecha(b.getFecha_cierre()) + " Hora: "+b.getHora_cierre().substring(0,5));
        printer.printTextWrap(2, 2, 33, 80, "Fecha: " + mf.convertirFecha(mf.convertirFechaInversa(b.getFecha_cierre())) + " Hora: " + b.getHora_cierre().substring(0, 5));
        printer.printTextWrap(3, 3, 1, 58, "Cli: " + c.getNombre_cliente() + " " + c.getApellido_cliente() + " Mesa:" + b.getMesa());
        printer.printTextWrap(3, 3, 33, 80, "Vendedor: " + b.getVendedor().getNombre_usuario() + " " + b.getVendedor().getApellido_usuario());

        printer.printTextWrap(4, 4, 1, 58, "===============================");
        printer.printTextWrap(4, 4, 33, 80, "Desc.   Cant.  P.Unit   P.Total");
        printer.printTextWrap(5, 5, 1, 58, "-------------------------------");

        int fila = 5;

        for (int i = 0; i < aDB.size(); i++) {
            String art;
            if (ma.traerArticuloPorCodigo(aDB.get(i).getCodigo()).getDesc_articulo().length() > 10) {
                art = ma.traerArticuloPorCodigo(aDB.get(i).getCodigo()).getDesc_articulo().substring(0, 10);
            } else {
                art = ma.traerArticuloPorCodigo(aDB.get(i).getCodigo()).getDesc_articulo();
            }
            String esp1 = "";
            String esp2 = "";
            float total = aDB.get(i).getPrecio() * aDB.get(i).getCantidad();

            if (aDB.get(i).getCantidad() < 10) {
                if (aDB.get(i).getPrecio() < 100) {
                    for (int j = 0; j < 4; j++) {
                        esp1 = esp1 + " ";
                    }
                } else {
                    for (int j = 0; j < 3; j++) {
                        esp1 = esp1 + " ";
                    }
                }

            } else if (aDB.get(i).getPrecio() < 100) {
                for (int j = 0; j < 3; j++) {
                    esp1 = esp1 + " ";
                }
            } else {

                for (int j = 0; j < 2; j++) {
                    esp1 = esp1 + " ";
                }
            }

            if (total < 1000) {
                if (aDB.get(i).getCantidad() < 10) {
                    if (aDB.get(i).getPrecio() < 100) {
                        for (int j = 0; j < 4; j++) {
                            esp2 = esp2 + " ";
                        }
                    } else {
                        for (int j = 0; j < 4; j++) {
                            esp2 = esp2 + " ";
                        }
                    }

                } else if (aDB.get(i).getPrecio() < 100) {
                    for (int j = 0; j < 4; j++) {
                        esp2 = esp2 + " ";
                    }
                } else {
                    for (int j = 0; j < 2; j++) {
                        esp2 = esp2 + " ";
                    }
                }

            } else {

                if (aDB.get(i).getCantidad() < 10) {
                    for (int j = 0; j < 3; j++) {
                        esp2 = esp2 + " ";
                    }
                } else if (aDB.get(i).getPrecio() < 100) {
                    for (int j = 0; j < 5; j++) {
                        esp2 = esp2 + " ";
                        System.out.println("");
                    }
                } else {
                    for (int j = 0; j < 3; j++) {
                        esp2 = esp2 + " ";
                    }
                }
            }

            printer.printTextWrap(fila, fila, 33, 150, art);
            printer.printTextWrap(fila, fila, 44, 150, aDB.get(i).getCantidad() + esp1 + "$" + aDB.get(i).getPrecio() + esp2 + "$" + total);
            fila++;
        }

        String esp = "";

        if (b.getTotal()
                < 1000) {
            for (int i = 0; i < 11; i++) {
                esp = esp + " ";
            }
        } else {
            for (int i = 0; i < 10; i++) {
                esp = esp + " ";
            }
        }

        printer.printTextWrap(fila, fila,
                1, 32, "-------------------------------");
        printer.printTextWrap(fila, fila,
                33, 80, "TOTAL A PAGAR:" + esp + "$" + b.getTotal());
        printer.printTextWrap(fila
                + 2, fila + 2, 1, 32, "Boleta sin valor fiscal");
        printer.printTextWrap(fila
                + 2, fila + 2, 33, 80, "Solo uso interno");

        printer.toFile(
                "impresion.txt");

        FileInputStream inputStream = null;

        try {
            inputStream = new FileInputStream("impresion.txt");
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        if (inputStream
                == null) {
            return;
        }

        DocFlavor docFormat = DocFlavor.INPUT_STREAM.AUTOSENSE;
        Doc document = new SimpleDoc(inputStream, docFormat, null);

        PrintRequestAttributeSet attributeSet = new HashPrintRequestAttributeSet();

        PrintService defaultPrintService = PrintServiceLookup.lookupDefaultPrintService();

        if (defaultPrintService
                != null) {
            DocPrintJob printJob = defaultPrintService.createPrintJob();
            try {
                printJob.print(document, attributeSet);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            System.err.println("No existen impresoras instaladas");
        }
        //inputStream.close();
    }

    public void imprimirFactura(Boleta b, Cliente c, ArrayList<Detalle_boleta> aDB) {

        Calendar calendario = new GregorianCalendar();
        int horas, minutos, segundos;

        horas = calendario.get(Calendar.HOUR_OF_DAY);
        minutos = calendario.get(Calendar.MINUTE);
        segundos = calendario.get(Calendar.SECOND);

        PrinterMatrix printer = new PrinterMatrix();

        Extenso e = new Extenso();

        String ceros = "";

        if (b.getId_boleta() < 10) {
            ceros = "00000000000000000";
        } else if (b.getId_boleta() < 100) {
            ceros = "0000000000000000";
        } else if (b.getId_boleta() < 1000) {
            ceros = "000000000000000";
        } else if (b.getId_boleta() < 10000) {
            ceros = "0000000000000";
        }else{
            ceros = "000000000000";
        }

        e.setNumber(70.85);

        //Definir el tamanho del papel para la impresion  aca 25 lineas y 80 columnas
        printer.setOutSize(18 + aDB.size(), 32);

        //printer.printCharAtCol(1, 1, 32, "=");
        printer.printTextWrap(1, 1, 1, 32, "===============================");
        printer.printTextWrap(2, 2, 1, 32, "BOSTON BEER & CO.");
        printer.printTextWrap(3, 3, 1, 14, "Num. Boleta: ");
        printer.printTextWrap(3, 3, 15, 32, printer.alinharADireita(16, ceros + b.getId_boleta()));
        printer.printTextWrap(4, 4, 1, 17, "Fecha:" + mf.convertirFecha(mf.convertirFechaInversa(b.getFecha_cierre())));
        printer.printTextWrap(4, 4, 18, 32, printer.alinharADireita(14, " Hora:" + b.getHora_cierre().substring(0, 5)));
        printer.printTextWrap(5, 5, 1, 24, "Cli: " + c.getNombre_cliente() + " " + c.getApellido_cliente());
        printer.printTextWrap(5, 5, 25, 32, printer.alinharADireita(7, "Mesa:" + b.getMesa()));
        printer.printTextWrap(6, 6, 1, 32, "Vendedor: " + b.getVendedor().getNombre_usuario() + " " + b.getVendedor().getApellido_usuario());
        printer.printTextWrap(7, 7, 1, 32, "===============================");
        printer.printTextWrap(8, 8, 1, 32, "Q. Desc        P.Unit   P.Total");
        printer.printTextWrap(9, 9, 1, 32, "-------------------------------");

        int fila = 10;

        for (int i = 0; i < aDB.size(); i++, fila++) {
            String art;
            art = ma.traerArticuloPorCodigo(aDB.get(i).getCodigo()).getDesc_articulo().replace(" ", "");

            float total = aDB.get(i).getPrecio() * aDB.get(i).getCantidad();

            printer.printTextWrap(fila, fila, 1, 3, String.valueOf(aDB.get(i).getCantidad()));
            //printer.printTextWrap(fila, fila, 6, 32, art + esp1 + "$" + aDB.get(i).getPrecio() + esp2 + "$" + total);
            printer.printTextWrap(fila, fila, 4, 14, art);
            printer.printTextWrap(fila, fila, 16, 23, "$" + printer.alinharADireita(6, String.valueOf(aDB.get(i).getPrecio())));
            printer.printTextWrap(fila, fila, 25, 32, "$" + printer.alinharADireita(6, String.valueOf(total)));

        }

        printer.printTextWrap(fila, fila, 1, 32, "-------------------------------");
        printer.printTextWrap(fila + 1, fila + 1, 1, 15, "TOTAL A PAGAR:");
        printer.printTextWrap(fila + 1, fila + 1, 25, 32, "$" + printer.alinharADireita(6, String.valueOf(b.getTotal())));
        printer.printTextWrap(fila + 3, fila + 3, 1, 32, "Boleta sin valor fiscal");
        printer.printTextWrap(fila + 4, fila + 4, 1, 32, "Solo uso interno");

        printer.toFile(
                "impresion.txt");

        FileInputStream inputStream = null;

        try {
            inputStream = new FileInputStream("impresion.txt");
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        if (inputStream
                == null) {
            return;
        }

        DocFlavor docFormat = DocFlavor.INPUT_STREAM.AUTOSENSE;
        Doc document = new SimpleDoc(inputStream, docFormat, null);

        PrintRequestAttributeSet attributeSet = new HashPrintRequestAttributeSet();

        PrintService defaultPrintService = PrintServiceLookup.lookupDefaultPrintService();

        if (defaultPrintService
                != null) {
            DocPrintJob printJob = defaultPrintService.createPrintJob();
            try {
                printJob.print(document, attributeSet);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            System.err.println("No existen impresoras instaladas");
        }
        //inputStream.close();
    }

    public void imprimirFacturaVtaRapida(Boleta b, Cliente c, ArrayList<Detalle_boleta> aDB) {

        Calendar calendario = new GregorianCalendar();
        int horas, minutos, segundos;

        horas = calendario.get(Calendar.HOUR_OF_DAY);
        minutos = calendario.get(Calendar.MINUTE);
        segundos = calendario.get(Calendar.SECOND);

        PrinterMatrix printer = new PrinterMatrix();

        Extenso e = new Extenso();

        String ceros = "";

        if (b.getId_boleta() < 10) {
            ceros = "00000000000000";
        } else if (b.getId_boleta() < 100) {
            ceros = "0000000000000";
        } else if (b.getId_boleta() < 1000) {
            ceros = "000000000000";
        } else if (b.getId_boleta() < 10000) {
            ceros = "00000000000";
        }

        e.setNumber(70.85);

        //Definir el tamanho del papel para la impresion  aca 25 lineas y 80 columnas
        printer.setOutSize(10 + aDB.size(), 58);
        //Imprimir * de la 2da linea a 25 en la columna 1;
        // printer.printCharAtLin(2, 25, 1, "*");
        //Imprimir * 1ra linea de la columa de 1 a 80
        printer.printCharAtCol(1, 1, 32, "=");
        //Imprimir Encabezado nombre del La EMpresa
        printer.printTextWrap(1, 1, 1, 58, "BOSTON BEER & CO.");
        //printer.printTextWrap(linI, linE, colI, colE, null);
        printer.printTextWrap(2, 2, 1, 58, "Num. Boleta: " + ceros + b.getId_boleta());
        //printer.printTextWrap(2, 2, 33, 80, "Fecha de Emision: " + mf.convertirFecha(b.getFecha_boleta()));
        //printer.printTextWrap(2, 2, 33, 80, "Fecha de Emision: " + mf.convertirFecha(b.getFecha_boleta()) + "    Hora: "+ horas+":"+minutos+":"+segundos);
        printer.printTextWrap(2, 2, 33, 80, "Fecha: " + mf.convertirFecha(b.getFecha_cierre()) + " Hora: " + b.getHora_cierre().substring(0, 5));
        printer.printTextWrap(3, 3, 1, 58, "Cliente: " + c.getNombre_cliente() + " " + c.getApellido_cliente());
        printer.printTextWrap(3, 3, 33, 80, "Vendedor: " + b.getVendedor().getNombre_usuario() + " " + b.getVendedor().getApellido_usuario());

        printer.printTextWrap(4, 4, 1, 58, "===============================");
        printer.printTextWrap(4, 4, 33, 80, "Desc.   Cant.  P.Unit   P.Total");
        printer.printTextWrap(5, 5, 1, 58, "-------------------------------");

        int fila = 5;

        for (int i = 0; i < aDB.size(); i++) {
            String art;
            if (ma.traerArticuloPorCodigo(aDB.get(i).getCodigo()).getDesc_articulo().length() > 10) {
                art = ma.traerArticuloPorCodigo(aDB.get(i).getCodigo()).getDesc_articulo().substring(0, 10);
            } else {
                art = ma.traerArticuloPorCodigo(aDB.get(i).getCodigo()).getDesc_articulo();
            }
            String esp1 = "";
            String esp2 = "";
            float total = aDB.get(i).getPrecio() * aDB.get(i).getCantidad();

            if (aDB.get(i).getCantidad() < 10) {
                if (aDB.get(i).getPrecio() < 100) {
                    for (int j = 0; j < 4; j++) {
                        esp1 = esp1 + " ";
                    }
                } else {
                    for (int j = 0; j < 3; j++) {
                        esp1 = esp1 + " ";
                    }
                }

            } else if (aDB.get(i).getPrecio() < 100) {
                for (int j = 0; j < 3; j++) {
                    esp1 = esp1 + " ";
                }
            } else {

                for (int j = 0; j < 2; j++) {
                    esp1 = esp1 + " ";
                }
            }

            if (total < 1000) {
                if (aDB.get(i).getCantidad() < 10) {
                    if (aDB.get(i).getPrecio() < 100) {
                        for (int j = 0; j < 4; j++) {
                            esp2 = esp2 + " ";
                        }
                    } else {
                        for (int j = 0; j < 4; j++) {
                            esp2 = esp2 + " ";
                        }
                    }

                } else if (aDB.get(i).getPrecio() < 100) {
                    for (int j = 0; j < 4; j++) {
                        esp2 = esp2 + " ";
                    }
                } else {
                    for (int j = 0; j < 2; j++) {
                        esp2 = esp2 + " ";
                    }
                }

            } else {

                if (aDB.get(i).getCantidad() < 10) {
                    for (int j = 0; j < 3; j++) {
                        esp2 = esp2 + " ";
                    }
                } else if (aDB.get(i).getPrecio() < 100) {
                    for (int j = 0; j < 5; j++) {
                        esp2 = esp2 + " ";
                        System.out.println("");
                    }
                } else {
                    for (int j = 0; j < 3; j++) {
                        esp2 = esp2 + " ";
                    }
                }
            }

            printer.printTextWrap(fila, fila, 33, 150, art);
            printer.printTextWrap(fila, fila, 44, 150, aDB.get(i).getCantidad() + esp1 + "$" + aDB.get(i).getPrecio() + esp2 + "$" + total);
            fila++;
        }

        String esp = "";

        if (b.getTotal()
                < 1000) {
            for (int i = 0; i < 11; i++) {
                esp = esp + " ";
            }
        } else {
            for (int i = 0; i < 10; i++) {
                esp = esp + " ";
            }
        }

        printer.printTextWrap(fila, fila,
                1, 32, "-------------------------------");
        printer.printTextWrap(fila, fila,
                33, 80, "TOTAL A PAGAR:" + esp + "$" + b.getTotal());
        printer.printTextWrap(fila
                + 2, fila + 2, 1, 32, "Boleta sin valor fiscal");
        printer.printTextWrap(fila
                + 2, fila + 2, 33, 80, "Solo uso interno");

        printer.toFile(
                "impresion.txt");

        FileInputStream inputStream = null;

        try {
            inputStream = new FileInputStream("impresion.txt");
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        if (inputStream
                == null) {
            return;
        }

        DocFlavor docFormat = DocFlavor.INPUT_STREAM.AUTOSENSE;
        Doc document = new SimpleDoc(inputStream, docFormat, null);

        PrintRequestAttributeSet attributeSet = new HashPrintRequestAttributeSet();

        PrintService defaultPrintService = PrintServiceLookup.lookupDefaultPrintService();

        if (defaultPrintService
                != null) {
            DocPrintJob printJob = defaultPrintService.createPrintJob();
            try {
                printJob.print(document, attributeSet);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            System.err.println("No existen impresoras instaladas");
        }

        //inputStream.close();
    }

    public void imprimirCierreCaja2(Usuario u, ArrayList<Object[]> ventas, ArrayList<Object[]> comidas, ArrayList<Object[]> bebidas, ArrayList<Object[]> promociones, ArrayList<Object[]> descuentos, ArrayList<Object[]> costos, ArrayList<Object[]> facturacion, Cierre_Caja cj) {
        PrinterMatrix printer = new PrinterMatrix();

        Extenso e = new Extenso();

        e.setNumber(70.85);

        //Definir el tamanho del papel para la impresion  aca 25 lineas y 80 columnas
        printer.setOutSize(ventas.size() + comidas.size() + bebidas.size() + promociones.size() + descuentos.size() + costos.size() + facturacion.size() + 14, 58);
        //Imprimir * de la 2da linea a 25 en la columna 1;
        // printer.printCharAtLin(2, 25, 1, "*");
        //Imprimir * 1ra linea de la columa de 1 a 80
        printer.printCharAtCol(1, 1, 32, "=");
        //Imprimir Encabezado nombre del La EMpresa
        printer.printTextWrap(1, 1, 1, 58, "BOSTON BEER & CO.");
        //printer.printTextWrap(linI, linE, colI, colE, null);
        printer.printTextWrap(2, 2, 1, 32, "CIERRE DE CAJA N° " + mfac.obtenerID_cierreCaja());
        printer.printTextWrap(2, 2, 33, 80, "-------------------------------");
        //printer.printTextWrap(3, 3, 1, 32, "Fecha de Cierre: " + mf.convertirFecha(mf.obtenerFechaActual()));
        printer.printTextWrap(3, 3, 1, 32, "Fecha: " + cj.getFecha_cierre() + " Hora: " + cj.getHora_cierre());
        printer.printTextWrap(3, 3, 33, 80, "Responsable: " + u.getNombre_usuario() + " " + u.getApellido_usuario());

        float totalVtas = 0;
        float totalCostos = 0;

        for (int i = 0; i < facturacion.size(); i++) {
            totalVtas = totalVtas + Float.parseFloat(facturacion.get(i)[1].toString());
            totalCostos = totalCostos + Float.parseFloat(facturacion.get(i)[2].toString());
        }

        float ganancias = totalVtas - totalCostos;

        String v = String.valueOf(totalVtas);

        if (totalVtas == 0) {
            v = "                     $" + v;
        } else if (totalVtas < 100) {
            v = "                    $" + v;
        } else if (totalVtas < 1000) {
            v = "                   $" + v;
        } else if (totalVtas < 10000) {
            v = "                  $" + v;
        } else {
            v = "                 $" + v;
        }

        printer.printTextWrap(5, 5, 1, 32, "VENTAS" + v);
        printer.printTextWrap(5, 5, 33, 80, "===============================");

        int fila = 6;

        for (int i = 0; i < ventas.size(); i++) {
            String ven = ventas.get(i)[0].toString();
            String espa2;

            if (ven.length() < 14) {
                int dif = 14 - ven.length();
                String esp = "";
                for (int k = 0; k < dif; k++) {
                    esp = esp + " ";
                }
                ven = ven + esp;
            } else {
                ven = ven.substring(0, 14);
            }

            if (!ventas.get(i)[0].toString().equals("Descuento")) {
                if (Float.parseFloat(ventas.get(i)[1].toString()) < 100) {
                    espa2 = "            ";
                } else if (Float.parseFloat(ventas.get(i)[1].toString()) < 1000) {
                    espa2 = "           ";
                } else if (Float.parseFloat(ventas.get(i)[1].toString()) < 10000) {
                    espa2 = "          ";
                } else {
                    espa2 = "         ";
                }
            } else {
                if (Float.parseFloat(ventas.get(i)[1].toString()) < -1000) {
                    espa2 = "         ";
                } else if (Float.parseFloat(ventas.get(i)[1].toString()) < -100) {
                    espa2 = "           ";
                } else if (Float.parseFloat(ventas.get(i)[1].toString()) < -10) {
                    espa2 = "            ";
                } else {
                    espa2 = "             ";
                }
            }

            //printer.printTextWrap(fila, fila, 1, 32, ventas.get(i)[0].toString() + "                 $" + ventas.get(i)[1]);
            printer.printTextWrap(fila, fila, 1, 32, ven + espa2 + "$" + ventas.get(i)[1]);
            printer.printTextWrap(fila, fila, 33, 80, "-------------------------------");
            fila++;

            if (ventas.get(i)[0].toString().equals("Comida")) {

                for (int j = 0; j < comidas.size(); j++) {
                    String art = comidas.get(j)[0].toString();
                    String esp2;

                    if (art.length() < 12) {
                        int dif = 12 - art.length();
                        String esp = "";
                        for (int k = 0; k < dif; k++) {
                            esp = esp + " ";
                        }
                        art = art + esp;
                    } else {
                        art = art.substring(0, 12);
                    }

                    if (Float.parseFloat(comidas.get(j)[1].toString()) < 100) {
                        esp2 = "             ";
                    } else if (Float.parseFloat(comidas.get(j)[1].toString()) < 1000) {
                        esp2 = "            ";
                    } else if (Float.parseFloat(comidas.get(j)[1].toString()) < 10000) {
                        esp2 = "           ";
                    } else {
                        esp2 = "          ";
                    }

                    printer.printTextWrap(fila, fila, 1, 64, " " + art + esp2 + "$" + comidas.get(j)[1]);
                    //printer.printTextWrap(fila, fila, 33, 64, "  Pizzas               $1500.0");
                    fila++;
                }
            } else if (ventas.get(i)[0].toString().equals("Bebida")) {
                for (int j = 0; j < bebidas.size(); j++) {

                    String art = bebidas.get(j)[0].toString();
                    String esp2;

                    if (art.length() < 12) {
                        int dif = 12 - art.length();
                        String esp = "";
                        for (int k = 0; k < dif; k++) {
                            esp = esp + " ";
                        }
                        art = art + esp;
                    } else {
                        art = art.substring(0, 12);
                    }

                    if (Float.parseFloat(bebidas.get(j)[1].toString()) < 100) {
                        esp2 = "             ";
                    } else if (Float.parseFloat(bebidas.get(j)[1].toString()) < 1000) {
                        esp2 = "            ";
                    } else if (Float.parseFloat(bebidas.get(j)[1].toString()) < 10000) {
                        esp2 = "           ";
                    } else {
                        esp2 = "          ";
                    }

                    printer.printTextWrap(fila, fila, 1, 64, " " + art + esp2 + "$" + bebidas.get(j)[1]);
                    //printer.printTextWrap(fila, fila, 33, 64, "  Pizzas               $1500.0");
                    fila++;
                }
            } else if (ventas.get(i)[0].toString().equals("Promocion")) {
                for (int j = 0; j < promociones.size(); j++) {

                    String art = promociones.get(j)[0].toString();
                    String esp2;

                    if (art.length() < 12) {
                        int dif = 12 - art.length();
                        String esp = "";
                        for (int k = 0; k < dif; k++) {
                            esp = esp + " ";
                        }
                        art = art + esp;
                    } else {
                        art = art.substring(0, 12);
                    }

                    if (Float.parseFloat(promociones.get(j)[1].toString()) < 100) {
                        esp2 = "             ";
                    } else if (Float.parseFloat(promociones.get(j)[1].toString()) < 1000) {
                        esp2 = "            ";
                    } else if (Float.parseFloat(promociones.get(j)[1].toString()) < 10000) {
                        esp2 = "           ";
                    } else {
                        esp2 = "          ";
                    }

                    printer.printTextWrap(fila, fila, 1, 64, " " + art + esp2 + "$" + promociones.get(j)[1]);
                    //printer.printTextWrap(fila, fila, 33, 64, "  Pizzas               $1500.0");
                    fila++;
                }
            } else {
                for (int j = 0; j < descuentos.size(); j++) {

                    String art = descuentos.get(j)[0].toString();
                    String esp2;

                    if (art.length() < 12) {
                        int dif = 12 - art.length();
                        String esp = "";
                        for (int k = 0; k < dif; k++) {
                            esp = esp + " ";
                        }
                        art = art + esp;
                    } else {
                        art = art.substring(0, 12);
                    }

                    if (Float.parseFloat(descuentos.get(j)[1].toString()) < -1000) {
                        esp2 = "          ";
                    } else if (Float.parseFloat(descuentos.get(j)[1].toString()) < -100) {
                        esp2 = "            ";
                    } else if (Float.parseFloat(descuentos.get(j)[1].toString()) < -10) {
                        esp2 = "             ";
                    } else {
                        esp2 = "              ";
                    }

                    printer.printTextWrap(fila, fila, 1, 64, " " + art + esp2 + "$" + descuentos.get(j)[1]);
                    //printer.printTextWrap(fila, fila, 33, 64, "  Pizzas               $1500.0");
                    fila++;
                }
            }

        }

        fila++;

        String c = String.valueOf(totalCostos);

        if (totalCostos == 0) {
            c = "                     $" + c;
        } else if (totalCostos < 100) {
            c = "                    $" + c;
        } else if (totalCostos < 1000) {
            c = "                   $" + c;
        } else if (totalCostos < 10000) {
            c = "                  $" + c;
        } else {
            c = "                 $" + c;
        }

        //printer.printTextWrap(fila, fila, 1, 32, "COSTOS                 $" + totalCostos);
        printer.printTextWrap(fila, fila, 1, 32, "COSTOS" + c);
        printer.printTextWrap(fila, fila, 33, 80, "===============================");
        fila++;

        for (int i = 0; i < costos.size(); i++) {
            String cos = costos.get(i)[0].toString();
            String esp2;

            if (cos.length() < 12) {
                int dif = 12 - cos.length();
                String esp = "";
                for (int k = 0; k < dif; k++) {
                    esp = esp + " ";
                }
                cos = cos + esp;
            } else {
                cos = cos.substring(0, 12);
            }

            if (Float.parseFloat(costos.get(i)[1].toString()) < 100) {
                esp2 = "            ";
            } else if (Float.parseFloat(costos.get(i)[1].toString()) < 1000) {
                esp2 = "           ";
            } else if (Float.parseFloat(costos.get(i)[1].toString()) < 10000) {
                esp2 = "          ";
            } else {
                esp2 = "         ";
            }

            printer.printTextWrap(fila, fila, 1, 64, "  " + cos + esp2 + "$" + costos.get(i)[1]);
            fila++;

        }

        fila++;

        printer.printTextWrap(fila, fila, 1, 32, "FACTURACION");
        printer.printTextWrap(fila, fila, 33, 80, "===============================");
        fila++;
        printer.printTextWrap(fila, fila, 1, 32, "TIPO   VENTAS   COSTOS   TOTAL");
        printer.printTextWrap(fila, fila, 33, 80, "-------------------------------");
        fila++;

        for (int i = 0; i < facturacion.size(); i++) {
            printer.printTextWrap(fila, fila, 1, 32, facturacion.get(i)[0].toString().substring(0, 4));
            String venta = facturacion.get(i)[1].toString();
            String costo = facturacion.get(i)[2].toString();
            String total = facturacion.get(i)[3].toString();

            if (Float.parseFloat(venta) == 0) {
                venta = "      $" + venta;
            } else if (Float.parseFloat(venta) < 100) {
                venta = "   $" + venta;
            } else if (Float.parseFloat(venta) < 1000) {
                venta = "  $" + venta;
            } else if (Float.parseFloat(venta) < 10000) {
                venta = " $" + venta;
            } else {
                venta = "$" + venta;
            }

            if (Float.parseFloat(costo) == 0) {
                costo = "|      $" + costo;
            } else if (Float.parseFloat(costo) < 100) {
                costo = "|   $" + costo;
            } else if (Float.parseFloat(costo) < 1000) {
                costo = "|  $" + costo;
            } else if (Float.parseFloat(costo) < 10000) {
                costo = "| $" + costo;
            } else {
                costo = "|$" + costo;
            }

            if (Float.parseFloat(total) == 0) {
                total = "|      $" + total;
            } else if (Float.parseFloat(total) < 100) {
                total = "|   $" + total;
            } else if (Float.parseFloat(total) < 1000) {
                total = "|  $" + total;
            } else if (Float.parseFloat(total) < 10000) {
                total = "| $" + total;
            } else {
                total = "|$" + total;
            }

            //printer.printTextWrap(fila, fila, 38, 80,"$"+facturacion.get(i)[1].toString() + "|$" + facturacion.get(i)[2].toString() + "|$" + facturacion.get(i)[3].toString());
            printer.printTextWrap(fila, fila, 36, 80, venta + costo + total);
            fila++;
        }

        //fila++;
        String g = String.valueOf(ganancias);

        if (ganancias < -10000) {
            g = "        $" + g;
        } else if (ganancias < -1000) {
            g = "         $" + g;
        } else if (ganancias < -100) {
            g = "          $" + g;
        } else if (ganancias < -10) {
            g = "           $" + g;
        } else if (ganancias == 0) {
            g = "             $" + g;
        } else if (ganancias < 100) {
            g = "            $" + g;
        } else if (ganancias < 1000) {
            g = "           $" + g;
        } else if (ganancias < 10000) {
            g = "          $" + g;
        } else {
            g = "         $" + g;
        }

        printer.printTextWrap(fila, fila, 1, 32, "===============================");
        //printer.printTextWrap(fila, fila, 33, 80, "GANANCIA NETA:         $" + ganancias);
        printer.printTextWrap(fila, fila, 33, 80, "GANANCIA NETA:" + g);
        printer.printTextWrap(fila + 1, fila + 1, 1, 32, "===============================");

        printer.toFile(
                "impresionCierreCaja.txt");

        FileInputStream inputStream = null;

        try {
            inputStream = new FileInputStream("impresionCierreCaja.txt");
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        if (inputStream
                == null) {
            return;
        }

        DocFlavor docFormat = DocFlavor.INPUT_STREAM.AUTOSENSE;
        Doc document = new SimpleDoc(inputStream, docFormat, null);

        PrintRequestAttributeSet attributeSet = new HashPrintRequestAttributeSet();

        PrintService defaultPrintService = PrintServiceLookup.lookupDefaultPrintService();

        if (defaultPrintService
                != null) {
            DocPrintJob printJob = defaultPrintService.createPrintJob();
            try {
                printJob.print(document, attributeSet);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            System.err.println("No existen impresoras instaladas");
        }
    }

    public void imprimirCierreCaja(Usuario u, ArrayList<Object[]> ventas, ArrayList<Object[]> comidas, ArrayList<Object[]> bebidas, ArrayList<Object[]> promociones, ArrayList<Object[]> descuentos, ArrayList<Object[]> costos, ArrayList<Object[]> facturacion, Cierre_Caja cj, ArrayList<Object[]> catVentas) {
        PrinterMatrix printer = new PrinterMatrix();

        Extenso e = new Extenso();

        e.setNumber(70.85);

        //Definir el tamanho del papel para la impresion  aca 25 lineas y 80 columnas
        printer.setOutSize(ventas.size() + comidas.size() + bebidas.size() + promociones.size() + descuentos.size() + costos.size() + facturacion.size() + catVentas.size() + 33, 32);
        printer.printTextWrap(1, 1, 1, 32, "===============================");
        //Imprimir Encabezado nombre del La EMpresa
        printer.printTextWrap(2, 2, 1, 32, "BOSTON BEER & CO.");
        printer.printTextWrap(3, 3, 1, 32, "CIERRE DE CAJA Nro " + mfac.obtenerID_cierreCaja());
        printer.printTextWrap(4, 4, 1, 32, "Fecha: " + mf.convertirFecha(cj.getFecha_cierre()) + " Hora: " + cj.getHora_cierre().substring(0, 5));
        printer.printTextWrap(5, 5, 1, 32, "Responsable: " + u.getNombre_usuario() + " " + u.getApellido_usuario());

        float totalVtas = 0;
        float totalCostos = 0;

        for (int i = 0; i < facturacion.size(); i++) {
            totalVtas = totalVtas + Float.parseFloat(facturacion.get(i)[1].toString());
            totalCostos = totalCostos + Float.parseFloat(facturacion.get(i)[2].toString());
        }

        float ganancias = totalVtas - totalCostos;

        String v = String.valueOf(totalVtas);

        printer.printTextWrap(7, 7, 1, 22, "VENTAS");
        printer.printTextWrap(7, 7, 23, 32, "$" + printer.alinharADireita(8, v));
        //printer.printTextWrap(9, 9, 1, 32, "===============================");

        int fila = 8;

        for (int i = 0; i < ventas.size(); i++) {
            String ven = ventas.get(i)[0].toString();

            if (i == 0) {
                printer.printTextWrap(fila, fila, 1, 32, "===============================");
                for (int k = 0; k < catVentas.size(); k++, fila++){
                    String art = catVentas.get(k)[0].toString();
                    System.out.println(art + " $"+catVentas.get(k)[1]);
                    printer.printTextWrap(fila+1, fila+1, 1, 22, art);
                    printer.printTextWrap(fila+1, fila+1, 23, 32, "$" + printer.alinharADireita(8, catVentas.get(k)[1].toString()));
                }
                fila++;
                printer.printTextWrap(fila, fila, 1, 32, "===============================");
                fila++;
            } else {
                //fila++;
                printer.printTextWrap(fila, fila, 1, 32, "-------------------------------");
                //fila=fila+3;
            }
            //fila++;
            printer.printTextWrap(fila + 1, fila + 1, 1, 22, ven);
            //printer.printTextWrap(fila, fila, 1, 32, "-------------------------------");
            if (Float.parseFloat(ventas.get(i)[1].toString()) > 0) {
                printer.printTextWrap(fila + 1, fila + 1, 23, 32, "$" + printer.alinharADireita(8, ventas.get(i)[1].toString()));
            } else {
                float descPos = Float.parseFloat(ventas.get(i)[1].toString()) * (-1);
                String descPosS = String.valueOf(descPos);
                printer.printTextWrap(fila + 1, fila + 1, 22, 32, "-$" + printer.alinharADireita(8, descPosS));
            }
            //fila++;
            printer.printTextWrap(fila + 2, fila + 2, 1, 32, "-------------------------------");
            fila = fila + 3;

            if (ventas.get(i)[0].toString().equals("Comida")) {

                for (int j = 0; j < comidas.size(); j++) {
                    String art = comidas.get(j)[0].toString();

                    printer.printTextWrap(fila, fila, 1, 22, art);
                    printer.printTextWrap(fila, fila, 23, 32, "$" + printer.alinharADireita(8, comidas.get(j)[1].toString()));
                    //printer.printTextWrap(fila, fila, 33, 64, "  Pizzas               $1500.0");
                    fila++;
                }
                //printer.printTextWrap(fila, fila, 1, 32, "-------------------------------");
                //fila++;
            } else if (ventas.get(i)[0].toString().equals("Bebida")) {
                for (int j = 0; j < bebidas.size(); j++) {

                    String art = bebidas.get(j)[0].toString();

                    printer.printTextWrap(fila, fila, 1, 22, art);
                    printer.printTextWrap(fila, fila, 23, 32, "$" + printer.alinharADireita(8, bebidas.get(j)[1].toString()));
                    //printer.printTextWrap(fila, fila, 33, 64, "  Pizzas               $1500.0");
                    fila++;
                }
                //printer.printTextWrap(fila, fila, 1, 32, "-------------------------------");
                //fila++;
            } else if (ventas.get(i)[0].toString().equals("Promocion")) {
                for (int j = 0; j < promociones.size(); j++) {

                    String art = promociones.get(j)[0].toString();

                    printer.printTextWrap(fila, fila, 1, 22, art);
                    printer.printTextWrap(fila, fila, 23, 32, "$" + printer.alinharADireita(8, promociones.get(j)[1].toString()));

                    fila++;
                }
                //printer.printTextWrap(fila, fila, 1, 32, "-------------------------------");
                //fila++;
            } else {
                for (int j = 0; j < descuentos.size(); j++) {

                    String art = descuentos.get(j)[0].toString();

                    //printer.printTextWrap(fila, fila, 1, 64, " " + art + esp2 + "$" + descuentos.get(j)[1]);
                    printer.printTextWrap(fila, fila, 1, 22, art);
                    float descPos = Float.parseFloat(descuentos.get(j)[1].toString()) * (-1);
                    String descPosS = String.valueOf(descPos);
                    printer.printTextWrap(fila, fila, 22, 32, "-$" + printer.alinharADireita(8, descPosS));
                    //printer.printTextWrap(fila, fila, 33, 64, "  Pizzas               $1500.0");
                    fila++;
                }
                //printer.printTextWrap(fila, fila, 1, 32, "-------------------------------");
                //fila++;
            }

        }

        fila++;
        String c = String.valueOf(totalCostos);

        //printer.printTextWrap(fila, fila, 1, 32, "COSTOS                 $" + totalCostos);
        printer.printTextWrap(fila, fila, 1, 21, "COSTOS");
        //printer.printTextWrap(fila, fila, 22, 32, printer.alinharADireita(8, "$" + c));
        printer.printTextWrap(fila, fila, 22, 32, "-$" + printer.alinharADireita(8, c));
        //printer.printTextWrap(fila, fila, 1, 32, "COSTOS" + c);
        printer.printTextWrap(fila + 1, fila + 1, 1, 32, "===============================");
        fila = fila + 2;

        for (int i = 0; i < costos.size(); i++) {
            String cos = costos.get(i)[0].toString();

            printer.printTextWrap(fila, fila, 1, 22, cos);
            //float cosPos = Float.parseFloat(costos.get(i)[1].toString()) * (-1);
            //String cosPosS = String.valueOf(cosPos);
            printer.printTextWrap(fila, fila, 22, 32, "-$" + printer.alinharADireita(8, costos.get(i)[1].toString()));
            //printer.printTextWrap(fila, fila, 23, 32, "$" + printer.alinharADireita(8, costos.get(i)[1].toString()));
            //printer.printTextWrap(fila, fila, 1, 64, "  " + cos + esp2 + "$" + costos.get(i)[1]);
            fila++;

        }

        fila++;

        printer.printTextWrap(fila, fila, 1, 32, "FACTURACION DETALLADA");
        printer.printTextWrap(fila + 1, fila + 1, 1, 32, "===============================");
        fila = fila + 2;

        printer.printTextWrap(fila, fila, 1, 32, "VENTAS     |  COSTOS  |   TOTAL ");
        printer.printTextWrap(fila + 1, fila + 1, 1, 32, "-------------------------------");

        fila = fila + 2;

        for (int i = 0; i < facturacion.size(); i++) {
            printer.printTextWrap(fila, fila, 1, 32, facturacion.get(i)[0].toString());
            String venta = facturacion.get(i)[1].toString();
            String costo = facturacion.get(i)[2].toString();
            String total = facturacion.get(i)[3].toString();

            printer.printTextWrap(fila + 1, fila + 1, 1, 10, "$" + printer.alinharADireita(8, venta));
            printer.printTextWrap(fila + 1, fila + 1, 12, 21, "$" + printer.alinharADireita(8, costo));
            printer.printTextWrap(fila + 1, fila + 1, 23, 32, "$" + printer.alinharADireita(8, total));
            fila = fila + 2;
        }

        String g = String.valueOf(ganancias);

        printer.printTextWrap(fila, fila, 1, 32, "===============================");
        printer.printTextWrap(fila + 1, fila + 1, 1, 20, "GANANCIA NETA:");
        printer.printTextWrap(fila + 1, fila + 1, 23, 32, "$" + printer.alinharADireita(8, g));
        printer.printTextWrap(fila + 2, fila + 2, 1, 32, "===============================");

        printer.toFile(
                "impresionCierreCaja.txt");

        FileInputStream inputStream = null;

        try {
            inputStream = new FileInputStream("impresionCierreCaja.txt");
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        if (inputStream
                == null) {
            return;
        }

        
        
        DocFlavor docFormat = DocFlavor.INPUT_STREAM.AUTOSENSE;
        Doc document = new SimpleDoc(inputStream, docFormat, null);

        PrintRequestAttributeSet attributeSet = new HashPrintRequestAttributeSet();

        PrintService defaultPrintService = PrintServiceLookup.lookupDefaultPrintService();

        if (defaultPrintService
                != null) {
            DocPrintJob printJob = defaultPrintService.createPrintJob();
            try {
                printJob.print(document, attributeSet);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            System.err.println("No existen impresoras instaladas");
        }
    }

    public void imprimirDelivery(Boleta b, Cliente c, ArrayList<Detalle_boleta> aDB, int tipo) {

        Calendar calendario = new GregorianCalendar();
        int horas, minutos, segundos;

        horas = calendario.get(Calendar.HOUR_OF_DAY);
        minutos = calendario.get(Calendar.MINUTE);
        segundos = calendario.get(Calendar.SECOND);

        PrinterMatrix printer = new PrinterMatrix();

        Extenso e = new Extenso();

        String ceros = "";

        if (b.getId_boleta() < 10) {
            ceros = "00000000000000000";
        } else if (b.getId_boleta() < 100) {
            ceros = "0000000000000000";
        } else if (b.getId_boleta() < 1000) {
            ceros = "000000000000000";
        } else if (b.getId_boleta() < 10000) {
            ceros = "0000000000000";
        }else{
            ceros = "000000000000";
        }

        e.setNumber(70.85);

        //Definir el tamanho del papel para la impresion  aca 25 lineas y 80 columnas
        printer.setOutSize(18 + aDB.size(), 32);
        //Imprimir * de la 2da linea a 25 en la columna 1;
        // printer.printCharAtLin(2, 25, 1, "*");
        //Imprimir * 1ra linea de la columa de 1 a 80
        //printer.printCharAtCol(1, 1, 32, "=");
        printer.printTextWrap(1, 1, 1, 32, "------- COMANDA DELIVERY -----");
        //Imprimir Encabezado nombre del La EMpresa
        printer.printTextWrap(2, 2, 1, 32, "BOSTON BEER & CO.");
        printer.printTextWrap(3, 3, 1, 32, "Num. Boleta: " + ceros + b.getId_boleta());
        if (tipo == 1) {
            printer.printTextWrap(4, 4, 1, 32, "Fecha: " + mf.convertirFecha(b.getFecha_cierre()) + " Hora: " + b.getHora_apertura().substring(0, 5));
        } else {
            printer.printTextWrap(4, 4, 1, 32, "Fecha: " + mf.convertirFecha(mf.convertirFechaInversa(b.getFecha_cierre())) + " Hora: " + b.getHora_apertura().substring(0, 5));
        }
        printer.printTextWrap(5, 5, 1, 32, "Delivery: " + b.getVendedor().getNombre_usuario() + " " + b.getVendedor().getApellido_usuario());
        printer.printTextWrap(6, 6, 1, 32, "Cliente: " + c.getNombre_cliente() + " " + c.getApellido_cliente());
        printer.printTextWrap(7, 7, 1, 32, b.getDireccion_delivery());
        printer.printTextWrap(8, 8, 1, 32, "================================");
        printer.printTextWrap(9, 9, 1, 32, " Q.  Desc.");
        printer.printTextWrap(10, 10, 1, 32, "-------------------------------");
        int fila = 11;

        for (int i = 0; i < aDB.size(); i++, fila++) {
            String art = ma.traerArticuloPorCodigo(aDB.get(i).getCodigo()).getDesc_articulo();
            int cantidad = aDB.get(i).getCantidad();
            printer.printTextWrap(fila, fila, 2, 4, String.valueOf(cantidad));
            printer.printTextWrap(fila, fila, 6, 32, art);
        }

        printer.printTextWrap(fila, fila, 1, 32, "--------------------------------");
        printer.printTextWrap(fila + 1, fila + 1, 1, 10, "TOTAL   : ");
        printer.printTextWrap(fila + 1, fila + 1, 11, 32, printer.alinharADireita(21, "$" + b.getTotal()));
        printer.printTextWrap(fila + 2, fila + 2, 1, 10, "PAGA CON: ");
        printer.printTextWrap(fila + 2, fila + 2, 11, 32, printer.alinharADireita(21, "$" + b.getPago_delivery()));
        printer.printTextWrap(fila + 3, fila + 3, 1, 10, "VUELTO  : ");
        printer.printTextWrap(fila + 3, fila + 3, 11, 32, printer.alinharADireita(21, "$" + (b.getPago_delivery() - b.getTotal())));

        printer.toFile(
                "impresionDelivery.txt");

        FileInputStream inputStream = null;

        try {
            inputStream = new FileInputStream("impresionDelivery.txt");
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        if (inputStream
                == null) {
            return;
        }

        DocFlavor docFormat = DocFlavor.INPUT_STREAM.AUTOSENSE;
        Doc document = new SimpleDoc(inputStream, docFormat, null);

        PrintRequestAttributeSet attributeSet = new HashPrintRequestAttributeSet();

        PrintService defaultPrintService = PrintServiceLookup.lookupDefaultPrintService();

        if (defaultPrintService
                != null) {
            DocPrintJob printJob = defaultPrintService.createPrintJob();
            try {
                printJob.print(document, attributeSet);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            System.err.println("No existen impresoras instaladas");
        }
        //inputStream.close();
    }

    public void imprimirTicketDelivery(Boleta b, Cliente c, ArrayList<Detalle_boleta> aDB, int tipo) {

        Calendar calendario = new GregorianCalendar();
        int horas, minutos, segundos;

        horas = calendario.get(Calendar.HOUR_OF_DAY);
        minutos = calendario.get(Calendar.MINUTE);
        segundos = calendario.get(Calendar.SECOND);

        PrinterMatrix printer = new PrinterMatrix();

        Extenso e = new Extenso();

        String ceros = "";

        if (b.getId_boleta() < 10) {
            ceros = "00000000000000000";
        } else if (b.getId_boleta() < 100) {
            ceros = "0000000000000000";
        } else if (b.getId_boleta() < 1000) {
            ceros = "000000000000000";
        } else if (b.getId_boleta() < 10000) {
            ceros = "0000000000000";
        }else{
            ceros = "000000000000";
        }

        e.setNumber(70.85);

        //Definir el tamanho del papel para la impresion  aca 25 lineas y 80 columnas
        printer.setOutSize(18 + aDB.size(), 32);
        printer.printTextWrap(1, 1, 1, 32, "===============================");
        printer.printTextWrap(2, 2, 1, 32, "BOSTON BEER & CO. - DELIVERY");
        printer.printTextWrap(3, 3, 1, 32, "Num. Boleta: " + ceros + b.getId_boleta());
        if (tipo == 1) {
            printer.printTextWrap(4, 4, 1, 32, "Fecha: " + mf.convertirFecha(b.getFecha_cierre()) + " Hora: " + b.getHora_apertura().substring(0, 5));
        } else {
            printer.printTextWrap(4, 4, 1, 32, "Fecha: " + mf.convertirFecha(mf.convertirFechaInversa(b.getFecha_cierre())) + "   Hora: " + b.getHora_apertura().substring(0, 5));
        }
        printer.printTextWrap(5, 5, 1, 32, "Cliente: " + c.getNombre_cliente() + " " + c.getApellido_cliente());
        printer.printTextWrap(6, 6, 1, 32, "Delivery: " + b.getVendedor().getNombre_usuario() + " " + b.getVendedor().getApellido_usuario());
        printer.printTextWrap(7, 7, 1, 32, "===============================");
        printer.printTextWrap(8, 8, 1, 32, "Q. Desc        P.Unit   P.Total");
        printer.printTextWrap(9, 9, 1, 32, "-------------------------------");

        int fila = 10;
        /*
        for (int i = 0; i < aDB.size(); i++, fila++) {
            String art;
            art = ma.traerArticuloPorCodigo(aDB.get(i).getCodigo()).getDesc_articulo().replace(" ", "");

            float total = aDB.get(i).getPrecio() * aDB.get(i).getCantidad();

            printer.printTextWrap(fila, fila, 1, 3, String.valueOf(aDB.get(i).getCantidad()));
            //printer.printTextWrap(fila, fila, 6, 32, art + esp1 + "$" + aDB.get(i).getPrecio() + esp2 + "$" + total);
            printer.printTextWrap(fila, fila, 4, 14, art);
            printer.printTextWrap(fila, fila, 16, 23, printer.alinharADireita(7, "$" + String.valueOf(aDB.get(i).getPrecio())));
            printer.printTextWrap(fila, fila, 25, 32, printer.alinharADireita(7, "$" + String.valueOf(total)));

        }*/

        for (int i = 0; i < aDB.size(); i++, fila++) {
            String art;
            art = ma.traerArticuloPorCodigo(aDB.get(i).getCodigo()).getDesc_articulo().replace(" ", "");

            float total = aDB.get(i).getPrecio() * aDB.get(i).getCantidad();

            printer.printTextWrap(fila, fila, 1, 3, String.valueOf(aDB.get(i).getCantidad()));
            //printer.printTextWrap(fila, fila, 6, 32, art + esp1 + "$" + aDB.get(i).getPrecio() + esp2 + "$" + total);
            printer.printTextWrap(fila, fila, 4, 14, art);
            printer.printTextWrap(fila, fila, 16, 23, "$" + printer.alinharADireita(6, String.valueOf(aDB.get(i).getPrecio())));
            printer.printTextWrap(fila, fila, 25, 32, "$" + printer.alinharADireita(6, String.valueOf(total)));

        }

        printer.printTextWrap(fila, fila, 1, 32, "-------------------------------");
        printer.printTextWrap(fila + 1, fila + 1, 1, 15, "TOTAL A PAGAR:");
        printer.printTextWrap(fila + 1, fila + 1, 16, 32, printer.alinharADireita(16, "$" + b.getTotal()));
        printer.printTextWrap(fila + 3, fila + 3, 1, 32, "Boleta sin valor fiscal");
        printer.printTextWrap(fila + 4, fila + 4, 1, 32, "Solo uso interno");

        printer.toFile(
                "impresion.txt");

        FileInputStream inputStream = null;

        try {
            inputStream = new FileInputStream("impresion.txt");
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        if (inputStream
                == null) {
            return;
        }

        DocFlavor docFormat = DocFlavor.INPUT_STREAM.AUTOSENSE;
        Doc document = new SimpleDoc(inputStream, docFormat, null);

        PrintRequestAttributeSet attributeSet = new HashPrintRequestAttributeSet();

        PrintService defaultPrintService = PrintServiceLookup.lookupDefaultPrintService();

        if (defaultPrintService
                != null) {
            DocPrintJob printJob = defaultPrintService.createPrintJob();
            try {
                printJob.print(document, attributeSet);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            System.err.println("No existen impresoras instaladas");
        }
    }
}
