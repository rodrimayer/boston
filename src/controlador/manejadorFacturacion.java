/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import boston.conexionDB;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import modelo.Articulo;
import modelo.Boleta;
import modelo.Cierre_Caja;
import modelo.Cliente;
import modelo.Detalle_boleta;
import modelo.Facturacion_boleta;
import modelo.Facturacion_costo;
import modelo.Usuario;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author rmmayer
 */
public class manejadorFacturacion {

    manejadorArticulos ma = new manejadorArticulos();
    manejadorFechas mf = new manejadorFechas();

    public void insertarBoleta(Boleta b, int tipo) {

        conexionDB con = new conexionDB();
        Connection reg = con.conectar();

        System.out.println("id_cliente: " + b.getCliente().getId_cliente() + " fecha_boleta: " + b.getFecha_boleta());

        String sql = "INSERT INTO boleta (id_cliente, fecha_boleta, total, cerrada, id_mozo, mesa, pagada, hora_apertura, hora_cierre, fecha_cierre) VALUES (?,?,?,?,?,?,?,?,?,?)";
        System.out.println(sql);

        try {
            PreparedStatement ps = reg.prepareStatement(sql);
            ps.setInt(1, b.getCliente().getId_cliente());
            ps.setString(2, b.getFecha_boleta());
            ps.setFloat(3, b.getTotal());
            ps.setInt(4, 0);
            ps.setInt(5, b.getVendedor().getId_usuario());
            ps.setInt(6, b.getMesa());
            if (tipo == 1) {
                ps.setInt(7, 1);
            } else {
                ps.setInt(7, 0);
            }
            ps.setString(8, b.getHora_apertura());
            ps.setString(9, b.getHora_cierre());
            ps.setString(10, b.getFecha_cierre());

            ps.executeUpdate();

            reg.close();

        } catch (SQLException e) {
            System.out.println("ERROR EN EL AGREGADO DE HEADER DE BOLETA: " + e);
        }
    }
    
    public void insertarDelivery(Boleta b, int tipo) {

        conexionDB con = new conexionDB();
        Connection reg = con.conectar();

        System.out.println("id_cliente: " + b.getCliente().getId_cliente() + " fecha_boleta: " + b.getFecha_boleta());

        String sql = "INSERT INTO boleta (id_cliente, fecha_boleta, total, cerrada, id_mozo, mesa, pagada, hora_apertura, hora_cierre, fecha_cierre, direccion_delivery, pago_delivery, id_tipoVta) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
        System.out.println(sql);

        try {
            PreparedStatement ps = reg.prepareStatement(sql);
            ps.setInt(1, b.getCliente().getId_cliente());
            ps.setString(2, b.getFecha_boleta());
            ps.setFloat(3, b.getTotal());
            ps.setInt(4, 0);
            ps.setInt(5, b.getVendedor().getId_usuario());
            ps.setInt(6, b.getMesa());
            if (tipo == 1) {
                ps.setInt(7, 1);
            } else {
                ps.setInt(7, 0);
            }
            ps.setString(8, b.getHora_apertura());
            ps.setString(9, b.getHora_cierre());
            ps.setString(10, b.getFecha_cierre());
            ps.setString(11, b.getDireccion_delivery());
            ps.setFloat(12, b.getPago_delivery());
            ps.setInt(13, b.getId_tipoVta());

            ps.executeUpdate();

            reg.close();

        } catch (SQLException e) {
            System.out.println("ERROR EN EL AGREGADO DE HEADER DE BOLETA: " + e);
        }
    }

    public void insertarDetallesBoleta(Detalle_boleta db, DefaultCategoryDataset datosBarriles) {
        Articulo a = new Articulo();
        conexionDB con = new conexionDB();
        Connection reg = con.conectar();

        String sql = "INSERT INTO detalle_mesa (id_boleta, codigo, cantidad, precio) VALUES (?,?,?,?)";
        System.out.println(sql);
        int posicion = 0;

        try {
            PreparedStatement ps = reg.prepareStatement(sql);
            ps.setInt(1, db.getId_boleta());
            ps.setString(2, db.getCodigo());
            ps.setInt(3, db.getCantidad());
            ps.setFloat(4, db.getPrecio());

            a = ma.traerArticuloPorCodigo(db.getCodigo());

            ps.executeUpdate();

            reg.close();

        } catch (SQLException e) {
            System.out.println("ERROR EN EL AGREGADO DE DETALLE DE BOLETA: " + e);
        }

    }

    public void actualizarDetallesBoleta(Detalle_boleta db, DefaultCategoryDataset datosBarriles) {
        Articulo a = new Articulo();
        conexionDB con = new conexionDB();
        Connection reg = con.conectar();

        //String sql = "INSERT INTO detalle_mesa (id_boleta, codigo, cantidad, precio) VALUES (?,?,?,?)";
        String sql = "UPDATE Detalle_mesa "
                + "SET cantidad = " + db.getCantidad() + " "
                + "WHERE id_detalleMesa = " + db.getId_detalleboleta();

        System.out.println(sql);
        int posicion = 0;

        Statement st1;
        try {
            st1 = reg.createStatement();
            st1.executeUpdate(sql);

            /*
            if (a.getId_tipoProd2() == 5) {
                posicion = ma.obtenerPosicionBarril(a.getDesc_articulo());
                System.out.println("SE DEBEN DESCONTAR DEL BARRIL DE " + a.getDesc_articulo() + " la cantidad de: " + db.getCantidad() * 0.5 + " del barril que esta en la posicion: " + posicion);
                ma.descontarPintaBarril((float) (db.getCantidad() * 0.5), posicion, a.getDesc_articulo(), datosBarriles);
                //datosBarriles.incrementValue(-(db.getCantidad()*0.5), a.getDesc_articulo(), String.valueOf(posicion));
            }*/
            reg.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    public void actualizarPagoDelivery (int id, float pago){
        conexionDB con = new conexionDB();
        Connection reg = con.conectar();

        //String sql = "INSERT INTO detalle_mesa (id_boleta, codigo, cantidad, precio) VALUES (?,?,?,?)";
        String sql = "UPDATE Boleta "
                + "SET pago_delivery = " + pago + " "
                + "WHERE id_boleta = " + id;

        System.out.println(sql);
        
        Statement st1;
        try {
            st1 = reg.createStatement();
            st1.executeUpdate(sql);

            reg.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void actualizarDelivery (int id, int mozo){
        conexionDB con = new conexionDB();
        Connection reg = con.conectar();

        //String sql = "INSERT INTO detalle_mesa (id_boleta, codigo, cantidad, precio) VALUES (?,?,?,?)";
        String sql = "UPDATE Boleta "
                + "SET id_mozo = " + mozo + " "
                + "WHERE id_boleta = " + id;

        System.out.println(sql);
        
        Statement st1;
        try {
            st1 = reg.createStatement();
            st1.executeUpdate(sql);

            reg.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void actualizarDireccionDelivery (int id_boleta, String nva_dir){
        conexionDB con = new conexionDB();
        Connection reg = con.conectar();

        //String sql = "INSERT INTO detalle_mesa (id_boleta, codigo, cantidad, precio) VALUES (?,?,?,?)";
        String sql = "UPDATE Boleta "
                + "SET direccion_delivery = '" + nva_dir + "' "
                + "WHERE id_boleta = " + id_boleta;

        System.out.println(sql);
        
        Statement st1;
        try {
            st1 = reg.createStatement();
            st1.executeUpdate(sql);

            reg.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setHoraCierreBoleta(String hora, String fecha, int id) {
        conexionDB con = new conexionDB();
        Connection reg = con.conectar();

        String sql = "UPDATE Boleta "
                + "SET hora_cierre = '" + hora + "', fecha_cierre = '"+fecha+"' "
                + "WHERE id_boleta = " + id;

        System.out.println(sql);

        Statement st1;
        try {
            st1 = reg.createStatement();
            st1.executeUpdate(sql);

            reg.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex + ": no se pudo actualizar la hora de cierre");
        }

    }

    public void actualizarTotalBoleta(int id, float total) {
        conexionDB con = new conexionDB();
        Connection reg = con.conectar();

        String sql = "UPDATE Boleta "
                + "SET total = " + total + " "
                + "WHERE id_boleta = " + id;

        System.out.println(sql);

        Statement st1;
        try {
            st1 = reg.createStatement();
            st1.executeUpdate(sql);

            reg.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void insertarFormasDePago(Facturacion_boleta dc) {
        conexionDB con = new conexionDB();
        Connection reg = con.conectar();

        String sql = "INSERT INTO facturacion_boleta (id_boleta, id_condvta, monto) VALUES (?,?,?)";
        System.out.println(sql);

        try {
            PreparedStatement ps = reg.prepareStatement(sql);
            ps.setInt(1, dc.getId_boleta());
            ps.setInt(2, dc.getId_condvta());
            ps.setFloat(3, dc.getTotal());

            ps.executeUpdate();

            reg.close();

        } catch (SQLException e) {
            System.out.println("ERROR EN EL AGREGADO DE DETALLE DE BOLETA: " + e);
        }
    }

    public void insertarFormasDePagoCosto(Facturacion_costo dc) {
        conexionDB con = new conexionDB();
        Connection reg = con.conectar();

        String sql = "INSERT INTO facturacion_costo (id_costo, id_condvta, monto) VALUES (?,?,?)";
        System.out.println(sql);

        try {
            PreparedStatement ps = reg.prepareStatement(sql);
            ps.setInt(1, dc.getId_costo());
            ps.setInt(2, dc.getId_condVta());
            ps.setFloat(3, dc.getMonto());

            ps.executeUpdate();

            reg.close();

        } catch (SQLException e) {
            System.out.println("ERROR EN EL AGREGADO DEL PAGO DEL COSTO: " + e);
        }
    }

    public int obtenerID_boleta() {
        int ultBoleta = 0;

        conexionDB con = new conexionDB();
        Connection reg = con.conectar();

        String sql = "SELECT id_boleta from boleta order by id_boleta desc limit 1";
        System.out.println(sql);

        try {
            Statement st = reg.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                ultBoleta = rs.getInt(1);
                System.out.println("ULTIMA BOLETA: " + ultBoleta);
            }

            reg.close();

        } catch (SQLException e) {
            System.out.println("ERROR AL TRAER ULTIMA BOLETA");
        }

        return ultBoleta;
    }

    public Boleta traerBoletaID(int id) {
        Boleta b = new Boleta();
        conexionDB con = new conexionDB();
        Connection reg = con.conectar();
        manejadorCliente mc = new manejadorCliente();
        manejadorUsuario mu = new manejadorUsuario();
        manejadorFechas mf = new manejadorFechas();

        String sql = "SELECT id_mozo, fecha_boleta, id_cliente, total, mesa, cerrada, hora_apertura, hora_cierre, fecha_cierre, id_mozo, direccion_delivery, pago_delivery, id_tipoVta "
                + "FROM BOLETA "
                + "WHERE id_boleta = " + id;

        System.out.println(sql);

        try {
            Statement st = reg.createStatement();
            ResultSet rs1 = st.executeQuery(sql);

            while (rs1.next()) {
                b.setId_boleta(id);
                b.setFecha_boleta(rs1.getString(2));
                b.setCliente(mc.traerCliente(rs1.getInt(3)));
                b.setTotal(rs1.getFloat(4));
                b.setMesa(rs1.getInt(5));
                b.setCerrada(rs1.getInt(6));
                b.setHora_apertura(rs1.getString(7));
                b.setHora_cierre(rs1.getString(8));
                b.setFecha_cierre(rs1.getString(9));
                b.setVendedor(mu.traerMozoID(rs1.getInt(10)));
                b.setDireccion_delivery(rs1.getString(11));
                b.setPago_delivery(rs1.getFloat(12));
                b.setId_tipoVta(rs1.getInt(13));
            }

            reg.close();

        } catch (SQLException e) {
            System.out.println("ERROR AL TRAER LA BOLETA" + e);
        }

        return b;
    }

    public ArrayList<Detalle_boleta> traerDetalle(int id) {
        ArrayList<Detalle_boleta> array = new ArrayList<>();
        conexionDB con = new conexionDB();
        Connection reg = con.conectar();

        String sql = "SELECT id_detalleMesa, id_boleta, codigo, cantidad, precio "
                + "FROM detalle_mesa "
                + "WHERE id_boleta = " + id;

        System.out.println(sql);

        try {
            Statement st = reg.createStatement();
            ResultSet rs1 = st.executeQuery(sql);

            while (rs1.next()) {
                Detalle_boleta db = new Detalle_boleta();

                db.setId_boleta(rs1.getInt(2));
                db.setId_detalleboleta(rs1.getInt(1));
                db.setCodigo(rs1.getString(3));
                db.setCantidad(rs1.getInt(4));
                db.setPrecio(rs1.getFloat(5));

                array.add(db);

            }

            reg.close();

        } catch (SQLException e) {
            System.out.println("ERROR AL TRAER EL DETALLE DE LA BOLETA " + e);
        }

        return array;
    }
    
    public Detalle_boleta traerDetalleUnico(int id) {
        Detalle_boleta db = new Detalle_boleta();
        conexionDB con = new conexionDB();
        Connection reg = con.conectar();

        String sql = "SELECT id_detalleMesa, id_boleta, codigo, cantidad, precio "
                + "FROM detalle_mesa "
                + "WHERE id_detalleMesa = " + id;

        System.out.println(sql);

        try {
            Statement st = reg.createStatement();
            ResultSet rs1 = st.executeQuery(sql);

            while (rs1.next()) {
                db.setId_boleta(rs1.getInt(2));
                db.setId_detalleboleta(rs1.getInt(1));
                db.setCodigo(rs1.getString(3));
                db.setCantidad(rs1.getInt(4));
                db.setPrecio(rs1.getFloat(5));

            }

            reg.close();

        } catch (SQLException e) {
            System.out.println("ERROR AL TRAER EL DETALLE DE LA BOLETA " + e);
        }

        return db;
    }

    public void actualizarMesa(int id, int mesa) {
        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        String sql = "UPDATE BOLETA "
                + "SET MESA = " + mesa + " "
                + "WHERE id_boleta = " + id;

        System.out.println(sql);

        Statement st1;
        try {
            st1 = con.createStatement();
            st1.executeUpdate(sql);

            con.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public boolean existeMesa(int mesa) {
        boolean b = false;

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        String sql = "SELECT mesa "
                + "from boleta "
                + "WHERE mesa = " + mesa + " "
                + "AND pagada = 0";

        System.out.println(sql);

        try {
            Statement st = con.createStatement();
            ResultSet rs1 = st.executeQuery(sql);

            while (rs1.next()) {
                b = true;
                System.out.println("hay una mesa con ese numero");
            }

            con.close();

        } catch (SQLException e) {
            System.out.println("ERROR AL EJECUTAR LA BUSQUEDA DE MESAS OCUPADAS " + e);
        }

        return b;
    }

    public void pagarBoleta(int id) {
        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        String sql = "UPDATE BOLETA "
                + "SET pagada = 1 "
                + "WHERE id_boleta = " + id;

        System.out.println(sql);

        Statement st1;
        try {
            st1 = con.createStatement();
            st1.executeUpdate(sql);

            con.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<Object[]> traerVentasTipo() {
        ArrayList<Object[]> ventas = new ArrayList<>();

        String sql = "SELECT t1.id_tipoProd1, t1.desc_tipoProd1, SUM( d.cantidad * d.precio ) "
                + "FROM Detalle_mesa AS d "
                + "JOIN articulo AS a ON a.codigo = d.codigo "
                + "JOIN Tipo_Prod AS t1 ON a.id_tipoProd = t1.id_tipoProd1 "
                + "JOIN Boleta AS b ON b.id_boleta = d.id_boleta "
                + "WHERE b.cerrada =0 "
                + "AND b.pagada =1 "
                + "GROUP BY t1.id_tipoProd1, t1.desc_tipoProd1";

        conexionDB c = new conexionDB();
        Connection con = c.conectar();
        
        System.out.println("SQL PARA TRAER VENTAS TIPO: "+sql);

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                Object[] venta = new Object[2];
                venta[0] = rs1.getString(2);
                venta[1] = rs1.getFloat(3);

                ventas.add(venta);

                //modeloTablaFacturacion.addRow(tipo);
            }

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO CIERRE FACTURACION VTAS: " + e);
        }

        for (int i = 0; i < ventas.size(); i++) {
            System.out.println(ventas.get(i)[0] + " " + ventas.get(i)[1]);
        }

        return ventas;
    }
    
    public ArrayList<Object[]> traerVentasCat() {
        ArrayList<Object[]> catVentas = new ArrayList<>();

        String sql = "SELECT t1.id_tipoVta, t1.desc_tipoVta, SUM( d.cantidad * d.precio ) "
                + "FROM Detalle_mesa AS d "
                + "JOIN Boleta AS b "
                + "ON b.id_boleta = d.id_boleta "
                + "JOIN Tipo_vta AS t1 "
                + "ON b.id_tipoVta = t1.id_tipoVta "
                + "WHERE b.cerrada =0 AND b.pagada =1 "
                + "GROUP BY t1.id_tipoVta, t1.desc_tipoVta";

        conexionDB c = new conexionDB();
        Connection con = c.conectar();
        
        System.out.println("SQL PARA TRAER VENTAS TIPO: "+sql);

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                Object[] venta = new Object[2];
                venta[0] = rs1.getString(2);
                venta[1] = rs1.getFloat(3);

                catVentas.add(venta);

                //modeloTablaFacturacion.addRow(tipo);
            }

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO CIERRE FACTURACION VTAS: " + e);
        }

        for (int i = 0; i < catVentas.size(); i++) {
            System.out.println(catVentas.get(i)[0] + " " + catVentas.get(i)[1]);
        }

        return catVentas;
    }

    public ArrayList<Object[]> traerVentasComida() {
        ArrayList<Object[]> comidas = new ArrayList<>();

        String sql = "SELECT t2.id_tipoProd2, t2.desc_tipoProd2, SUM( d.cantidad * d.precio ) "
                + "FROM Detalle_mesa AS d "
                + "JOIN articulo AS a ON a.codigo = d.codigo "
                + "JOIN Tipo_Prod2 AS t2 ON a.id_tipoProd2 = t2.id_tipoProd2 "
                + "JOIN Boleta AS b ON b.id_boleta = d.id_boleta "
                + "WHERE b.cerrada =0 "
                + "AND b.pagada =1 "
                + "AND a.id_tipoProd = 1 "
                + "GROUP BY t2.id_tipoProd2, t2.desc_tipoProd2 ";

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                Object[] comida = new Object[2];
                comida[0] = rs1.getString(2);
                comida[1] = rs1.getFloat(3);

                comidas.add(comida);

                //modeloTablaFacturacion.addRow(tipo);
            }

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO CIERRE FACTURACION VTAS: " + e);
        }

        for (int i = 0; i < comidas.size(); i++) {
            System.out.println(comidas.get(i)[0] + " " + comidas.get(i)[1]);
        }

        return comidas;
    }

    public ArrayList<Object[]> traerVentasBebida() {
        ArrayList<Object[]> bebidas = new ArrayList<>();

        String sql = "SELECT t2.id_tipoProd2, t2.desc_tipoProd2, SUM( d.cantidad * d.precio ) "
                + "FROM Detalle_mesa AS d "
                + "JOIN articulo AS a ON a.codigo = d.codigo "
                + "JOIN Tipo_Prod2 AS t2 ON a.id_tipoProd2 = t2.id_tipoProd2 "
                + "JOIN Boleta AS b ON b.id_boleta = d.id_boleta "
                + "WHERE b.cerrada =0 "
                + "AND b.pagada =1 "
                + "AND a.id_tipoProd = 2 "
                + "GROUP BY t2.id_tipoProd2, t2.desc_tipoProd2 ";

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                Object[] bebida = new Object[2];
                bebida[0] = rs1.getString(2);
                bebida[1] = rs1.getFloat(3);

                bebidas.add(bebida);

                //modeloTablaFacturacion.addRow(tipo);
            }

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO CIERRE FACTURACION VTAS: " + e);
        }

        for (int i = 0; i < bebidas.size(); i++) {
            System.out.println(bebidas.get(i)[0] + " " + bebidas.get(i)[1]);
        }

        return bebidas;
    }

    public ArrayList<Object[]> traerVentasPromocion() {
        ArrayList<Object[]> promociones = new ArrayList<>();

        String sql = "SELECT t2.id_tipoProd2, t2.desc_tipoProd2, SUM( d.cantidad * d.precio ) "
                + "FROM Detalle_mesa AS d "
                + "JOIN articulo AS a ON a.codigo = d.codigo "
                + "JOIN Tipo_Prod2 AS t2 ON a.id_tipoProd2 = t2.id_tipoProd2 "
                + "JOIN Boleta AS b ON b.id_boleta = d.id_boleta "
                + "WHERE b.cerrada =0 "
                + "AND b.pagada =1 "
                + "AND a.id_tipoProd = 3 "
                + "GROUP BY t2.id_tipoProd2, t2.desc_tipoProd2";

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                Object[] promocion = new Object[2];
                promocion[0] = rs1.getString(2);
                promocion[1] = rs1.getFloat(3);

                promociones.add(promocion);

                //modeloTablaFacturacion.addRow(tipo);
            }

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO CIERRE FACTURACION PROMOCIONES: " + e);
        }

        for (int i = 0; i < promociones.size(); i++) {
            System.out.println(promociones.get(i)[0] + " " + promociones.get(i)[1]);
        }

        return promociones;
    }

    public ArrayList<Object[]> traerVentasDescuento() {
        ArrayList<Object[]> descuentos = new ArrayList<>();

        String sql = "SELECT a.desc_articulo, SUM( d.cantidad * d.precio ) "
                + "FROM Detalle_mesa AS d "
                + "JOIN articulo AS a ON a.codigo = d.codigo "
                + "JOIN Tipo_Prod AS t1 ON a.id_tipoProd = t1.id_tipoProd1 "
                + "JOIN Boleta AS b ON b.id_boleta = d.id_boleta "
                + "WHERE b.cerrada =0 "
                + "AND b.pagada =1 "
                + "AND t1.id_tipoProd1 = 4 "
                + "GROUP BY a.desc_articulo ";

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                Object[] descuento = new Object[2];
                descuento[0] = rs1.getString(1);
                descuento[1] = rs1.getFloat(2);

                descuentos.add(descuento);

                //modeloTablaFacturacion.addRow(tipo);
            }

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO CIERRE FACTURACION PROMOCIONES: " + e);
        }

        for (int i = 0; i < descuentos.size(); i++) {
            System.out.println(descuentos.get(i)[0] + " " + descuentos.get(i)[1]);
        }

        return descuentos;
    }

    public ArrayList<Object[]> traerCostos() {
        ArrayList<Object[]> costos = new ArrayList<>();

        String sql = "SELECT t.id_tipoCosto, t.desc_tipoCosto, SUM( c.valor ) "
                + "FROM Costo AS c "
                + "JOIN Tipo_Costo AS t ON t.id_tipoCosto = c.id_tipoCosto "
                + "WHERE cerrado =0 "
                + "GROUP BY t.id_tipoCosto, t.desc_tipoCosto";

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                Object[] costo = new Object[2];
                costo[0] = rs1.getString(2);
                costo[1] = rs1.getFloat(3);

                costos.add(costo);

                //modeloTablaFacturacion.addRow(tipo);
            }

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO CIERRE COSTOS: " + e);
        }

        for (int i = 0; i < costos.size(); i++) {
            System.out.println(costos.get(i)[0] + " " + costos.get(i)[1]);
        }

        return costos;
    }

    public ArrayList<Object[]> traerFacturacionVentas() {
        ArrayList<Object[]> facVtas = new ArrayList<>();

        String sql = "SELECT fb.id_condVta, c.desc_condVta, SUM(fb.monto) "
                + "FROM Facturacion_boleta as fb "
                + "JOIN Cond_Vta as c "
                + "on fb.id_condVta = c.id_condVta "
                + "JOIN Boleta as b "
                + "on fb.id_boleta = b.id_boleta "
                + "WHERE cerrada = 0 "
                + "group by fb.id_condVta, c.desc_condVta";

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                Object[] fv = new Object[4];
                fv[0] = rs1.getString(2);
                fv[1] = rs1.getFloat(3);
                fv[2] = 0;
                fv[3] = 0;

                facVtas.add(fv);

                //modeloTablaFacturacion.addRow(tipo);
            }

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO FACTURACION VENTAS: " + e);
        }

        for (int i = 0; i < facVtas.size(); i++) {
            System.out.println(facVtas.get(i)[0] + " " + facVtas.get(i)[1]);
        }

        return facVtas;
    }

    public ArrayList<Object[]> traerFacturacionCostos() {
        ArrayList<Object[]> facCostos = new ArrayList<>();

        String sql = "SELECT fc.id_condVta, c.desc_condVta, SUM(fc.monto) "
                + "FROM Facturacion_costo as fc "
                + "JOIN Cond_Vta as c "
                + "on fc.id_condVta = c.id_condVta "
                + "JOIN Costo as co "
                + "on fc.id_costo = co.id_costo "
                + "WHERE co.cerrado = 0 "
                + "group by fc.id_condVta, c.desc_condVta";

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                Object[] fc = new Object[4];
                fc[0] = rs1.getString(2);
                fc[1] = 0;
                fc[2] = rs1.getFloat(3);
                fc[3] = 0;

                facCostos.add(fc);

                //modeloTablaFacturacion.addRow(tipo);
            }

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO FACTURACION COSTOS: " + e);
        }

        for (int i = 0; i < facCostos.size(); i++) {
            System.out.println(facCostos.get(i)[0] + " " + facCostos.get(i)[2]);
        }

        return facCostos;
    }

    public void insertarCierreCaja(Cierre_Caja cj) {
        conexionDB con = new conexionDB();
        Connection reg = con.conectar();

        String sql = "INSERT INTO cierre_caja (fecha_cierre, hora_cierre, venta_contado, venta_debito, venta_credito, costo_contado, costo_debito, costo_credito) VALUES (?,?,?,?,?,?,?,?)";
        System.out.println(sql);

        try {
            PreparedStatement ps = reg.prepareStatement(sql);
            ps.setString(1, cj.getFecha_cierre());
            ps.setString(2, cj.getHora_cierre());
            ps.setFloat(3, cj.getVenta_contado());
            ps.setFloat(4, cj.getVenta_debito());
            ps.setFloat(5, cj.getVenta_credito());
            ps.setFloat(6, cj.getCosto_contado());
            ps.setFloat(7, cj.getCosto_debito());
            ps.setFloat(8, cj.getCosto_credito());

            ps.executeUpdate();

            reg.close();

        } catch (SQLException e) {
            System.out.println("ERROR EN EL AGREGADO DE CIERRE DE CAJA: " + e);
        }
    }

    public void cerrarCaja(Cierre_Caja cj) {
        conexionDB c = new conexionDB();
        Connection con = c.conectar();
        
        insertarCierreCaja(cj);
        
        int id_ult = obtenerID_cierreCaja();

        String sql = "UPDATE BOLETA "
                + "SET cerrada = "+id_ult+" "
                + "WHERE cerrada = 0 and pagada = 1";

        System.out.println(sql);

        Statement st1;

        try {
            st1 = con.createStatement();
            st1.executeUpdate(sql);

            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }

        String sql2 = "UPDATE COSTO "
                + "SET cerrado = "+id_ult+" "
                + "WHERE cerrado = 0";

        Statement st;

        try {
            st = con.createStatement();
            st.executeUpdate(sql2);

            con.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void eliminarDetalleMesa(int id) {
        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        String sql = "DELETE from detalle_mesa "
                + "where id_detallemesa = " + id;

        System.out.println(sql);

        Statement st;

        try {
            st = con.createStatement();
            st.executeUpdate(sql);

            con.close();

        } catch (SQLException ex) {
            Logger.getLogger("ERROR AL ELIMINAR DETALLE MESA: " + manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public float traerVentasContado() {
        float total = 0;

        String sql = "SELECT fb.id_condVta, c.desc_condVta, SUM(fb.monto) "
                + "FROM Facturacion_boleta as fb "
                + "JOIN Cond_Vta as c "
                + "on fb.id_condVta = c.id_condVta "
                + "JOIN Boleta as b "
                + "on fb.id_boleta = b.id_boleta "
                + "WHERE cerrada = 0 and fb.id_condVta = 1 "
                + "group by fb.id_condVta, c.desc_condVta";

        System.out.println(sql);

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                total = rs1.getFloat(3);

            }

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO FACTURACION VENTAS CONTADO: " + e);
        }

        return total;
    }

    public float traerVentasDebito() {
        float total = 0;

        String sql = "SELECT fb.id_condVta, c.desc_condVta, SUM(fb.monto) "
                + "FROM Facturacion_boleta as fb "
                + "JOIN Cond_Vta as c "
                + "on fb.id_condVta = c.id_condVta "
                + "JOIN Boleta as b "
                + "on fb.id_boleta = b.id_boleta "
                + "WHERE cerrada = 0 and fb.id_condVta = 3 "
                + "group by fb.id_condVta, c.desc_condVta";

        System.out.println(sql);

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                total = rs1.getFloat(3);

            }

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO FACTURACION VENTAS CONTADO: " + e);
        }

        return total;
    }

    public float traerVentasCredito() {
        float total = 0;

        String sql = "SELECT fb.id_condVta, c.desc_condVta, SUM(fb.monto) "
                + "FROM Facturacion_boleta as fb "
                + "JOIN Cond_Vta as c "
                + "on fb.id_condVta = c.id_condVta "
                + "JOIN Boleta as b "
                + "on fb.id_boleta = b.id_boleta "
                + "WHERE cerrada = 0 and fb.id_condVta = 2 "
                + "group by fb.id_condVta, c.desc_condVta";

        System.out.println(sql);

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                total = rs1.getFloat(3);

            }

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO FACTURACION VENTAS CONTADO: " + e);
        }

        return total;
    }

    public float traerCostosContado() {
        float total = 0;
        
        String sql = "SELECT fc.id_condVta, c.desc_condVta, SUM(fc.monto) "
                + "FROM Facturacion_costo as fc "
                + "JOIN Cond_Vta as c "
                + "on fc.id_condVta = c.id_condVta "
                + "JOIN Costo as co "
                + "on fc.id_costo = co.id_costo "
                + "WHERE co.cerrado = 0 and fc.id_condVta = 1 "
                + "group by fc.id_condVta, c.desc_condVta";

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                total = rs1.getFloat(3);
            }

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO FACTURACION COSTOS: " + e);
        }


        return total;

    }
    
    public float traerCostosDebito() {
        float total = 0;
        
        String sql = "SELECT fc.id_condVta, c.desc_condVta, SUM(fc.monto) "
                + "FROM Facturacion_costo as fc "
                + "JOIN Cond_Vta as c "
                + "on fc.id_condVta = c.id_condVta "
                + "JOIN Costo as co "
                + "on fc.id_costo = co.id_costo "
                + "WHERE co.cerrado = 0 and fc.id_condVta = 3 "
                + "group by fc.id_condVta, c.desc_condVta";

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                total = rs1.getFloat(3);
            }

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO FACTURACION COSTOS: " + e);
        }


        return total;

    }
    
    public float traerCostosCredito() {
        float total = 0;
        
        String sql = "SELECT fc.id_condVta, c.desc_condVta, SUM(fc.monto) "
                + "FROM Facturacion_costo as fc "
                + "JOIN Cond_Vta as c "
                + "on fc.id_condVta = c.id_condVta "
                + "JOIN Costo as co "
                + "on fc.id_costo = co.id_costo "
                + "WHERE co.cerrado = 0 and fc.id_condVta = 2 "
                + "group by fc.id_condVta, c.desc_condVta";

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                total = rs1.getFloat(3);
            }

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO FACTURACION COSTOS: " + e);
        }


        return total;

    }
    
    public int obtenerID_cierreCaja() {
        int ultCierre = 0;

        conexionDB con = new conexionDB();
        Connection reg = con.conectar();

        String sql = "SELECT id_cierreCaja from cierre_caja order by id_cierreCaja desc limit 1";
        System.out.println(sql);

        try {
            Statement st = reg.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                ultCierre = rs.getInt(1);
                System.out.println("ULTIMO CIERRE: " + ultCierre);
            }

            reg.close();

        } catch (SQLException e) {
            System.out.println("ERROR AL TRAER ULTIMO CIERRE");
        }

        return ultCierre;
    }
    
    public Cierre_Caja traerCierreCaja (int id){
        Cierre_Caja cj = new Cierre_Caja();
        
        conexionDB con = new conexionDB();
        Connection reg = con.conectar();

        String sql = "SELECT id_cierreCaja, fecha_cierre, hora_cierre, venta_contado, venta_debito, venta_credito, costo_contado, costo_debito, costo_credito "
                + "from cierre_caja "
                + "where id_cierreCaja = "+id;
        
        System.out.println(sql);

        try {
            Statement st = reg.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                cj.setId_cierreCaja(id);
                cj.setFecha_cierre(mf.convertirFecha(mf.convertirFechaInversa(rs.getString(2))));
                cj.setHora_cierre(rs.getString(3));
                cj.setVenta_contado(rs.getFloat(4));
                cj.setVenta_debito(rs.getFloat(5));
                cj.setVenta_credito(rs.getFloat(6));
                cj.setCosto_contado(rs.getFloat(7));
                cj.setCosto_debito(rs.getFloat(8));
                cj.setCosto_credito(rs.getFloat(9));
            }

            reg.close();

        } catch (SQLException e) {
            System.out.println("ERROR AL TRAER ULTIMO CIERRE");
        }
        
        
        
        return cj;
    }
    
    public ArrayList<Cierre_Caja> traerCierreCajaFechas (String fechaDesde, String fechaHasta){
        
         ArrayList<Cierre_Caja> cierres = new ArrayList<>();
        
        conexionDB con = new conexionDB();
        Connection reg = con.conectar();

        String sql = "SELECT id_cierreCaja, fecha_cierre, hora_cierre, venta_contado, venta_debito, venta_credito, costo_contado, costo_debito, costo_credito "
                + "from cierre_caja "
                + "where fecha_cierre >= '"+fechaDesde+"' and fecha_cierre <= '"+fechaHasta+"' ";
        
        System.out.println(sql);

        try {
            Statement st = reg.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                Cierre_Caja cj = new Cierre_Caja();
                cj.setId_cierreCaja(rs.getInt(1));
                cj.setFecha_cierre(mf.convertirFecha(mf.convertirFechaInversa(rs.getString(2))));
                cj.setHora_cierre(rs.getString(3));
                cj.setVenta_contado(rs.getFloat(4));
                cj.setVenta_debito(rs.getFloat(5));
                cj.setVenta_credito(rs.getFloat(6));
                cj.setCosto_contado(rs.getFloat(7));
                cj.setCosto_debito(rs.getFloat(8));
                cj.setCosto_credito(rs.getFloat(9));
                
                cierres.add(cj);
            }

            reg.close();

        } catch (SQLException e) {
            System.out.println("ERROR AL TRAER ULTIMO CIERRE");
        }
        
        
        
        return cierres;
    }
    
    public void destrabarMesa (Boleta b){
        conexionDB con = new conexionDB();
        Connection reg = con.conectar();
        int cerrada = traerCierreBoleta(b.getId_boleta());

        String sql = "UPDATE Boleta "
                + "SET pagada = 1, cerrada = "+ cerrada +" "
                + "WHERE id_boleta = " + b.getId_boleta();

        System.out.println(sql);

        Statement st1;
        
        try {
            st1 = reg.createStatement();
            st1.executeUpdate(sql);


        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        sql = "INSERT INTO facturacion_boleta (id_boleta, id_condVta, monto) VALUES (?,?,?)";
        System.out.println("ID : "+b.getId_boleta());
        System.out.println("CONDVTA : "+1);
        System.out.println("MONTO : "+b.getTotal());

        try {
            PreparedStatement ps = reg.prepareStatement(sql);
            ps.setInt(1, b.getId_boleta());
            ps.setInt(2, 1);
            ps.setFloat(3, b.getTotal());

            ps.executeUpdate();

            reg.close();

        } catch (SQLException e) {
            System.out.println("ERROR EN EL AGREGADO DE DETALLE DE BOLETA: " + e);
        }
        
    }
    
    public int traerCierreBoleta (int id_boleta){
        conexionDB con = new conexionDB();
        Connection reg = con.conectar();
        int id = id_boleta+1;
        int cierre = 0;
        
        String sql = "SELECT cerrada "
                + "FROM Boleta "
                + "WHERE id_boleta = "+id;
        
        System.out.println(sql);
        
        
        try {
            Statement st = reg.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                cierre = rs.getInt(1);
            }

            reg.close();

        } catch (SQLException e) {
            System.out.println("ERROR AL TRAER ULTIMO CIERRE");
        }
        
        
        return cierre;
        
    }
    

}
