/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import boston.conexionDB;
import controlador.manejadorArticulos;
import controlador.manejadorConfiguraciones;
import controlador.manejadorFacturacion;
import controlador.manejadorFechas;
import controlador.manejadorReportes;
import controlador.manejadorTablas;
import controlador.manejadorUsuario;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Paint;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.Barril;
import modelo.Boleta;
import modelo.Configuracion;
import modelo.Usuario;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.ui.RefineryUtilities;

/**
 *
 * @author rmmayer
 */
public class Inicio extends javax.swing.JFrame {

    /**
     * Creates new form Inicio
     */
    public class ModeloTablaInicio extends DefaultTableModel {

        public boolean isCellEditable(int row, int column) {
            return false;
        }
    }

    public class ModeloTablaMesasOcupadas extends DefaultTableModel {

        public boolean isCellEditable(int row, int column) {
            return false;
        }
    }

    public class ModeloTablaDelivery extends DefaultTableModel {

        public boolean isCellEditable(int row, int column) {
            return false;
        }
    }

    ModeloTablaInicio modelo = new ModeloTablaInicio();
    ModeloTablaDelivery modeloDelivery = new ModeloTablaDelivery();
    ModeloTablaMesasOcupadas modeloMesasOcupadas = new ModeloTablaMesasOcupadas();

    Dimension dimTotal;
    Dimension dimPanelVtas;
    Dimension dimPanelOpciones;
    Dimension dimPanelBarriles;
    Dimension dimPanelMesas;
    Dimension dimPanelNuevaMesa;
    Dimension dimPanelReportes;
    Configuracion canos;
    JLabel labelGraficoBarriles = new JLabel();
    DefaultCategoryDataset datosBarriles = new DefaultCategoryDataset();
    JFreeChart barraBarriles;
    ChartPanel cp;
    DefaultPieDataset datosVentas = new DefaultPieDataset();
    //PieDataset dataset;
    JFreeChart tortaConsumos;
    ChartPanel chartPanelTorta;
    BufferedImage graficoBarraBarriles;
    private Paint[] colors = new Paint[]{Color.red, Color.blue, Color.green,
        Color.yellow, Color.orange, Color.cyan,
        Color.magenta, Color.blue
    };

    float cantPinta = (float) 0.5;
    float cantidadBarril1;
    float cantidadBarril2;
    float cantidadBarril3;
    //boolean sesion = true;
    Usuario user = new Usuario();
    Usuario mozo = new Usuario();
    Boleta boleta = new Boleta();
    //Barril barril = new Barril();

    manejadorTablas mt = new manejadorTablas();
    manejadorArticulos ma = new manejadorArticulos();
    manejadorFacturacion mb = new manejadorFacturacion();
    manejadorUsuario mu = new manejadorUsuario();
    manejadorReportes mr = new manejadorReportes();
    manejadorConfiguraciones mc = new manejadorConfiguraciones();

    public Inicio(Usuario u) {
        initComponents();
        this.setFocusable(true);
        if(conexionDB.db_name == "db_boston_tst"){
            this.setTitle("Usuario registrado: " + u.getNombre_usuario() + " " + u.getApellido_usuario() + " |  VERSION V210615 | TST");
        }else{
            this.setTitle("Usuario registrado: " + u.getNombre_usuario() + " " + u.getApellido_usuario() + " |  VERSION V210615 | PRD");
        }
        
        mt.modeloTablaVentaInicio(tablaInicio, modelo);
        mt.modeloTablaMesasOcupadas(tablaMesasOcupadas, modeloMesasOcupadas);
        mt.modeloTablaDelivery(tablaDelivery, modeloDelivery);

        user = u;
        botonImprimirReportes.setVisible(false);

        dimTotal = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize(dimTotal);
        this.setExtendedState(6);

        this.jMenuBar.removeAll();

        JMenu menu = new JMenu("Menu");
        jMenuBar.add(menu);
        //JMenuItem reportes = new JMenuItem("Reportes(3)");
        JMenuItem gestionCostos = new JMenuItem("Gestion de Costos (4)");
        JMenuItem modificarDatos = new JMenuItem("Modificar Datos del Sistema (5)");
        JMenuItem cuentasCorrientes = new JMenuItem("Cuentas Corrientes (6)");
        JMenuItem cerrarCaja = new JMenuItem("Cerrar Caja (7)");
        JMenuItem liberarMesas = new JMenuItem("Liberar Mesas");
        JMenuItem cerrarSesion = new JMenuItem("Cerrar Sesion");
        JMenu abm = new JMenu("ABM");
        menu.add(abm);
        JMenuItem abmCliente = new JMenuItem("Cliente (8)");
        JMenuItem abmArticulo = new JMenuItem("Articulo (9)");
        abm.add(abmCliente);
        abm.add(abmArticulo);
        menu.add(gestionCostos);
        menu.add(modificarDatos);
        menu.add(cuentasCorrientes);
        menu.add(cerrarCaja);
        if (u.getRoot() == 1) {
            menu.add(liberarMesas);
        }
        menu.add(cerrarSesion);

        JMenu ventas = new JMenu("Ventas");
        jMenuBar.add(ventas);
        JMenuItem ventaRapida = new JMenuItem("Nueva Venta Rapida (1)");
        JMenuItem ventaMesa = new JMenuItem("Nueva Mesa (2)");
        JMenuItem ventaDelivery = new JMenuItem("Delivery (3)");
        ventas.add(ventaRapida);
        ventas.add(ventaMesa);
        ventas.add(ventaDelivery);

        JMenu ayuda = new JMenu("?");
        jMenuBar.add(ayuda);
        JMenuItem acerca = new JMenuItem("Acerca de...");
        ayuda.add(acerca);

        if (u.getRoot() == 1) {
            JMenu root = new JMenu("Root");
            jMenuBar.add(root);
            JMenuItem bd = new JMenuItem("Cambiar BD");
            root.add(bd);
        }

        acerca.addActionListener(e -> {
            JOptionPane.showMessageDialog(null, "BISS IT ®\n"
                    + "SOFT VERSION 210531 \n"
                    + "ING. RODRIGO MAYER \n"
                    + "3886-525523");
        });

        liberarMesas.addActionListener(e -> {
            MesasTrabadas mtr = new MesasTrabadas();
            mtr.setLocationRelativeTo(null);
            mtr.setVisible(true);
        });

        /*
        reportes.addActionListener(e -> {
            if (jPanelReportes.isVisible()) {
                jPanelReportes.setVisible(false);
            } else {
                jPanelReportes.setVisible(true);
            }
        });*/
        cuentasCorrientes.addActionListener(e -> {
            if (this.user.getPrivilegio().getId_privilegio() == 1) {
                CuentasCorrientes cc = new CuentasCorrientes(1, null, user);
                cc.setLocationRelativeTo(null);
                cc.setResizable(false);
                cc.setVisible(true);
            } else {
                JOptionPane.showMessageDialog(null, "SU CUENTA DE USUARIO NO TIENE PERMISOS PARA ACCEDER A ESTE MENU");
            }
        });

        modificarDatos.addActionListener(e -> {
            //Configuraciones con = new Configuraciones();
            if (this.user.getPrivilegio().getId_privilegio() == 1) {
                MenuConfiguraciones con = new MenuConfiguraciones(user, this);
                con.setVisible(true);
                con.setResizable(false);
                con.setLocationRelativeTo(null);
            } else {
                JOptionPane.showMessageDialog(null, "SU CUENTA DE USUARIO NO TIENE PERMISOS PARA ACCEDER A ESTE MENU");
            }
        });

        gestionCostos.addActionListener(e -> {
            System.out.println("Opcion elegida: " + e.getActionCommand());
            if (this.user.getPrivilegio().getId_privilegio() == 1) {
                Gestion_Costos gc = new Gestion_Costos();
                gc.setResizable(false);
                gc.setVisible(true);
                gc.setLocationRelativeTo(null);
            } else {
                JOptionPane.showMessageDialog(null, "SU CUENTA DE USUARIO NO TIENE PERMISOS PARA ACCEDER A ESTE MENU");
            }

        });

        cerrarCaja.addActionListener(e -> {
            System.out.println("Opcion elegida: " + e.getActionCommand());
            if (this.user.getPrivilegio().getId_privilegio() == 1) {
                //CierreCaja cc = new CierreCaja(user, this);
                CierreCajaResumido cc = new CierreCajaResumido(user, this);
                cc.setResizable(false);
                cc.setVisible(true);
                cc.setLocationRelativeTo(null);
            } else {
                JOptionPane.showMessageDialog(null, "SU CUENTA DE USUARIO NO TIENE PERMISOS PARA ACCEDER A ESTE MENU");
            }
        });

        cerrarSesion.addActionListener(e -> {
            System.out.println("Opcion elegida: " + e.getActionCommand());

            this.dispose();

        });

        abmCliente.addActionListener(e -> {
            if (this.user.getPrivilegio().getId_privilegio() == 1) {
                Clientes cli = new Clientes(user);
                cli.setVisible(true);
                cli.setResizable(false);
                cli.setLocationRelativeTo(null);
            } else {
                JOptionPane.showMessageDialog(null, "SU CUENTA DE USUARIO NO TIENE PERMISOS PARA ACCEDER A ESTE MENU");
            }
        });

        abmArticulo.addActionListener(e -> {
            if (this.user.getPrivilegio().getId_privilegio() == 1) {
                Articulos a = new Articulos(1);
                a.setVisible(true);
                a.setResizable(false);
                a.setLocationRelativeTo(null);
            } else {
                JOptionPane.showMessageDialog(null, "SU CUENTA DE USUARIO NO TIENE PERMISOS PARA ACCEDER A ESTE MENU");
            }
        });

        ventaRapida.addActionListener(e -> {
            nuevaVtaRapida nvr = new nuevaVtaRapida(this, user, datosBarriles, this.checkHappyHour.isSelected(), modelo);
            nvr.setVisible(true);
            nvr.setLocationRelativeTo(null);
            nvr.setResizable(false);
        });

        ventaMesa.addActionListener(e -> {
            NuevaMesa nm = new NuevaMesa(user, datosBarriles, this.checkHappyHour.isSelected(), modelo, modeloMesasOcupadas, modeloDelivery, boleta, 1, this);
            nm.setLocationRelativeTo(null);
            nm.setVisible(true);
            nm.setResizable(false);
        });

        ventaDelivery.addActionListener(e -> {
            NuevaMesa nm = new NuevaMesa(user, datosBarriles, this.checkHappyHour.isSelected(), modelo, modeloMesasOcupadas, modeloDelivery, boleta, 4, this);
            nm.setLocationRelativeTo(null);
            nm.setVisible(true);
            nm.setResizable(false);
        });

        ////////////////////////  PANEL BARRILES    ////////////////////////
        System.out.println("TAMAÑO DE PANTALLA: " + dimTotal.width + " x " + dimTotal.height);
        dimPanelVtas = new Dimension((dimTotal.width / 2), (dimTotal.height / 2) - 200);
        System.out.println("dimension panel ventas: " + dimTotal.width / 2 + " " + dimTotal.height / 2);
        jPanelVtas.setLayout(new GridLayout());
        jPanelVtas.setPreferredSize(dimPanelVtas);
        jPanelVtas.setMinimumSize(dimPanelVtas);
        tablaInicio.setMinimumSize(dimTotal);
        ///////////////////////////////////////////////////////////////////

        ////////////////////////  PANEL BARRILES    ////////////////////////
        dimPanelBarriles = new Dimension((int) (dimTotal.getWidth() / 2), (int) (dimTotal.getHeight() / 2) - 100);
        jPanelBarriles.setLayout(new GridLayout());
        jPanelBarriles.setPreferredSize(dimPanelBarriles);
        jPanelBarriles.setMinimumSize(dimPanelBarriles);
        jPanelBarriles.setMaximumSize(dimPanelBarriles);

        barraBarriles = ChartFactory.createBarChart3D("", "Posicion", "Cantidad", datosBarriles, PlotOrientation.VERTICAL, true, true, true);
        cp = new ChartPanel(barraBarriles);
        jPanelBarriles.add(cp);

        cp.repaint();

        canos = mc.valorVariableString("caños");

        for (int i = 1; i <= canos.getValor_configuracion(); i++) {
            ma.traerBarrilesActivos(datosBarriles, i);
        }
        ////////////////////////////////////////////////////////////////////////

        ////////////////////////  PANEL DE MESAS OCUPADAS    ////////////////////////
        dimPanelNuevaMesa = new Dimension((dimTotal.width / 2 - 30), (int) ((dimTotal.height / 2) - (dimTotal.height * 0.097)));
        jPanelMesasOcupadas.setPreferredSize(dimPanelNuevaMesa);
        jPanelMesasOcupadas.setMinimumSize(dimPanelNuevaMesa);
        jPanelMesasOcupadas.setMaximumSize(dimPanelNuevaMesa);
        jPanelMesasOcupadas.setLayout(new GridLayout());
        ////////////////////////////////////////////////////////////////////////

        ////////////////////////  PANEL DE REPORTES    ////////////////////////
        dimPanelReportes = new Dimension((dimTotal.width / 2 - 30), (int) ((dimTotal.height / 2) - (dimTotal.height * 0.097)));
        jPanelReportes.setPreferredSize(dimPanelReportes);
        jPanelReportes.setMinimumSize(dimPanelReportes);
        jPanelReportes.setMaximumSize(dimPanelReportes);
        jPanelReportes.setLayout(new GridLayout());
        jPanelReportes.setVisible(true);

        /*tortaConsumos = ChartFactory.createPieChart3D("", datosVentas, false, false, false);
        chartPanelTorta = new ChartPanel(tortaConsumos);
        jPanelReportes.add(chartPanelTorta);
        mr.ventasActuales(datosVentas);
        chartPanelTorta.repaint();
        jPanelReportes.setVisible(false);*/
        ////////////////////////////////////////////////////////////////////////
        mt.traerVentasSinCerrar(modelo);
        mt.traerMesasOcupadas(modeloMesasOcupadas);
        mt.traerDeliverys(modeloDelivery);

    }

    public ModeloTablaInicio getModeloVentas() {
        return modelo;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelVtas = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaInicio = new javax.swing.JTable();
        jPanelBarriles = new javax.swing.JPanel();
        jPanelMesasOcupadas = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablaMesasOcupadas = new javax.swing.JTable();
        checkHappyHour = new javax.swing.JCheckBox();
        botonCambiarBarril = new javax.swing.JButton();
        jPanelReportes = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tablaDelivery = new javax.swing.JTable();
        botonImprimirReportes = new javax.swing.JButton();
        jMenuBar = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                formKeyReleased(evt);
            }
        });

        jPanelVtas.setBorder(javax.swing.BorderFactory.createTitledBorder("VENTAS DEL DIA"));

        tablaInicio.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablaInicio.setMaximumSize(new java.awt.Dimension(2147, 64));
        tablaInicio.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaInicioMouseClicked(evt);
            }
        });
        tablaInicio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablaInicioKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tablaInicioKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tablaInicio);

        javax.swing.GroupLayout jPanelVtasLayout = new javax.swing.GroupLayout(jPanelVtas);
        jPanelVtas.setLayout(jPanelVtasLayout);
        jPanelVtasLayout.setHorizontalGroup(
            jPanelVtasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelVtasLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 846, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(22, Short.MAX_VALUE))
        );
        jPanelVtasLayout.setVerticalGroup(
            jPanelVtasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelVtasLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(277, Short.MAX_VALUE))
        );

        jPanelBarriles.setBorder(javax.swing.BorderFactory.createTitledBorder("BARRILES DE CERVEZA"));

        javax.swing.GroupLayout jPanelBarrilesLayout = new javax.swing.GroupLayout(jPanelBarriles);
        jPanelBarriles.setLayout(jPanelBarrilesLayout);
        jPanelBarrilesLayout.setHorizontalGroup(
            jPanelBarrilesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanelBarrilesLayout.setVerticalGroup(
            jPanelBarrilesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanelMesasOcupadas.setBorder(javax.swing.BorderFactory.createTitledBorder("MESAS OCUPADAS"));

        tablaMesasOcupadas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablaMesasOcupadas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaMesasOcupadasMouseClicked(evt);
            }
        });
        tablaMesasOcupadas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablaMesasOcupadasKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tablaMesasOcupadasKeyReleased(evt);
            }
        });
        jScrollPane2.setViewportView(tablaMesasOcupadas);

        javax.swing.GroupLayout jPanelMesasOcupadasLayout = new javax.swing.GroupLayout(jPanelMesasOcupadas);
        jPanelMesasOcupadas.setLayout(jPanelMesasOcupadasLayout);
        jPanelMesasOcupadasLayout.setHorizontalGroup(
            jPanelMesasOcupadasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelMesasOcupadasLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 738, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanelMesasOcupadasLayout.setVerticalGroup(
            jPanelMesasOcupadasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelMesasOcupadasLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 269, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        checkHappyHour.setText("Happy Hour");
        checkHappyHour.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                checkHappyHourMouseClicked(evt);
            }
        });
        checkHappyHour.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkHappyHourActionPerformed(evt);
            }
        });

        botonCambiarBarril.setText("CAMBIAR BARRIL");
        botonCambiarBarril.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonCambiarBarrilActionPerformed(evt);
            }
        });

        jPanelReportes.setBorder(javax.swing.BorderFactory.createTitledBorder("DELIVERYS ENVIADOS SIN CERRAR"));

        tablaDelivery.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablaDelivery.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaDeliveryMouseClicked(evt);
            }
        });
        tablaDelivery.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablaDeliveryKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tablaDeliveryKeyReleased(evt);
            }
        });
        jScrollPane3.setViewportView(tablaDelivery);

        javax.swing.GroupLayout jPanelReportesLayout = new javax.swing.GroupLayout(jPanelReportes);
        jPanelReportes.setLayout(jPanelReportesLayout);
        jPanelReportesLayout.setHorizontalGroup(
            jPanelReportesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelReportesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3)
                .addContainerGap())
        );
        jPanelReportesLayout.setVerticalGroup(
            jPanelReportesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelReportesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(423, Short.MAX_VALUE))
        );

        botonImprimirReportes.setText("IMPRIMIR REPORTES");

        jMenuBar.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jMenu1.setText("File");
        jMenuBar.add(jMenu1);

        jMenu2.setText("Edit");
        jMenuBar.add(jMenu2);

        setJMenuBar(jMenuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanelVtas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanelBarriles, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(checkHappyHour)
                        .addGap(18, 18, 18)
                        .addComponent(botonCambiarBarril, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanelMesasOcupadas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanelReportes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(botonImprimirReportes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanelVtas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanelMesasOcupadas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanelReportes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanelBarriles, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(botonImprimirReportes)
                    .addComponent(checkHappyHour)
                    .addComponent(botonCambiarBarril))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonCambiarBarrilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonCambiarBarrilActionPerformed
        // TODO add your handling code here:
        if (this.user.getPrivilegio().getId_privilegio() == 1) {
            cambiarBarril cb = new cambiarBarril(datosBarriles, this, user);
            cb.setVisible(true);
            cb.setLocationRelativeTo(null);

        } else {
            JOptionPane.showMessageDialog(null, "SU CUENTA DE USUARIO NO TIENE PERMISOS PARA ACCEDER A ESTE MENU");
        }

    }//GEN-LAST:event_botonCambiarBarrilActionPerformed

    private void checkHappyHourActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkHappyHourActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_checkHappyHourActionPerformed

    private void formKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyReleased
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_1) {

            nuevaVtaRapida nvr = new nuevaVtaRapida(this, user, datosBarriles, this.checkHappyHour.isSelected(), modelo);
            nvr.setVisible(true);
            nvr.setLocationRelativeTo(null);
            nvr.setResizable(false);

        }
        if (evt.getKeyCode() == KeyEvent.VK_2) {

            NuevaMesa nm = new NuevaMesa(user, datosBarriles, this.checkHappyHour.isSelected(), modelo, modeloMesasOcupadas, modeloDelivery, boleta, 1, this);
            nm.setLocationRelativeTo(null);
            nm.setVisible(true);
            nm.setResizable(false);

        }

        if (evt.getKeyCode() == KeyEvent.VK_3) {
            NuevaMesa nm = new NuevaMesa(user, datosBarriles, this.checkHappyHour.isSelected(), modelo, modeloMesasOcupadas, modeloDelivery, boleta, 4, this);
            nm.setLocationRelativeTo(null);
            nm.setVisible(true);
            nm.setResizable(false);
        }

        if (evt.getKeyCode() == KeyEvent.VK_6) {

            if (this.user.getPrivilegio().getId_privilegio() == 1) {
                CuentasCorrientes cc = new CuentasCorrientes(1, null, user);
                cc.setLocationRelativeTo(null);
                cc.setResizable(false);
                cc.setVisible(true);
            } else {
                JOptionPane.showMessageDialog(null, "SU CUENTA DE USUARIO NO TIENE PERMISOS PARA ACCEDER A ESTE MENU");
            }

        }

        if (evt.getKeyCode() == KeyEvent.VK_0) {

            if (jPanelReportes.isVisible()) {
                jPanelReportes.setVisible(false);
            } else {
                jPanelReportes.setVisible(true);
            }

        }

        if (evt.getKeyCode() == KeyEvent.VK_4) {
            if (this.user.getPrivilegio().getId_privilegio() == 1) {
                Gestion_Costos gc = new Gestion_Costos();
                gc.setResizable(false);
                gc.setVisible(true);
                gc.setLocationRelativeTo(null);
            } else {
                JOptionPane.showMessageDialog(null, "SU CUENTA DE USUARIO NO TIENE PERMISOS PARA ACCEDER A ESTE MENU");
            }

        }

        if (evt.getKeyCode() == KeyEvent.VK_5) {
            //Configuraciones con = new Configuraciones();
            //con.setVisible(true);
            //con.setResizable(false);
            //con.setLocationRelativeTo(null);
            if (this.user.getPrivilegio().getId_privilegio() == 1) {
                MenuConfiguraciones mconf = new MenuConfiguraciones(user, this);
                mconf.setVisible(true);
                mconf.setLocationRelativeTo(null);
                mconf.setResizable(false);
            } else {
                JOptionPane.showMessageDialog(null, "SU CUENTA DE USUARIO NO TIENE PERMISOS PARA ACCEDER A ESTE MENU");
            }
        }

        if (evt.getKeyCode() == KeyEvent.VK_7) {
            if (this.user.getPrivilegio().getId_privilegio() == 1) {
                //CierreCaja cc = new CierreCaja(user, this);
                CierreCajaResumido cc = new CierreCajaResumido(user, this);
                cc.setResizable(false);
                cc.setVisible(true);
                cc.setLocationRelativeTo(null);
            } else {
                JOptionPane.showMessageDialog(null, "SU CUENTA DE USUARIO NO TIENE PERMISOS PARA ACCEDER A ESTE MENU");
            }
        }

        if (evt.getKeyCode() == KeyEvent.VK_8) {
            if (this.user.getPrivilegio().getId_privilegio() == 1) {
                Clientes a = new Clientes(user);
                a.setVisible(true);
                a.setResizable(false);
                a.setLocationRelativeTo(null);

            } else {
                JOptionPane.showMessageDialog(null, "SU CUENTA DE USUARIO NO TIENE PERMISOS PARA ACCEDER A ESTE MENU");
            }
        }

        if (evt.getKeyCode() == KeyEvent.VK_9) {
            if (this.user.getPrivilegio().getId_privilegio() == 1) {
                Articulos a = new Articulos(1);
                a.setVisible(true);
                a.setResizable(false);
                a.setLocationRelativeTo(null);

            } else {
                JOptionPane.showMessageDialog(null, "SU CUENTA DE USUARIO NO TIENE PERMISOS PARA ACCEDER A ESTE MENU");
            }
        }

        /*if (evt.getKeyCode() == KeyEvent.VK_9) {

            float acumulado = 0;

            for (int i = 0; i < this.tablaInicio.getRowCount(); i++) {
                float suma = Float.parseFloat(this.tablaInicio.getValueAt(i, 3).toString());
                acumulado = acumulado + suma;
            }

            //JOptionPane.showMessageDialog(null, "TOTAL FACTURADO HASTA EL MOMENTO: $ " + acumulado);
            if (this.user.getPrivilegio().getId_privilegio() == 1) {
                DatosRelevantes dr = new DatosRelevantes(acumulado);
                dr.setVisible(true);
                dr.setLocationRelativeTo(null);
                dr.setResizable(false);

            } else {
                JOptionPane.showMessageDialog(null, "SU CUENTA DE USUARIO NO TIENE PERMISOS PARA ACCEDER A ESTE MENU");
            }
        }*/
        if (evt.getKeyCode() == KeyEvent.VK_F5) {
            mt.traerMesasOcupadas(modeloMesasOcupadas);
            mt.traerVentasSinCerrar(modelo);
            mt.traerDeliverys(modeloDelivery);
        }

        this.tablaMesasOcupadas.clearSelection();
        this.tablaInicio.clearSelection();
        this.tablaDelivery.clearSelection();
    }//GEN-LAST:event_formKeyReleased

    private void tablaInicioMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaInicioMouseClicked
        // TODO add your handling code here:
        if (evt.getClickCount() == 2 && !evt.isConsumed()) {
            evt.consume();
            int filaS = this.tablaInicio.getSelectedRow();

            boleta = mb.traerBoletaID(Integer.parseInt(this.tablaInicio.getValueAt(filaS, 0).toString()));
            System.out.println("FECHA BOLETA: " + boleta.getFecha_cierre() + " HORA: " + boleta.getHora_cierre());
            manejadorFechas mfe = new manejadorFechas();
            //System.out.println("FECHA CONVERTIDA: "+mfe.convertirFecha(boleta.getFecha_cierre())+" HORAS CONV: "+boleta.getHora_cierre().substring(0,5));
            System.out.println("FECHA CONVERTIDA: " + mfe.convertirFecha2(boleta.getFecha_cierre()) + " HORAS CONV: " + boleta.getHora_cierre().substring(0, 5));
            //boleta.setId_boleta(Integer.parseInt(this.tablaMesasOcupadas.getValueAt(filaS, 0).toString()));
            mozo = mu.traerMozo(tablaInicio.getValueAt(filaS, 2).toString());
            boleta.setVendedor(mozo);
            boleta.setTotal(Float.parseFloat(tablaInicio.getValueAt(filaS, 3).toString()));

            NuevaMesa nm = null;

            if (boleta.getMesa() == 0) {
                if (this.tablaInicio.getValueAt(filaS, 1).toString().equals("Delivery")) {
                    nm = new NuevaMesa(user, datosBarriles, this.checkHappyHour.isSelected(), modelo, modeloMesasOcupadas, modeloDelivery, boleta, 6, this);
                } else {
                    nm = new NuevaMesa(user, datosBarriles, this.checkHappyHour.isSelected(), modelo, modeloMesasOcupadas, modeloDelivery, boleta, 7, this);
                }

            } else {
                nm = new NuevaMesa(user, datosBarriles, this.checkHappyHour.isSelected(), modelo, modeloMesasOcupadas, modeloDelivery, boleta, 3, this);
            }

            nm.setVisible(true);
            nm.setLocationRelativeTo(null);

            this.tablaMesasOcupadas.clearSelection();
            this.tablaInicio.clearSelection();
            this.tablaDelivery.clearSelection();
        }

    }//GEN-LAST:event_tablaInicioMouseClicked

    private void tablaInicioKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaInicioKeyReleased
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_1) {

            nuevaVtaRapida nvr = new nuevaVtaRapida(this, user, datosBarriles, this.checkHappyHour.isSelected(), modelo);
            nvr.setVisible(true);
            nvr.setLocationRelativeTo(null);
            nvr.setResizable(false);

        }
        if (evt.getKeyCode() == KeyEvent.VK_2) {

            NuevaMesa nm = new NuevaMesa(user, datosBarriles, this.checkHappyHour.isSelected(), modelo, modeloMesasOcupadas, modeloDelivery, boleta, 1, this);
            nm.setLocationRelativeTo(null);
            nm.setVisible(true);
            nm.setResizable(false);

        }

        if (evt.getKeyCode() == KeyEvent.VK_3) {
            NuevaMesa nm = new NuevaMesa(user, datosBarriles, this.checkHappyHour.isSelected(), modelo, modeloMesasOcupadas, modeloDelivery, boleta, 4, this);
            nm.setLocationRelativeTo(null);
            nm.setVisible(true);
            nm.setResizable(false);
        }

        if (evt.getKeyCode() == KeyEvent.VK_6) {

            if (this.user.getPrivilegio().getId_privilegio() == 1) {
                CuentasCorrientes cc = new CuentasCorrientes(1, null, user);
                cc.setLocationRelativeTo(null);
                cc.setResizable(false);
                cc.setVisible(true);
            } else {
                JOptionPane.showMessageDialog(null, "SU CUENTA DE USUARIO NO TIENE PERMISOS PARA ACCEDER A ESTE MENU");
            }

        }

        if (evt.getKeyCode() == KeyEvent.VK_0) {

            if (jPanelReportes.isVisible()) {
                jPanelReportes.setVisible(false);
            } else {
                jPanelReportes.setVisible(true);
            }

        }

        if (evt.getKeyCode() == KeyEvent.VK_4) {
            if (this.user.getPrivilegio().getId_privilegio() == 1) {
                Gestion_Costos gc = new Gestion_Costos();
                gc.setResizable(false);
                gc.setVisible(true);
                gc.setLocationRelativeTo(null);
            } else {
                JOptionPane.showMessageDialog(null, "SU CUENTA DE USUARIO NO TIENE PERMISOS PARA ACCEDER A ESTE MENU");
            }

        }

        if (evt.getKeyCode() == KeyEvent.VK_5) {
            //Configuraciones con = new Configuraciones();
            //con.setVisible(true);
            //con.setResizable(false);
            //con.setLocationRelativeTo(null);
            if (this.user.getPrivilegio().getId_privilegio() == 1) {
                MenuConfiguraciones mconf = new MenuConfiguraciones(user, this);
                mconf.setVisible(true);
                mconf.setLocationRelativeTo(null);
                mconf.setResizable(false);
            } else {
                JOptionPane.showMessageDialog(null, "SU CUENTA DE USUARIO NO TIENE PERMISOS PARA ACCEDER A ESTE MENU");
            }
        }

        if (evt.getKeyCode() == KeyEvent.VK_7) {
            if (this.user.getPrivilegio().getId_privilegio() == 1) {
                //CierreCaja cc = new CierreCaja(user, this);
                CierreCajaResumido cc = new CierreCajaResumido(user, this);
                cc.setResizable(false);
                cc.setVisible(true);
                cc.setLocationRelativeTo(null);
            } else {
                JOptionPane.showMessageDialog(null, "SU CUENTA DE USUARIO NO TIENE PERMISOS PARA ACCEDER A ESTE MENU");
            }
        }

        if (evt.getKeyCode() == KeyEvent.VK_8) {
            if (this.user.getPrivilegio().getId_privilegio() == 1) {
                Clientes a = new Clientes(user);
                a.setVisible(true);
                a.setResizable(false);
                a.setLocationRelativeTo(null);

            } else {
                JOptionPane.showMessageDialog(null, "SU CUENTA DE USUARIO NO TIENE PERMISOS PARA ACCEDER A ESTE MENU");
            }
        }

        if (evt.getKeyCode() == KeyEvent.VK_9) {
            if (this.user.getPrivilegio().getId_privilegio() == 1) {
                Articulos a = new Articulos(1);
                a.setVisible(true);
                a.setResizable(false);
                a.setLocationRelativeTo(null);

            } else {
                JOptionPane.showMessageDialog(null, "SU CUENTA DE USUARIO NO TIENE PERMISOS PARA ACCEDER A ESTE MENU");
            }
        }

        /*if (evt.getKeyCode() == KeyEvent.VK_9) {

            float acumulado = 0;

            for (int i = 0; i < this.tablaInicio.getRowCount(); i++) {
                float suma = Float.parseFloat(this.tablaInicio.getValueAt(i, 3).toString());
                acumulado = acumulado + suma;
            }

            //JOptionPane.showMessageDialog(null, "TOTAL FACTURADO HASTA EL MOMENTO: $ " + acumulado);
            if (this.user.getPrivilegio().getId_privilegio() == 1) {
                DatosRelevantes dr = new DatosRelevantes(acumulado);
                dr.setVisible(true);
                dr.setLocationRelativeTo(null);
                dr.setResizable(false);

            } else {
                JOptionPane.showMessageDialog(null, "SU CUENTA DE USUARIO NO TIENE PERMISOS PARA ACCEDER A ESTE MENU");
            }
        }*/
        if (evt.getKeyCode() == KeyEvent.VK_F5) {
            mt.traerMesasOcupadas(modeloMesasOcupadas);
            mt.traerVentasSinCerrar(modelo);
            mt.traerDeliverys(modeloDelivery);
        }

        this.tablaMesasOcupadas.clearSelection();
        this.tablaInicio.clearSelection();
        this.tablaDelivery.clearSelection();
    }//GEN-LAST:event_tablaInicioKeyReleased

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        // TODO add your handling code here:

        int o = JOptionPane.showConfirmDialog(null, "Está seguro que desea cerrar la sesion?");

        if (o == 0) {
            Window[] windows = Window.getWindows();

            for (Window window : windows) {
                window.dispose();
            }

            Login l = new Login();
            l.setVisible(true);
            l.setLocationRelativeTo(null);
            //this.dispose();
        } else {
            Inicio i = new Inicio(user);
            i.setVisible(true);
            i.setLocationRelativeTo(null);
        }


    }//GEN-LAST:event_formWindowClosed

    private void tablaMesasOcupadasKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaMesasOcupadasKeyReleased
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_1) {

            nuevaVtaRapida nvr = new nuevaVtaRapida(this, user, datosBarriles, this.checkHappyHour.isSelected(), modelo);
            nvr.setVisible(true);
            nvr.setLocationRelativeTo(null);
            nvr.setResizable(false);

        }
        if (evt.getKeyCode() == KeyEvent.VK_2) {

            NuevaMesa nm = new NuevaMesa(user, datosBarriles, this.checkHappyHour.isSelected(), modelo, modeloMesasOcupadas, modeloDelivery, boleta, 1, this);
            nm.setLocationRelativeTo(null);
            nm.setVisible(true);
            nm.setResizable(false);

        }

        if (evt.getKeyCode() == KeyEvent.VK_3) {
            NuevaMesa nm = new NuevaMesa(user, datosBarriles, this.checkHappyHour.isSelected(), modelo, modeloMesasOcupadas, modeloDelivery, boleta, 4, this);
            nm.setLocationRelativeTo(null);
            nm.setVisible(true);
            nm.setResizable(false);
        }

        if (evt.getKeyCode() == KeyEvent.VK_6) {

            if (this.user.getPrivilegio().getId_privilegio() == 1) {
                CuentasCorrientes cc = new CuentasCorrientes(1, null, user);
                cc.setLocationRelativeTo(null);
                cc.setResizable(false);
                cc.setVisible(true);
            } else {
                JOptionPane.showMessageDialog(null, "SU CUENTA DE USUARIO NO TIENE PERMISOS PARA ACCEDER A ESTE MENU");
            }

        }

        if (evt.getKeyCode() == KeyEvent.VK_0) {

            if (jPanelReportes.isVisible()) {
                jPanelReportes.setVisible(false);
            } else {
                jPanelReportes.setVisible(true);
            }

        }

        if (evt.getKeyCode() == KeyEvent.VK_4) {
            if (this.user.getPrivilegio().getId_privilegio() == 1) {
                Gestion_Costos gc = new Gestion_Costos();
                gc.setResizable(false);
                gc.setVisible(true);
                gc.setLocationRelativeTo(null);
            } else {
                JOptionPane.showMessageDialog(null, "SU CUENTA DE USUARIO NO TIENE PERMISOS PARA ACCEDER A ESTE MENU");
            }

        }

        if (evt.getKeyCode() == KeyEvent.VK_5) {
            //Configuraciones con = new Configuraciones();
            //con.setVisible(true);
            //con.setResizable(false);
            //con.setLocationRelativeTo(null);
            if (this.user.getPrivilegio().getId_privilegio() == 1) {
                MenuConfiguraciones mconf = new MenuConfiguraciones(user, this);
                mconf.setVisible(true);
                mconf.setLocationRelativeTo(null);
                mconf.setResizable(false);
            } else {
                JOptionPane.showMessageDialog(null, "SU CUENTA DE USUARIO NO TIENE PERMISOS PARA ACCEDER A ESTE MENU");
            }
        }

        if (evt.getKeyCode() == KeyEvent.VK_7) {
            if (this.user.getPrivilegio().getId_privilegio() == 1) {
                //CierreCaja cc = new CierreCaja(user, this);
                CierreCajaResumido cc = new CierreCajaResumido(user, this);
                cc.setResizable(false);
                cc.setVisible(true);
                cc.setLocationRelativeTo(null);
            } else {
                JOptionPane.showMessageDialog(null, "SU CUENTA DE USUARIO NO TIENE PERMISOS PARA ACCEDER A ESTE MENU");
            }
        }

        if (evt.getKeyCode() == KeyEvent.VK_8) {
            if (this.user.getPrivilegio().getId_privilegio() == 1) {
                Clientes a = new Clientes(user);
                a.setVisible(true);
                a.setResizable(false);
                a.setLocationRelativeTo(null);

            } else {
                JOptionPane.showMessageDialog(null, "SU CUENTA DE USUARIO NO TIENE PERMISOS PARA ACCEDER A ESTE MENU");
            }
        }

        if (evt.getKeyCode() == KeyEvent.VK_9) {
            if (this.user.getPrivilegio().getId_privilegio() == 1) {
                Articulos a = new Articulos(1);
                a.setVisible(true);
                a.setResizable(false);
                a.setLocationRelativeTo(null);

            } else {
                JOptionPane.showMessageDialog(null, "SU CUENTA DE USUARIO NO TIENE PERMISOS PARA ACCEDER A ESTE MENU");
            }
        }

        /*if (evt.getKeyCode() == KeyEvent.VK_9) {

            float acumulado = 0;

            for (int i = 0; i < this.tablaInicio.getRowCount(); i++) {
                float suma = Float.parseFloat(this.tablaInicio.getValueAt(i, 3).toString());
                acumulado = acumulado + suma;
            }

            //JOptionPane.showMessageDialog(null, "TOTAL FACTURADO HASTA EL MOMENTO: $ " + acumulado);
            if (this.user.getPrivilegio().getId_privilegio() == 1) {
                DatosRelevantes dr = new DatosRelevantes(acumulado);
                dr.setVisible(true);
                dr.setLocationRelativeTo(null);
                dr.setResizable(false);

            } else {
                JOptionPane.showMessageDialog(null, "SU CUENTA DE USUARIO NO TIENE PERMISOS PARA ACCEDER A ESTE MENU");
            }
        }*/
        if (evt.getKeyCode() == KeyEvent.VK_F5) {
            mt.traerMesasOcupadas(modeloMesasOcupadas);
            mt.traerVentasSinCerrar(modelo);
            mt.traerDeliverys(modeloDelivery);
        }

        this.tablaMesasOcupadas.clearSelection();
        this.tablaInicio.clearSelection();
        this.tablaDelivery.clearSelection();


    }//GEN-LAST:event_tablaMesasOcupadasKeyReleased

    private void tablaMesasOcupadasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaMesasOcupadasKeyPressed
        // TODO add your handling code here:

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            int filaS = this.tablaMesasOcupadas.getSelectedRow();

            boleta = mb.traerBoletaID(Integer.parseInt(this.tablaMesasOcupadas.getValueAt(filaS, 0).toString()));
            //boleta.setId_boleta(Integer.parseInt(this.tablaMesasOcupadas.getValueAt(filaS, 0).toString()));
            mozo = mu.traerMozo(tablaMesasOcupadas.getValueAt(filaS, 2).toString());
            boleta.setVendedor(mozo);
            boleta.setTotal(Float.parseFloat(tablaMesasOcupadas.getValueAt(filaS, 3).toString()));

            NuevaMesa nm = new NuevaMesa(user, datosBarriles, this.checkHappyHour.isSelected(), modelo, modeloMesasOcupadas, modeloDelivery, boleta, 2, this);
            nm.setVisible(true);
            nm.setLocationRelativeTo(null);

            this.tablaMesasOcupadas.clearSelection();
            this.tablaInicio.clearSelection();

        } else if (evt.getKeyCode() == KeyEvent.VK_DELETE) {

        }
    }//GEN-LAST:event_tablaMesasOcupadasKeyPressed

    private void checkHappyHourMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_checkHappyHourMouseClicked
        // TODO add your handling code here:
        this.requestFocus();
        this.requestFocusInWindow();
    }//GEN-LAST:event_checkHappyHourMouseClicked

    private void tablaInicioKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaInicioKeyPressed
        // TODO add your handling code here:

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            int filaS = this.tablaInicio.getSelectedRow();

            boleta = mb.traerBoletaID(Integer.parseInt(this.tablaInicio.getValueAt(filaS, 0).toString()));
            System.out.println("FECHA BOLETA: " + boleta.getFecha_cierre() + " HORA: " + boleta.getHora_cierre());
            manejadorFechas mfe = new manejadorFechas();
            //System.out.println("FECHA CONVERTIDA: "+mfe.convertirFecha(boleta.getFecha_cierre())+" HORAS CONV: "+boleta.getHora_cierre().substring(0,5));
            System.out.println("FECHA CONVERTIDA: " + mfe.convertirFecha2(boleta.getFecha_cierre()) + " HORAS CONV: " + boleta.getHora_cierre().substring(0, 5));
            //boleta.setId_boleta(Integer.parseInt(this.tablaMesasOcupadas.getValueAt(filaS, 0).toString()));
            mozo = mu.traerMozo(tablaInicio.getValueAt(filaS, 2).toString());
            boleta.setVendedor(mozo);
            boleta.setTotal(Float.parseFloat(tablaInicio.getValueAt(filaS, 3).toString()));

            NuevaMesa nm = null;

            if (boleta.getMesa() == 0) {
                if (this.tablaInicio.getValueAt(filaS, 1).toString().equals("Delivery")) {
                    nm = new NuevaMesa(user, datosBarriles, this.checkHappyHour.isSelected(), modelo, modeloMesasOcupadas, modeloDelivery, boleta, 6, this);
                } else {
                    nm = new NuevaMesa(user, datosBarriles, this.checkHappyHour.isSelected(), modelo, modeloMesasOcupadas, modeloDelivery, boleta, 7, this);
                }

            } else {
                nm = new NuevaMesa(user, datosBarriles, this.checkHappyHour.isSelected(), modelo, modeloMesasOcupadas, modeloDelivery, boleta, 3, this);
            }

            nm.setVisible(true);
            nm.setLocationRelativeTo(null);

            this.tablaInicio.clearSelection();

        }
    }//GEN-LAST:event_tablaInicioKeyPressed

    private void tablaMesasOcupadasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaMesasOcupadasMouseClicked
        // TODO add your handling code here:
        if (evt.getClickCount() == 2 && !evt.isConsumed()) {
            int filaS = this.tablaMesasOcupadas.getSelectedRow();

            boleta = mb.traerBoletaID(Integer.parseInt(this.tablaMesasOcupadas.getValueAt(filaS, 0).toString()));
            //boleta.setId_boleta(Integer.parseInt(this.tablaMesasOcupadas.getValueAt(filaS, 0).toString()));
            mozo = mu.traerMozo(tablaMesasOcupadas.getValueAt(filaS, 2).toString());
            boleta.setVendedor(mozo);
            boleta.setTotal(Float.parseFloat(tablaMesasOcupadas.getValueAt(filaS, 3).toString()));

            NuevaMesa nm = new NuevaMesa(user, datosBarriles, this.checkHappyHour.isSelected(), modelo, modeloMesasOcupadas, modeloDelivery, boleta, 2, this);
            nm.setVisible(true);
            nm.setLocationRelativeTo(null);

            this.tablaMesasOcupadas.clearSelection();
            this.tablaInicio.clearSelection();
        }

    }//GEN-LAST:event_tablaMesasOcupadasMouseClicked

    private void tablaDeliveryMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaDeliveryMouseClicked
        // TODO add your handling code here:
        if (evt.getClickCount() == 2 && !evt.isConsumed()) {
            int filaS = this.tablaDelivery.getSelectedRow();

            boleta = mb.traerBoletaID(Integer.parseInt(this.tablaDelivery.getValueAt(filaS, 0).toString()));
            //boleta.setId_boleta(Integer.parseInt(this.tablaMesasOcupadas.getValueAt(filaS, 0).toString()));
            mozo = mu.traerMozo(tablaDelivery.getValueAt(filaS, 1).toString());
            boleta.setVendedor(mozo);
            boleta.setTotal(Float.parseFloat(tablaDelivery.getValueAt(filaS, 4).toString()));

            NuevaMesa nm = new NuevaMesa(user, datosBarriles, this.checkHappyHour.isSelected(), modelo, modeloMesasOcupadas, modeloDelivery, boleta, 5, this);
            nm.setVisible(true);
            nm.setLocationRelativeTo(null);

            this.tablaMesasOcupadas.clearSelection();
            this.tablaDelivery.clearSelection();
            this.tablaInicio.clearSelection();
        }
    }//GEN-LAST:event_tablaDeliveryMouseClicked

    private void tablaDeliveryKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaDeliveryKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            int filaS = this.tablaDelivery.getSelectedRow();

            boleta = mb.traerBoletaID(Integer.parseInt(this.tablaDelivery.getValueAt(filaS, 0).toString()));
            //boleta.setId_boleta(Integer.parseInt(this.tablaMesasOcupadas.getValueAt(filaS, 0).toString()));
            mozo = mu.traerMozo(tablaDelivery.getValueAt(filaS, 1).toString());
            boleta.setVendedor(mozo);
            boleta.setTotal(Float.parseFloat(tablaDelivery.getValueAt(filaS, 4).toString()));

            NuevaMesa nm = new NuevaMesa(user, datosBarriles, this.checkHappyHour.isSelected(), modelo, modeloMesasOcupadas, modeloDelivery, boleta, 5, this);
            nm.setVisible(true);
            nm.setLocationRelativeTo(null);

            this.tablaMesasOcupadas.clearSelection();
            this.tablaDelivery.clearSelection();
            this.tablaInicio.clearSelection();

        }

    }//GEN-LAST:event_tablaDeliveryKeyPressed

    private void tablaDeliveryKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaDeliveryKeyReleased
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            this.tablaMesasOcupadas.clearSelection();
            this.tablaInicio.clearSelection();
            this.tablaDelivery.clearSelection();
        }
    }//GEN-LAST:event_tablaDeliveryKeyReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Inicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Inicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Inicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Inicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Inicio(null).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonCambiarBarril;
    private javax.swing.JButton botonImprimirReportes;
    private javax.swing.JCheckBox checkHappyHour;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar;
    private javax.swing.JPanel jPanelBarriles;
    private javax.swing.JPanel jPanelMesasOcupadas;
    private javax.swing.JPanel jPanelReportes;
    private javax.swing.JPanel jPanelVtas;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable tablaDelivery;
    private javax.swing.JTable tablaInicio;
    private javax.swing.JTable tablaMesasOcupadas;
    // End of variables declaration//GEN-END:variables
}
