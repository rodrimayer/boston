/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.manejadorCliente;
import controlador.manejadorControl;
import controlador.manejadorCtaCte;
import controlador.manejadorFacturacion;
import controlador.manejadorFechas;
import controlador.manejadorTablas;
import controlador.manejadorUsuario;
import controlador.metodoBusquedaCliente;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.Boleta;
import modelo.Cliente;
import modelo.Cta_Cte;
import modelo.Usuario;

/**
 *
 * @author Rodri Mayer
 */
public class CuentasCorrientes extends javax.swing.JFrame {

    /**
     * Creates new form CuentasCorrientes
     */
    public class ModeloTablaBoletasCanceladas extends DefaultTableModel {

        public boolean isCellEditable(int row, int column) {
            return false;
        }
    }

    public class ModeloTablaBoletasAdeudadas extends DefaultTableModel {

        public boolean isCellEditable(int row, int column) {
            return false;
        }
    }

    public class ModeloTablaPagosRealizados extends DefaultTableModel {

        public boolean isCellEditable(int row, int column) {
            return false;
        }
    }

    ModeloTablaBoletasCanceladas mbc = new ModeloTablaBoletasCanceladas();
    ModeloTablaBoletasAdeudadas mba = new ModeloTablaBoletasAdeudadas();
    ModeloTablaPagosRealizados mpr = new ModeloTablaPagosRealizados();

    manejadorCliente mc = new manejadorCliente();
    manejadorTablas mt = new manejadorTablas();
    manejadorCtaCte mcc = new manejadorCtaCte();
    manejadorFacturacion mf = new manejadorFacturacion();
    manejadorUsuario mu = new manejadorUsuario();
    manejadorControl mcon = new manejadorControl();

    final metodoBusquedaCliente mbcl = new metodoBusquedaCliente(this, 2);

    Cliente c;
    Cta_Cte cc;
    Boleta b;
    Usuario mozo;
    Usuario user;
    ArrayList<Integer> boletasPagar = new ArrayList<Integer>();
    

    int t; //t = 1 -> general, t = 2 -> cliente en particular
    float montoParcial;

    public CuentasCorrientes(int tipo, Cta_Cte cta, Usuario user) {
        initComponents();        
        t = tipo;
        cc = cta;
        this.user = user;

        mt.modeloTablaBoletasCanceladas(tablaBoletasCanceladas, mbc);
        mt.modeloTablaPagosRealizados(tablaPagosRealizados, mpr);
        mt.modeloTablaBoletasAdeudadas(tablaBoletasAdeudadas, mba);
        this.botonPagarBoletas.setEnabled(false);

        comboBoxCliente.removeAllItems();
        if (t == 2) {
            System.out.println("ID CLIENTE: " + cc.getId_cliente());
            c = mc.traerCliente(cc.getId_cliente());

            this.setTitle("CUENTA CORRIENTE CLIENTE: " + c.getNombre_cliente() + " " + c.getApellido_cliente());

            comboBoxCliente.setEditable(false);
            comboBoxCliente.addItem(c.getNombre_cliente() + " " + c.getApellido_cliente());
            comboBoxCliente.setEnabled(false);
            txtDNI.setText(String.valueOf(c.getId_cliente()));
            txtDireccion.setText(c.getDireccion());
            txtTelefono.setText(c.getTelefono());
            txtMail.setText(c.getMail());
            cc.setTotal(mcc.obtenerDeudaCtaCte(cc.getId_cta_cte()));
            txtDeuda.setText("$" + String.valueOf(cc.getTotal()));
            mt.traerBoletasAdeudadas(mba, cc.getId_cta_cte());
            mt.traerBoletasCtaCtePagadas(mbc, cc.getId_cta_cte());
            mt.traerPagosCtaCte(mpr, cc.getId_cta_cte());
            if (cc.getHabilitada() == 1) {
                this.checkBoxHabilitada.setSelected(true);
            } else {
                this.checkBoxHabilitada.setSelected(false);
            }

            if (cc.getTotal() == 0) {
                this.botonCancelarDeuda.setEnabled(false);
                this.botonPagoParcial.setEnabled(false);
            } else {
                botonCancelarDeuda.setEnabled(true);
                botonPagoParcial.setEnabled(true);
            }
            //comboBoxCliente.getEditor().setItem(a.getDesc_articulo());

        } else if (t == 1) {
            this.setTitle("CUENTAS CORRIENTES CLIENTES");
            comboBoxCliente.setEditable(true);
            txtDNI.setText("");
            txtDireccion.setText("");
            txtTelefono.setText("");
            txtMail.setText("");
            txtDeuda.setText("");
        } else {
            System.out.println("ID CLIENTE: " + cc.getId_cliente());
            c = mc.traerCliente(cc.getId_cliente());

            this.setTitle("CUENTAS CORRIENTES");

            comboBoxCliente.setEditable(true);
            comboBoxCliente.addItem(c.getNombre_cliente() + " " + c.getApellido_cliente());
            //comboBoxCliente.setEnabled(false);
            comboBoxCliente.getEditor().setItem(c.getNombre_cliente() + " " + c.getApellido_cliente());
            txtDNI.setText(String.valueOf(c.getId_cliente()));
            txtDireccion.setText(c.getDireccion());
            txtTelefono.setText(c.getTelefono());
            txtMail.setText(c.getMail());
            cc.setTotal(mcc.obtenerDeudaCtaCte(cc.getId_cta_cte()));
            txtDeuda.setText("$" + String.valueOf(cc.getTotal()));
            mt.traerBoletasAdeudadas(mba, cc.getId_cta_cte());
            mt.traerBoletasCtaCtePagadas(mbc, cc.getId_cta_cte());
            mt.traerPagosCtaCte(mpr, cc.getId_cta_cte());
            if (cc.getHabilitada() == 1) {
                this.checkBoxHabilitada.setSelected(true);
            } else {
                this.checkBoxHabilitada.setSelected(false);
            }

            if (cc.getTotal() == 0) {
                this.botonCancelarDeuda.setEnabled(false);
                this.botonPagoParcial.setEnabled(false);
            } else {
                botonCancelarDeuda.setEnabled(true);
                botonPagoParcial.setEnabled(true);
            }
        }

        this.comboBoxCliente.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {

            public void keyReleased(KeyEvent evt) {

                String campo;
                String cadenaEscrita = comboBoxCliente.getEditor().getItem().toString();

                System.out.println("CADENA ESCRITA: " + cadenaEscrita);

                if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                    if (mbcl.comparar(cadenaEscrita, 2)) {// compara si el texto escrito se encuentra en la lista
                        // busca el texto escrito en la base de datos                      

                        c = mc.obtenerCliente(cadenaEscrita);

                        System.out.println("ENTRO 1");
                        System.out.println("ELEMENTO SELECCIONADO: " + comboBoxCliente.getEditor().getItem().toString());
                        String comparar = comboBoxCliente.getEditor().getItem().toString();

                        //btnCerrarCuenta.setVisible(true);
                    } else {// en caso contrario toma como default el elemento 0 o sea el primero de la lista y lo busca.
                        if (comboBoxCliente.getItemAt(0) != null) {

                            comboBoxCliente.setSelectedIndex(0);
                            cadenaEscrita = comboBoxCliente.getItemAt(0).toString();
                            c = mc.obtenerCliente(cadenaEscrita);
                            System.out.println("ENTRO 2");
                            System.out.println("ELEMENTO SELECCIONADO: " + comboBoxCliente.getEditor().getItem().toString());
                            String comparar = comboBoxCliente.getItemAt(0).toString();
                        }

                    }

                    txtDNI.setText(String.valueOf(c.getId_cliente()));
                    txtDireccion.setText(c.getDireccion());
                    txtTelefono.setText(c.getTelefono());
                    txtMail.setText(c.getMail());
                    cc = mcc.obtenerCtaCte(c.getId_cliente());
                    cc.setTotal(mcc.obtenerDeudaCtaCte(cc.getId_cta_cte()));
                    txtDeuda.setText("$" + String.valueOf(cc.getTotal()));
                    mt.traerBoletasAdeudadas(mba, cc.getId_cta_cte());
                    mt.traerBoletasCtaCtePagadas(mbc, cc.getId_cta_cte());
                    mt.traerPagosCtaCte(mpr, cc.getId_cta_cte());
                    if (cc.getHabilitada() == 1) {
                        checkBoxHabilitada.setSelected(true);
                    } else {
                        checkBoxHabilitada.setSelected(false);
                    }
                    if (cc.getTotal() == 0) {
                        botonCancelarDeuda.setEnabled(false);
                        botonPagoParcial.setEnabled(false);
                    } else {
                        botonCancelarDeuda.setEnabled(true);
                        botonPagoParcial.setEnabled(true);
                    }

                }

                if (evt.getKeyCode() >= 65 && evt.getKeyCode() <= 90 || evt.getKeyCode() >= 96 && evt.getKeyCode() <= 105 || evt.getKeyCode() == 8) {

                    comboBoxCliente.setModel(mbcl.getLista(cadenaEscrita, 2));

                    if (comboBoxCliente.getItemCount() > 0) {
                        comboBoxCliente.getEditor().setItem(cadenaEscrita);
                        comboBoxCliente.showPopup();

                    } else {
                        comboBoxCliente.addItem(cadenaEscrita);
                    }
                    int cant = mba.getRowCount();

                    for (int i = 0; i < cant; i++) {
                        mba.removeRow(0);
                    }

                    int cant2 = mpr.getRowCount();

                    for (int i = 0; i < cant2; i++) {
                        mpr.removeRow(0);
                    }

                    int cant3 = mbc.getRowCount();

                    for (int i = 0; i < cant3; i++) {
                        mbc.removeRow(0);
                    }

                    txtDNI.setText("");
                    txtDireccion.setText("");
                    txtTelefono.setText("");
                    txtMail.setText("");
                    txtDeuda.setText("");
                    checkBoxHabilitada.setSelected(false);
                    botonCancelarDeuda.setEnabled(false);
                    botonPagoParcial.setEnabled(false);

                }

            }
        });

    }

    public JComboBox getcomboBoxCliente() {
        return comboBoxCliente;
    }

    public void setcomboBoxCliente(JComboBox comboBoxCliente) {
        this.comboBoxCliente = comboBoxCliente;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        comboBoxCliente = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtDNI = new javax.swing.JLabel();
        txtDireccion = new javax.swing.JLabel();
        txtTelefono = new javax.swing.JLabel();
        txtMail = new javax.swing.JLabel();
        checkBoxHabilitada = new javax.swing.JCheckBox();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaBoletasAdeudadas = new javax.swing.JTable();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        tablaPagosRealizados = new javax.swing.JTable();
        jPanel10 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        txtDeuda = new javax.swing.JLabel();
        botonCancelarDeuda = new javax.swing.JButton();
        botonPagarBoletas = new javax.swing.JButton();
        botonPagoParcial = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        tablaBoletasCanceladas = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos Cliente"));

        jLabel1.setText("Cliente: ");

        comboBoxCliente.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel2.setText("DNI: ");

        jLabel3.setText("Direccion:");

        jLabel4.setText("Telefono:");

        jLabel5.setText("Mail:");

        txtDNI.setText("jLabel6");

        txtDireccion.setText("jLabel6");

        txtTelefono.setText("jLabel6");

        txtMail.setText("jLabel6");

        checkBoxHabilitada.setText("Habilitada");
        checkBoxHabilitada.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkBoxHabilitadaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 57, Short.MAX_VALUE)
                                .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(jLabel3))
                        .addGap(30, 30, 30)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtDireccion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(txtMail, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 55, Short.MAX_VALUE))
                            .addComponent(txtTelefono, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 57, Short.MAX_VALUE))
                        .addGap(30, 30, 30)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtDNI, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(comboBoxCliente, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(12, 12, 12)
                                .addComponent(checkBoxHabilitada))))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(comboBoxCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(checkBoxHabilitada))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtDNI))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtDireccion))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtTelefono))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtMail))
                .addGap(19, 19, 19))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {txtDNI, txtDireccion, txtMail, txtTelefono});

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Boletas Adeudadas"));

        tablaBoletasAdeudadas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablaBoletasAdeudadas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaBoletasAdeudadasMouseClicked(evt);
            }
        });
        tablaBoletasAdeudadas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablaBoletasAdeudadasKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tablaBoletasAdeudadas);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder("Pagos Realizados"));

        tablaPagosRealizados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane5.setViewportView(tablaPagosRealizados);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 290, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel10.setBorder(javax.swing.BorderFactory.createTitledBorder("Pago de Deudas"));

        jLabel6.setText("Total Deuda:");

        txtDeuda.setText("jLabel7");

        botonCancelarDeuda.setText("CANCELAR DEUDA");
        botonCancelarDeuda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonCancelarDeudaActionPerformed(evt);
            }
        });

        botonPagarBoletas.setText("PAGAR BOLETAS SELECCIONADAS");
        botonPagarBoletas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonPagarBoletasActionPerformed(evt);
            }
        });

        botonPagoParcial.setText("PAGO PARCIAL");
        botonPagoParcial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonPagoParcialActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(botonCancelarDeuda, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtDeuda, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(botonPagarBoletas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(botonPagoParcial, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtDeuda))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(botonPagarBoletas, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(botonPagoParcial, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(botonCancelarDeuda, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder("Boletas Canceladas"));

        tablaBoletasCanceladas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablaBoletasCanceladas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaBoletasCanceladasMouseClicked(evt);
            }
        });
        tablaBoletasCanceladas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablaBoletasCanceladasKeyPressed(evt);
            }
        });
        jScrollPane6.setViewportView(tablaBoletasCanceladas);

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 166, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        // TODO add your handling code here:
        //Clientes c = new Clientes();
        //c.setLocationRelativeTo(null);
        //c.setVisible(true);
    }//GEN-LAST:event_formWindowClosed

    private void tablaBoletasAdeudadasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaBoletasAdeudadasKeyPressed
        // TODO add your handling code here:
        //String mozo;

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            int filaS = this.tablaBoletasAdeudadas.getSelectedRow();

            b = mf.traerBoletaID(Integer.parseInt(this.tablaBoletasAdeudadas.getValueAt(filaS, 1).toString()));
            System.out.println("FECHA BOLETA: " + b.getFecha_cierre() + " HORA: " + b.getHora_cierre());

            NuevaMesa nm = new NuevaMesa(user, null, false, null, null,null, b, 3, null);
            nm.setVisible(true);
            nm.setLocationRelativeTo(null);

            this.tablaBoletasAdeudadas.clearSelection();
        }
    }//GEN-LAST:event_tablaBoletasAdeudadasKeyPressed

    private void tablaBoletasAdeudadasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaBoletasAdeudadasMouseClicked
        // TODO add your handling code here:
        if (evt.getClickCount() == 2 && !evt.isConsumed()) {
            evt.consume();
            int filaS = this.tablaBoletasAdeudadas.getSelectedRow();

            b = mf.traerBoletaID(Integer.parseInt(this.tablaBoletasAdeudadas.getValueAt(filaS, 1).toString()));
            System.out.println("FECHA BOLETA: " + b.getFecha_cierre() + " HORA: " + b.getHora_cierre());

            NuevaMesa nm = new NuevaMesa(user, null, false, null, null,null, b, 3, null);
            nm.setVisible(true);
            nm.setLocationRelativeTo(null);

            this.tablaBoletasAdeudadas.clearSelection();

        } else {
            int cantFilas = tablaBoletasAdeudadas.getSelectedRowCount();

            if (cantFilas > 0) {
                this.botonPagarBoletas.setEnabled(true);
            } else {
                this.botonPagarBoletas.setEnabled(false);

            }
        }
    }//GEN-LAST:event_tablaBoletasAdeudadasMouseClicked

    private void checkBoxHabilitadaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkBoxHabilitadaActionPerformed
        // TODO add your handling code here:
        System.out.println("HIZO CLICK");
        if (!this.checkBoxHabilitada.isSelected()) {
            int opcion = JOptionPane.showConfirmDialog(null, "ESTA SEGURO QUE DESEA DESHABILITAR LA CUENTA CORRIENTE DE " + c.getNombre_cliente() + " " + c.getApellido_cliente() + "?");
            if (opcion == 0) {
                mcc.bajaCtaCte(cc.getId_cta_cte());
                this.checkBoxHabilitada.setSelected(false);
                JOptionPane.showMessageDialog(null, "CUENTA CORRIENTE DESHABILITADA CON EXITO");
            }
        } else {
            int opcion = JOptionPane.showConfirmDialog(null, "ESTA SEGURO QUE DESEA HABILITAR LA CUENTA CORRIENTE DE " + c.getNombre_cliente() + " " + c.getApellido_cliente() + "?");
            if (opcion == 0) {
                mcc.altaCtaCte(cc.getId_cta_cte());
                this.checkBoxHabilitada.setSelected(true);
                JOptionPane.showMessageDialog(null, "CUENTA CORRIENTE HABILITADA CON EXITO");
            }
        }
    }//GEN-LAST:event_checkBoxHabilitadaActionPerformed

    private void botonCancelarDeudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonCancelarDeudaActionPerformed
        // TODO add your handling code here:
        System.out.println("DEUDA A CANCELAR: " + cc.getTotal());
        //b = mf.traerBoletaID(Integer.parseInt(this.tablaBoletasAdeudadas.getValueAt(filaS, 1).toString()));
        AgregarPago ap = new AgregarPago(user, c, b, null, null, null, null, null, null, null, this, 5, null);
        ap.setLocationRelativeTo(null);
        ap.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_botonCancelarDeudaActionPerformed

    private void tablaBoletasCanceladasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaBoletasCanceladasMouseClicked
        // TODO add your handling code here:
        if (evt.getClickCount() == 2 && !evt.isConsumed()) {
            evt.consume();
            int filaS = this.tablaBoletasCanceladas.getSelectedRow();

            b = mf.traerBoletaID(Integer.parseInt(this.tablaBoletasCanceladas.getValueAt(filaS, 1).toString()));
            System.out.println("FECHA BOLETA: " + b.getFecha_cierre() + " HORA: " + b.getHora_cierre());

            NuevaMesa nm = new NuevaMesa(user, null, false, null, null,null, b, 3, null);
            nm.setVisible(true);
            nm.setLocationRelativeTo(null);

            this.tablaBoletasCanceladas.clearSelection();

        }
    }//GEN-LAST:event_tablaBoletasCanceladasMouseClicked

    private void tablaBoletasCanceladasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaBoletasCanceladasKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            int filaS = this.tablaBoletasCanceladas.getSelectedRow();

            b = mf.traerBoletaID(Integer.parseInt(this.tablaBoletasCanceladas.getValueAt(filaS, 1).toString()));
            System.out.println("FECHA BOLETA: " + b.getFecha_cierre() + " HORA: " + b.getHora_cierre());

            NuevaMesa nm = new NuevaMesa(user, null, false, null, null,null, b, 3, null);
            nm.setVisible(true);
            nm.setLocationRelativeTo(null);

            this.tablaBoletasCanceladas.clearSelection();
        }
    }//GEN-LAST:event_tablaBoletasCanceladasKeyPressed

    private void botonPagoParcialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonPagoParcialActionPerformed
        // TODO add your handling code here:
        boolean ban = true;

        while (ban) {
            String mon = JOptionPane.showInputDialog(null, "INGRESE EL MONTO QUE QUIERE ABONAR\nMONTO MAXIMO: $" + this.cc.getTotal());

            if (!mcon.isNumeric(mon)) {
                JOptionPane.showMessageDialog(null, "INGRESE UN NUMERO VALIDO");
            } else {
                System.out.println("DEUDA A CANCELAR: " + cc.getTotal());
                montoParcial = Float.parseFloat(mon);

                if (montoParcial > cc.getTotal() || montoParcial <= 0) {
                    JOptionPane.showMessageDialog(null, "EL IMPORTE A ABONAR NO PUEDE SER MAYOR\n"
                            + "AL SALDO DE LA CUENTA CORRIENTE NI TAMPOCO NEGATIVO");
                } else {

                    AgregarPago ap = new AgregarPago(user, c, b, null, null, null, null, null, null, null, this, 6, null);
                    ap.setLocationRelativeTo(null);
                    ap.setVisible(true);

                    this.dispose();

                    ban = false;

                    /*
                    System.out.println("MONTO A PAGAR: " + montoParcial);
                    while (montoParcial > 0) {
                        boolean band = true;
                        for (int i = 0; i < this.tablaBoletasAdeudadas.getRowCount() && ban; i++) {

                            if (montoParcial > Float.parseFloat(this.tablaBoletasAdeudadas.getValueAt(i, 3).toString())) {
                                montoPagar = Float.parseFloat(this.tablaBoletasAdeudadas.getValueAt(i, 3).toString());
                            } else {
                                montoPagar = montoParcial;
                                band = false;
                            }

                            mcc.pagoBoletasParcial(Integer.parseInt(this.tablaBoletasAdeudadas.getValueAt(i, 1).toString()), montoPagar);
                            montoParcial = montoParcial - montoPagar;
                        }

                    }

                    
                     */
                }

            }
        }


    }//GEN-LAST:event_botonPagoParcialActionPerformed

    private void botonPagarBoletasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonPagarBoletasActionPerformed
        // TODO add your handling code here:

        int cantS = this.tablaBoletasAdeudadas.getRowCount();
        this.montoParcial = 0;
        boletasPagar.clear();

        for (int i = 0; i < cantS; i++) {
            if (tablaBoletasAdeudadas.isRowSelected(i)) {
                montoParcial = montoParcial + Float.parseFloat(tablaBoletasAdeudadas.getValueAt(i, 3).toString());
                boletasPagar.add(Integer.parseInt(tablaBoletasAdeudadas.getValueAt(i, 1).toString()));
            }

        }
        
        System.out.println(boletasPagar);

        AgregarPago ap = new AgregarPago(user, c, b, null, null, null, null, null, null, null, this, 7, null);
        ap.setLocationRelativeTo(null);
        ap.setVisible(true);

        this.dispose();

        System.out.println("TOTAL A CANCELAR: " + montoParcial);
    }//GEN-LAST:event_botonPagarBoletasActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CuentasCorrientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CuentasCorrientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CuentasCorrientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CuentasCorrientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CuentasCorrientes(0, null, null).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonCancelarDeuda;
    private javax.swing.JButton botonPagarBoletas;
    private javax.swing.JButton botonPagoParcial;
    private javax.swing.JCheckBox checkBoxHabilitada;
    private javax.swing.JComboBox<String> comboBoxCliente;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    public javax.swing.JTable tablaBoletasAdeudadas;
    private javax.swing.JTable tablaBoletasCanceladas;
    private javax.swing.JTable tablaPagosRealizados;
    private javax.swing.JLabel txtDNI;
    private javax.swing.JLabel txtDeuda;
    private javax.swing.JLabel txtDireccion;
    private javax.swing.JLabel txtMail;
    private javax.swing.JLabel txtTelefono;
    // End of variables declaration//GEN-END:variables
}
