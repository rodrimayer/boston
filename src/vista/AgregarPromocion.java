/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.manejadorArticulos;
import controlador.manejadorTablas;
import controlador.metodoBusquedaArticuloPromocion;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.Articulo;
import modelo.Equiv_Prom;

/**
 *
 * @author rmmayer
 */
public class AgregarPromocion extends javax.swing.JFrame {

    /**
     * Creates new form AgregarPromocion
     */
    Articulo a;
    Articulo art;
    int t;
    Equiv_Prom ep = new Equiv_Prom();
    float totalTicket;
    
    final metodoBusquedaArticuloPromocion mb = new metodoBusquedaArticuloPromocion(this);

    public class ModeloPromocion extends DefaultTableModel {

        public boolean isCellEditable(int row, int column) {
            return false;
        }
    }
    ModeloPromocion modeloPromocion = new ModeloPromocion();
    manejadorArticulos ma = new manejadorArticulos();
    manejadorTablas mt = new manejadorTablas();

    public AgregarPromocion(Articulo articulo, int tipo) {
        initComponents();
        art = articulo;
        t = tipo;

        this.labelNombrePromo.setText(art.getDesc_articulo());
        this.labelPrecioPromo.setText("$" + art.getPrecio_venta());
        this.labelPrecioReal.setText("$0.0");
        this.botonEliminar.setEnabled(false);
        this.botonGuardar.setEnabled(false);

        mt.modeloTablaPromocion(tablaArticulosPromo, modeloPromocion);

        if (t == 2) {//t = 2 cuando se esta editando la promocion
            mt.traerArticulosPromocion(modeloPromocion, art.getCodigo());
            botonGuardar.setEnabled(true);
            
            float total = 0;
            
            for(int i = 0; i< tablaArticulosPromo.getRowCount(); i++){
                total = total + Float.parseFloat(tablaArticulosPromo.getValueAt(i, 4).toString());                
            }
            
            this.labelPrecioReal.setText("$"+total);
        }
        
        this.comboBoxArticulo.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {

            public void keyReleased(KeyEvent evt) {

                String campo;
                String cadenaEscrita = comboBoxArticulo.getEditor().getItem().toString();

                System.out.println(cadenaEscrita);

                if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                    if (mb.comparar(cadenaEscrita)) {// compara si el texto escrito se encuentra en la lista
                        // busca el texto escrito en la base de datos                      

                        a = ma.traerArticuloPorString(cadenaEscrita);

                        System.out.println("ENTRO 1");
                        System.out.println("ELEMENTO SELECCIONADO: " + comboBoxArticulo.getEditor().getItem().toString());
                        a.stringArticulo(a);
                        String comparar = comboBoxArticulo.getEditor().getItem().toString();

                        botonGuardar.setVisible(true);

                    } else {// en caso contrario toma como default el elemento 0 o sea el primero de la lista y lo busca.
                        if (comboBoxArticulo.getItemAt(0) != null) {

                            comboBoxArticulo.setSelectedIndex(0);
                            cadenaEscrita = comboBoxArticulo.getItemAt(0).toString();
                            System.out.println("ENTRO 2");
                            System.out.println("ELEMENTO SELECCIONADO: " + comboBoxArticulo.getEditor().getItem().toString());
                            a = ma.traerArticuloPorString(cadenaEscrita);
                            a.stringArticulo(a);
                            String comparar = comboBoxArticulo.getItemAt(0).toString();
                        }

                    }
                    txtCantidad.requestFocus();
                    botonGuardar.setEnabled(true);

                } 

                if (evt.getKeyCode() >= 65 && evt.getKeyCode() <= 90 || evt.getKeyCode() >= 96 && evt.getKeyCode() <= 105 || evt.getKeyCode() == 8) {

                    comboBoxArticulo.setModel(mb.getLista(cadenaEscrita));
                    txtCodigo.setText("");

                    if (comboBoxArticulo.getEditor().getItem().toString().equals("")) {
                        //System.out.println("Esta vacio");
                        txtCodigo.setEnabled(true);
                    } else {
                        //txtCodigo.setEnabled(false);
                        //System.out.println("Esta escribiendo");
                    }

                    if (comboBoxArticulo.getItemCount() > 0) {
                        comboBoxArticulo.getEditor().setItem(cadenaEscrita);
                        comboBoxArticulo.showPopup();

                    } else {
                        comboBoxArticulo.addItem(cadenaEscrita);
                    }
                }

            }
        });
    }
    
    public JComboBox getcomboBoxArticulo() {
        return comboBoxArticulo;
    }

    public void setcomboBoxArticulo(JComboBox comboBoxArticulo) {
        this.comboBoxArticulo = comboBoxArticulo;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        labelNombrePromo = new javax.swing.JLabel();
        labelPrecioPromo = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        labelPrecioReal = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaArticulosPromo = new javax.swing.JTable();
        botonEliminar = new javax.swing.JButton();
        botonGuardar = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        txtCodigo = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        comboBoxArticulo = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();
        txtCantidad = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Promocion"));

        jLabel1.setText("Nombre Promo: ");

        jLabel2.setText("Precio Promo: ");

        labelNombrePromo.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        labelNombrePromo.setText("jLabel3");

        labelPrecioPromo.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        labelPrecioPromo.setText("jLabel3");

        jLabel8.setText("Precio Real:");

        labelPrecioReal.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        labelPrecioReal.setText("jLabel9");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(labelPrecioPromo, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(labelNombrePromo, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel8)
                        .addGap(31, 31, 31)
                        .addComponent(labelPrecioReal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(labelNombrePromo)
                    .addComponent(jLabel8)
                    .addComponent(labelPrecioReal))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 25, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(labelPrecioPromo))
                .addContainerGap())
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Articulos"));

        tablaArticulosPromo.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablaArticulosPromo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaArticulosPromoMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablaArticulosPromo);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(25, Short.MAX_VALUE))
        );

        botonEliminar.setText("ELIMINAR ITEM");
        botonEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEliminarActionPerformed(evt);
            }
        });

        botonGuardar.setText("GUARDAR PROMOCION");
        botonGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonGuardarActionPerformed(evt);
            }
        });

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Insertar"));

        jLabel5.setText("Codigo");

        txtCodigo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCodigoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCodigoKeyReleased(evt);
            }
        });

        jLabel6.setText("Articulo:");

        comboBoxArticulo.setEditable(true);

        jLabel7.setText("Cantidad:");

        txtCantidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCantidadKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCantidadKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addComponent(jLabel6)
                .addGap(18, 18, 18)
                .addComponent(comboBoxArticulo, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32)
                .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, 76, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7)
                    .addComponent(comboBoxArticulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(botonEliminar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(botonGuardar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(botonEliminar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(botonGuardar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtCodigoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCodigoKeyPressed
        // TODO add your handling code here:

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            String codigo = this.txtCodigo.getText();
            a = ma.traerArticuloPorCodigo(codigo);
            if (a.getDesc_articulo() == null) {
                JOptionPane.showMessageDialog(null, "NO EXISTE EL ARTICULO SELECCIONADO");
            } else {
                this.comboBoxArticulo.removeAllItems();
                this.comboBoxArticulo.addItem(a.getDesc_articulo());
                this.comboBoxArticulo.setEnabled(false);
                txtCantidad.requestFocus();
            }

        }

    }//GEN-LAST:event_txtCodigoKeyPressed

    private void txtCodigoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCodigoKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCodigoKeyReleased

    private void txtCantidadKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCantidadKeyPressed
        // TODO add your handling code here:

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            Object[] ar = new Object[5];
            float total = a.getPrecio_venta() * Integer.parseInt(txtCantidad.getText());
            boolean coincidencia = false;
            int filaMismoArticulo = 0;
            float acumulado = 0;

            for (int i = 0; i < this.tablaArticulosPromo.getRowCount(); i++) {
                if (this.comboBoxArticulo.getEditor().getItem().toString().equals(this.tablaArticulosPromo.getValueAt(i, 1))) {
                    coincidencia = true;
                    filaMismoArticulo = i;
                }
            }

            if (coincidencia) {

                System.out.println("EXISTE UN VALOR IGUAL");
                int cantidadPrevia = Integer.parseInt(txtCantidad.getText());
                int cantidadAgregada = Integer.parseInt(this.tablaArticulosPromo.getValueAt(filaMismoArticulo, 2).toString());
                int cantidadNueva = cantidadPrevia + cantidadAgregada;
                this.tablaArticulosPromo.setValueAt(cantidadNueva, filaMismoArticulo, 2);
                this.tablaArticulosPromo.setValueAt(cantidadNueva * a.getPrecio_venta(), filaMismoArticulo, 4);
                total = Float.parseFloat(this.tablaArticulosPromo.getValueAt(filaMismoArticulo, 4).toString());

            } else {
                System.out.println("entro por el false");
                ar[0] = a.getCodigo();
                ar[1] = a.getDesc_articulo();
                ar[2] = txtCantidad.getText();
                ar[3] = a.getPrecio_venta();
                ar[4] = total;
                modeloPromocion.addRow(ar);

            }

            for (int i = 0; i < this.tablaArticulosPromo.getRowCount(); i++) {
                acumulado = acumulado + Float.parseFloat(this.tablaArticulosPromo.getValueAt(i, 4).toString());
            }

            txtCantidad.setText("");
            totalTicket = acumulado;
            labelPrecioReal.setText("$" + acumulado);
            this.comboBoxArticulo.removeAllItems();
            this.comboBoxArticulo.setEnabled(true);
            this.txtCodigo.setText("");
            txtCodigo.requestFocus();
            this.botonGuardar.setEnabled(true);

        }
    }//GEN-LAST:event_txtCantidadKeyPressed

    private void txtCantidadKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCantidadKeyReleased
        // TODO add your handling code here:

    }//GEN-LAST:event_txtCantidadKeyReleased

    private void botonEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEliminarActionPerformed
        // TODO add your handling code here:
        int filaS = this.tablaArticulosPromo.getSelectedRow();
        float acumulado = 0;

        for (int i = 0; i < this.tablaArticulosPromo.getRowCount(); i++) {
            acumulado = acumulado + Float.parseFloat(this.tablaArticulosPromo.getValueAt(i, 4).toString());
        }

        acumulado = acumulado - Float.parseFloat(this.tablaArticulosPromo.getValueAt(filaS, 4).toString());

        modeloPromocion.removeRow(filaS);

        labelPrecioReal.setText("$" + acumulado);
        totalTicket = acumulado;

        if (tablaArticulosPromo.getRowCount() == 0) {
            this.botonGuardar.setEnabled(false);
            this.botonEliminar.setEnabled(false);
        }
    }//GEN-LAST:event_botonEliminarActionPerformed

    private void tablaArticulosPromoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaArticulosPromoMouseClicked
        // TODO add your handling code here:
        if (this.tablaArticulosPromo.getSelectedRowCount() != 0) {
            this.botonEliminar.setEnabled(true);
        } else {
            this.botonEliminar.setEnabled(false);
        }
    }//GEN-LAST:event_tablaArticulosPromoMouseClicked

    private void botonGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonGuardarActionPerformed
        // TODO add your handling code here:
        int o = JOptionPane.showConfirmDialog(null, "Está seguro que desea guardar esta promocion? ");
        int n = 0;
        ArrayList<Equiv_Prom> eps = new ArrayList<>();

        System.out.println(a.stringArticulo(art));

        if (o == 0) {

            for (int i = 0; i < this.tablaArticulosPromo.getRowCount(); i++) {
                Equiv_Prom epr = new Equiv_Prom();
                String promocion = art.getCodigo();
                String codigo = this.tablaArticulosPromo.getValueAt(i, 0).toString();
                String desc = this.tablaArticulosPromo.getValueAt(i, 1).toString();
                int cantidad = Integer.parseInt(this.tablaArticulosPromo.getValueAt(i, 2).toString());

                epr.setCodigo_promocion(promocion);
                epr.setCodigo_articulo(codigo);
                epr.setCantidad_articulo(cantidad);

                eps.add(epr);

            }

            if (t == 1) {
                n = ma.insertarArticulo(art);
                ma.insertarPromocionArray(eps);
                if (n > 0) {
                    JOptionPane.showMessageDialog(null, "PROMOCION GUARDADA CON EXITO");
                    this.dispose();
                    Articulos aR = new Articulos(1);
                    aR.setLocationRelativeTo(null);
                    aR.setVisible(true);
                } else {
                    JOptionPane.showMessageDialog(null, "ERROR AL GUARDAR LA PROMOCION");
                }
            } else {
                ma.eliminarPromocion(art.getCodigo());
                ma.insertarPromocionArray(eps);
                JOptionPane.showMessageDialog(null, "PROMOCION GUARDADA CON EXITO");
                this.dispose();
                Articulos aR = new Articulos(1);
                aR.setLocationRelativeTo(null);
                aR.setVisible(true);
            }

        }

    }//GEN-LAST:event_botonGuardarActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        // TODO add your handling code here:
        /*Articulos a = new Articulos(1);
        a.setLocationRelativeTo(null);
        a.setVisible(true);*/
        
        //AgregarArticulo aa = new AgregarArticulo();
    }//GEN-LAST:event_formWindowClosed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AgregarPromocion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AgregarPromocion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AgregarPromocion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AgregarPromocion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AgregarPromocion(null, 0).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonEliminar;
    private javax.swing.JButton botonGuardar;
    private javax.swing.JComboBox<String> comboBoxArticulo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labelNombrePromo;
    private javax.swing.JLabel labelPrecioPromo;
    private javax.swing.JLabel labelPrecioReal;
    private javax.swing.JTable tablaArticulosPromo;
    private javax.swing.JTextField txtCantidad;
    private javax.swing.JTextField txtCodigo;
    // End of variables declaration//GEN-END:variables
}
