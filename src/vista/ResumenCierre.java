/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.manejadorFacturacion;
import controlador.manejadorReportes;
import controlador.manejadorTablas;
import controlador.manejadorUsuario;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import javax.swing.table.DefaultTableModel;
import modelo.Boleta;
import modelo.Usuario;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;

/**
 *
 * @author rmmayer
 */
public class ResumenCierre extends javax.swing.JFrame {

    /**
     * Creates new form ResumenCierre
     */
    public class ModeloTablaBoletas extends DefaultTableModel {

        public boolean isCellEditable(int row, int column) {
            return false;
        }
    }

    public class ModeloTablaResumen extends DefaultTableModel {

        public boolean isCellEditable(int row, int column) {
            return false;
        }
    }

    ModeloTablaBoletas modeloTablaBoletas = new ModeloTablaBoletas();
    ModeloTablaResumen modeloTablaResumen = new ModeloTablaResumen();

    manejadorTablas mt = new manejadorTablas();
    manejadorFacturacion mfac = new manejadorFacturacion();
    manejadorReportes mr = new manejadorReportes();
    manejadorUsuario mu = new manejadorUsuario();

    Dimension dimTotal;
    Dimension dimFrame;
    Dimension dimPanelBoletas;
    Dimension dimPanelResumen;
    Dimension dimPanelVentas;
    Dimension dimPanelCostos;
    DefaultPieDataset datosVentas = new DefaultPieDataset();
    JFreeChart tortaConsumos;
    ChartPanel chartPanelTorta;
    DefaultPieDataset datosCostos = new DefaultPieDataset();
    JFreeChart tortaCostos;
    ChartPanel chartPanelCostos;

    Boleta boleta = new Boleta();
    Usuario mozo = new Usuario();

    public ResumenCierre(int id, int id_desde, int id_hasta) {
        initComponents();

        if (id >= 0) {
            this.setTitle("RESUMEN CIERRE DE CAJA Nº: " + id);
            mt.traerBoletasCierre(modeloTablaBoletas, tablaBoletasContenido, id);
            mt.traerResumenCierre(modeloTablaResumen, tablaResumenCierre, id);
        }else{
            this.setTitle("RESUMEN CIERRES DESDE CIERRE: "+id_desde+" HASTA: "+((id_hasta)-1));
            mt.traerBoletasCierreLimites(modeloTablaBoletas, tablaBoletasContenido, id_desde, id_hasta);
            mt.traerResumenCierreLimites(modeloTablaResumen, tablaResumenCierre, id_desde, id_hasta);
        }

        dimTotal = Toolkit.getDefaultToolkit().getScreenSize();
        dimFrame = new Dimension((int) (dimTotal.width * 0.75), (int) (dimTotal.height * 0.7));
        //System.out.println("ancho: "+(dimTotal.width)+" alto: "+(dimTotal.height));
        this.setSize(dimFrame);
        this.setPreferredSize(dimFrame);
        this.setMaximumSize(dimFrame);
        this.setMinimumSize(dimFrame);
        //this.setLayout(new BorderLayout());
        
        //this.setExtendedState(6);

        //dimPanelBoletas = new Dimension((dimFrame.width / 2) - 50, (dimFrame.height / 2) - 50);
        //dimPanelBoletas = new Dimension((int) ((dimFrame.width / 2) - (dimFrame.width * 0.015)), (int) ((dimFrame.height / 2) - (dimFrame.height * 0.025)));
        dimPanelBoletas = new Dimension((int) ((dimFrame.width * 0.48)) , (int) ((dimFrame.height * 0.44)));
        panelBoletas.setSize(dimPanelBoletas);
        panelBoletas.setPreferredSize(dimPanelBoletas);
        panelBoletas.setMinimumSize(dimPanelBoletas);
        panelBoletas.setMaximumSize(dimPanelBoletas);
        panelBoletas.setLayout(new GridLayout());

        //dimPanelResumen = new Dimension((dimFrame.width / 2 - 30), (int) ((dimFrame.height / 2) - (dimFrame.height * 0.097)));
        //dimPanelResumen = new Dimension((dimFrame.width / 2) - 50 , (dimFrame.height / 2) - 50);
        //dimPanelResumen = new Dimension((int) ((dimFrame.width / 2) - (dimFrame.width * 0.015)), (int) ((dimFrame.height / 2) - (dimFrame.height * 0.025)));
        dimPanelResumen = new Dimension((int) ((dimFrame.width * 0.48)) , (int) ((dimFrame.height * 0.44)));
        panelResumen.setSize(dimPanelResumen);
        panelResumen.setPreferredSize(dimPanelResumen);
        panelResumen.setMinimumSize(dimPanelResumen);
        panelResumen.setMaximumSize(dimPanelResumen);
        panelResumen.setLayout(new GridLayout());

        //dimPanelVentas = new Dimension((dimFrame.width / 2 - 30), (int) ((dimFrame.height / 2) - (dimFrame.height * 0.097)));
        //dimPanelVentas = new Dimension((int) ((dimFrame.width / 2) - (dimFrame.width * 0.010)), (int) ((dimFrame.height / 2) - (dimFrame.height * 0.025)));
        dimPanelVentas = new Dimension((int) ((dimFrame.width * 0.48)) , (int) ((dimFrame.height * 0.44)));
        panelVentas.setSize(dimPanelVentas);
        panelVentas.setPreferredSize(dimPanelVentas);
        panelVentas.setMinimumSize(dimPanelVentas);
        panelVentas.setMaximumSize(dimPanelVentas);
        panelVentas.setLayout(new GridLayout());

        //dimPanelCostos = new Dimension((dimFrame.width / 2 - 30), (int) ((dimFrame.height / 2) - (dimFrame.height * 0.097)));
        //dimPanelCostos = new Dimension((int) ((dimFrame.width / 2) - (dimFrame.width * 0.010)), (int) ((dimFrame.height / 2) - (dimFrame.height * 0.025)));
        dimPanelCostos = new Dimension((int) ((dimFrame.width * 0.48)) , (int) ((dimFrame.height * 0.44)));
        panelCostos.setSize(dimPanelCostos);
        panelCostos.setPreferredSize(dimPanelCostos);
        panelCostos.setMinimumSize(dimPanelCostos);
        panelCostos.setMaximumSize(dimPanelCostos);
        panelCostos.setLayout(new GridLayout());

        tortaConsumos = ChartFactory.createPieChart3D("", datosVentas, false, false, false);
        chartPanelTorta = new ChartPanel(tortaConsumos);
        panelVentas.add(chartPanelTorta);
        if (id >= 0) {
            mr.traerVentasPorCierre(datosVentas, id);
        }else{
            mr.traerVentasPorCierreLimites(datosVentas, id_desde, id_hasta);
        }

        tortaCostos = ChartFactory.createPieChart3D("", datosCostos, false, false, false);
        chartPanelCostos = new ChartPanel(tortaCostos);
        panelCostos.add(chartPanelCostos);
        if (id >= 0) {
            mr.traerCostosPorCierre(datosCostos, id);
        }else{
            mr.traerCostosPorCierreLimites(datosCostos, id_desde, id_hasta);
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelBoletas = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaBoletasContenido = new javax.swing.JTable();
        panelVentas = new javax.swing.JPanel();
        panelResumen = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablaResumenCierre = new javax.swing.JTable();
        panelCostos = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("RESUMEN CIERRE CAJA");

        panelBoletas.setBorder(javax.swing.BorderFactory.createTitledBorder("BOLETAS"));

        tablaBoletasContenido.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablaBoletasContenido.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaBoletasContenidoMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablaBoletasContenido);

        javax.swing.GroupLayout panelBoletasLayout = new javax.swing.GroupLayout(panelBoletas);
        panelBoletas.setLayout(panelBoletasLayout);
        panelBoletasLayout.setHorizontalGroup(
            panelBoletasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelBoletasLayout.createSequentialGroup()
                .addContainerGap(20, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        panelBoletasLayout.setVerticalGroup(
            panelBoletasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBoletasLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(27, Short.MAX_VALUE))
        );

        panelVentas.setBorder(javax.swing.BorderFactory.createTitledBorder("VENTAS"));

        javax.swing.GroupLayout panelVentasLayout = new javax.swing.GroupLayout(panelVentas);
        panelVentas.setLayout(panelVentasLayout);
        panelVentasLayout.setHorizontalGroup(
            panelVentasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 482, Short.MAX_VALUE)
        );
        panelVentasLayout.setVerticalGroup(
            panelVentasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        panelResumen.setBorder(javax.swing.BorderFactory.createTitledBorder("RESUMEN"));

        tablaResumenCierre.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tablaResumenCierre);

        javax.swing.GroupLayout panelResumenLayout = new javax.swing.GroupLayout(panelResumen);
        panelResumen.setLayout(panelResumenLayout);
        panelResumenLayout.setHorizontalGroup(
            panelResumenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelResumenLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(20, Short.MAX_VALUE))
        );
        panelResumenLayout.setVerticalGroup(
            panelResumenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelResumenLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 220, Short.MAX_VALUE)
                .addContainerGap())
        );

        panelCostos.setBorder(javax.swing.BorderFactory.createTitledBorder("COSTOS"));

        javax.swing.GroupLayout panelCostosLayout = new javax.swing.GroupLayout(panelCostos);
        panelCostos.setLayout(panelCostosLayout);
        panelCostosLayout.setHorizontalGroup(
            panelCostosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 482, Short.MAX_VALUE)
        );
        panelCostosLayout.setVerticalGroup(
            panelCostosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 246, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(panelResumen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelBoletas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelVentas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelCostos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {panelBoletas, panelCostos, panelResumen, panelVentas});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(panelBoletas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelVentas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelResumen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelCostos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tablaBoletasContenidoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaBoletasContenidoMouseClicked
        // TODO add your handling code here:
        if (evt.getClickCount() == 2 && !evt.isConsumed()) {
            evt.consume();
            int filaS = this.tablaBoletasContenido.getSelectedRow();

            boleta = mfac.traerBoletaID(Integer.parseInt(this.tablaBoletasContenido.getValueAt(filaS, 0).toString()));
            //boleta.setId_boleta(Integer.parseInt(this.tablaMesasOcupadas.getValueAt(filaS, 0).toString()));
            mozo = mu.traerMozo(tablaBoletasContenido.getValueAt(filaS, 2).toString());
            boleta.setVendedor(mozo);
            boleta.setTotal(Float.parseFloat(tablaBoletasContenido.getValueAt(filaS, 3).toString()));

            NuevaMesa nm = new NuevaMesa(mozo, null, false, null, null,null, boleta, 3, null);
            nm.setVisible(true);
            nm.setLocationRelativeTo(null);

            this.tablaBoletasContenido.clearSelection();
        }
    }//GEN-LAST:event_tablaBoletasContenidoMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ResumenCierre.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ResumenCierre.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ResumenCierre.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ResumenCierre.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ResumenCierre(0, 0, 0).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPanel panelBoletas;
    private javax.swing.JPanel panelCostos;
    private javax.swing.JPanel panelResumen;
    private javax.swing.JPanel panelVentas;
    private javax.swing.JTable tablaBoletasContenido;
    private javax.swing.JTable tablaResumenCierre;
    // End of variables declaration//GEN-END:variables
}
