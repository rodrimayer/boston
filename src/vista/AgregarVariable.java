/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.manejadorConfiguraciones;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import modelo.Configuracion;

/**
 *
 * @author Rodri Mayer
 */
public class AgregarVariable extends javax.swing.JFrame {

    /**
     * Creates new form agregarVariable
     */
    manejadorConfiguraciones mc = new manejadorConfiguraciones();
    ArrayList<Configuracion> configuraciones = new ArrayList<>();

    boolean start = true;

    public AgregarVariable() {
        initComponents();
        configuraciones = mc.traerVariables(comboBoxVariables);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelVariables = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        comboBoxVariables = new javax.swing.JComboBox<>();
        botonEditarVariable = new javax.swing.JButton();
        panelAgregarVariable = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        txtNuevaVariable = new javax.swing.JTextField();
        botonAgregarVariable = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        txtValorVariable = new javax.swing.JTextField();
        botonNuevaVariable = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("AGREGAR / EDITAR VARIABLES");

        panelVariables.setBorder(javax.swing.BorderFactory.createTitledBorder("Modificacion de Variables"));

        jLabel1.setText("Variable: ");

        comboBoxVariables.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxVariablesActionPerformed(evt);
            }
        });

        botonEditarVariable.setText("EDITAR");
        botonEditarVariable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEditarVariableActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelVariablesLayout = new javax.swing.GroupLayout(panelVariables);
        panelVariables.setLayout(panelVariablesLayout);
        panelVariablesLayout.setHorizontalGroup(
            panelVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelVariablesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(botonEditarVariable, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(panelVariablesLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(comboBoxVariables, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(20, Short.MAX_VALUE))
        );
        panelVariablesLayout.setVerticalGroup(
            panelVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelVariablesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(comboBoxVariables, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(botonEditarVariable)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelAgregarVariable.setBorder(javax.swing.BorderFactory.createTitledBorder("Agregar Variable"));

        jLabel8.setText("Variable:");

        txtNuevaVariable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNuevaVariableActionPerformed(evt);
            }
        });
        txtNuevaVariable.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtNuevaVariableKeyReleased(evt);
            }
        });

        botonAgregarVariable.setText("AGREGAR");
        botonAgregarVariable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAgregarVariableActionPerformed(evt);
            }
        });

        jLabel2.setText("Valor:");

        javax.swing.GroupLayout panelAgregarVariableLayout = new javax.swing.GroupLayout(panelAgregarVariable);
        panelAgregarVariable.setLayout(panelAgregarVariableLayout);
        panelAgregarVariableLayout.setHorizontalGroup(
            panelAgregarVariableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelAgregarVariableLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelAgregarVariableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(botonAgregarVariable, javax.swing.GroupLayout.DEFAULT_SIZE, 211, Short.MAX_VALUE)
                    .addGroup(panelAgregarVariableLayout.createSequentialGroup()
                        .addGroup(panelAgregarVariableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addComponent(jLabel2))
                        .addGap(18, 18, 18)
                        .addGroup(panelAgregarVariableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtNuevaVariable, javax.swing.GroupLayout.DEFAULT_SIZE, 151, Short.MAX_VALUE)
                            .addComponent(txtValorVariable))))
                .addGap(45, 45, 45))
        );
        panelAgregarVariableLayout.setVerticalGroup(
            panelAgregarVariableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelAgregarVariableLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(panelAgregarVariableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txtNuevaVariable, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panelAgregarVariableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtValorVariable, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(botonAgregarVariable)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        botonNuevaVariable.setText("NUEVA VARIABLE");
        botonNuevaVariable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonNuevaVariableActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(panelVariables, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelAgregarVariable, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(botonNuevaVariable, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 10, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelVariables, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(panelAgregarVariable, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                .addComponent(botonNuevaVariable)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void comboBoxVariablesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxVariablesActionPerformed
        // TODO add your handling code here:
        //mc.traerVariables(comboBoxVariables);
        //String tipo = this.comboBoxTipoArticulo.getSelectedItem().toString();
        System.out.println(start);
        int index = this.comboBoxVariables.getSelectedIndex();
        System.out.println("INDEX SELECCIONADO: " + index);

        if (!start && index != -1) {
            int id_variable = configuraciones.get(index).getId_configuracion();
            this.txtNuevaVariable.setText(configuraciones.get(index).getDesc_configuracion());
            this.txtNuevaVariable.setEditable(false);
            this.txtValorVariable.setText(String.valueOf(configuraciones.get(index).getValor_configuracion()));
            System.out.println("entro al if");
            //this.botonEditarVariable.setEnabled(true);
            this.botonAgregarVariable.setText("ACTUALIZAR");

        } else {
            System.out.println("no entro al if");
            start = false;
            this.botonAgregarVariable.setText("AGREGAR");
        }
    }//GEN-LAST:event_comboBoxVariablesActionPerformed

    private void botonEditarVariableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEditarVariableActionPerformed
        // TODO add your handling code here:
        System.out.println(start);
        int index = this.comboBoxVariables.getSelectedIndex();
        System.out.println("INDEX SELECCIONADO: " + index);

        if (index != -1) {
            int id_variable = configuraciones.get(index).getId_configuracion();
            this.txtValorVariable.setText(String.valueOf(configuraciones.get(index).getValor_configuracion()));
            System.out.println("entro al if");
            //this.botonEditarVariable.setEnabled(true);
            this.botonAgregarVariable.setText("ACTUALIZAR");

        } else {
            System.out.println("no entro al if");
            start = false;
            this.botonAgregarVariable.setText("AGREGAR");
        }

    }//GEN-LAST:event_botonEditarVariableActionPerformed

    private void txtNuevaVariableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNuevaVariableActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNuevaVariableActionPerformed

    private void txtNuevaVariableKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNuevaVariableKeyReleased
        // TODO add your handling code here:
        if (this.txtNuevaVariable.getText().equals("")) {
            this.botonAgregarVariable.setEnabled(false);
        } else {
            this.botonAgregarVariable.setEnabled(true);
        }
    }//GEN-LAST:event_txtNuevaVariableKeyReleased

    private void botonAgregarVariableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAgregarVariableActionPerformed
        // TODO add your handling code here:
        Configuracion c = new Configuracion();

        c.setDesc_configuracion(this.txtNuevaVariable.getText());
        c.setValor_configuracion(Float.parseFloat(this.txtValorVariable.getText()));

        if (this.comboBoxVariables.getSelectedIndex() == -1) {
            int n = mc.insertarVariable(c, this.comboBoxVariables);
            
            if (n > 0) {
                JOptionPane.showMessageDialog(null, "VARIABLE AGREGADA CON EXITO");
            } else {
                JOptionPane.showMessageDialog(null, "ERROR AL AÑADIR VARIABLE");
            }

        } else {
            int index = this.comboBoxVariables.getSelectedIndex();
            c.setId_configuracion(configuraciones.get(index).getId_configuracion());
            mc.modificarVariable(c);
            JOptionPane.showMessageDialog(null, "VARIABLE MODIFICADA CON EXITO");
        }

        //this.txtNuevaVariable.setText("");
        this.dispose();
        AgregarVariable co = new AgregarVariable();
        co.setVisible(true);
        co.setResizable(false);
        co.setLocationRelativeTo(null);

    }//GEN-LAST:event_botonAgregarVariableActionPerformed

    private void botonNuevaVariableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonNuevaVariableActionPerformed
        // TODO add your handling code here:
        this.comboBoxVariables.setSelectedIndex(-1);
        this.txtNuevaVariable.setText("");
        this.txtValorVariable.setText("");
        this.botonAgregarVariable.setText("AGREGAR");
        this.txtNuevaVariable.setEditable(true);

    }//GEN-LAST:event_botonNuevaVariableActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AgregarVariable.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AgregarVariable.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AgregarVariable.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AgregarVariable.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AgregarVariable().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonAgregarVariable;
    private javax.swing.JButton botonEditarVariable;
    private javax.swing.JButton botonNuevaVariable;
    private javax.swing.JComboBox<String> comboBoxVariables;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel panelAgregarVariable;
    private javax.swing.JPanel panelVariables;
    private javax.swing.JTextField txtNuevaVariable;
    private javax.swing.JTextField txtValorVariable;
    // End of variables declaration//GEN-END:variables
}
