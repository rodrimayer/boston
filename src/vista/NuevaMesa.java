/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.manejadorArticulos;
import controlador.manejadorCliente;
import controlador.manejadorConfiguraciones;
import controlador.manejadorControl;
import controlador.manejadorFacturacion;
import controlador.manejadorFechas;
import controlador.manejadorImpresion;
import controlador.manejadorReportes;
import controlador.manejadorTablas;
import controlador.manejadorUsuario;
import controlador.metodoBuscarArtNuevaMesa;
import controlador.metodoBusquedaArticulo;
import controlador.metodoBusquedaCliente;
import controlador.metodoBusquedaMozo;
import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.Articulo;
import modelo.Barril;
import modelo.Boleta;
import modelo.Cliente;
import modelo.Detalle_boleta;
import modelo.Facturacion_boleta;
import modelo.Usuario;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author rmmayer
 */
public class NuevaMesa extends javax.swing.JFrame {

    /**
     * Creates new form NuevaMesa
     */
    public class ModeloNuevoMesa extends DefaultTableModel {

        public boolean isCellEditable(int row, int column) {
            return false;
        }
    }

    ModeloNuevoMesa modelo = new ModeloNuevoMesa();

    Articulo a = new Articulo();
    Boleta boleta = new Boleta();
    Cliente cli = new Cliente();
    Detalle_boleta db = new Detalle_boleta();
    Usuario user = new Usuario();
    ArrayList<Detalle_boleta> arrayDetalles = new ArrayList<Detalle_boleta>();
    ArrayList<Detalle_boleta> arrayBoletaMesa = new ArrayList<Detalle_boleta>();
    ArrayList<Barril> pintasReemplazo = new ArrayList<Barril>();
    ArrayList<Object[]> promocion = new ArrayList<Object[]>();
    DefaultCategoryDataset dB;
    float totalTicket = 0;
    float totalPaga = 0;
    AgregarPago ap;
    boolean b = false;
    boolean hh;
    int t;
    DefaultTableModel dtm;
    DefaultTableModel dmmo;
    DefaultTableModel dmtd;
    //DefaultTableModel dtd;
    Inicio inicio;
    NuevaMesa nm;

    manejadorArticulos ma = new manejadorArticulos();
    manejadorCliente mcli = new manejadorCliente();
    manejadorTablas mt = new manejadorTablas();
    manejadorFacturacion mf = new manejadorFacturacion();
    manejadorFechas mfechas = new manejadorFechas();
    manejadorUsuario mu = new manejadorUsuario();
    manejadorConfiguraciones mcon = new manejadorConfiguraciones();
    manejadorImpresion mi = new manejadorImpresion();
    manejadorControl mco = new manejadorControl();

    final metodoBuscarArtNuevaMesa mb = new metodoBuscarArtNuevaMesa(this);
    final metodoBusquedaCliente mc = new metodoBusquedaCliente(this, 1);
    final metodoBusquedaMozo mbm = new metodoBusquedaMozo(this);

    public NuevaMesa(Usuario u, DefaultCategoryDataset datosBarriles, boolean happyhour, Inicio.ModeloTablaInicio mti, Inicio.ModeloTablaMesasOcupadas mmo, Inicio.ModeloTablaDelivery mtd, Boleta b, int tipo, Inicio ini) {
        initComponents();
        this.txtMesa.requestFocus();
        //inicio = i;
        inicio = ini;
        user = u;
        dB = datosBarriles;
        hh = happyhour;
        dtm = mti;
        dmmo = mmo;
        dmtd = mtd;
        boleta = b;
        t = tipo;
        nm = this;

        this.setResizable(false);

        this.checkComanda.setEnabled(false);
        this.checkPedido.setEnabled(false);
        txtDomicilio.setEditable(false);

        ButtonGroup pagoDelivery = new ButtonGroup();
        pagoDelivery.add(radioOtros);
        pagoDelivery.add(radioEfectivo);

        if (hh) {
            System.out.println("ESTA ACTIVADO EL HAPPY HOUR");
        } else {
            System.out.println("ESTA DESACTIVADO EL HAPPY HOUR");
        }

        // TIPO 1 MESA NUEVA
        // TIPO 2 MESA YA CARGADA EN ESPERA DE CIERRE
        // TIPO 3 DETALLES MESA YA CERRADA
        // TIPO 4 DELIVERY NUEVO
        // TIPO 5 DELIVERY YA CARGADO EN ESPERA DE CIERRE
        // TIPO 6 DETALLES DELIVERY YA CERRADO
        // TIPO 7 DETALLES VTA RAPIDA YA CERRADA
        if (tipo == 2 || tipo == 5) { //TIPO 2 ES PARA CUANDO LO ABRO DESDE LA PANTALLA DE INICIO UNA BOLETA YA CARGADA y 5 ES UN DELIVERY YA CARGADO
            mt.modeloNuevaMesa(tablaVentaRapida, modelo);

            //this.setTitle("MESA: " + boleta.getMesa() + " | " + mfechas.convertirFecha(mfechas.obtenerFechaActual()) + " " + mfechas.obtenerHoraActual());
            if (tipo == 2) {
                this.setTitle("MESA: " + boleta.getMesa() + " - ATENDIDA POR: " + boleta.getVendedor().getNombre_usuario() + " " + boleta.getVendedor().getApellido_usuario());
                txtMesa.setText(String.valueOf(boleta.getMesa()));
                txtPaga.setText(String.valueOf(totalTicket));
                radioEfectivo.setEnabled(false);
                radioOtros.setEnabled(false);
                txtVuelto.setEditable(false);
                comboBoxMozo.setEditable(false);

            } else {
                totalPaga = boleta.getPago_delivery();
                this.setTitle("BOLETA: " + boleta.getId_boleta() + " - DELIVERY: " + boleta.getVendedor().getNombre_usuario() + " " + boleta.getVendedor().getApellido_usuario());
                //labelNro.setText("Domicilio:");
                txtDomicilio.setEditable(true);
                txtDomicilio.setText(boleta.getDireccion_delivery());
                txtMesa.setEnabled(false);
                txtMesa.setText("-");
                //txtPaga.setText("$" + String.valueOf(totalPaga));
                txtPaga.setText(String.valueOf(totalPaga));
                txtPaga.setEditable(true);
                txtVuelto.setEditable(false);
                this.btnCerrarCuenta.setText("COMPLETAR DELIVERY");
                this.checkComanda.setEnabled(true);
                this.checkPedido.setEnabled(true);
                this.checkPedido.setSelected(true);
                radioEfectivo.setSelected(true);
                comboBoxMozo.setEditable(true);
            }

            comboBoxArticulo.setEnabled(true);

            comboBoxMozo.removeAllItems();
            System.out.println("VENDEDOR A AGREGAR: " + boleta.getVendedor().getNombre_usuario() + " " + boleta.getVendedor().getApellido_usuario());
            comboBoxMozo.addItem(boleta.getVendedor().getNombre_usuario() + " " + boleta.getVendedor().getApellido_usuario());
            //comboBoxMozo.setEnabled(false);

            //comboBoxMozo.setEditable(false);
            comboBoxCliente.removeAllItems();
            System.out.println("VENDEDOR A AGREGAR: " + boleta.getCliente().getNombre_cliente() + " " + boleta.getCliente().getApellido_cliente());
            comboBoxCliente.addItem(boleta.getCliente().getNombre_cliente() + " " + boleta.getCliente().getApellido_cliente());
            //comboBoxCliente.setEnabled(false);
            comboBoxCliente.setEditable(false);

            txtCodigo.requestFocus();
            //txtTotal.setText("$" + String.valueOf(boleta.getTotal()));
            totalTicket = boleta.getTotal();
            System.out.println("TOTAL DEL TICKET :" + totalTicket);

            arrayBoletaMesa = mf.traerDetalle(boleta.getId_boleta());
            System.out.println("TAMAÑO DE LA BOLETA: " + arrayBoletaMesa.size());

            float acumulado = 0;

            for (int i = 0; i < arrayBoletaMesa.size(); i++) {
                System.out.println("CODIGO A INSERTAR: " + arrayBoletaMesa.get(i).getCodigo());
                Object[] ar = new Object[6];
                ar[0] = arrayBoletaMesa.get(i).getId_detalleboleta();
                ar[1] = arrayBoletaMesa.get(i).getCodigo();
                ar[2] = ma.traerArticuloPorCodigo(arrayBoletaMesa.get(i).getCodigo()).getDesc_articulo();
                ar[3] = arrayBoletaMesa.get(i).getCantidad();
                ar[4] = arrayBoletaMesa.get(i).getPrecio();
                ar[5] = Integer.parseInt(ar[3].toString()) * Float.parseFloat(ar[4].toString());
                acumulado = acumulado + Float.parseFloat(ar[5].toString());
                modelo.addRow(ar);
            }

            txtVuelto.setText("$" + String.valueOf(totalPaga - totalTicket));
            txtTotal.setText("$" + acumulado);

            txtFecha.setText(mfechas.convertirFecha(mfechas.convertirFechaInversa(b.getFecha_boleta())) + " " + b.getHora_apertura().substring(0, 5));
            txtFecha.setEditable(false);

            if (this.tablaVentaRapida.getRowCount() == 0) {
                this.btnCerrarCuenta.setEnabled(false);
                this.btnImprimirTicket.setEnabled(false);
            }

            //mt.cargarDetalles(arrayDetalles, modelo);
        } else if (tipo == 1) { /////////////////MESA NUEVA
            this.setTitle("NUEVA MESA | " + mfechas.convertirFecha(mfechas.obtenerFechaActual()) + " " + mfechas.obtenerHoraActual().substring(0, 5));

            txtFecha.setText(mfechas.convertirFecha(mfechas.obtenerFechaActual()) + " " + mfechas.obtenerHoraActual());
            txtFecha.setEditable(false);
            this.txtPaga.setEnabled(false);
            this.txtVuelto.setEnabled(false);

            tablaVentaRapida.removeAll();
            mt.modeloNuevaMesa(tablaVentaRapida, modelo);
            txtTotal.setText("$0");
            btnCerrarCuenta.setEnabled(false);
            btnGuardarCambios.setEnabled(false);
            btnImprimirTicket.setEnabled(false);
            txtMesa.requestFocus();
            //comboBoxCliente.requestFocus();
            radioEfectivo.setEnabled(false);
            radioOtros.setEnabled(false);

        } else if (tipo == 3 || tipo == 6 || tipo == 7) {//CUANDO SE QUIERE VER EL RESUMEN DE UNA CUENTA YA PAGADA Y REIMPRIMIR
            txtPaga.setEditable(false);
            txtVuelto.setEditable(false);
            radioEfectivo.setEnabled(false);
            radioOtros.setEnabled(false);
            mt.modeloNuevaMesa(tablaVentaRapida, modelo);
            this.setTitle(" RESUMEN BOLETA N°: " + boleta.getId_boleta());

            comboBoxArticulo.setEnabled(true);

            comboBoxMozo.removeAllItems();
            System.out.println("MOZO A AGREGAR: " + boleta.getVendedor().getNombre_usuario() + " " + boleta.getVendedor().getApellido_usuario());
            comboBoxMozo.addItem(boleta.getVendedor().getNombre_usuario() + " " + boleta.getVendedor().getApellido_usuario());
            //comboBoxMozo.setEnabled(false);
            comboBoxMozo.setEditable(false);

            comboBoxCliente.removeAllItems();
            System.out.println("CLIENTE A AGREGAR: " + boleta.getCliente().getNombre_cliente() + " " + boleta.getCliente().getApellido_cliente());
            comboBoxCliente.addItem(boleta.getCliente().getNombre_cliente() + " " + boleta.getCliente().getApellido_cliente());
            //comboBoxCliente.setEnabled(false);
            comboBoxCliente.setEditable(false);

            totalTicket = boleta.getTotal();
            System.out.println("TOTAL DEL TICKET :" + totalTicket);

            arrayBoletaMesa = mf.traerDetalle(boleta.getId_boleta());
            System.out.println("TAMAÑO DE LA BOLETA: " + arrayBoletaMesa.size());

            float acumulado = 0;

            for (int i = 0; i < arrayBoletaMesa.size(); i++) {
                System.out.println("CODIGO A INSERTAR: " + arrayBoletaMesa.get(i).getCodigo());
                Object[] ar = new Object[6];
                ar[0] = arrayBoletaMesa.get(i).getId_detalleboleta();
                ar[1] = arrayBoletaMesa.get(i).getCodigo();
                ar[2] = ma.traerArticuloPorCodigo(arrayBoletaMesa.get(i).getCodigo()).getDesc_articulo();
                ar[3] = arrayBoletaMesa.get(i).getCantidad();
                ar[4] = arrayBoletaMesa.get(i).getPrecio();
                ar[5] = Integer.parseInt(ar[3].toString()) * Float.parseFloat(ar[4].toString());
                acumulado = acumulado + Float.parseFloat(ar[5].toString());
                modelo.addRow(ar);
            }

            txtCantidad.setEditable(false);
            this.comboBoxArticulo.setEnabled(false);
            this.txtCodigo.setEditable(false);
            this.btnCerrarCuenta.setEnabled(false);
            this.tablaVentaRapida.setEnabled(false);
            this.txtMesa.setEditable(false);

            txtTotal.setText("$" + acumulado);

            System.out.println("FECHA APERTURA: " + b.getFecha_boleta() + " FECHA CIERRE: " + b.getFecha_cierre());
            txtFecha.setText(mfechas.convertirFecha(mfechas.convertirFechaInversa(b.getFecha_boleta())) + " " + b.getHora_apertura().substring(0, 5) + " | " + mfechas.convertirFecha(mfechas.convertirFechaInversa(b.getFecha_cierre())) + " " + b.getHora_cierre().substring(0, 5));
            txtFecha.setEditable(false);

            btnGuardarCambios.setEnabled(false);
            btnImprimirTicket.setVisible(true);
            if (tipo == 6) {
                this.btnCerrarCuenta.setEnabled(false);
                //labelNro.setText("Domicilio");
                txtDomicilio.setText(boleta.getDireccion_delivery());
                txtMesa.setText("-");
                this.txtPaga.setText("$ " + String.valueOf(boleta.getPago_delivery()));
                this.txtVuelto.setText("$ " + String.valueOf(boleta.getPago_delivery() - boleta.getTotal()));
            } else if (tipo == 3) {
                txtMesa.setText(String.valueOf(boleta.getMesa()));
            } else {
                txtMesa.setText("Vta Rapida");
            }

            btnCerrarCuenta.setText("DETALLES PAGO");
            btnCerrarCuenta.setEnabled(true);

        } else { /////////////////DELIVERY

            this.setTitle("NUEVO DELIVERY FECHA: " + mfechas.convertirFecha(mfechas.obtenerFechaActual()) + " " + mfechas.obtenerHoraActual());
            txtDomicilio.setEditable(true);
            txtFecha.setText(mfechas.convertirFecha(mfechas.obtenerFechaActual()) + " " + mfechas.obtenerHoraActual());
            txtFecha.setEditable(false);
            txtMesa.setText("-");
            txtMesa.setEnabled(false);
            //labelNro.setText("Domicilio");
            labelMozo.setText("Delivery");

            tablaVentaRapida.removeAll();
            mt.modeloNuevaMesa(tablaVentaRapida, modelo);
            txtTotal.setText("$0");
            btnCerrarCuenta.setVisible(false);
            btnGuardarCambios.setText("ALTA DELIVERY E IMPRESION COMANDA");
            btnGuardarCambios.setEnabled(false);
            btnImprimirTicket.setVisible(false);
            txtPaga.setText("$0");
            txtPaga.setEditable(false);
            txtVuelto.setText("$0");
            txtVuelto.setEditable(false);
            //txtMesa.requestFocus();
            comboBoxCliente.requestFocus();
            radioEfectivo.setSelected(true);

        }

        this.comboBoxArticulo.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {

            public void keyPressed(KeyEvent evt) {

                String campo;
                String cadenaEscrita = comboBoxArticulo.getEditor().getItem().toString();

                System.out.println(cadenaEscrita);

                if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                    if (mb.comparar(cadenaEscrita)) {// compara si el texto escrito se encuentra en la lista
                        // busca el texto escrito en la base de datos                      

                        a = ma.traerArticuloPorString(cadenaEscrita);

                        System.out.println("ENTRO 1");
                        System.out.println("ELEMENTO SELECCIONADO: " + comboBoxArticulo.getEditor().getItem().toString());
                        a.stringArticulo(a);
                        String comparar = comboBoxArticulo.getEditor().getItem().toString();
                        comboBoxArticulo.getEditor().setItem(a.getDesc_articulo());

                        if (a.getDesc_articulo() == null) {
                            JOptionPane.showMessageDialog(null, "NO EXISTE EL ARTICULO SELECCIONADO");
                        } else if (a.getId_tipoProd2() == 5) {
                            System.out.println("se selecciono cerveza artesanal");
                            if (ma.comprobarBarril(a.getDesc_articulo())) {
                                System.out.println("hay cerveza " + a.getDesc_articulo());
                                if (hh) {
                                    a.setPrecio_venta(ma.traerPrecioHH());
                                }

                                txtCantidad.setEditable(true);
                                txtCantidad.requestFocus();

                            } else {
                                txtCantidad.setEditable(false);
                                comboBoxArticulo.requestFocusInWindow();
                                JOptionPane.showMessageDialog(null, "NO HAY CERVEZA " + a.getDesc_articulo() + " CARGADA EN LA CHOPERA");

                            }
                        } else if (a.getId_tipoProd1() == 3) {
                            System.out.println("ES PROMOCION, SE REVISARA SI HAY CERVEZA");
                            ArrayList<Object[]> promocion = ma.traerArticulosPromocion(a.getCodigo());
                            boolean ban = true;

                            for (int i = 0; i < promocion.size(); i++) {
                                Articulo promo = new Articulo();
                                promo = ma.traerArticuloPorCodigo(promocion.get(i)[0].toString());
                                System.out.println("ARTICULO DE LA PROMO: " + promo.getDesc_articulo());

                                if (promo.getId_tipoProd2() == 5) {
                                    System.out.println("se selecciono cerveza artesanal");
                                    if (!ma.comprobarBarril(promo.getDesc_articulo())) {
                                        ban = false;
                                    }

                                }

                            }

                            if (ban) {
                                txtCantidad.setEditable(true);
                                txtCantidad.requestFocus();
                            } else {
                                JOptionPane.showMessageDialog(null, "LA CERVEZA QUE INTENTA CARGAR NO ESTA DISPONIBLE EN NINGUNO DE LOS BARRILES CARGADOS");
                                comboBoxArticulo.requestFocusInWindow();
                                txtCantidad.setEditable(false);
                            }

                        } else {
                            txtCantidad.setEditable(true);
                            txtCantidad.requestFocus();
                        }

                        //btnCerrarCuenta.setVisible(true);
                    } else {// en caso contrario toma como default el elemento 0 o sea el primero de la lista y lo busca.
                        if (comboBoxArticulo.getItemAt(0) != null) {

                            comboBoxArticulo.setSelectedIndex(0);
                            cadenaEscrita = comboBoxArticulo.getItemAt(0).toString();
                            System.out.println("ENTRO 2");
                            System.out.println("ELEMENTO SELECCIONADO: " + comboBoxArticulo.getEditor().getItem().toString());
                            a = ma.traerArticuloPorString(cadenaEscrita);
                            a.stringArticulo(a);
                            String comparar = comboBoxArticulo.getItemAt(0).toString();
                            comboBoxArticulo.getEditor().setItem(a.getDesc_articulo());

                            if (a.getDesc_articulo() == null) {
                                JOptionPane.showMessageDialog(null, "NO EXISTE EL ARTICULO SELECCIONADO");
                            } else if (a.getId_tipoProd2() == 5) {
                                System.out.println("se selecciono cerveza artesanal");
                                if (ma.comprobarBarril(a.getDesc_articulo())) {
                                    System.out.println("hay cerveza " + a.getDesc_articulo());
                                    if (hh) {
                                        a.setPrecio_venta(ma.traerPrecioHH());
                                    }

                                    txtCantidad.setEditable(true);
                                    txtCantidad.requestFocus();

                                } else {
                                    txtCantidad.setEditable(false);
                                    comboBoxArticulo.requestFocusInWindow();
                                    JOptionPane.showMessageDialog(null, "NO HAY CERVEZA " + a.getDesc_articulo() + " CARGADA EN LA CHOPERA");
                                    //comboBoxArticulo.getEditor().getEditorComponent().requestFocus();

                                }
                            } else if (a.getId_tipoProd1() == 3) {
                                System.out.println("ES PROMOCION, SE REVISARA SI HAY CERVEZA");
                                ArrayList<Object[]> promocion = ma.traerArticulosPromocion(a.getCodigo());
                                boolean ban = true;

                                for (int i = 0; i < promocion.size(); i++) {
                                    Articulo promo = new Articulo();
                                    promo = ma.traerArticuloPorCodigo(promocion.get(i)[0].toString());
                                    System.out.println("ARTICULO DE LA PROMO: " + promo.getDesc_articulo());

                                    if (promo.getId_tipoProd2() == 5) {
                                        System.out.println("se selecciono cerveza artesanal");
                                        if (!ma.comprobarBarril(promo.getDesc_articulo())) {
                                            ban = false;
                                        }

                                    }

                                }

                                if (ban) {
                                    txtCantidad.setEditable(true);
                                    txtCantidad.requestFocus();
                                } else {
                                    txtCantidad.setEditable(false);
                                    comboBoxArticulo.requestFocusInWindow();
                                    JOptionPane.showMessageDialog(null, "LA CERVEZA QUE INTENTA CARGAR NO ESTA DISPONIBLE EN NINGUNO DE LOS BARRILES CARGADOS");

                                }

                            } else {
                                txtCantidad.setEditable(true);
                                txtCantidad.requestFocus();
                            }
                        }

                    }

                    //btnCerrarCuenta.setEnabled(true);
                }

                if (evt.getKeyCode() >= 65 && evt.getKeyCode() <= 90 || evt.getKeyCode() >= 96 && evt.getKeyCode() <= 105 || evt.getKeyCode() == 8) {
                    System.out.println("entro por aquiiiiiiiiiii");
                    comboBoxArticulo.setModel(mb.getLista(cadenaEscrita));
                    txtCodigo.setText("");

                    if (comboBoxArticulo.getEditor().getItem().toString().equals("")) {
                        //System.out.println("Esta vacio");
                        txtCodigo.setEnabled(true);
                    } else {
                        //txtCodigo.setEnabled(false);
                        //System.out.println("Esta escribiendo");
                    }

                    if (comboBoxArticulo.getItemCount() > 0) {
                        comboBoxArticulo.getEditor().setItem(cadenaEscrita);
                        comboBoxArticulo.showPopup();

                    } else {
                        //comboBoxArticulo.addItem(cadenaEscrita);
                    }
                }

            }
        });

        this.comboBoxCliente.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {

            public void keyReleased(KeyEvent evt) {

                String campo;
                String cadenaEscrita = comboBoxCliente.getEditor().getItem().toString();

                System.out.println(cadenaEscrita);

                if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                    if (mc.comparar(cadenaEscrita, 1)) {// compara si el texto escrito se encuentra en la lista
                        // busca el texto escrito en la base de datos                      

                        cli = mcli.obtenerCliente(cadenaEscrita);

                        if (tipo == 4) {
                            txtDomicilio.setText(cli.getDireccion());
                        }

                        System.out.println("ENTRO 1");
                        System.out.println("ELEMENTO SELECCIONADO: " + comboBoxCliente.getEditor().getItem().toString());
                        String comparar = comboBoxCliente.getEditor().getItem().toString();

                        //btnCerrarCuenta.setVisible(true);
                    } else {// en caso contrario toma como default el elemento 0 o sea el primero de la lista y lo busca.
                        if (comboBoxCliente.getItemAt(0) != null) {

                            comboBoxCliente.setSelectedIndex(0);
                            cadenaEscrita = comboBoxCliente.getItemAt(0).toString();
                            cli = mcli.obtenerCliente(cadenaEscrita);

                            if (tipo == 4) {
                                txtDomicilio.setText(cli.getDireccion());
                            }

                            System.out.println("ENTRO 2");
                            System.out.println("ELEMENTO SELECCIONADO: " + comboBoxCliente.getEditor().getItem().toString());
                            String comparar = comboBoxCliente.getItemAt(0).toString();
                        }

                    }
                    comboBoxMozo.requestFocus();
                    //btnCerrarCuenta.setEnabled(true);

                }

                if (evt.getKeyCode() >= 65 && evt.getKeyCode() <= 90 || evt.getKeyCode() >= 96 && evt.getKeyCode() <= 105 || evt.getKeyCode() == 8) {

                    comboBoxCliente.setModel(mc.getLista(cadenaEscrita, 1));
                    txtCodigo.setText("");

                    if (comboBoxCliente.getEditor().getItem().toString().equals("")) {
                        //System.out.println("Esta vacio");
                        txtCodigo.setEnabled(true);
                    } else {
                        //txtCodigo.setEnabled(false);
                        //System.out.println("Esta escribiendo");
                    }

                    if (comboBoxCliente.getItemCount() > 0) {
                        comboBoxCliente.getEditor().setItem(cadenaEscrita);
                        comboBoxCliente.showPopup();

                    } else {
                        comboBoxCliente.addItem(cadenaEscrita);
                    }
                }

            }
        });

        this.comboBoxMozo.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {

            public void keyReleased(KeyEvent evt) {

                String campo;
                String cadenaEscrita = comboBoxMozo.getEditor().getItem().toString();

                System.out.println(cadenaEscrita);

                if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                    if (mbm.comparar(cadenaEscrita)) {// compara si el texto escrito se encuentra en la lista
                        // busca el texto escrito en la base de datos                      

                        //cli = mcli.obtenerCliente(cadenaEscrita);
                        user = mu.traerMozo(cadenaEscrita);

                        System.out.println("ENTRO 1");
                        System.out.println("ELEMENTO SELECCIONADO: " + comboBoxMozo.getEditor().getItem().toString());
                        String comparar = comboBoxMozo.getEditor().getItem().toString();

                        //btnCerrarCuenta.setVisible(true);
                    } else {// en caso contrario toma como default el elemento 0 o sea el primero de la lista y lo busca.
                        if (comboBoxMozo.getItemAt(0) != null) {

                            comboBoxMozo.setSelectedIndex(0);
                            cadenaEscrita = comboBoxMozo.getItemAt(0).toString();
                            //user = mu.traerMozo(cadenaEscrita);
                            user = mu.traerMozo(cadenaEscrita);
                            System.out.println("ENTRO 2");
                            System.out.println("ELEMENTO SELECCIONADO: " + comboBoxMozo.getEditor().getItem().toString());
                            String comparar = comboBoxMozo.getItemAt(0).toString();
                        }

                    }
                    //txtMesa.requestFocus();
                    if (t != 4) {
                        txtCodigo.requestFocus();
                    } else {
                        txtDomicilio.requestFocus();
                    }

                    //btnCerrarCuenta.setEnabled(true);
                }
                if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
                    System.out.println("SE APRETO TECLA ESCAPE");
                    nm.dispose();
                }

                if (evt.getKeyCode() >= 65 && evt.getKeyCode() <= 90 || evt.getKeyCode() >= 96 && evt.getKeyCode() <= 105 || evt.getKeyCode() == 8) {

                    comboBoxMozo.setModel(mbm.getLista(cadenaEscrita));
                    txtCodigo.setText("");

                    if (comboBoxMozo.getEditor().getItem().toString().equals("")) {
                        //System.out.println("Esta vacio");
                        txtCodigo.setEnabled(true);
                    } else {
                        //txtCodigo.setEnabled(false);
                        //System.out.println("Esta escribiendo");
                    }

                    if (comboBoxMozo.getItemCount() > 0) {
                        comboBoxMozo.getEditor().setItem(cadenaEscrita);
                        comboBoxMozo.showPopup();

                    } else {
                        comboBoxMozo.addItem(cadenaEscrita);
                    }
                }

            }
        });

    }

    public JComboBox getcomboBoxArticulo() {
        return comboBoxArticulo;
    }

    public void setcomboBoxArticulo(JComboBox comboBoxArticulo) {
        this.comboBoxArticulo = comboBoxArticulo;
    }

    public JComboBox getcomboBoxCliente() {
        return comboBoxCliente;
    }

    public void setcomboBoxCliente(JComboBox comboBoxCliente) {
        this.comboBoxCliente = comboBoxCliente;
    }

    public JComboBox getcomboBoxMozo() {
        return comboBoxMozo;
    }

    public void setcomboBoxMozo(JComboBox getcomboBoxMozo) {
        this.comboBoxMozo = getcomboBoxMozo;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        txtMesa = new javax.swing.JTextField();
        labelNro = new javax.swing.JLabel();
        comboBoxMozo = new javax.swing.JComboBox<>();
        labelMozo = new javax.swing.JLabel();
        comboBoxCliente = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();
        txtFecha = new javax.swing.JTextField();
        labelNro1 = new javax.swing.JLabel();
        txtDomicilio = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtCodigo = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        comboBoxArticulo = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        txtCantidad = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaVentaRapida = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        txtTotal = new javax.swing.JTextField();
        btnCerrarCuenta = new javax.swing.JButton();
        btnGuardarCambios = new javax.swing.JButton();
        btnImprimirTicket = new javax.swing.JButton();
        labelPaga = new javax.swing.JLabel();
        txtPaga = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtVuelto = new javax.swing.JTextField();
        checkComanda = new javax.swing.JCheckBox();
        checkPedido = new javax.swing.JCheckBox();
        radioEfectivo = new javax.swing.JRadioButton();
        radioOtros = new javax.swing.JRadioButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                formKeyReleased(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("MESA"));

        jLabel5.setText("Cliente:");

        txtMesa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtMesaKeyReleased(evt);
            }
        });

        labelNro.setText("Nro:");

        comboBoxMozo.setEditable(true);

        labelMozo.setText("Mozo:");

        comboBoxCliente.setEditable(true);

        jLabel8.setText("Fecha:");

        txtFecha.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtFecha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtFechaActionPerformed(evt);
            }
        });
        txtFecha.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtFechaKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtFechaKeyReleased(evt);
            }
        });

        labelNro1.setText("Domicilio:");

        txtDomicilio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtDomicilioKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(labelNro1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelNro, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtMesa, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtDomicilio, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(comboBoxCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelMozo, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(comboBoxMozo, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel5, jLabel8, labelMozo, labelNro, labelNro1});

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {comboBoxCliente, comboBoxMozo});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelNro)
                    .addComponent(txtMesa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(25, 25, 25)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(comboBoxCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelMozo)
                    .addComponent(comboBoxMozo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(25, 25, 25)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelNro1)
                    .addComponent(txtDomicilio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addComponent(txtFecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(25, 25, 25))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("VENTA"));

        jLabel1.setText("Codigo");

        txtCodigo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCodigoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCodigoKeyReleased(evt);
            }
        });

        jLabel2.setText("Articulo:");

        comboBoxArticulo.setEditable(true);
        comboBoxArticulo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxArticuloActionPerformed(evt);
            }
        });

        jLabel3.setText("Cantidad:");

        txtCantidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCantidadActionPerformed(evt);
            }
        });
        txtCantidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCantidadKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCantidadKeyReleased(evt);
            }
        });

        tablaVentaRapida.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablaVentaRapida.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaVentaRapidaMouseClicked(evt);
            }
        });
        tablaVentaRapida.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tablaVentaRapidaKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tablaVentaRapida);

        jLabel4.setText("Total:");

        txtTotal.setEditable(false);
        txtTotal.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtTotal.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtTotal.setToolTipText("");
        txtTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotalActionPerformed(evt);
            }
        });
        txtTotal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtTotalKeyReleased(evt);
            }
        });

        btnCerrarCuenta.setText("CERRAR CUENTA");
        btnCerrarCuenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarCuentaActionPerformed(evt);
            }
        });

        btnGuardarCambios.setText("GUARDAR CAMBIOS");
        btnGuardarCambios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarCambiosActionPerformed(evt);
            }
        });

        btnImprimirTicket.setText("IMPRIMIR TICKET");
        btnImprimirTicket.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImprimirTicketActionPerformed(evt);
            }
        });

        labelPaga.setText("Paga:");

        txtPaga.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtPaga.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtPaga.setToolTipText("");
        txtPaga.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPagaActionPerformed(evt);
            }
        });
        txtPaga.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPagaKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPagaKeyReleased(evt);
            }
        });

        jLabel7.setText("Vuelto:");

        txtVuelto.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtVuelto.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtVuelto.setToolTipText("");
        txtVuelto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtVueltoActionPerformed(evt);
            }
        });
        txtVuelto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtVueltoKeyReleased(evt);
            }
        });

        checkComanda.setText("Comanda Delivery");

        checkPedido.setText("Ticket Pedido");

        radioEfectivo.setText("Efectivo");

        radioOtros.setText("Otros");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(15, 15, 15)
                        .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(33, 33, 33)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(11, 11, 11)
                        .addComponent(comboBoxArticulo, javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(btnGuardarCambios, javax.swing.GroupLayout.PREFERRED_SIZE, 389, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                                .addGap(1, 1, 1)
                                .addComponent(checkComanda, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(checkPedido))
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(btnCerrarCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, 389, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnImprimirTicket, javax.swing.GroupLayout.PREFERRED_SIZE, 389, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(labelPaga, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtPaga, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(radioEfectivo)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(radioOtros))
                                    .addComponent(txtVuelto, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(txtCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(comboBoxArticulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnGuardarCambios))
                .addGap(20, 20, 20)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCerrarCuenta)
                    .addComponent(labelPaga)
                    .addComponent(txtPaga, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtVuelto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnImprimirTicket))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(checkComanda)
                    .addComponent(checkPedido)
                    .addComponent(radioEfectivo)
                    .addComponent(radioOtros))
                .addContainerGap())
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnCerrarCuenta, btnGuardarCambios, btnImprimirTicket});

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(10, 10, 10))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 154, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtCodigoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCodigoKeyReleased
        // TODO add your handling code here:

        /*
        if (!(evt.getKeyCode() == KeyEvent.VK_ENTER) || !(evt.getKeyCode() == KeyEvent.VK_DELETE)) {
            //this.comboBoxArticulo.setSelectedItem("");
            this.comboBoxArticulo.getEditor().getEditorComponent().setEnabled(false);
        }else{
            this.comboBoxArticulo.getEditor().getEditorComponent().setEnabled(true);
        }
         */
        if (this.txtCodigo.getText().equals("")) {
            this.comboBoxArticulo.setEnabled(true);
        } else {
            this.comboBoxArticulo.setEnabled(false);
        }


    }//GEN-LAST:event_txtCodigoKeyReleased

    private void txtCantidadKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCantidadKeyReleased
        // TODO add your handling code here:
        this.comboBoxArticulo.setEnabled(true);
        this.comboBoxArticulo.getEditor().getEditorComponent().setEnabled(true);

    }//GEN-LAST:event_txtCantidadKeyReleased

    private void tablaVentaRapidaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaVentaRapidaMouseClicked
        // TODO add your handling code here:


    }//GEN-LAST:event_tablaVentaRapidaMouseClicked

    private void tablaVentaRapidaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaVentaRapidaKeyReleased
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_DELETE) {
            int filaS = this.tablaVentaRapida.getSelectedRow();
            int id_det = Integer.parseInt(tablaVentaRapida.getValueAt(filaS, 0).toString());

            if (id_det == 0) {
                float acumulado = 0;

                for (int i = 0; i < this.tablaVentaRapida.getRowCount(); i++) {
                    acumulado = acumulado + Float.parseFloat(this.tablaVentaRapida.getValueAt(i, 5).toString());
                }

                acumulado = acumulado - Float.parseFloat(this.tablaVentaRapida.getValueAt(filaS, 5).toString());

                modelo.removeRow(filaS);

                txtTotal.setText("$" + acumulado);
                totalTicket = acumulado;
                txtPaga.setText("$" + acumulado);
                txtVuelto.setText("$ 0");

                if (tablaVentaRapida.getRowCount() == 0) {
                    this.btnCerrarCuenta.setEnabled(false);
                    this.btnImprimirTicket.setEnabled(false);
                    this.txtPaga.setEnabled(false);
                    this.txtVuelto.setEnabled(false);
                    //this.btnGuardarCambios.setEnabled(false);
                } else {
                    this.btnCerrarCuenta.setEnabled(true);
                    this.btnImprimirTicket.setEnabled(true);
                    this.txtPaga.setEnabled(true);
                    this.txtVuelto.setEnabled(true);
                }

                mf.actualizarTotalBoleta(boleta.getId_boleta(), acumulado);
                mt.traerMesasOcupadas((Inicio.ModeloTablaMesasOcupadas) dmmo);

                txtPaga.setEnabled(true);
                txtPaga.setEditable(true);

            } else {
                Detalle_boleta dm = new Detalle_boleta();
                dm = mf.traerDetalleUnico(id_det);
                int o = JOptionPane.showConfirmDialog(null, "ESTA SEGURO QUE DESEA ELIMINAR ESTE ARTICULO QUE YA FUE GUARDADO PREVIAMENTE? ");

                if (o == 0) {
                    //String cod = this.tablaVentaRapida.getValueAt(filaS, 1).toString();
                    String cod = dm.getCodigo();

                    a = ma.traerArticuloPorCodigo(cod);

                    if (a.getId_tipoProd2() == 5) {
                        if (ma.comprobarBarril(a.getDesc_articulo())) {
                            //JOptionPane.showMessageDialog(null, "NO SE PUEDE ELIMINAR UNA CERVEZA ARTESANAL QUE YA FUE TIRADA");
                            System.out.println("PINTAS A REPONER: " + dm.getCantidad() * (-0.5));
                            //ma.descontarPintaBarril((float) (Float.parseFloat(tablaVentaRapida.getValueAt(filaS, 3).toString()) * (-0.5)), ma.obtenerPosicionBarril(a.getDesc_articulo()), a.getDesc_articulo(), dB);
                            ma.descontarPintaBarril((float) (dm.getCantidad() * (-0.5)), ma.obtenerPosicionBarril(a.getDesc_articulo()), a.getDesc_articulo(), dB);

                        }

                        mf.eliminarDetalleMesa(id_det);
                        //JOptionPane.showMessageDialog(null, "No se puede eliminar una cerveza artesanal que ya fue tirada");
                        float acumulado = 0;

                        for (int i = 0; i < this.tablaVentaRapida.getRowCount(); i++) {
                            acumulado = acumulado + Float.parseFloat(this.tablaVentaRapida.getValueAt(i, 5).toString());
                        }

                        acumulado = acumulado - Float.parseFloat(this.tablaVentaRapida.getValueAt(filaS, 5).toString());

                        modelo.removeRow(filaS);

                        txtTotal.setText("$" + acumulado);
                        totalTicket = acumulado;
                        totalPaga = acumulado;
                        txtPaga.setText("$" + acumulado);
                        txtVuelto.setText("$ 0");

                        if (tablaVentaRapida.getRowCount() == 0) {
                            this.btnCerrarCuenta.setEnabled(false);
                            this.btnImprimirTicket.setEnabled(false);
                            this.txtPaga.setEnabled(false);
                            this.txtVuelto.setEnabled(false);
                            //this.btnGuardarCambios.setEnabled(false);
                        } else {
                            this.btnCerrarCuenta.setEnabled(true);
                            this.btnImprimirTicket.setEnabled(true);
                            this.txtPaga.setEnabled(true);
                            this.txtVuelto.setEnabled(true);
                        }

                        mf.actualizarTotalBoleta(boleta.getId_boleta(), acumulado);
                        mt.traerMesasOcupadas((Inicio.ModeloTablaMesasOcupadas) dmmo);

                    } else if (a.getId_tipoProd1() != 3) {
                        mf.eliminarDetalleMesa(id_det);
                        //JOptionPane.showMessageDialog(null, "No se puede eliminar una cerveza artesanal que ya fue tirada");
                        float acumulado = 0;

                        for (int i = 0; i < this.tablaVentaRapida.getRowCount(); i++) {
                            acumulado = acumulado + Float.parseFloat(this.tablaVentaRapida.getValueAt(i, 5).toString());
                        }

                        acumulado = acumulado - Float.parseFloat(this.tablaVentaRapida.getValueAt(filaS, 5).toString());

                        modelo.removeRow(filaS);

                        txtTotal.setText("$" + acumulado);
                        totalTicket = acumulado;
                        txtPaga.setText("$" + acumulado);
                        txtVuelto.setText("$ 0");

                        if (tablaVentaRapida.getRowCount() == 0) {
                            this.btnCerrarCuenta.setEnabled(false);
                            this.btnImprimirTicket.setEnabled(false);
                            this.txtPaga.setEnabled(false);
                            this.txtVuelto.setEnabled(false);
                            //this.btnGuardarCambios.setEnabled(false);
                        } else {
                            this.btnCerrarCuenta.setEnabled(true);
                            this.btnImprimirTicket.setEnabled(true);
                            this.txtPaga.setEnabled(true);
                            this.txtVuelto.setEnabled(true);
                        }

                        mf.actualizarTotalBoleta(boleta.getId_boleta(), acumulado);
                        mt.traerMesasOcupadas((Inicio.ModeloTablaMesasOcupadas) dmmo);

                    } else {
                        System.out.println("ES PROMOCION, SE REVISARA SI HAY CERVEZA");
                        ArrayList<Object[]> promocion = ma.traerArticulosPromocion(a.getCodigo());
                        boolean ban = true;
                        for (int i = 0; i < promocion.size(); i++) {
                            Articulo promo = new Articulo();
                            promo = ma.traerArticuloPorCodigo(promocion.get(i)[0].toString());
                            System.out.println("ARTICULO DE LA PROMO: " + promo.getDesc_articulo());

                            if (promo.getId_tipoProd2() == 5) {
                                Detalle_boleta db = new Detalle_boleta();
                                db = mf.traerDetalleUnico(id_det);
                                System.out.println("se selecciono cerveza artesanal");

                                if (ma.comprobarBarril(promo.getDesc_articulo())) {
                                    //float litros = Float.parseFloat(this.tablaVentaRapida.getValueAt(filaS, 3).toString()) * Float.parseFloat(promocion.get(i)[2].toString()) * (float) (-0.5);
                                    float litros = dm.getCantidad() * Float.parseFloat(promocion.get(i)[2].toString()) * (float) (-0.5);
                                    System.out.println("LA CANTIDAD DE LITROS QUE SE VAN A DESCONTAR POR ELIMINAR LA PROMOCION ES: " + litros);
                                    ma.descontarPintaBarril(litros, ma.obtenerPosicionBarril(promo.getDesc_articulo()), promo.getDesc_articulo(), dB);
                                }

                                ban = false;
                            }
                        }

                        if (ban) {//si no encontro cerveza artesanal dentro de la promocion
                            mf.eliminarDetalleMesa(id_det);
                            //JOptionPane.showMessageDialog(null, "No se puede eliminar una cerveza artesanal que ya fue tirada");
                            float acumulado = 0;

                            for (int i = 0; i < this.tablaVentaRapida.getRowCount(); i++) {
                                acumulado = acumulado + Float.parseFloat(this.tablaVentaRapida.getValueAt(i, 5).toString());
                            }

                            acumulado = acumulado - Float.parseFloat(this.tablaVentaRapida.getValueAt(filaS, 5).toString());

                            modelo.removeRow(filaS);

                            txtTotal.setText("$" + acumulado);
                            totalTicket = acumulado;
                            txtPaga.setText("$" + acumulado);
                            txtVuelto.setText("$ 0");

                            if (tablaVentaRapida.getRowCount() == 0) {
                                this.btnCerrarCuenta.setEnabled(false);
                                this.btnImprimirTicket.setEnabled(false);
                                this.txtPaga.setEnabled(false);
                                this.txtVuelto.setEnabled(false);
                                //this.btnGuardarCambios.setEnabled(false);
                            } else {
                                this.btnCerrarCuenta.setEnabled(true);
                                this.btnImprimirTicket.setEnabled(true);
                                this.txtPaga.setEnabled(true);
                                this.txtVuelto.setEnabled(true);
                            }

                            mf.actualizarTotalBoleta(boleta.getId_boleta(), acumulado);
                            mt.traerMesasOcupadas((Inicio.ModeloTablaMesasOcupadas) dmmo);

                        } else {
                            //JOptionPane.showMessageDialog(null, "LA PROMOCION POSEE UNA CERVEZA ARTESANAL TIRADA\n"
                            //        + "NO SE PUEDE ELIMINAR UNA CERVEZA ARTESANAL QUE YA FUE TIRADA");
                            mf.eliminarDetalleMesa(id_det);
                            //JOptionPane.showMessageDialog(null, "No se puede eliminar una cerveza artesanal que ya fue tirada");
                            float acumulado = 0;

                            for (int i = 0; i < this.tablaVentaRapida.getRowCount(); i++) {
                                acumulado = acumulado + Float.parseFloat(this.tablaVentaRapida.getValueAt(i, 5).toString());
                            }

                            acumulado = acumulado - Float.parseFloat(this.tablaVentaRapida.getValueAt(filaS, 5).toString());

                            modelo.removeRow(filaS);

                            txtTotal.setText("$" + acumulado);
                            totalTicket = acumulado;
                            txtPaga.setText("$" + acumulado);
                            txtVuelto.setText("$ 0");

                            if (tablaVentaRapida.getRowCount() == 0) {
                                this.btnCerrarCuenta.setEnabled(false);
                                this.btnImprimirTicket.setEnabled(false);
                                this.txtPaga.setEnabled(false);
                                this.txtVuelto.setEnabled(false);
                                //this.btnGuardarCambios.setEnabled(false);
                            } else {
                                this.btnCerrarCuenta.setEnabled(true);
                                this.btnImprimirTicket.setEnabled(true);
                                this.txtPaga.setEnabled(true);
                                this.txtVuelto.setEnabled(true);
                            }

                            mf.actualizarTotalBoleta(boleta.getId_boleta(), acumulado);
                            mt.traerMesasOcupadas((Inicio.ModeloTablaMesasOcupadas) dmmo);
                        }
                    }

                    txtPaga.setEnabled(true);
                    txtPaga.setEditable(true);

                }

            }
            pintasReemplazo.clear();
        }
    }//GEN-LAST:event_tablaVentaRapidaKeyReleased

    private void txtTotalKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTotalKeyReleased
        // TODO add your handling code here:

    }//GEN-LAST:event_txtTotalKeyReleased

    private void btnCerrarCuentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarCuentaActionPerformed
        // TODO add your handling code here:
        if (t != 3 && t != 6 && t != 7) {
            if (t != 5 && mf.existeMesa(Integer.parseInt(txtMesa.getText())) && Integer.parseInt(txtMesa.getText()) != boleta.getMesa()) {
                JOptionPane.showMessageDialog(null, "LA MESA YA SE ENCUENTRA OCUPADA");
            } else {
                int opcion;
                if (t == 5) {
                    opcion = JOptionPane.showConfirmDialog(null, "ESTA SEGURO QUE DESEA COMPLETAR EL DELIVERY?");
                } else {
                    opcion = JOptionPane.showConfirmDialog(null, "ESTA SEGURO QUE DESEA GUARDAR ESTA BOLETA?");
                }

                if (opcion == 0) {

                    for (int i = 0; i < this.tablaVentaRapida.getRowCount(); i++) {

                        int id_det = Integer.parseInt(tablaVentaRapida.getValueAt(i, 0).toString());
                        a = ma.traerArticuloPorCodigo(tablaVentaRapida.getValueAt(i, 1).toString());
                        int cantidad = Integer.parseInt(tablaVentaRapida.getValueAt(i, 3).toString());

                        if (id_det != 0) {
                            Detalle_boleta db = new Detalle_boleta();
                            db = mf.traerDetalleUnico(id_det);
                            System.out.println("SON ARTICULOS YA CARGADOS");
                            System.out.println("CANTIDAD PREVIA: " + db.getCantidad());
                            System.out.println("CANTIDAD NUEVA: " + cantidad);
                            cantidad = cantidad - db.getCantidad();
                        }

                        if (a.getId_tipoProd2() == 5) {
                            //ma.descontarPintaBarril((float) (cantidadAgregada * 0.5), ma.obtenerPosicionBarril(a.getDesc_articulo()), a.getDesc_articulo(), dB);

                            Barril barril = new Barril();
                            barril.setPosicion(ma.obtenerPosicionBarril(a.getDesc_articulo()));
                            barril.setCantidad_consumida((float) (cantidad * 0.5));
                            barril.setDesc_barril(a.getDesc_articulo());
                            System.out.println("SE AGREGO BARRIL EN ZONA 1 " + barril.getDesc_barril() + " LITROS: " + barril.getCantidad_consumida());
                            pintasReemplazo.add(barril);
                        } else if (a.getId_tipoProd1() == 3) {
                            promocion = ma.traerArticulosPromocion(a.getCodigo());

                            for (int k = 0; k < promocion.size(); k++) {
                                Articulo prom = new Articulo();
                                prom = ma.traerArticuloPorCodigo(promocion.get(k)[0].toString());

                                if (prom.getId_tipoProd2() == 5) {
                                    Barril barril = new Barril();
                                    barril.setPosicion(ma.obtenerPosicionBarril(prom.getDesc_articulo()));
                                    int cant = Integer.parseInt(promocion.get(k)[2].toString());
                                    //barril.setCantidad_consumida((float) (cant * 0.5));
                                    barril.setCantidad_consumida((float) (cantidad * cant * 0.5));
                                    barril.setDesc_barril(prom.getDesc_articulo());
                                    System.out.println("SE AGREGO BARRIL EN ZONA 2 " + barril.getDesc_barril() + " LITROS: " + barril.getCantidad_consumida());
                                    pintasReemplazo.add(barril);
                                    System.out.println("SE AGREGO PINTA DE LA PROMOCION HABIENDO HECHO UN ADICIONAL");
                                }
                            }
                        }

                    }

                    if (t == 1) {//si se acaba de abrir esta mesa
                        String busquedaCliente = this.comboBoxCliente.getEditor().getItem().toString();
                        System.out.println("CLIENTE A BUSCAR: " + busquedaCliente);
                        cli = mcli.obtenerCliente(busquedaCliente);
                        //System.out.println("CLIENTE COMPRADOR: "+cli.getNombre_cliente()+" "+cli.getApellido_cliente());

                        String busquedaMozo = this.comboBoxMozo.getEditor().getItem().toString();
                        user = mu.traerUsuarioString(busquedaMozo);

                        boleta.setCliente(cli);
                        boleta.setFecha_boleta(mfechas.obtenerFechaActual());
                        boleta.setHora_apertura(mfechas.obtenerHoraActual());
                        boleta.setVendedor(user);
                        boleta.setTotal(totalTicket);
                        boleta.setMesa(Integer.parseInt(txtMesa.getText()));
                        boleta.setHora_cierre("00:00:00");
                        boleta.setFecha_cierre(mfechas.obtenerFechaActual());

                        mf.insertarBoleta(boleta, 2);
                        boleta.setId_boleta(mf.obtenerID_boleta());
                    } else if (t != 5) {
                        if (Integer.parseInt(txtMesa.getText()) != boleta.getMesa()) {
                            mf.actualizarMesa(boleta.getId_boleta(), Integer.parseInt(txtMesa.getText()));
                            System.out.println("SON DISTINTAS");
                        } else {
                            System.out.println("SON IGUALES");
                        }

                        cli = mcli.obtenerCliente(this.comboBoxCliente.getEditor().getItem().toString());
                        //boleta.setFecha_boleta(mfechas.obtenerFechaActual());
                    }

                    for (int i = 0; i < this.tablaVentaRapida.getRowCount(); i++) {
                        int id_a_modificar = Integer.parseInt(tablaVentaRapida.getValueAt(i, 0).toString());
                        Detalle_boleta detalleNuevo = new Detalle_boleta();

                        int cantidad = Integer.parseInt(tablaVentaRapida.getValueAt(i, 3).toString());
                        float precio = 0;
                        if (a.getId_tipoProd1() != 4) {
                            precio = Float.parseFloat(this.tablaVentaRapida.getValueAt(i, 4).toString());
                        } else {
                            precio = Float.parseFloat(this.tablaVentaRapida.getValueAt(i, 5).toString());
                            System.out.println("el descuento es de: " + precio);
                        }

                        String codigo = tablaVentaRapida.getValueAt(i, 1).toString();

                        detalleNuevo.setId_boleta(boleta.getId_boleta());
                        detalleNuevo.setPrecio(precio);
                        detalleNuevo.setCantidad(cantidad);
                        detalleNuevo.setId_detalleboleta(id_a_modificar);
                        detalleNuevo.setCodigo(codigo);

                        if (id_a_modificar == 0) {
                            System.out.println("SE DEBE AÑADIR ESTE ARTICULO");
                            mf.insertarDetallesBoleta(detalleNuevo, dB);

                        } else {
                            System.out.println("ESTE ARTICULO SE DEBE ACTUALIZAR");
                            mf.actualizarDetallesBoleta(detalleNuevo, dB);
                        }

                        arrayDetalles.add(detalleNuevo);
                    }
                    if (t == 2 || t == 5) {
                        float acumulado = 0;

                        for (int k = 0; k < this.tablaVentaRapida.getRowCount(); k++) {
                            acumulado = acumulado + Float.parseFloat(this.tablaVentaRapida.getValueAt(k, 5).toString());
                        }

                        totalTicket = acumulado;

                        mf.actualizarTotalBoleta(boleta.getId_boleta(), acumulado);
                        boleta.setTotal(acumulado);

                        for (int j = 0; j < dmmo.getRowCount(); j++) {
                            if (Integer.parseInt(dmmo.getValueAt(j, 0).toString()) == boleta.getId_boleta()) {
                                dmmo.setValueAt(acumulado, j, 3);
                                dmmo.setValueAt(txtMesa.getText(), j, 1);
                            }
                        }
                    }

                    if (t == 5) {
                        if (boleta.getTotal() != totalTicket) {
                            System.out.println("se agrego o quito elementos, el total es diferente");
                            mf.actualizarPagoDelivery(boleta.getId_boleta(), totalPaga);
                        } else {
                            System.out.println("no se modifico nada");
                        }
                        mf.actualizarPagoDelivery(boleta.getId_boleta(), totalPaga);

                        if (!boleta.getDireccion_delivery().equals(txtDomicilio.getText())) {
                            System.out.println("SE MODIFICO LA DIRECCION");
                            mf.actualizarDireccionDelivery(boleta.getId_boleta(), txtDomicilio.getText());
                        } else {
                            System.out.println("NO SE MODIFICO LA DIRECCION");
                        }

                        System.out.println("SE QUIERE CERRAR EL DELIVERY ");
                        if (radioEfectivo.isSelected()) {
                            Facturacion_boleta fb = new Facturacion_boleta();
                            fb.setId_boleta(boleta.getId_boleta());
                            fb.setId_condvta(1);
                            fb.setTotal(this.totalTicket);
                            mf.insertarFormasDePago(fb);
                            mf.pagarBoleta(boleta.getId_boleta());
                            mf.setHoraCierreBoleta(mfechas.obtenerHoraActual(), mfechas.obtenerFechaActual(), boleta.getId_boleta());
                            JOptionPane.showMessageDialog(null, "DELIVERY CERRADO CON EXITO");
                        } else {
                            cli = mcli.obtenerCliente(this.comboBoxCliente.getEditor().getItem().toString());
                            JOptionPane.showMessageDialog(null, "DELIVERY GUARDADA CON EXITO");
                            ap = new AgregarPago(user, cli, boleta, null, inicio, arrayDetalles, dB, (Inicio.ModeloTablaInicio) dtm, (Inicio.ModeloTablaMesasOcupadas) dmmo, null, null, 2, (Inicio.ModeloTablaDelivery) dmtd);
                            ap.setVisible(true);
                            ap.setLocationRelativeTo(null);
                        }

                    } else {
                        JOptionPane.showMessageDialog(null, "MESA GUARDADA CON EXITO");
                        ap = new AgregarPago(user, cli, boleta, null, inicio, arrayDetalles, dB, (Inicio.ModeloTablaInicio) dtm, (Inicio.ModeloTablaMesasOcupadas) dmmo, null, null, 2, (Inicio.ModeloTablaDelivery) dmtd);
                        ap.setVisible(true);
                        ap.setLocationRelativeTo(null);
                    }

                    mt.traerMesasOcupadas((Inicio.ModeloTablaMesasOcupadas) dmmo);
                    mt.traerDeliverys((Inicio.ModeloTablaDelivery) dmtd);
                    mt.traerVentasSinCerrar((Inicio.ModeloTablaInicio) dtm);
                    ma.actualizarBarriles(pintasReemplazo, dB);
                    pintasReemplazo.clear();
                    this.dispose();

                }
            }
        } else {
            System.out.println("SE QUIERE VER EL DETALLE DEL PAGO");
            ap = new AgregarPago(user, cli, boleta, null, inicio, arrayDetalles, dB, (Inicio.ModeloTablaInicio) dtm, (Inicio.ModeloTablaMesasOcupadas) dmmo, null, null, 4, (Inicio.ModeloTablaDelivery) dmtd);
            ap.setVisible(true);
            ap.setLocationRelativeTo(null);

        }


    }//GEN-LAST:event_btnCerrarCuentaActionPerformed

    private void btnGuardarCambiosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarCambiosActionPerformed
        // TODO add your handling code here:

        System.out.println("ENTRO POR AQUIII");
        boolean ban = true;

        if (t != 4 && t != 5) {
            if (t == 2 && Integer.parseInt(txtMesa.getText()) == boleta.getMesa()) {
                System.out.println("ES LA MISMA MESA PREVIAMENTE CARGADA O ES DELIVERY");
                //JOptionPane.showMessageDialog(null, "LA MESA YA SE ENCUENTRA OCUPADA");
            } else {
                System.out.println("SON DISTINTAS MESAS O ES MESA NUEVA");

                for (int i = 0; i < dmmo.getRowCount() && ban; i++) {
                    if (Integer.parseInt(dmmo.getValueAt(i, 1).toString()) == Integer.parseInt(txtMesa.getText())) {
                        ban = false;
                    }
                }
            }
        }

        if (!ban) {
            System.out.println("----------------LA MESA YA SE ENCUENTRA OCUPADA------------------");
            JOptionPane.showMessageDialog(null, "LA MESA YA SE ENCUENTRA OCUPADA");
        } else {
            System.out.println("la mesa actual es " + txtMesa.getText() + " y el numero de mesa que ya tenia es " + boleta.getMesa());
            int opcion = JOptionPane.showConfirmDialog(null, "ESTA SEGURO QUE DESEA GUARDAR ESTA BOLETA?");
            if (opcion == 0) {

                for (int i = 0; i < this.tablaVentaRapida.getRowCount(); i++) {

                    int id_det = Integer.parseInt(tablaVentaRapida.getValueAt(i, 0).toString());
                    a = ma.traerArticuloPorCodigo(tablaVentaRapida.getValueAt(i, 1).toString());
                    int cantidad = Integer.parseInt(tablaVentaRapida.getValueAt(i, 3).toString());

                    if (id_det != 0) {
                        Detalle_boleta db = new Detalle_boleta();
                        db = mf.traerDetalleUnico(id_det);
                        System.out.println("SON ARTICULOS YA CARGADOS");
                        System.out.println("CANTIDAD PREVIA: " + db.getCantidad());
                        System.out.println("CANTIDAD NUEVA: " + cantidad);
                        cantidad = cantidad - db.getCantidad();
                    }

                    if (a.getId_tipoProd2() == 5) {
                        //ma.descontarPintaBarril((float) (cantidadAgregada * 0.5), ma.obtenerPosicionBarril(a.getDesc_articulo()), a.getDesc_articulo(), dB);

                        Barril barril = new Barril();
                        barril.setPosicion(ma.obtenerPosicionBarril(a.getDesc_articulo()));
                        barril.setCantidad_consumida((float) (cantidad * 0.5));
                        barril.setDesc_barril(a.getDesc_articulo());
                        System.out.println("SE AGREGO BARRIL EN ZONA 1 " + barril.getDesc_barril() + " LITROS: " + barril.getCantidad_consumida());
                        pintasReemplazo.add(barril);
                    } else if (a.getId_tipoProd1() == 3) {
                        promocion = ma.traerArticulosPromocion(a.getCodigo());

                        for (int k = 0; k < promocion.size(); k++) {
                            Articulo prom = new Articulo();
                            prom = ma.traerArticuloPorCodigo(promocion.get(k)[0].toString());

                            if (prom.getId_tipoProd2() == 5) {
                                Barril barril = new Barril();
                                barril.setPosicion(ma.obtenerPosicionBarril(prom.getDesc_articulo()));
                                int cant = Integer.parseInt(promocion.get(k)[2].toString());
                                //barril.setCantidad_consumida((float) (cant * 0.5));
                                barril.setCantidad_consumida((float) (cantidad * cant * 0.5));
                                barril.setDesc_barril(prom.getDesc_articulo());
                                System.out.println("SE AGREGO BARRIL EN ZONA 2 " + barril.getDesc_barril() + " LITROS: " + barril.getCantidad_consumida());
                                pintasReemplazo.add(barril);
                                System.out.println("SE AGREGO PINTA DE LA PROMOCION HABIENDO HECHO UN ADICIONAL");
                            }
                        }
                    }

                }

                if (t == 1 | t == 4) {//si se acaba de abrir esta mesa
                    String busquedaCliente = this.comboBoxCliente.getEditor().getItem().toString();
                    System.out.println("CLIENTE A BUSCAR: " + busquedaCliente);
                    cli = mcli.obtenerCliente(busquedaCliente);
                    //System.out.println("CLIENTE COMPRADOR: "+cli.getNombre_cliente()+" "+cli.getApellido_cliente());

                    String busquedaMozo = this.comboBoxMozo.getEditor().getItem().toString();
                    user = mu.traerUsuarioString(busquedaMozo);

                    boleta.setCliente(cli);
                    boleta.setFecha_boleta(mfechas.obtenerFechaActual());
                    boleta.setHora_apertura(mfechas.obtenerHoraActual());
                    boleta.setVendedor(user);
                    boleta.setTotal(totalTicket);
                    boleta.setHora_cierre("00:00:00");
                    boleta.setFecha_cierre(mfechas.obtenerFechaActual());
                    if (t == 1) {
                        System.out.println("------- SE VA A INSERTAR LA MESA NUEVA -----");
                        boleta.setMesa(Integer.parseInt(txtMesa.getText()));
                        mf.insertarBoleta(boleta, 2);
                        //boleta.setMesa(Integer.parseInt(this.txtMesa.getText()));
                    } else {
                        boleta.setMesa(0);
                        boleta.setDireccion_delivery(txtDomicilio.getText());
                        boleta.setId_tipoVta(2);
                        boleta.setPago_delivery(totalPaga);
                        mf.insertarDelivery(boleta, 2);
                    }
                    boleta.setId_boleta(mf.obtenerID_boleta());

                } else if (t != 5) {
                    if (Integer.parseInt(txtMesa.getText()) != boleta.getMesa()) {
                        mf.actualizarMesa(boleta.getId_boleta(), Integer.parseInt(txtMesa.getText()));
                        System.out.println("SON DISTINTAS");
                    } else {
                        System.out.println("SON IGUALES");
                    }
                }

                for (int i = 0; i < this.tablaVentaRapida.getRowCount(); i++) {
                    int id_a_modificar = Integer.parseInt(tablaVentaRapida.getValueAt(i, 0).toString());
                    Detalle_boleta detalleNuevo = new Detalle_boleta();

                    int cantidad = Integer.parseInt(tablaVentaRapida.getValueAt(i, 3).toString());
                    float precio = 0;
                    if (a.getId_tipoProd1() != 4) {
                        precio = Float.parseFloat(this.tablaVentaRapida.getValueAt(i, 4).toString());
                    } else {
                        precio = Float.parseFloat(this.tablaVentaRapida.getValueAt(i, 5).toString());
                        System.out.println("el descuento es de: " + precio);
                    }

                    String codigo = tablaVentaRapida.getValueAt(i, 1).toString();

                    detalleNuevo.setId_boleta(boleta.getId_boleta());
                    detalleNuevo.setPrecio(precio);
                    detalleNuevo.setCantidad(cantidad);
                    detalleNuevo.setId_detalleboleta(id_a_modificar);
                    detalleNuevo.setCodigo(codigo);

                    if (id_a_modificar == 0) {
                        System.out.println("SE DEBE AÑADIR ESTE ARTICULO");
                        mf.insertarDetallesBoleta(detalleNuevo, dB);

                    } else {
                        System.out.println("ESTE ARTICULO SE DEBE ACTUALIZAR");
                        mf.actualizarDetallesBoleta(detalleNuevo, dB);
                    }
                }

                if (t == 2 || t == 5) {
                    float acumulado = 0;

                    for (int k = 0; k < this.tablaVentaRapida.getRowCount(); k++) {
                        acumulado = acumulado + Float.parseFloat(this.tablaVentaRapida.getValueAt(k, 5).toString());
                    }

                    totalTicket = acumulado;

                    if (t == 5) {
                        String busquedaMozo = this.comboBoxMozo.getEditor().getItem().toString();
                        user = mu.traerUsuarioString(busquedaMozo);
                        if (user.getId_usuario() != boleta.getVendedor().getId_usuario()) {
                            System.out.println("SON DISTINTOS DELIVERYS");
                            mf.actualizarDelivery(boleta.getId_boleta(), user.getId_usuario());
                        }
                        mf.actualizarPagoDelivery(boleta.getId_boleta(), totalPaga);

                        if (!boleta.getDireccion_delivery().equals(txtDomicilio.getText())) {
                            System.out.println("SE MODIFICO LA DIRECCION");
                            mf.actualizarDireccionDelivery(boleta.getId_boleta(), txtDomicilio.getText());
                        } else {
                            System.out.println("NO SE MODIFICO LA DIRECCION");
                        }
                    }
                    if (boleta.getTotal() != totalTicket) {
                        System.out.println("se agrego o quito elementos, el total es diferente");
                        System.out.println("PAGA CON: " + totalPaga);
                        //mf.actualizarPagoDelivery(boleta.getId_boleta(), totalPaga);
                        mf.actualizarTotalBoleta(boleta.getId_boleta(), acumulado);
                    } else {
                        System.out.println("no se modifico nada");
                    }

                    //mf.actualizarTotalBoleta(boleta.getId_boleta(), acumulado);
                    for (int j = 0; j < dmmo.getRowCount(); j++) {
                        if (Integer.parseInt(dmmo.getValueAt(j, 0).toString()) == boleta.getId_boleta()) {
                            dmmo.setValueAt(acumulado, j, 3);
                            dmmo.setValueAt(txtMesa.getText(), j, 1);
                        }
                    }
                }

                mt.traerMesasOcupadas((Inicio.ModeloTablaMesasOcupadas) dmmo);
                mt.traerDeliverys((Inicio.ModeloTablaDelivery) dmtd);
                ma.actualizarBarriles(pintasReemplazo, dB);
                pintasReemplazo.clear();
                if (t != 5) {
                    JOptionPane.showMessageDialog(null, "MESA GUARDADA CON EXITO");
                } else {
                    JOptionPane.showMessageDialog(null, "DELIVERY GUARDADO CON EXITO");
                }

                this.dispose();
            }
        }

        if (t == 4) {
            arrayDetalles = mf.traerDetalle(boleta.getId_boleta());
            mi.imprimirDelivery(boleta, cli, arrayDetalles, 1);

            /*PARA DAR UN TIEMPO ANTES DE IMPRIMIR OTRO TICKET
            try {
                //Ponemos a "Dormir" el programa durante los ms que queremos
                Thread.sleep(4 * 1000);
            } catch (Exception e) {
                System.out.println(e);
            }*/
            //mi.imprimirTicketDelivery(boleta, boleta.getCliente(), arrayDetalles, 1);
        }


    }//GEN-LAST:event_btnGuardarCambiosActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        // TODO add your handling code here:
        pintasReemplazo.clear();
    }//GEN-LAST:event_formWindowClosed

    private void txtCodigoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCodigoKeyPressed
        // TODO add your handling code here:

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            String codigo = this.txtCodigo.getText();
            a = ma.traerArticuloPorCodigo(codigo);
            System.out.println("ID TIPO: " + a.getId_tipoProd1());
            if (a.getDesc_articulo() == null) {
                JOptionPane.showMessageDialog(null, "NO EXISTE EL ARTICULO SELECCIONADO");
            } else if (a.getId_tipoProd2() == 5) {
                System.out.println("se selecciono cerveza artesanal");
                if (ma.comprobarBarril(a.getDesc_articulo())) {
                    System.out.println("hay cerveza " + a.getDesc_articulo());
                    if (hh) {
                        a.setPrecio_venta(ma.traerPrecioHH());
                    }
                    //this.comboBoxArticulo.removeAllItems();
                    comboBoxArticulo.addItem(a.getDesc_articulo());
                    //this.comboBoxArticulo.setEnabled(false);
                    comboBoxArticulo.setSelectedItem(a.getDesc_articulo());
                    txtCantidad.setEditable(true);
                    txtCantidad.requestFocus();

                } else {
                    txtCantidad.setEditable(false);
                    this.txtCodigo.requestFocus();
                    JOptionPane.showMessageDialog(null, "NO HAY CERVEZA " + a.getDesc_articulo() + " CARGADA EN LA CHOPERA");
                }
            } else if (a.getId_tipoProd1() == 3) {
                System.out.println("ES PROMOCION, SE REVISARA SI HAY CERVEZA");
                ArrayList<Object[]> promocion = ma.traerArticulosPromocion(a.getCodigo());
                System.out.println("cantidad de objetos de la promo: " + promocion.size());
                for (int i = 0; i < promocion.size(); i++) {
                    Articulo promo = new Articulo();
                    promo = ma.traerArticuloPorCodigo(promocion.get(i)[0].toString());
                    System.out.println("ARTICULO DE LA PROMO: " + promo.getDesc_articulo());
                    if (promo.getId_tipoProd2() == 5) {
                        System.out.println("se selecciono cerveza artesanal");
                        if (ma.comprobarBarril(promo.getDesc_articulo())) {
                            System.out.println("hay cerveza " + a.getDesc_articulo());
                            //this.comboBoxArticulo.removeAllItems();
                            comboBoxArticulo.addItem(a.getDesc_articulo());
                            //this.comboBoxArticulo.setEnabled(false);
                            comboBoxArticulo.setSelectedItem(a.getDesc_articulo());
                            txtCantidad.setEditable(true);
                            txtCantidad.requestFocus();

                        } else {
                            txtCantidad.setEditable(false);
                            this.txtCodigo.requestFocus();
                            JOptionPane.showMessageDialog(null, "NO HAY CERVEZA " + promo.getDesc_articulo() + " CARGADA EN LA CHOPERA");
                        }
                    } else {
                        //this.comboBoxArticulo.removeAllItems();
                        comboBoxArticulo.addItem(a.getDesc_articulo());
                        comboBoxArticulo.setSelectedItem(a.getDesc_articulo());
                        //this.comboBoxArticulo.setEnabled(false);
                        txtCantidad.setEditable(true);
                        txtCantidad.requestFocus();
                    }
                }

            } else {
                //this.comboBoxArticulo.removeAllItems();
                comboBoxArticulo.addItem(a.getDesc_articulo());
                comboBoxArticulo.setSelectedItem(a.getDesc_articulo());
                //this.comboBoxArticulo.setEnabled(false);
                txtCantidad.setEditable(true);
                txtCantidad.requestFocus();
            }

        } else if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
            System.out.println("SE APRETO TECLA ESCAPE");
            this.dispose();
        }


    }//GEN-LAST:event_txtCodigoKeyPressed

    private void txtCantidadKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCantidadKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtPaga.setEditable(true);
            txtVuelto.setEditable(true);
            Object[] ar = new Object[6];
            int cantidad = 0;

            if (txtCantidad.getText().equals("")) {
                cantidad = 1;
            } else {
                cantidad = Integer.parseInt(txtCantidad.getText());
            }

            //float total = a.getPrecio_venta() * Integer.parseInt(txtCantidad.getText());
            float total = a.getPrecio_venta() * cantidad;
            boolean coincidencia = false;
            int filaMismoArticulo = 0;
            float acumulado = 0;

            for (int i = 0; i < this.tablaVentaRapida.getRowCount(); i++) {
                if (a.getDesc_articulo().equals(this.tablaVentaRapida.getValueAt(i, 2))) {
                    coincidencia = true;
                    filaMismoArticulo = i;
                }
            }

            if (coincidencia) {

                System.out.println("EXISTE UN VALOR IGUAL");
                int cantidadPrevia = Integer.parseInt(this.tablaVentaRapida.getValueAt(filaMismoArticulo, 3).toString());
                //int cantidadAgregada = Integer.parseInt(this.tablaVentaRapida.getValueAt(filaMismoArticulo, 3).toString());
                //int cantidadAgregada = Integer.parseInt(txtCantidad.getText());
                int cantidadAgregada = cantidad;
                int cantidadNueva = cantidadPrevia + cantidadAgregada;
                this.tablaVentaRapida.setValueAt(cantidadNueva, filaMismoArticulo, 3);
                this.tablaVentaRapida.setValueAt(cantidadNueva * a.getPrecio_venta(), filaMismoArticulo, 5);
                //total = Float.parseFloat(this.tablaVentaRapida.getValueAt(filaMismoArticulo, 5).toString());
                total = cantidadAgregada * a.getPrecio_venta();
                System.out.println("TOTAL A SUMAR EN COINCIDENCIA: " + total);

            } else {
                System.out.println("entro por el false");
                //a = ma.traerArticuloPorCodigo(codigo);
                if (a.getId_tipoProd1() == 4) {
                    ar[0] = 0;
                    ar[1] = a.getCodigo();
                    ar[2] = a.getDesc_articulo();
                    System.out.println("codigo a ingresar: " + a.getCodigo());
                    //ar[3] = txtCantidad.getText();
                    ar[3] = cantidad;
                    ar[4] = "%" + a.getPrecio_venta();
                    System.out.println("TOTAL TICKET A DESCONTAR: " + totalTicket);
                    float descuento = totalTicket * a.getPrecio_venta() / 100;
                    System.out.println("TOTAL DEL DESCUENTO: " + descuento);
                    ar[5] = "-" + descuento;
                    totalTicket = totalTicket - descuento;
                } else {
                    ar[0] = 0;
                    ar[1] = a.getCodigo();
                    ar[2] = a.getDesc_articulo();
                    //ar[3] = txtCantidad.getText();
                    ar[3] = cantidad;
                    if (a.getId_tipoProd2() == 5 && hh) {
                        ar[4] = ma.traerPrecioHH();
                    } else {
                        ar[4] = a.getPrecio_venta();
                    }
                    ar[5] = total;
                    System.out.println(a.stringArticulo(a));
                    //modelo.addRow(ar);
                }

                modelo.addRow(ar);

            }

            for (int i = 0; i < this.tablaVentaRapida.getRowCount(); i++) {
                acumulado = acumulado + Float.parseFloat(this.tablaVentaRapida.getValueAt(i, 5).toString());
            }

            txtCantidad.setText("");
            totalTicket = acumulado;
            totalPaga = totalTicket;
            txtTotal.setText("$" + acumulado);
            txtPaga.setText(String.valueOf(totalPaga));
            txtVuelto.setText(String.valueOf(totalPaga - totalTicket));
            this.comboBoxArticulo.removeAllItems();
            this.comboBoxArticulo.setEnabled(true);
            this.comboBoxArticulo.getEditor().getEditorComponent().setEnabled(true);
            this.txtCodigo.setText("");
            txtCodigo.requestFocus();

            btnCerrarCuenta.setEnabled(true);
            btnImprimirTicket.setEnabled(true);
            btnGuardarCambios.setEnabled(true);

            this.txtPaga.setEnabled(true);
            this.txtVuelto.setEnabled(true);

        } else if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
            System.out.println("SE APRETO TECLA ESCAPE");
            this.dispose();
        }
    }//GEN-LAST:event_txtCantidadKeyPressed

    private void formKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyReleased
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
            System.out.println("SE APRETO TECLA ESCAPE");
            this.dispose();
        }
    }//GEN-LAST:event_formKeyReleased

    private void comboBoxArticuloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxArticuloActionPerformed
        // TODO add your handling code here:

        /*
        a = ma.traerArticuloPorString(comboBoxArticulo.getEditor().getItem().toString());
        //a = ma.traerArticuloPorString(comboBoxArticulo.getSelectedItem().toString());
        //comboBoxDetalleArticulo.requestFocus();
        //mt.agregarArticuloVenta(tablaResumenVenta, modeloTablaResumenPedido ,m);
        System.out.println("ENTRO 1");
        System.out.println("ELEMENTO SELECCIONADO: " + comboBoxArticulo.getEditor().getItem().toString());
        //String comparar = comboBoxArticulo.getEditor().getItem().toString();
        //comboBoxArticulo.getEditor().setItem(a.getDesc_articulo());

        if (a.getId_tipoProd2() == 5) {
            System.out.println("se selecciono cerveza artesanal");
            if (ma.comprobarBarril(a.getDesc_articulo())) {
                System.out.println("hay cerveza " + a.getDesc_articulo());
                if (hh) {
                    a.setPrecio_venta(ma.traerPrecioHH());
                }

                txtCantidad.setEditable(true);
                txtCantidad.requestFocus();

            } else {
                txtCantidad.setEditable(false);
                JOptionPane.showMessageDialog(null, "NO HAY CERVEZA " + a.getDesc_articulo() + " CARGADA EN LA CHOPERA");
                //comboBoxArticulo.requestFocus();

            }
        } else if (a.getId_tipoProd1() == 3) {
            System.out.println("ES PROMOCION, SE REVISARA SI HAY CERVEZA");
            ArrayList<Object[]> promocion = ma.traerArticulosPromocion(a.getCodigo());

            for (int i = 0; i < promocion.size(); i++) {
                Articulo promo = new Articulo();
                promo = ma.traerArticuloPorCodigo(promocion.get(i)[0].toString());
                System.out.println("ARTICULO DE LA PROMO: " + promo.getDesc_articulo());
                if (promo.getId_tipoProd2() == 5) {
                    System.out.println("se selecciono cerveza artesanal");
                    if (ma.comprobarBarril(promo.getDesc_articulo())) {
                        System.out.println("hay cerveza " + a.getDesc_articulo());
                        txtCantidad.setEditable(true);
                        txtCantidad.requestFocus();

                    } else {
                        txtCantidad.setEditable(false);
                        txtCodigo.requestFocus();
                        JOptionPane.showMessageDialog(null, "NO HAY CERVEZA " + promo.getDesc_articulo() + " CARGADA EN LA CHOPERA");
                    }
                }
            }

        } else {
            txtCantidad.setEditable(true);
            txtCantidad.requestFocus();
        }


        /*
        this.comboBoxArticulo.getEditor().getEditorComponent().addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent mEvt) {

            }

        });*/

    }//GEN-LAST:event_comboBoxArticuloActionPerformed

    private void btnImprimirTicketActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImprimirTicketActionPerformed
        // TODO add your handling code here:
        boolean ban = true;

        if (t != 4 && t != 5) {
            if (t == 2 && Integer.parseInt(txtMesa.getText()) == boleta.getMesa()) {
                System.out.println("ES LA MISMA MESA PREVIAMENTE CARGADA O ES DELIVERY");
                //JOptionPane.showMessageDialog(null, "LA MESA YA SE ENCUENTRA OCUPADA");
            } else {
                System.out.println("SON DISTINTAS MESAS O ES MESA NUEVA");

                for (int i = 0; i < dmmo.getRowCount() && ban; i++) {
                    if (Integer.parseInt(dmmo.getValueAt(i, 1).toString()) == Integer.parseInt(txtMesa.getText())) {
                        ban = false;
                    }
                }
            }
        }

        if (!ban) {
            System.out.println("----------------LA MESA YA SE ENCUENTRA OCUPADA------------------");
            JOptionPane.showMessageDialog(null, "LA MESA YA SE ENCUENTRA OCUPADA");
        } else {
            if (t != 3 && t != 6 && t != 7) {

                for (int i = 0; i < this.tablaVentaRapida.getRowCount(); i++) {

                    int id_det = Integer.parseInt(tablaVentaRapida.getValueAt(i, 0).toString());
                    a = ma.traerArticuloPorCodigo(tablaVentaRapida.getValueAt(i, 1).toString());
                    int cantidad = Integer.parseInt(tablaVentaRapida.getValueAt(i, 3).toString());

                    if (id_det != 0) {
                        Detalle_boleta db = new Detalle_boleta();
                        db = mf.traerDetalleUnico(id_det);
                        System.out.println("SON ARTICULOS YA CARGADOS");
                        System.out.println("CANTIDAD PREVIA: " + db.getCantidad());
                        System.out.println("CANTIDAD NUEVA: " + cantidad);
                        cantidad = cantidad - db.getCantidad();
                    }

                    if (a.getId_tipoProd2() == 5) {
                        //ma.descontarPintaBarril((float) (cantidadAgregada * 0.5), ma.obtenerPosicionBarril(a.getDesc_articulo()), a.getDesc_articulo(), dB);

                        Barril barril = new Barril();
                        barril.setPosicion(ma.obtenerPosicionBarril(a.getDesc_articulo()));
                        barril.setCantidad_consumida((float) (cantidad * 0.5));
                        barril.setDesc_barril(a.getDesc_articulo());
                        System.out.println("SE AGREGO BARRIL EN ZONA 1 " + barril.getDesc_barril() + " LITROS: " + barril.getCantidad_consumida());
                        pintasReemplazo.add(barril);
                    } else if (a.getId_tipoProd1() == 3) {
                        promocion = ma.traerArticulosPromocion(a.getCodigo());

                        for (int k = 0; k < promocion.size(); k++) {
                            Articulo prom = new Articulo();
                            prom = ma.traerArticuloPorCodigo(promocion.get(k)[0].toString());

                            if (prom.getId_tipoProd2() == 5) {
                                Barril barril = new Barril();
                                barril.setPosicion(ma.obtenerPosicionBarril(prom.getDesc_articulo()));
                                int cant = Integer.parseInt(promocion.get(k)[2].toString());
                                //barril.setCantidad_consumida((float) (cant * 0.5));
                                barril.setCantidad_consumida((float) (cantidad * cant * 0.5));
                                barril.setDesc_barril(prom.getDesc_articulo());
                                System.out.println("SE AGREGO BARRIL EN ZONA 2 " + barril.getDesc_barril() + " LITROS: " + barril.getCantidad_consumida());
                                pintasReemplazo.add(barril);
                                System.out.println("SE AGREGO PINTA DE LA PROMOCION HABIENDO HECHO UN ADICIONAL");
                            }
                        }
                    }

                }

                if (t == 1 | t == 4) {//si se acaba de abrir esta mesa
                    String busquedaCliente = this.comboBoxCliente.getEditor().getItem().toString();
                    System.out.println("CLIENTE A BUSCAR: " + busquedaCliente);
                    cli = mcli.obtenerCliente(busquedaCliente);
                    //System.out.println("CLIENTE COMPRADOR: "+cli.getNombre_cliente()+" "+cli.getApellido_cliente());

                    String busquedaMozo = this.comboBoxMozo.getEditor().getItem().toString();
                    user = mu.traerUsuarioString(busquedaMozo);

                    boleta.setCliente(cli);
                    boleta.setFecha_boleta(mfechas.obtenerFechaActual());
                    boleta.setHora_apertura(mfechas.obtenerHoraActual());
                    boleta.setVendedor(user);
                    boleta.setTotal(totalTicket);
                    boleta.setHora_cierre("00:00:00");
                    boleta.setFecha_cierre(mfechas.obtenerFechaActual());
                    if (t == 1) {
                        System.out.println("------- SE VA A INSERTAR LA MESA NUEVA -----");
                        boleta.setMesa(Integer.parseInt(txtMesa.getText()));
                        mf.insertarBoleta(boleta, 2);
                        //boleta.setMesa(Integer.parseInt(this.txtMesa.getText()));
                    } else {
                        boleta.setMesa(0);
                        boleta.setDireccion_delivery(txtDomicilio.getText());
                        boleta.setId_tipoVta(2);
                        boleta.setPago_delivery(totalPaga);
                        mf.insertarDelivery(boleta, 2);
                    }
                    boleta.setId_boleta(mf.obtenerID_boleta());

                } else if (t != 5) {
                    if (Integer.parseInt(txtMesa.getText()) != boleta.getMesa()) {
                        mf.actualizarMesa(boleta.getId_boleta(), Integer.parseInt(txtMesa.getText()));
                        System.out.println("SON DISTINTAS");
                    } else {
                        System.out.println("SON IGUALES");
                    }
                }

                for (int i = 0; i < this.tablaVentaRapida.getRowCount(); i++) {
                    int id_a_modificar = Integer.parseInt(tablaVentaRapida.getValueAt(i, 0).toString());
                    Detalle_boleta detalleNuevo = new Detalle_boleta();

                    int cantidad = Integer.parseInt(tablaVentaRapida.getValueAt(i, 3).toString());
                    float precio = 0;
                    if (a.getId_tipoProd1() != 4) {
                        precio = Float.parseFloat(this.tablaVentaRapida.getValueAt(i, 4).toString());
                    } else {
                        precio = Float.parseFloat(this.tablaVentaRapida.getValueAt(i, 5).toString());
                        System.out.println("el descuento es de: " + precio);
                    }

                    String codigo = tablaVentaRapida.getValueAt(i, 1).toString();

                    detalleNuevo.setId_boleta(boleta.getId_boleta());
                    detalleNuevo.setPrecio(precio);
                    detalleNuevo.setCantidad(cantidad);
                    detalleNuevo.setId_detalleboleta(id_a_modificar);
                    detalleNuevo.setCodigo(codigo);

                    if (id_a_modificar == 0) {
                        System.out.println("SE DEBE AÑADIR ESTE ARTICULO");
                        mf.insertarDetallesBoleta(detalleNuevo, dB);

                    } else {
                        System.out.println("ESTE ARTICULO SE DEBE ACTUALIZAR");
                        mf.actualizarDetallesBoleta(detalleNuevo, dB);
                    }
                }

                if (t == 2 || t == 5) {
                    float acumulado = 0;

                    for (int k = 0; k < this.tablaVentaRapida.getRowCount(); k++) {
                        acumulado = acumulado + Float.parseFloat(this.tablaVentaRapida.getValueAt(k, 5).toString());
                    }

                    totalTicket = acumulado;

                    if (t == 5) {
                        String busquedaMozo = this.comboBoxMozo.getEditor().getItem().toString();
                        user = mu.traerUsuarioString(busquedaMozo);
                        if (user.getId_usuario() != boleta.getVendedor().getId_usuario()) {
                            System.out.println("SON DISTINTOS DELIVERYS");
                            mf.actualizarDelivery(boleta.getId_boleta(), user.getId_usuario());
                        }
                        mf.actualizarPagoDelivery(boleta.getId_boleta(), totalPaga);

                        if (!boleta.getDireccion_delivery().equals(txtDomicilio.getText())) {
                            System.out.println("SE MODIFICO LA DIRECCION");
                            mf.actualizarDireccionDelivery(boleta.getId_boleta(), txtDomicilio.getText());
                        } else {
                            System.out.println("NO SE MODIFICO LA DIRECCION");
                        }
                    }
                    if (boleta.getTotal() != totalTicket) {
                        System.out.println("se agrego o quito elementos, el total es diferente");
                        System.out.println("PAGA CON: " + totalPaga);
                        //mf.actualizarPagoDelivery(boleta.getId_boleta(), totalPaga);
                        mf.actualizarTotalBoleta(boleta.getId_boleta(), acumulado);
                    } else {
                        System.out.println("no se modifico nada");
                    }

                    //mf.actualizarTotalBoleta(boleta.getId_boleta(), acumulado);
                    for (int j = 0; j < dmmo.getRowCount(); j++) {
                        if (Integer.parseInt(dmmo.getValueAt(j, 0).toString()) == boleta.getId_boleta()) {
                            dmmo.setValueAt(acumulado, j, 3);
                            dmmo.setValueAt(txtMesa.getText(), j, 1);
                        }
                    }
                }

                mt.traerMesasOcupadas((Inicio.ModeloTablaMesasOcupadas) dmmo);
                mt.traerDeliverys((Inicio.ModeloTablaDelivery) dmtd);
                ma.actualizarBarriles(pintasReemplazo, dB);
                pintasReemplazo.clear();
                if (t != 5) {
                    JOptionPane.showMessageDialog(null, "MESA GUARDADA CON EXITO");
                } else {
                    JOptionPane.showMessageDialog(null, "DELIVERY GUARDADO CON EXITO");
                }

                //this.dispose();

                //IMPRIME AHORA EL TICKET CORRESPONDIENTE
                boleta.setTotal(totalTicket);
                arrayDetalles = mf.traerDetalle(boleta.getId_boleta());
                //System.out.println("FECHA Y HORA DE CIERRE: " + mfechas.convertirFecha(mfechas.convertirFechaInversa(boleta.getFecha_cierre())) + " " + boleta.getHora_cierre().substring(0, 5));
                //System.out.println("FECHA Y HORA DE CIERRE: " + mfechas.convertirFecha(mfechas.convertirFechaInversa(boleta.getFecha_cierre())) + " " + mfechas.obtenerHoraActual().substring(0, 5));
                System.out.println("FECHA Y HORA DE CIERRE: " + mfechas.convertirFecha(mfechas.convertirFechaInversa(boleta.getFecha_cierre())) + " " + mfechas.obtenerHoraActual());
                //System.out.println("FECHA Y HORA DE CIERRE: " + boleta.getFecha_cierre() + " " + boleta.getHora_cierre().substring(0, 5));
                //JOptionPane.showMessageDialog(null, "TICKET IMPRESO CON EXITO");

                if (t != 5) {
                    mi.imprimirFactura(boleta, boleta.getCliente(), arrayDetalles);
                } else {
                    //mi.imprimirDelivery(boleta, boleta.getCliente(), arrayDetalles);

                    if (!checkComanda.isSelected() && !checkPedido.isSelected()) {
                        JOptionPane.showMessageDialog(null, "SELECCIONE AL MENOS UNA OPCION DE IMPRESIÓN");
                    } else if (checkComanda.isSelected() && checkPedido.isSelected()) {
                        mi.imprimirDelivery(boleta, boleta.getCliente(), arrayDetalles, 2);
                        try {
                            Thread.sleep(4 * 1000);
                        } catch (Exception e) {
                            System.out.println(e);
                        }
                        mi.imprimirTicketDelivery(boleta, boleta.getCliente(), arrayDetalles, 2);
                    } else {
                        if (checkComanda.isSelected()) {
                            mi.imprimirDelivery(boleta, boleta.getCliente(), arrayDetalles, 2);
                        }
                        if (checkPedido.isSelected()) {
                            mi.imprimirTicketDelivery(boleta, boleta.getCliente(), arrayDetalles, 2);
                        }
                    }

                }

                //this.dispose();
            } else {
                System.out.println("Es reimpresion de ticket");
                System.out.println("ES VENTA POR MESA");
                arrayDetalles = mf.traerDetalle(boleta.getId_boleta());
                System.out.println("FECHA Y HORA DE CIERRE: " + mfechas.convertirFecha(mfechas.convertirFechaInversa(boleta.getFecha_cierre())) + " " + boleta.getHora_cierre().substring(0, 5));
                if (t == 3 || t == 7) {
                    mi.imprimirFactura(boleta, boleta.getCliente(), arrayDetalles);
                } else {
                    mi.imprimirTicketDelivery(boleta, boleta.getCliente(), arrayDetalles, 0);
                }

            }

        }
    }//GEN-LAST:event_btnImprimirTicketActionPerformed

    private void txtCantidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCantidadActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCantidadActionPerformed

    private void txtTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotalActionPerformed

    private void txtPagaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPagaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPagaActionPerformed

    private void txtPagaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPagaKeyReleased
        // TODO add your handling code here:
        /*if ((evt.getKeyCode() >= 48 && evt.getKeyCode() <= 57) || (evt.getKeyCode() >= 96 && evt.getKeyCode() <= 105) || evt.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
            //System.out.println("presiono un numero de pad");
            String cadena = txtPaga.getText();

            totalPaga = Float.parseFloat(txtPaga.getText());
            float vuelto;
            vuelto = totalPaga - totalTicket;
            txtVuelto.setText("$ " + String.valueOf(vuelto));

        }else{
            String cadena = txtPaga.getText();
            if (mco.isNumeric(cadena)) {
                this.btnGuardarCambios.setEnabled(true);
            }else{
                this.btnGuardarCambios.setEnabled(false);                
            }
        }*/

        String cadena = txtPaga.getText();
        if (mco.isNumeric(cadena)) {
            totalPaga = Float.parseFloat(txtPaga.getText());
            System.out.println("PAGA CON: " + totalPaga);
            float vuelto;
            vuelto = totalPaga - totalTicket;
            txtVuelto.setText("$ " + String.valueOf(vuelto));
            System.out.println("EL TIPO INGRESADO ES: " + t);
            if ((t == 4 || t == 5)) {
                System.out.println("ENTRO AQUI CON EL TIPO: " + t);
                if (totalPaga >= totalTicket) {
                    txtVuelto.setBackground(Color.GREEN);
                    this.btnGuardarCambios.setEnabled(true);
                    this.btnCerrarCuenta.setEnabled(true);

                } else {
                    txtVuelto.setBackground(Color.RED);
                    this.btnGuardarCambios.setEnabled(false);
                    this.btnCerrarCuenta.setEnabled(false);
                }
            }

        } else if ((t == 4 || t == 5)) {
            this.btnGuardarCambios.setEnabled(false);
            this.btnCerrarCuenta.setEnabled(false);
        }


    }//GEN-LAST:event_txtPagaKeyReleased

    private void txtVueltoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtVueltoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtVueltoActionPerformed

    private void txtVueltoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtVueltoKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtVueltoKeyReleased

    private void txtPagaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPagaKeyPressed
        // TODO add your handling code here:
        /*if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            String cadena = txtPaga.getText();
            if (mco.isNumeric(cadena)) {

                totalPaga = Float.parseFloat(txtPaga.getText());
                float vuelto = 0;

                if (totalPaga < this.totalTicket) {
                    JOptionPane.showMessageDialog(null, "EL MONTO A PAGAR NO PUEDE SER MENOS QUE EL TOTAL");
                } else {
                    txtPaga.setText(String.valueOf(totalPaga));
                    vuelto = totalPaga - totalTicket;
                    txtVuelto.setText("$ " + String.valueOf(vuelto));
                }
            } else {
                JOptionPane.showMessageDialog(null, "INGRESE UN NUMERO");
            }

        }*/
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

        }

    }//GEN-LAST:event_txtPagaKeyPressed

    private void txtDomicilioKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDomicilioKeyReleased
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtCodigo.requestFocus();
            //txtCodigo.requestFocus();
        }
    }//GEN-LAST:event_txtDomicilioKeyReleased

    private void txtFechaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFechaKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtFechaKeyReleased

    private void txtFechaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFechaKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtFechaKeyPressed

    private void txtFechaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtFechaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtFechaActionPerformed

    private void txtMesaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMesaKeyReleased
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            comboBoxCliente.requestFocus();
            //txtCodigo.requestFocus();
        } else if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
            System.out.println("SE APRETO TECLA ESCAPE");
            this.dispose();
        }
    }//GEN-LAST:event_txtMesaKeyReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(NuevaMesa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(NuevaMesa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(NuevaMesa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(NuevaMesa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new NuevaMesa(null, null, false, null, null, null, null, 0, null).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCerrarCuenta;
    private javax.swing.JButton btnGuardarCambios;
    private javax.swing.JButton btnImprimirTicket;
    private javax.swing.JCheckBox checkComanda;
    private javax.swing.JCheckBox checkPedido;
    private javax.swing.JComboBox<String> comboBoxArticulo;
    private javax.swing.JComboBox<String> comboBoxCliente;
    private javax.swing.JComboBox<String> comboBoxMozo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labelMozo;
    private javax.swing.JLabel labelNro;
    private javax.swing.JLabel labelNro1;
    private javax.swing.JLabel labelPaga;
    private javax.swing.JRadioButton radioEfectivo;
    private javax.swing.JRadioButton radioOtros;
    private javax.swing.JTable tablaVentaRapida;
    private javax.swing.JTextField txtCantidad;
    private javax.swing.JTextField txtCodigo;
    private javax.swing.JTextField txtDomicilio;
    private javax.swing.JTextField txtFecha;
    private javax.swing.JTextField txtMesa;
    private javax.swing.JTextField txtPaga;
    private javax.swing.JTextField txtTotal;
    private javax.swing.JTextField txtVuelto;
    // End of variables declaration//GEN-END:variables
}
