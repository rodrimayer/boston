/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.manejadorArticulos;
import controlador.manejadorConfiguraciones;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import modelo.Tipo_Prod2;

/**
 *
 * @author Rodri Mayer
 */
public class AgregarTipo2 extends javax.swing.JFrame {

    /**
     * Creates new form AgregarTipo2
     */
    boolean start = true;
    manejadorConfiguraciones mc = new manejadorConfiguraciones();
    manejadorArticulos ma = new manejadorArticulos();
    ArrayList<Tipo_Prod2> tipos = new ArrayList<>();
    
    public AgregarTipo2() {
        initComponents();
        tipos = mc.traerTipoProd2(comboBoxVariables);
        ma.traerTipoProd(comboBoxTipo);
        this.comboBoxTipo.setSelectedIndex(-1);
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelVariables = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        comboBoxVariables = new javax.swing.JComboBox<>();
        botonEditarVariable = new javax.swing.JButton();
        panelAgregarVariable = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        txtNuevaVariable = new javax.swing.JTextField();
        botonAgregarVariable = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        comboBoxTipo = new javax.swing.JComboBox<>();
        botonNuevaVariable = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("AGREGAR / EDITAR TIPO");

        panelVariables.setBorder(javax.swing.BorderFactory.createTitledBorder("Modificacion de Tipos"));

        jLabel1.setText("Variable: ");

        comboBoxVariables.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxVariablesActionPerformed(evt);
            }
        });

        botonEditarVariable.setText("EDITAR");
        botonEditarVariable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEditarVariableActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelVariablesLayout = new javax.swing.GroupLayout(panelVariables);
        panelVariables.setLayout(panelVariablesLayout);
        panelVariablesLayout.setHorizontalGroup(
            panelVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelVariablesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(botonEditarVariable, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(panelVariablesLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(comboBoxVariables, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(20, Short.MAX_VALUE))
        );
        panelVariablesLayout.setVerticalGroup(
            panelVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelVariablesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(comboBoxVariables, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(botonEditarVariable)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelAgregarVariable.setBorder(javax.swing.BorderFactory.createTitledBorder("Agregar Tipo"));

        jLabel8.setText("Variable:");

        txtNuevaVariable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNuevaVariableActionPerformed(evt);
            }
        });
        txtNuevaVariable.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtNuevaVariableKeyReleased(evt);
            }
        });

        botonAgregarVariable.setText("AGREGAR");
        botonAgregarVariable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAgregarVariableActionPerformed(evt);
            }
        });

        jLabel2.setText("Tipo:");

        javax.swing.GroupLayout panelAgregarVariableLayout = new javax.swing.GroupLayout(panelAgregarVariable);
        panelAgregarVariable.setLayout(panelAgregarVariableLayout);
        panelAgregarVariableLayout.setHorizontalGroup(
            panelAgregarVariableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelAgregarVariableLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelAgregarVariableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(botonAgregarVariable, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(panelAgregarVariableLayout.createSequentialGroup()
                        .addGroup(panelAgregarVariableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addComponent(jLabel2))
                        .addGap(18, 18, 18)
                        .addGroup(panelAgregarVariableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtNuevaVariable, javax.swing.GroupLayout.DEFAULT_SIZE, 151, Short.MAX_VALUE)
                            .addComponent(comboBoxTipo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(45, 45, 45))
        );
        panelAgregarVariableLayout.setVerticalGroup(
            panelAgregarVariableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelAgregarVariableLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(panelAgregarVariableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txtNuevaVariable, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panelAgregarVariableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(comboBoxTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(botonAgregarVariable)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        botonNuevaVariable.setText("NUEVA VARIABLE");
        botonNuevaVariable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonNuevaVariableActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(panelVariables, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelAgregarVariable, javax.swing.GroupLayout.PREFERRED_SIZE, 252, Short.MAX_VALUE)
                    .addComponent(botonNuevaVariable, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 17, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelVariables, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(panelAgregarVariable, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(botonNuevaVariable)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void comboBoxVariablesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxVariablesActionPerformed
        // TODO add your handling code here:
        //mc.traerVariables(comboBoxVariables);
        //String tipo = this.comboBoxTipoArticulo.getSelectedItem().toString();
        System.out.println(start);
        int index = this.comboBoxVariables.getSelectedIndex();
        System.out.println("INDEX SELECCIONADO: " + index);

        if (!start && index != -1) {
            int id_variable = tipos.get(index).getId_tipoProd2();
            this.txtNuevaVariable.setText(tipos.get(index).getDesc_tipoProd2());
            //this.txtNuevaVariable.setEditable(false);
            System.out.println("ID: "+tipos.get(index).getId_tipoProd2()+" DESC: "+tipos.get(index).getDesc_tipoProd2()+" ");
            this.comboBoxTipo.setSelectedIndex(tipos.get(index).getId_tipoProd1()-1);
            System.out.println("entro al if");
            //this.botonEditarVariable.setEnabled(true);
            this.botonAgregarVariable.setText("ACTUALIZAR");

        } else {
            System.out.println("no entro al if");
            start = false;
            this.botonAgregarVariable.setText("AGREGAR");
        }
    }//GEN-LAST:event_comboBoxVariablesActionPerformed

    private void botonEditarVariableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEditarVariableActionPerformed
        // TODO add your handling code here:
        System.out.println(start);
        int index = this.comboBoxVariables.getSelectedIndex();
        System.out.println("INDEX SELECCIONADO: " + index);

        if (index != -1) {
            int id_variable = tipos.get(index).getId_tipoProd2();
            this.txtNuevaVariable.setText(tipos.get(index).getDesc_tipoProd2());
            //this.txtNuevaVariable.setEditable(false);
            this.comboBoxTipo.setSelectedIndex(tipos.get(index).getId_tipoProd1()-1);
            System.out.println("entro al if");
            //this.botonEditarVariable.setEnabled(true);
            this.botonAgregarVariable.setText("ACTUALIZAR");

        } else {
            System.out.println("no entro al if");
            start = false;
            this.botonAgregarVariable.setText("AGREGAR");
        }
    }//GEN-LAST:event_botonEditarVariableActionPerformed

    private void txtNuevaVariableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNuevaVariableActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNuevaVariableActionPerformed

    private void txtNuevaVariableKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNuevaVariableKeyReleased
        // TODO add your handling code here:
        if (this.txtNuevaVariable.getText().equals("")) {
            this.botonAgregarVariable.setEnabled(false);
        } else {
            this.botonAgregarVariable.setEnabled(true);
        }
    }//GEN-LAST:event_txtNuevaVariableKeyReleased

    private void botonAgregarVariableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAgregarVariableActionPerformed
        // TODO add your handling code here:
        Tipo_Prod2 c = new Tipo_Prod2();

        c.setDesc_tipoProd2(this.txtNuevaVariable.getText());
        c.setId_tipoProd1(this.comboBoxTipo.getSelectedIndex()+1);

        if (this.comboBoxVariables.getSelectedIndex() == -1) {
            int n = ma.insertarTipoProd2(c);

            if (n > 0) {
                JOptionPane.showMessageDialog(null, "VARIABLE AGREGADA CON EXITO");
            } else {
                JOptionPane.showMessageDialog(null, "ERROR AL AÑADIR VARIABLE");
            }

        } else {
            int index = this.comboBoxVariables.getSelectedIndex();
            c.setId_tipoProd2(tipos.get(index).getId_tipoProd2());
            int n = ma.actualizarTipo2(c);

            if (n > 0) {
                JOptionPane.showMessageDialog(null, "VARIABLE MODIFICADA CON EXITO");
            } else {
                JOptionPane.showMessageDialog(null, "ERROR AL MODIFICAR VARIABLE");
            }
            
        }

        //this.txtNuevaVariable.setText("");
        this.dispose();
        AgregarTipo2 co = new AgregarTipo2();
        co.setVisible(true);
        co.setResizable(false);
        co.setLocationRelativeTo(null);
    }//GEN-LAST:event_botonAgregarVariableActionPerformed

    private void botonNuevaVariableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonNuevaVariableActionPerformed
        // TODO add your handling code here:
        this.comboBoxVariables.setSelectedIndex(-1);
        this.txtNuevaVariable.setText("");
        this.comboBoxTipo.setSelectedIndex(-1);
        this.botonAgregarVariable.setText("AGREGAR");
        this.txtNuevaVariable.setEditable(true);
    }//GEN-LAST:event_botonNuevaVariableActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AgregarTipo2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AgregarTipo2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AgregarTipo2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AgregarTipo2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AgregarTipo2().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonAgregarVariable;
    private javax.swing.JButton botonEditarVariable;
    private javax.swing.JButton botonNuevaVariable;
    private javax.swing.JComboBox<String> comboBoxTipo;
    private javax.swing.JComboBox<String> comboBoxVariables;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel panelAgregarVariable;
    private javax.swing.JPanel panelVariables;
    private javax.swing.JTextField txtNuevaVariable;
    // End of variables declaration//GEN-END:variables
}
