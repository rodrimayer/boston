/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.manejadorConfiguraciones;
import controlador.manejadorFacturacion;
import controlador.manejadorFechas;
import controlador.manejadorImpresion;
import controlador.manejadorReportes;
import controlador.manejadorTablas;
import java.util.ArrayList;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.Cierre_Caja;
import modelo.Usuario;

/**
 *
 * @author Rodri Mayer
 */
public class CierreCajaResumido extends javax.swing.JFrame {

    /**
     * Creates new form CierreCajaResumido
     */
    public class ModeloTablaVentasTipo extends DefaultTableModel {

        public boolean isCellEditable(int row, int column) {
            return false;
        }
    }

    public class ModeloTablaVentasCategoria extends DefaultTableModel {

        public boolean isCellEditable(int row, int column) {
            return false;
        }
    }

    public class ModeloTablaVentaComida extends DefaultTableModel {

        public boolean isCellEditable(int row, int column) {
            return false;
        }
    }

    public class ModeloTablaVentaBebida extends DefaultTableModel {

        public boolean isCellEditable(int row, int column) {
            return false;
        }
    }

    public class ModeloTablaGastos extends DefaultTableModel {

        public boolean isCellEditable(int row, int column) {
            return false;
        }
    }

    public class ModeloTablaFacturacion extends DefaultTableModel {

        public boolean isCellEditable(int row, int column) {
            return false;
        }
    }

    public class ModeloTablaPromociones extends DefaultTableModel {

        public boolean isCellEditable(int row, int column) {
            return false;
        }
    }

    public class ModeloTablaDescuentos extends DefaultTableModel {

        public boolean isCellEditable(int row, int column) {
            return false;
        }
    }

    ModeloTablaVentasTipo modeloTablaVentasTipo = new ModeloTablaVentasTipo();
    ModeloTablaVentaComida modeloTablaVentaComida = new ModeloTablaVentaComida();
    ModeloTablaVentaBebida modeloTablaVentaBebida = new ModeloTablaVentaBebida();
    ModeloTablaPromociones modeloTablaVentaPromociones = new ModeloTablaPromociones();
    ModeloTablaDescuentos modeloTablaVentaDescuentos = new ModeloTablaDescuentos();
    ModeloTablaVentasCategoria modeloTablaVentasCategoria = new ModeloTablaVentasCategoria();
    ModeloTablaGastos modeloTablaGastos = new ModeloTablaGastos();
    ModeloTablaFacturacion modeloTablaFacturacion = new ModeloTablaFacturacion();

    manejadorTablas mt = new manejadorTablas();
    manejadorFechas mf = new manejadorFechas();
    manejadorFacturacion mfac = new manejadorFacturacion();
    manejadorReportes mr = new manejadorReportes();
    manejadorConfiguraciones mconf = new manejadorConfiguraciones();

    ArrayList<Object[]> ventas = new ArrayList<>();
    ArrayList<Object[]> catVentas = new ArrayList<>();
    ArrayList<Object[]> comidas = new ArrayList<>();
    ArrayList<Object[]> bebidas = new ArrayList<>();
    ArrayList<Object[]> promociones = new ArrayList<>();
    ArrayList<Object[]> descuentos = new ArrayList<>();
    ArrayList<Object[]> costos = new ArrayList<>();
    ArrayList<Object[]> facVtas = new ArrayList<>();
    ArrayList<Object[]> facCostos = new ArrayList<>();

    Usuario user = new Usuario();
    Inicio inicio;

    public CierreCajaResumido(Usuario u, Inicio i) {
        initComponents();
        this.setTitle("CIERRE DE CAJA | BOSTON BEER & CO.");
        float ganancia = 0;
        user = u;
        inicio = i;

        mt.modeloTablaVentasTipo(tablaVentasTipo, modeloTablaVentasTipo);
        mt.modeloTablaVentasCategoria(tablaVentasCategoria, modeloTablaVentasCategoria);
        mt.modeloTablaGastos(tablaGastos, modeloTablaGastos);
        ganancia = mt.modeloTablaFacturacion(tablaFacturacion, modeloTablaFacturacion);

        this.labelGanancia.setText("$ " + ganancia);
        this.labelResponsable.setText(u.getNombre_usuario() + " " + u.getApellido_usuario());
        this.labelFechaCierre.setText(mf.convertirFecha(mf.obtenerFechaActual()));

        menuCierre.removeAll();

        JMenu menu = new JMenu("Menu");
        menuCierre.add(menu);

        JMenuItem busqueda = new JMenuItem("Busqueda (1)");
        menu.add(busqueda);

        busqueda.addActionListener(e -> {
            BusquedaCierres bc = new BusquedaCierres();
            bc.setVisible(true);
            bc.setLocationRelativeTo(null);
            bc.setResizable(false);
        });

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        labelFechaCierre = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        labelResponsable = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        labelGanancia = new javax.swing.JLabel();
        panelPorTipo = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaVentasTipo = new javax.swing.JTable();
        panelGastos = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tablaGastos = new javax.swing.JTable();
        panelFacturacion = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        tablaFacturacion = new javax.swing.JTable();
        botonCerrarCaja = new javax.swing.JButton();
        botonResumen = new javax.swing.JButton();
        panelPorTipo1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablaVentasCategoria = new javax.swing.JTable();
        menuCierre = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Cierre Caja - Resumen"));

        jLabel2.setText("Fecha Cierre: ");

        labelFechaCierre.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        labelFechaCierre.setText("jLabel3");

        jLabel5.setText("Realizado por:");

        labelResponsable.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        labelResponsable.setText("jLabel6");

        jLabel6.setText("Ganancia Neta:");

        labelGanancia.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        labelGanancia.setText("jLabel6");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(labelFechaCierre, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(labelResponsable, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37)
                .addComponent(jLabel6)
                .addGap(18, 18, 18)
                .addComponent(labelGanancia, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5)
                        .addComponent(labelResponsable))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2)
                        .addComponent(labelGanancia)
                        .addComponent(labelFechaCierre, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel6)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelPorTipo.setBorder(javax.swing.BorderFactory.createTitledBorder("Por Categoria"));

        tablaVentasTipo.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tablaVentasTipo);

        javax.swing.GroupLayout panelPorTipoLayout = new javax.swing.GroupLayout(panelPorTipo);
        panelPorTipo.setLayout(panelPorTipoLayout);
        panelPorTipoLayout.setHorizontalGroup(
            panelPorTipoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPorTipoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 365, Short.MAX_VALUE)
                .addContainerGap())
        );
        panelPorTipoLayout.setVerticalGroup(
            panelPorTipoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPorTipoLayout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
                .addContainerGap())
        );

        panelGastos.setBorder(javax.swing.BorderFactory.createTitledBorder("Gastos"));

        tablaGastos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane4.setViewportView(tablaGastos);

        javax.swing.GroupLayout panelGastosLayout = new javax.swing.GroupLayout(panelGastos);
        panelGastos.setLayout(panelGastosLayout);
        panelGastosLayout.setHorizontalGroup(
            panelGastosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelGastosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );
        panelGastosLayout.setVerticalGroup(
            panelGastosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelGastosLayout.createSequentialGroup()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 138, Short.MAX_VALUE)
                .addContainerGap())
        );

        panelFacturacion.setBorder(javax.swing.BorderFactory.createTitledBorder("Facturacion"));

        tablaFacturacion.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane6.setViewportView(tablaFacturacion);

        javax.swing.GroupLayout panelFacturacionLayout = new javax.swing.GroupLayout(panelFacturacion);
        panelFacturacion.setLayout(panelFacturacionLayout);
        panelFacturacionLayout.setHorizontalGroup(
            panelFacturacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelFacturacionLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );
        panelFacturacionLayout.setVerticalGroup(
            panelFacturacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelFacturacionLayout.createSequentialGroup()
                .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 138, Short.MAX_VALUE)
                .addContainerGap())
        );

        botonCerrarCaja.setText("CERRAR CAJA");
        botonCerrarCaja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonCerrarCajaActionPerformed(evt);
            }
        });

        botonResumen.setText("RESUMEN CAJA");
        botonResumen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonResumenActionPerformed(evt);
            }
        });

        panelPorTipo1.setBorder(javax.swing.BorderFactory.createTitledBorder("Por Tipo"));

        tablaVentasCategoria.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tablaVentasCategoria);

        javax.swing.GroupLayout panelPorTipo1Layout = new javax.swing.GroupLayout(panelPorTipo1);
        panelPorTipo1.setLayout(panelPorTipo1Layout);
        panelPorTipo1Layout.setHorizontalGroup(
            panelPorTipo1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPorTipo1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 365, Short.MAX_VALUE)
                .addContainerGap())
        );
        panelPorTipo1Layout.setVerticalGroup(
            panelPorTipo1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPorTipo1Layout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );

        jMenu1.setText("File");
        menuCierre.add(jMenu1);

        jMenu2.setText("Edit");
        menuCierre.add(jMenu2);

        setJMenuBar(menuCierre);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(botonResumen, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(panelPorTipo1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(panelGastos, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(panelFacturacion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(panelPorTipo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(botonCerrarCaja, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {panelPorTipo, panelPorTipo1});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(panelPorTipo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelPorTipo1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelGastos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelFacturacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botonResumen)
                    .addComponent(botonCerrarCaja))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelPorTipo.getAccessibleContext().setAccessibleName("Por Categoria");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonCerrarCajaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonCerrarCajaActionPerformed
        // TODO add your handling code here:

        int cantMesasSinCerrar = this.inicio.modeloMesasOcupadas.getRowCount();
        int cantDeliverysSinCerrar = this.inicio.modeloDelivery.getRowCount();

        if (cantMesasSinCerrar != 0 || cantDeliverysSinCerrar != 0) {
            JOptionPane.showMessageDialog(null, "EXISTEN MESAS Y/O DELIVERYS SIN CERRAR \n"
                    + "POR FAVOR, CIERRE LOS MOVIMIENTOS PENDIENTES \n"
                    + "ANTES DE CERRAR LA CAJA");
        } else {

            manejadorImpresion mi = new manejadorImpresion();

            ventas = mfac.traerVentasTipo();
            catVentas = mfac.traerVentasCat();
            comidas = mfac.traerVentasComida();
            bebidas = mfac.traerVentasBebida();
            promociones = mfac.traerVentasPromocion();
            descuentos = mfac.traerVentasDescuento();
            costos = mfac.traerCostos();
            facVtas = mfac.traerFacturacionVentas();
            facCostos = mfac.traerFacturacionCostos();

            if (facVtas.size() >= facCostos.size()) {

                for (int i = 0; i < facVtas.size(); i++) {

                    for (int j = 0; j < facCostos.size(); j++) {

                        if (facVtas.get(i)[0].equals(facCostos.get(j)[0])) {
                            facVtas.get(i)[2] = facCostos.get(j)[2];
                        }

                    }

                    facVtas.get(i)[3] = Float.parseFloat(facVtas.get(i)[1].toString()) - Float.parseFloat(facVtas.get(i)[2].toString());

                    System.out.println(facVtas.get(i)[0] + " " + facVtas.get(i)[1] + " " + facVtas.get(i)[2] + " " + facVtas.get(i)[3]);
                }

                int o = JOptionPane.showConfirmDialog(null, "Está seguro que desea realizar el cierre de Caja?\n"
                        + "ATENCION! Esta opción no tiene vuelta atras");

                if (o == 0) {
                    Cierre_Caja cj = new Cierre_Caja();
                    cj.setFecha_cierre(mf.obtenerFechaActual());
                    cj.setHora_cierre(mf.obtenerHoraActual());
                    cj.setVenta_contado(mfac.traerVentasContado());
                    cj.setVenta_debito(mfac.traerVentasDebito());
                    cj.setVenta_credito(mfac.traerVentasCredito());
                    cj.setCosto_contado(mfac.traerCostosContado());
                    cj.setCosto_debito(mfac.traerCostosDebito());
                    cj.setCosto_credito(mfac.traerCostosCredito());

                    mfac.cerrarCaja(cj);

                    if (mconf.valorVariableString("Impresion caja").getValor_configuracion() == 1) {
                        System.out.println("entro a imprimir el cierre");
                        mi.imprimirCierreCaja(user, ventas, comidas, bebidas, promociones, descuentos, costos, facVtas, cj, catVentas);
                    }

                    JOptionPane.showMessageDialog(null, "CAJA CERRADA CON ÉXITO");
                    this.dispose();

                    int cant = inicio.modelo.getRowCount();

                    for (int i = 0; i < cant; i++) {
                        inicio.modelo.removeRow(0);
                    }

                    mt.traerVentasSinCerrar(inicio.modelo);
                    mr.ventasActuales(inicio.datosVentas);
                    inicio.datosVentas.remove("Comida");
                    inicio.datosVentas.remove("Bebida");
                    inicio.datosVentas.remove("Promocion");

                }

            } else {

                for (int i = 0; i < facCostos.size(); i++) {

                    for (int j = 0; j < facVtas.size(); j++) {

                        if (facCostos.get(i)[0].equals(facVtas.get(j)[0])) {
                            facCostos.get(i)[1] = facVtas.get(j)[1];
                        }

                    }

                    facCostos.get(i)[3] = Float.parseFloat(facCostos.get(i)[1].toString()) - Float.parseFloat(facCostos.get(i)[2].toString());
                    System.out.println(facCostos.get(i)[0] + " " + facCostos.get(i)[1] + " " + facCostos.get(i)[2] + " " + facCostos.get(i)[3]);
                }

                int o = JOptionPane.showConfirmDialog(null, "Está seguro que desea realizar el cierre de Caja?\n"
                        + "ATENCION! Esta opción no tiene vuelta atras");

                if (o == 0) {
                    Cierre_Caja cj = new Cierre_Caja();
                    cj.setFecha_cierre(mf.obtenerFechaActual());
                    cj.setHora_cierre(mf.obtenerHoraActual());
                    cj.setVenta_contado(mfac.traerVentasContado());
                    cj.setVenta_debito(mfac.traerVentasDebito());
                    cj.setVenta_credito(mfac.traerVentasCredito());
                    cj.setCosto_contado(mfac.traerCostosContado());
                    cj.setCosto_debito(mfac.traerCostosDebito());
                    cj.setCosto_credito(mfac.traerCostosCredito());

                    mfac.cerrarCaja(cj);

                    if (mconf.valorVariableString("Impresion caja").getValor_configuracion() == 1) {
                        mi.imprimirCierreCaja(user, ventas, comidas, bebidas, promociones, descuentos, costos, facCostos, cj, catVentas);
                    }

                    JOptionPane.showMessageDialog(null, "CAJA CERRADA CON ÉXITO");
                    this.dispose();

                    int cant = inicio.modelo.getRowCount();

                    for (int i = 0; i < cant; i++) {
                        inicio.modelo.removeRow(0);
                    }

                    mt.traerVentasSinCerrar(inicio.modelo);
                    mr.ventasActuales(inicio.datosVentas);
                    inicio.datosVentas.remove("Comida");
                    inicio.datosVentas.remove("Bebida");
                    inicio.datosVentas.remove("Promocion");

                }

            }
        }


    }//GEN-LAST:event_botonCerrarCajaActionPerformed

    private void botonResumenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonResumenActionPerformed
        // TODO add your handling code here:
        ResumenCierre rc = new ResumenCierre(0, 0, 0);
        rc.setLocationRelativeTo(null);
        rc.setResizable(false);
        rc.setVisible(true);
    }//GEN-LAST:event_botonResumenActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CierreCajaResumido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CierreCajaResumido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CierreCajaResumido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CierreCajaResumido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CierreCajaResumido(null, null).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonCerrarCaja;
    private javax.swing.JButton botonResumen;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JLabel labelFechaCierre;
    private javax.swing.JLabel labelGanancia;
    private javax.swing.JLabel labelResponsable;
    private javax.swing.JMenuBar menuCierre;
    private javax.swing.JPanel panelFacturacion;
    private javax.swing.JPanel panelGastos;
    private javax.swing.JPanel panelPorTipo;
    private javax.swing.JPanel panelPorTipo1;
    private javax.swing.JTable tablaFacturacion;
    private javax.swing.JTable tablaGastos;
    private javax.swing.JTable tablaVentasCategoria;
    private javax.swing.JTable tablaVentasTipo;
    // End of variables declaration//GEN-END:variables
}
