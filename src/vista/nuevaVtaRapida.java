/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.manejadorArticulos;
import controlador.manejadorCliente;
import controlador.manejadorConfiguraciones;
import controlador.manejadorFacturacion;
import controlador.manejadorFechas;
import controlador.manejadorImpresion;
import controlador.manejadorTablas;
import controlador.manejadorUsuario;
import controlador.metodoBuscarArtNuevaMesa;
import controlador.metodoBusquedaArticulo;
import controlador.metodoBusquedaMozoVtaRapida;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.Articulo;
import modelo.Barril;
import modelo.Boleta;
import modelo.Cliente;
import modelo.Detalle_boleta;
import modelo.Usuario;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author Rodri Mayer
 */
public class nuevaVtaRapida extends javax.swing.JFrame {

    /**
     * Creates new form nuevaVtaRapida
     */
    public class ModeloTablaVtaRapida extends DefaultTableModel {

        public boolean isCellEditable(int row, int column) {
            return false;
        }
    }

    ModeloTablaVtaRapida modelo = new ModeloTablaVtaRapida();

    Articulo a = new Articulo();
    Boleta boleta = new Boleta();
    Cliente cli = new Cliente();
    Detalle_boleta db = new Detalle_boleta();
    Usuario user = new Usuario();
    ArrayList<Detalle_boleta> arrayDetalles = new ArrayList<Detalle_boleta>();
    ArrayList<Barril> pintasReemplazo = new ArrayList<Barril>();
    ArrayList<Object[]> promocion = new ArrayList<Object[]>();
    DefaultCategoryDataset dB;
    float totalTicket = 0;
    Inicio inicio;
    AgregarPago ap;
    boolean b = false;
    boolean hh;
    DefaultTableModel dtm;
    nuevaVtaRapida nvr;

    manejadorArticulos ma = new manejadorArticulos();
    manejadorTablas mt = new manejadorTablas();
    manejadorFacturacion mf = new manejadorFacturacion();
    manejadorFechas mfechas = new manejadorFechas();
    manejadorImpresion mi = new manejadorImpresion();
    manejadorCliente mc = new manejadorCliente();
    manejadorConfiguraciones mcon = new manejadorConfiguraciones();
    manejadorUsuario mu = new manejadorUsuario();

    final metodoBusquedaArticulo mb = new metodoBusquedaArticulo(this);
    final metodoBusquedaMozoVtaRapida mbm = new metodoBusquedaMozoVtaRapida(this);
    //metodoBusquedaArticulo mb;

    public nuevaVtaRapida(Inicio i, Usuario u, DefaultCategoryDataset datosBarriles, boolean happyhour, Inicio.ModeloTablaInicio mti) {
        initComponents();
        this.requestFocusInWindow();
        this.setTitle("NUEVA VENTA RAPIDA | BOSTON BEER & CO.");
        inicio = i;
        user = u;
        dB = datosBarriles;
        hh = happyhour;
        dtm = mti;
        nvr = this;

        if (hh) {
            System.out.println("ESTA ACTIVADO EL HAPPY HOUR");
        } else {
            System.out.println("ESTA DESACTIVADO EL HAPPY HOUR");
        }

        comboBoxArticulo.removeAllItems();
        //ma.traerArticulosComboBox(comboBoxArticulo);
        tablaVentaRapida.removeAll();
        mt.modeloNvaVtaRapida(tablaVentaRapida, modelo);
        txtTotal.setText("$0");
        btnEliminarItems.setEnabled(false);
        btnImprimirTicket.setEnabled(false);
        txtFecha.setText(mfechas.convertirFecha(mfechas.obtenerFechaActual()) + " " + mfechas.obtenerHoraActual().substring(0,5));
        txtFecha.setEnabled(false);

        this.comboBoxArticulo.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {

            public void keyPressed(KeyEvent evt) {

                String campo;
                String cadenaEscrita = comboBoxArticulo.getEditor().getItem().toString();

                System.out.println(cadenaEscrita);

                if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                    if (mb.comparar(cadenaEscrita)) {// compara si el texto escrito se encuentra en la lista
                        // busca el texto escrito en la base de datos                      

                        a = ma.traerArticuloPorString(cadenaEscrita);

                        System.out.println("ENTRO 1");
                        System.out.println("ELEMENTO SELECCIONADO: " + comboBoxArticulo.getEditor().getItem().toString());
                        a.stringArticulo(a);
                        String comparar = comboBoxArticulo.getEditor().getItem().toString();
                        comboBoxArticulo.getEditor().setItem(a.getDesc_articulo());

                        if (a.getDesc_articulo() == null) {
                            JOptionPane.showMessageDialog(null, "NO EXISTE EL ARTICULO SELECCIONADO");
                        } else if (a.getId_tipoProd2() == 5) {
                            System.out.println("se selecciono cerveza artesanal");
                            if (ma.comprobarBarril(a.getDesc_articulo())) {
                                System.out.println("hay cerveza " + a.getDesc_articulo());
                                if (hh) {
                                    a.setPrecio_venta(ma.traerPrecioHH());
                                }

                                txtCantidad.setEditable(true);
                                txtCantidad.requestFocus();

                            } else {
                                txtCantidad.setEditable(false);
                                comboBoxArticulo.requestFocusInWindow();
                                JOptionPane.showMessageDialog(null, "NO HAY CERVEZA " + a.getDesc_articulo() + " CARGADA EN LA CHOPERA");

                            }
                        } else if (a.getId_tipoProd1() == 3) {
                            System.out.println("ES PROMOCION, SE REVISARA SI HAY CERVEZA");
                            ArrayList<Object[]> promocion = ma.traerArticulosPromocion(a.getCodigo());
                            boolean ban = true;

                            for (int i = 0; i < promocion.size(); i++) {
                                Articulo promo = new Articulo();
                                promo = ma.traerArticuloPorCodigo(promocion.get(i)[0].toString());
                                System.out.println("ARTICULO DE LA PROMO: " + promo.getDesc_articulo());

                                if (promo.getId_tipoProd2() == 5) {
                                    System.out.println("se selecciono cerveza artesanal");
                                    if (!ma.comprobarBarril(promo.getDesc_articulo())) {
                                        ban = false;
                                    }

                                }

                            }

                            if (ban) {
                                txtCantidad.setEditable(true);
                                txtCantidad.requestFocus();
                            } else {
                                JOptionPane.showMessageDialog(null, "LA CERVEZA QUE INTENTA CARGAR NO ESTA DISPONIBLE EN NINGUNO DE LOS BARRILES CARGADOS");
                                comboBoxArticulo.requestFocusInWindow();
                                txtCantidad.setEditable(false);
                            }

                        } else {
                            txtCantidad.setEditable(true);
                            txtCantidad.requestFocus();
                        }

                        btnImprimirTicket.setVisible(true);

                    } else {// en caso contrario toma como default el elemento 0 o sea el primero de la lista y lo busca.
                        if (comboBoxArticulo.getItemAt(0) != null) {

                            comboBoxArticulo.setSelectedIndex(0);
                            cadenaEscrita = comboBoxArticulo.getItemAt(0).toString();
                            System.out.println("ENTRO 2");
                            System.out.println("ELEMENTO SELECCIONADO: " + comboBoxArticulo.getEditor().getItem().toString());
                            a = ma.traerArticuloPorString(cadenaEscrita);
                            a.stringArticulo(a);
                            String comparar = comboBoxArticulo.getItemAt(0).toString();
                            comboBoxArticulo.getEditor().setItem(a.getDesc_articulo());

                            if (a.getDesc_articulo() == null) {
                                JOptionPane.showMessageDialog(null, "NO EXISTE EL ARTICULO SELECCIONADO");
                            } else if (a.getId_tipoProd2() == 5) {
                                System.out.println("se selecciono cerveza artesanal");
                                if (ma.comprobarBarril(a.getDesc_articulo())) {
                                    System.out.println("hay cerveza " + a.getDesc_articulo());
                                    if (hh) {
                                        a.setPrecio_venta(ma.traerPrecioHH());
                                    }

                                    txtCantidad.setEditable(true);
                                    txtCantidad.requestFocus();

                                } else {
                                    txtCantidad.setEditable(false);
                                    comboBoxArticulo.requestFocusInWindow();
                                    JOptionPane.showMessageDialog(null, "NO HAY CERVEZA " + a.getDesc_articulo() + " CARGADA EN LA CHOPERA");
                                    //comboBoxArticulo.getEditor().getEditorComponent().requestFocus();

                                }
                            } else if (a.getId_tipoProd1() == 3) {
                                System.out.println("ES PROMOCION, SE REVISARA SI HAY CERVEZA");
                                ArrayList<Object[]> promocion = ma.traerArticulosPromocion(a.getCodigo());
                                boolean ban = true;

                                for (int i = 0; i < promocion.size(); i++) {
                                    Articulo promo = new Articulo();
                                    promo = ma.traerArticuloPorCodigo(promocion.get(i)[0].toString());
                                    System.out.println("ARTICULO DE LA PROMO: " + promo.getDesc_articulo());

                                    if (promo.getId_tipoProd2() == 5) {
                                        System.out.println("se selecciono cerveza artesanal");
                                        if (!ma.comprobarBarril(promo.getDesc_articulo())) {
                                            ban = false;
                                        }

                                    }

                                }

                                if (ban) {
                                    txtCantidad.setEditable(true);
                                    txtCantidad.requestFocus();
                                } else {
                                    txtCantidad.setEditable(false);
                                    comboBoxArticulo.requestFocusInWindow();
                                    JOptionPane.showMessageDialog(null, "LA CERVEZA QUE INTENTA CARGAR NO ESTA DISPONIBLE EN NINGUNO DE LOS BARRILES CARGADOS");

                                }

                            } else {
                                txtCantidad.setEditable(true);
                                txtCantidad.requestFocus();
                            }
                        }

                    }

                    btnImprimirTicket.setEnabled(true);

                }

                if (evt.getKeyCode() >= 65 && evt.getKeyCode() <= 90 || evt.getKeyCode() >= 96 && evt.getKeyCode() <= 105 || evt.getKeyCode() == 8) {
                    System.out.println("entro por aquiiiiiiiiiii");
                    comboBoxArticulo.setModel(mb.getLista(cadenaEscrita));
                    txtCodigo.setText("");

                    if (comboBoxArticulo.getEditor().getItem().toString().equals("")) {
                        //System.out.println("Esta vacio");
                        txtCodigo.setEnabled(true);
                    } else {
                        //txtCodigo.setEnabled(false);
                        //System.out.println("Esta escribiendo");
                    }

                    if (comboBoxArticulo.getItemCount() > 0) {
                        comboBoxArticulo.getEditor().setItem(cadenaEscrita);
                        comboBoxArticulo.showPopup();

                    } else {
                        //comboBoxArticulo.addItem(cadenaEscrita);
                    }
                }

            }
        });

        this.comboBoxMozo.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {

            public void keyReleased(KeyEvent evt) {

                String campo;
                String cadenaEscrita = comboBoxMozo.getEditor().getItem().toString();

                System.out.println(cadenaEscrita);

                if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                    if (mbm.comparar(cadenaEscrita)) {// compara si el texto escrito se encuentra en la lista
                        // busca el texto escrito en la base de datos                      

                        user = mu.traerMozo(cadenaEscrita);

                        System.out.println("ENTRO 1");
                        System.out.println("ELEMENTO SELECCIONADO: " + comboBoxMozo.getEditor().getItem().toString());
                        String comparar = comboBoxMozo.getEditor().getItem().toString();

                        //btnCerrarCuenta.setVisible(true);
                    } else {// en caso contrario toma como default el elemento 0 o sea el primero de la lista y lo busca.
                        if (comboBoxMozo.getItemAt(0) != null) {

                            comboBoxMozo.setSelectedIndex(0);
                            cadenaEscrita = comboBoxMozo.getItemAt(0).toString();
                            user = mu.traerMozo(cadenaEscrita);
                            System.out.println("ENTRO 2");
                            System.out.println("ELEMENTO SELECCIONADO: " + comboBoxMozo.getEditor().getItem().toString());
                            String comparar = comboBoxMozo.getItemAt(0).toString();
                        }

                    }
                    txtCodigo.requestFocus();
                    //btnCerrarCuenta.setEnabled(true);

                } else if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
                    System.out.println("SE APRETO TECLA ESCAPE");
                    nvr.dispose();
                }

                if (evt.getKeyCode() >= 65 && evt.getKeyCode() <= 90 || evt.getKeyCode() >= 96 && evt.getKeyCode() <= 105 || evt.getKeyCode() == 8) {

                    comboBoxMozo.setModel(mbm.getLista(cadenaEscrita));
                    txtCodigo.setText("");

                    if (comboBoxMozo.getEditor().getItem().toString().equals("")) {
                        //System.out.println("Esta vacio");
                        txtCodigo.setEnabled(true);
                    } else {
                        //txtCodigo.setEnabled(false);
                        //System.out.println("Esta escribiendo");
                    }

                    if (comboBoxMozo.getItemCount() > 0) {
                        comboBoxMozo.getEditor().setItem(cadenaEscrita);
                        comboBoxMozo.showPopup();

                    } else {
                        comboBoxMozo.addItem(cadenaEscrita);
                    }
                }

            }
        });

    }

    public JComboBox getcomboBoxArticulo() {
        return comboBoxArticulo;
    }

    public void setcomboBoxArticulo(JComboBox comboBoxArticulo) {
        this.comboBoxArticulo = comboBoxArticulo;
    }

    public JComboBox getcomboBoxMozo() {
        return comboBoxMozo;
    }

    public void setcomboBoxMozo(JComboBox getcomboBoxMozo) {
        this.comboBoxMozo = getcomboBoxMozo;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        txtCodigo = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        comboBoxArticulo = new javax.swing.JComboBox<>();
        txtCantidad = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaVentaRapida = new javax.swing.JTable();
        btnImprimirTicket = new javax.swing.JButton();
        btnEliminarItems = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        txtTotal = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        comboBoxMozo = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        txtFecha = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });

        jLabel1.setText("Codigo:");

        txtCodigo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCodigoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCodigoKeyReleased(evt);
            }
        });

        jLabel2.setText("Articulo:");

        jLabel3.setText("Cantidad:");

        comboBoxArticulo.setEditable(true);
        comboBoxArticulo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                comboBoxArticuloMouseClicked(evt);
            }
        });
        comboBoxArticulo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxArticuloActionPerformed(evt);
            }
        });

        txtCantidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCantidadKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCantidadKeyReleased(evt);
            }
        });

        tablaVentaRapida.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablaVentaRapida.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaVentaRapidaMouseClicked(evt);
            }
        });
        tablaVentaRapida.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tablaVentaRapidaKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tablaVentaRapida);

        btnImprimirTicket.setText("CERRAR PEDIDO");
        btnImprimirTicket.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImprimirTicketActionPerformed(evt);
            }
        });

        btnEliminarItems.setText("ELIMINAR ITEM/S");
        btnEliminarItems.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarItemsActionPerformed(evt);
            }
        });

        jLabel4.setText("Total:");

        txtTotal.setEditable(false);
        txtTotal.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtTotal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtTotalKeyReleased(evt);
            }
        });

        jLabel5.setText("Mozo:");

        comboBoxMozo.setEditable(true);

        jLabel6.setText("Fecha:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jSeparator1)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnEliminarItems, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnImprimirTicket, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(comboBoxMozo, javax.swing.GroupLayout.PREFERRED_SIZE, 314, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(30, 30, 30)
                                .addComponent(jLabel2)
                                .addGap(18, 18, 18)
                                .addComponent(comboBoxArticulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(38, 38, 38)
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(comboBoxMozo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(txtFecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(comboBoxArticulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(11, 11, 11)
                .addComponent(btnEliminarItems)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnImprimirTicket)
                .addContainerGap())
        );

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {txtCantidad, txtCodigo});

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtCodigoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCodigoKeyReleased
        // TODO add your handling code here:

    }//GEN-LAST:event_txtCodigoKeyReleased

    private void txtCantidadKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCantidadKeyReleased
        // TODO add your handling code here:


    }//GEN-LAST:event_txtCantidadKeyReleased

    private void btnEliminarItemsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarItemsActionPerformed
        // TODO add your handling code here:

        int filaS = this.tablaVentaRapida.getSelectedRow();
        float acumulado = 0;

        for (int i = 0; i < this.tablaVentaRapida.getRowCount(); i++) {
            acumulado = acumulado + Float.parseFloat(this.tablaVentaRapida.getValueAt(i, 4).toString());
        }

        acumulado = acumulado - Float.parseFloat(this.tablaVentaRapida.getValueAt(filaS, 4).toString());

        modelo.removeRow(filaS);

        txtTotal.setText("$" + acumulado);
        totalTicket = acumulado;

        if (tablaVentaRapida.getRowCount() == 0) {
            this.btnImprimirTicket.setEnabled(false);
            this.btnEliminarItems.setEnabled(false);
        }


    }//GEN-LAST:event_btnEliminarItemsActionPerformed

    private void txtTotalKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTotalKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotalKeyReleased

    private void tablaVentaRapidaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaVentaRapidaMouseClicked
        // TODO add your handling code here:

        if (this.tablaVentaRapida.getSelectedRowCount() != 0) {
            this.btnEliminarItems.setEnabled(true);
        } else {
            this.btnEliminarItems.setEnabled(false);
        }


    }//GEN-LAST:event_tablaVentaRapidaMouseClicked

    private void tablaVentaRapidaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaVentaRapidaKeyReleased
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_DELETE) {
            int filaS = this.tablaVentaRapida.getSelectedRow();
            float acumulado = 0;

            for (int i = 0; i < this.tablaVentaRapida.getRowCount(); i++) {
                acumulado = acumulado + Float.parseFloat(this.tablaVentaRapida.getValueAt(i, 4).toString());
            }

            acumulado = acumulado - Float.parseFloat(this.tablaVentaRapida.getValueAt(filaS, 4).toString());

            modelo.removeRow(filaS);

            txtTotal.setText("$" + acumulado);
            totalTicket = acumulado;

            if (tablaVentaRapida.getRowCount() == 0) {
                this.btnImprimirTicket.setEnabled(false);
                this.btnEliminarItems.setEnabled(false);
            }
        }

    }//GEN-LAST:event_tablaVentaRapidaKeyReleased

    private void btnImprimirTicketActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImprimirTicketActionPerformed

        // TODO add your handling code here:
        cli.setId_cliente(1);

        boleta.setCliente(cli);
        boleta.setFecha_boleta(mfechas.obtenerFechaActual());
        boleta.setHora_apertura(mfechas.obtenerHoraActual());
        boleta.setVendedor(user);
        boleta.setTotal(totalTicket);
        boleta.setMesa(0);
        boleta.setHora_cierre(mfechas.obtenerHoraActual());
        boleta.setFecha_cierre(mfechas.obtenerFechaActual());

        //mf.insertarBoleta(boleta, user);
        //boleta.setId_boleta(mf.obtenerID_boleta());
        for (int i = 0; i < this.tablaVentaRapida.getRowCount(); i++) {
            String codigo = this.tablaVentaRapida.getValueAt(i, 0).toString();
            a = ma.traerArticuloPorCodigo(codigo);

            int cantidad = Integer.parseInt(this.tablaVentaRapida.getValueAt(i, 2).toString());
            //float precio = Float.parseFloat(this.tablaVentaRapida.getValueAt(i, 3).toString());
            float precio = 0;
            if (a.getId_tipoProd1() != 4) {
                precio = a.getPrecio_venta();
            } else {
                precio = Float.parseFloat(this.tablaVentaRapida.getValueAt(i, 4).toString());
                System.out.println("el descuento es de: " + precio);
            }

            Detalle_boleta detB = new Detalle_boleta();

            detB.setId_boleta(mf.obtenerID_boleta());
            detB.setCodigo(codigo);
            detB.setCantidad(cantidad);
            detB.setPrecio(precio);

            //mf.insertarDetallesBoleta(db, dB);
            arrayDetalles.add(detB);

            if (a.getId_tipoProd1() == 3) {
                promocion = ma.traerArticulosPromocion(codigo);
            }

            if (a.getId_tipoProd2() == 5) {
                //ma.descontarPintaBarril((float) (Integer.parseInt(txtCantidad.getText()) * 0.5), ma.obtenerPosicionBarril(a.getDesc_articulo()), a.getDesc_articulo(), dB);
                Barril barril = new Barril();
                barril.setPosicion(ma.obtenerPosicionBarril(a.getDesc_articulo()));
                barril.setCantidad_consumida((float) (cantidad * 0.5));
                barril.setDesc_barril(a.getDesc_articulo());
                pintasReemplazo.add(barril);
            }

            for (int k = 0; k < promocion.size(); k++) {
                Articulo prom = new Articulo();
                prom = ma.traerArticuloPorCodigo(promocion.get(k)[0].toString());

                if (prom.getId_tipoProd2() == 5) {
                    Barril barril = new Barril();
                    barril.setPosicion(ma.obtenerPosicionBarril(prom.getDesc_articulo()));
                    int cant = Integer.parseInt(promocion.get(k)[2].toString());
                    barril.setCantidad_consumida((float) (cant * 0.5));
                    barril.setDesc_barril(prom.getDesc_articulo());
                    pintasReemplazo.add(barril);
                    System.out.println("SE AGREGO PINTA DE LA PROMOCION");
                }
            }

        }
        ma.actualizarBarriles(pintasReemplazo, dB);
        pintasReemplazo.clear();
        cli = mc.traerCliente(1);
        ap = new AgregarPago(user, cli, boleta, null, inicio, arrayDetalles, dB, (Inicio.ModeloTablaInicio) dtm, null, null, null, 1, null);
        ap.setVisible(true);
        ap.setLocationRelativeTo(null);
        this.dispose();
    }//GEN-LAST:event_btnImprimirTicketActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        // TODO add your handling code here:
        /*if (!ap.isShowing()) {
            inicio.requestFocus();
        }*/

    }//GEN-LAST:event_formWindowClosed

    private void txtCodigoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCodigoKeyPressed
        // TODO add your handling code here:

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            String codigo = this.txtCodigo.getText();
            a = ma.traerArticuloPorCodigo(codigo);
            System.out.println("ID TIPO: " + a.getId_tipoProd1());
            if (a.getDesc_articulo() == null) {
                JOptionPane.showMessageDialog(null, "NO EXISTE EL ARTICULO SELECCIONADO");
            } else if (a.getId_tipoProd2() == 5) {
                System.out.println("se selecciono cerveza artesanal");
                if (ma.comprobarBarril(a.getDesc_articulo())) {
                    System.out.println("hay cerveza " + a.getDesc_articulo());
                    if (hh) {
                        a.setPrecio_venta(ma.traerPrecioHH());
                    }
                    //this.comboBoxArticulo.removeAllItems();
                    //this.comboBoxArticulo.addItem(a.getDesc_articulo());
                    //this.comboBoxArticulo.setSelectedIndex(0);
                    //this.comboBoxArticulo.setEnabled(false);
                    comboBoxArticulo.addItem(a.getDesc_articulo());
                    comboBoxArticulo.setSelectedItem(a.getDesc_articulo());
                    txtCantidad.setEditable(true);
                    txtCantidad.requestFocus();

                } else {
                    txtCantidad.setEditable(false);
                    this.txtCodigo.requestFocus();
                    JOptionPane.showMessageDialog(null, "NO HAY CERVEZA " + a.getDesc_articulo() + " CARGADA EN LA CHOPERA");
                }
            } else if (a.getId_tipoProd1() == 3) {
                System.out.println("ES PROMOCION, SE REVISARA SI HAY CERVEZA");
                ArrayList<Object[]> promocion = ma.traerArticulosPromocion(a.getCodigo());

                for (int i = 0; i < promocion.size(); i++) {
                    Articulo promo = new Articulo();
                    promo = ma.traerArticuloPorCodigo(promocion.get(i)[0].toString());
                    System.out.println("ARTICULO DE LA PROMO: " + promo.getDesc_articulo());
                    if (promo.getId_tipoProd2() == 5) {
                        System.out.println("se selecciono cerveza artesanal");
                        if (ma.comprobarBarril(promo.getDesc_articulo())) {
                            System.out.println("hay cerveza " + a.getDesc_articulo());
                            //this.comboBoxArticulo.removeAllItems();
                            //this.comboBoxArticulo.addItem(a.getDesc_articulo());
                            //this.comboBoxArticulo.setSelectedIndex(0);
                            //this.comboBoxArticulo.setEnabled(false);
                            comboBoxArticulo.addItem(a.getDesc_articulo());
                            comboBoxArticulo.setSelectedItem(a.getDesc_articulo());
                            txtCantidad.setEditable(true);
                            txtCantidad.requestFocus();

                        } else {
                            txtCantidad.setEditable(false);
                            this.txtCodigo.requestFocus();
                            JOptionPane.showMessageDialog(null, "NO HAY CERVEZA " + promo.getDesc_articulo() + " CARGADA EN LA CHOPERA");
                        }
                    } else {
                        //this.comboBoxArticulo.removeAllItems();
                        //this.comboBoxArticulo.addItem(a.getDesc_articulo());
                        //this.comboBoxArticulo.setEnabled(false);
                        //this.comboBoxArticulo.setSelectedIndex(0);
                        comboBoxArticulo.addItem(a.getDesc_articulo());
                        comboBoxArticulo.setSelectedItem(a.getDesc_articulo());
                        txtCantidad.setEditable(true);
                        txtCantidad.requestFocus();
                    }
                }

            } else {
                //this.comboBoxArticulo.removeAllItems();
                //this.comboBoxArticulo.addItem(a.getDesc_articulo());
                //this.comboBoxArticulo.setEnabled(false);
                //this.comboBoxArticulo.setSelectedIndex(0);
                comboBoxArticulo.addItem(a.getDesc_articulo());
                comboBoxArticulo.setSelectedItem(a.getDesc_articulo());
                txtCantidad.setEditable(true);
                txtCantidad.requestFocus();
            }

        } else if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
            System.out.println("SE APRETO TECLA ESCAPE");
            this.dispose();
        }
    }//GEN-LAST:event_txtCodigoKeyPressed

    private void txtCantidadKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCantidadKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            Object[] ar = new Object[5];
            int cantidad = 0;

            if (txtCantidad.getText().equals("")) {
                cantidad = 1;
            } else {
                cantidad = Integer.parseInt(txtCantidad.getText());
            }

            //float total = a.getPrecio_venta() * Integer.parseInt(txtCantidad.getText());
            float total = a.getPrecio_venta() * cantidad;
            boolean coincidencia = false;
            int filaMismoArticulo = 0;
            float acumulado = 0;

            for (int i = 0; i < this.tablaVentaRapida.getRowCount(); i++) {
                if (a.getDesc_articulo().equals(this.tablaVentaRapida.getValueAt(i, 1))) {
                    coincidencia = true;
                    filaMismoArticulo = i;
                }
            }

            if (coincidencia) {

                System.out.println("EXISTE UN VALOR IGUAL");
                //int cantidadPrevia = Integer.parseInt(txtCantidad.getText());
                int cantidadPrevia = cantidad;
                int cantidadAgregada = Integer.parseInt(this.tablaVentaRapida.getValueAt(filaMismoArticulo, 2).toString());
                int cantidadNueva = cantidadPrevia + cantidadAgregada;
                this.tablaVentaRapida.setValueAt(cantidadNueva, filaMismoArticulo, 2);
                this.tablaVentaRapida.setValueAt(cantidadNueva * a.getPrecio_venta(), filaMismoArticulo, 4);
                total = Float.parseFloat(this.tablaVentaRapida.getValueAt(filaMismoArticulo, 4).toString());

            } else {
                System.out.println("entro por el false");
                if (a.getId_tipoProd1() == 4) {
                    ar[0] = a.getCodigo();
                    ar[1] = a.getDesc_articulo();
                    //ar[2] = txtCantidad.getText();
                    ar[2] = cantidad;
                    ar[3] = "%" + a.getPrecio_venta();
                    float descuento = totalTicket * a.getPrecio_venta() / 100;
                    ar[4] = "-" + descuento;
                    totalTicket = totalTicket - descuento;
                } else {
                    ar[0] = a.getCodigo();
                    ar[1] = a.getDesc_articulo();
                    //ar[2] = txtCantidad.getText();
                    ar[2] = cantidad;
                    ar[3] = a.getPrecio_venta();
                    ar[4] = total;
                }

                modelo.addRow(ar);

            }

            for (int i = 0; i < this.tablaVentaRapida.getRowCount(); i++) {
                acumulado = acumulado + Float.parseFloat(this.tablaVentaRapida.getValueAt(i, 4).toString());
            }

            txtCantidad.setText("");
            totalTicket = acumulado;
            txtTotal.setText("$" + acumulado);
            //this.comboBoxArticulo.removeAllItems();
            this.comboBoxArticulo.setSelectedItem("");
            this.comboBoxArticulo.setEnabled(true);
            this.txtCodigo.setText("");
            txtCodigo.requestFocus();
            btnImprimirTicket.setEnabled(true);

        } else if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
            System.out.println("SE APRETO TECLA ESCAPE");
            this.dispose();
        }
    }//GEN-LAST:event_txtCantidadKeyPressed

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
            System.out.println("SE APRETO TECLA ESCAPE");
            this.dispose();
        }

    }//GEN-LAST:event_formKeyPressed

    private void comboBoxArticuloMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_comboBoxArticuloMouseClicked
        // TODO add your handling code here:

    }//GEN-LAST:event_comboBoxArticuloMouseClicked

    private void comboBoxArticuloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxArticuloActionPerformed
        // TODO add your handling code here:


    }//GEN-LAST:event_comboBoxArticuloActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(nuevaVtaRapida.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(nuevaVtaRapida.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(nuevaVtaRapida.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(nuevaVtaRapida.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new nuevaVtaRapida(null, null, null, false, null).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnEliminarItems;
    private javax.swing.JButton btnImprimirTicket;
    private javax.swing.JComboBox<String> comboBoxArticulo;
    private javax.swing.JComboBox<String> comboBoxMozo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTable tablaVentaRapida;
    private javax.swing.JTextField txtCantidad;
    private javax.swing.JTextField txtCodigo;
    private javax.swing.JTextField txtFecha;
    private javax.swing.JTextField txtTotal;
    // End of variables declaration//GEN-END:variables
}
