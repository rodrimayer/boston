/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author rmmayer
 */
public class Configuracion {
    
    private int id_configuracion;
    private String desc_configuracion;
    private float valor_configuracion;

    public int getId_configuracion() {
        return id_configuracion;
    }

    public void setId_configuracion(int id_configuracion) {
        this.id_configuracion = id_configuracion;
    }

    public String getDesc_configuracion() {
        return desc_configuracion;
    }

    public void setDesc_configuracion(String desc_configuracion) {
        this.desc_configuracion = desc_configuracion;
    }

    public float getValor_configuracion() {
        return valor_configuracion;
    }

    public void setValor_configuracion(float valor_configuracion) {
        this.valor_configuracion = valor_configuracion;
    }
    
    
    
}
