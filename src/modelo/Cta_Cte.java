/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Rodri Mayer
 */
public class Cta_Cte {
    
    private int id_cta_cte;
    private int id_cliente;
    private float total;
    private int habilitada;

    public int getId_cta_cte() {
        return id_cta_cte;
    }

    public void setId_cta_cte(int id_cta_cte) {
        this.id_cta_cte = id_cta_cte;
    }

    public int getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(int id_cliente) {
        this.id_cliente = id_cliente;
    }

    public int getHabilitada() {
        return habilitada;
    }

    public void setHabilitada(int habilitada) {
        this.habilitada = habilitada;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }
    
    
    
}
