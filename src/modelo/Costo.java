/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author rmmayer
 */
public class Costo {

    private int id_costo;
    private int id_tipoCosto;
    private String desc_costo;
    private float valor;
    private int cerrado;
    private String fecha_costo;
    private String hora_costo;

    public int getId_costo() {
        return id_costo;
    }

    public void setId_costo(int id_costo) {
        this.id_costo = id_costo;
    }

    public int getId_tipoCosto() {
        return id_tipoCosto;
    }

    public void setId_tipoCosto(int id_tipoCosto) {
        this.id_tipoCosto = id_tipoCosto;
    }

    public String getDesc_costo() {
        return desc_costo;
    }

    public void setDesc_costo(String desc_costo) {
        this.desc_costo = desc_costo;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public int getCerrado() {
        return cerrado;
    }

    public void setCerrado(int cerrado) {
        this.cerrado = cerrado;
    }

    public String getFecha_costo() {
        return fecha_costo;
    }

    public void setFecha_costo(String fecha_costo) {
        this.fecha_costo = fecha_costo;
    }

    public String getHora_costo() {
        return hora_costo;
    }

    public void setHora_costo(String hora_costo) {
        this.hora_costo = hora_costo;
    }

}
