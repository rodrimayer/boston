/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author rmmayer
 */
public class Cierre_Caja {
    
    private int id_cierreCaja;
    private String fecha_cierre;
    private String hora_cierre;
    private float venta_contado;
    private float venta_credito;
    private float venta_debito;
    private float costo_contado;
    private float costo_credito;
    private float costo_debito;

    public int getId_cierreCaja() {
        return id_cierreCaja;
    }

    public void setId_cierreCaja(int id_cierreCaja) {
        this.id_cierreCaja = id_cierreCaja;
    }

    public String getFecha_cierre() {
        return fecha_cierre;
    }

    public void setFecha_cierre(String fecha_cierre) {
        this.fecha_cierre = fecha_cierre;
    }

    public String getHora_cierre() {
        return hora_cierre;
    }

    public void setHora_cierre(String hora_cierre) {
        this.hora_cierre = hora_cierre;
    }

    public float getVenta_contado() {
        return venta_contado;
    }

    public void setVenta_contado(float venta_contado) {
        this.venta_contado = venta_contado;
    }

    public float getVenta_credito() {
        return venta_credito;
    }

    public void setVenta_credito(float venta_credito) {
        this.venta_credito = venta_credito;
    }

    public float getVenta_debito() {
        return venta_debito;
    }

    public void setVenta_debito(float venta_debito) {
        this.venta_debito = venta_debito;
    }

    public float getCosto_contado() {
        return costo_contado;
    }

    public void setCosto_contado(float costo_contado) {
        this.costo_contado = costo_contado;
    }

    public float getCosto_credito() {
        return costo_credito;
    }

    public void setCosto_credito(float costo_credito) {
        this.costo_credito = costo_credito;
    }

    public float getCosto_debito() {
        return costo_debito;
    }

    public void setCosto_debito(float costo_debito) {
        this.costo_debito = costo_debito;
    }
    
    
    
}
