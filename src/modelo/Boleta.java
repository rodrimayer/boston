/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Rodri Mayer
 */
public class Boleta {

    private int id_boleta;
    private String fecha_boleta;
    private String hora_apertura;
    private Cliente cliente;
    private float total;
    private int cerrada;
    private Usuario vendedor;
    private int mesa;
    private int pagada;
    private String fecha_cierre;
    private String hora_cierre;
    private String direccion_delivery;
    private float pago_delivery;
    private int id_tipoVta;

    public int getId_boleta() {
        return id_boleta;
    }

    public void setId_boleta(int id_boleta) {
        this.id_boleta = id_boleta;
    }

    public String getFecha_boleta() {
        return fecha_boleta;
    }

    public void setFecha_boleta(String fecha_boleta) {
        this.fecha_boleta = fecha_boleta;
    }

    public String getHora_apertura() {
        return hora_apertura;
    }

    public void setHora_apertura(String hora_apertura) {
        this.hora_apertura = hora_apertura;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public int getCerrada() {
        return cerrada;
    }

    public void setCerrada(int cerrada) {
        this.cerrada = cerrada;
    }

    public Usuario getVendedor() {
        return vendedor;
    }

    public void setVendedor(Usuario vendedor) {
        this.vendedor = vendedor;
    }

    public int getMesa() {
        return mesa;
    }

    public void setMesa(int mesa) {
        this.mesa = mesa;
    }

    public int getPagada() {
        return pagada;
    }

    public void setPagada(int pagada) {
        this.pagada = pagada;
    }

    public String getFecha_cierre() {
        return fecha_cierre;
    }

    public void setFecha_cierre(String fecha_cierre) {
        this.fecha_cierre = fecha_cierre;
    }

    public String getHora_cierre() {
        return hora_cierre;
    }

    public void setHora_cierre(String hora_cierre) {
        this.hora_cierre = hora_cierre;
    }

    public String getDireccion_delivery() {
        return direccion_delivery;
    }

    public void setDireccion_delivery(String direccion_delivery) {
        this.direccion_delivery = direccion_delivery;
    }   

    public float getPago_delivery() {
        return pago_delivery;
    }

    public void setPago_delivery(float pago_delivery) {
        this.pago_delivery = pago_delivery;
    }

    public int getId_tipoVta() {
        return id_tipoVta;
    }

    public void setId_tipoVta(int id_tipoVta) {
        this.id_tipoVta = id_tipoVta;
    }
    

}
