/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 *
 * @author Rodri Mayer
 */
public class Facturacion_Cta_Cte {
    private int id_factCtaCte;
    private int id_cta_cte;
    private int id_condVta;
    private float monto;
    private LocalDateTime fecha;
    
    DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");    
    //private String fechasS = dateFormat.format(fecha);

    public int getId_factCtaCte() {
        return id_factCtaCte;
    }

    public void setId_factCtaCte(int id_factCtaCte) {
        this.id_factCtaCte = id_factCtaCte;
    }

    public int getId_cta_cte() {
        return id_cta_cte;
    }

    public void setId_cta_cte(int id_cta_cte) {
        this.id_cta_cte = id_cta_cte;
    }

    public DateTimeFormatter getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(DateTimeFormatter dateFormat) {
        this.dateFormat = dateFormat;
    }

    

    public int getId_condVta() {
        return id_condVta;
    }

    public void setId_condVta(int id_condVta) {
        this.id_condVta = id_condVta;
    }

    public float getMonto() {
        return monto;
    }

    public void setMonto(float monto) {
        this.monto = monto;
    }

    public LocalDateTime getFecha() {
        return fecha;
    }

    public void setFecha(LocalDateTime fecha) {
        this.fecha = fecha;
    }

    public String getFechasS() {
        return dateFormat.format(fecha);
    }

    
    
    
    
    
}
