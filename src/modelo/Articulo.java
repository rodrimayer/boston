/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Rodri Mayer
 */
public class Articulo {

    private String codigo;
    private int id_tipoProd1;
    private int id_tipoProd2;
    private float precio_costo;
    private float precio_venta;
    private String desc_articulo;
    private int baja;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public int getId_tipoProd1() {
        return id_tipoProd1;
    }

    public void setId_tipoProd1(int id_tipo) {
        this.id_tipoProd1 = id_tipo;
    }

    public float getPrecio_costo() {
        return precio_costo;
    }

    public void setPrecio_costo(float precio_costo) {
        this.precio_costo = precio_costo;
    }

    public float getPrecio_venta() {
        return precio_venta;
    }

    public void setPrecio_venta(float precio_venta) {
        this.precio_venta = precio_venta;
    }

    public String getDesc_articulo() {
        return desc_articulo;
    }

    public void setDesc_articulo(String desc_articulo) {
        this.desc_articulo = desc_articulo;
    }

    public int getId_tipoProd2() {
        return id_tipoProd2;
    }

    public void setId_tipoProd2(int id_tipoProd2) {
        this.id_tipoProd2 = id_tipoProd2;
    }

    public String stringArticulo(Articulo a) {
        String txt = "CODIGO: " + a.getCodigo() + ", DESC: " + a.getDesc_articulo() + ", PRECIO VTA: " + a.getPrecio_venta();

        return txt;
    }

    public int getBaja() {
        return baja;
    }

    public void setBaja(int baja) {
        this.baja = baja;
    }

    

}
