/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author rmmayer
 */
public class Facturacion_costo {
    
    private int id_factCosto;
    private int id_costo;
    private int id_condVta;
    private float monto;

    public int getId_factCosto() {
        return id_factCosto;
    }

    public void setId_factCosto(int id_factCosto) {
        this.id_factCosto = id_factCosto;
    }

    public int getId_costo() {
        return id_costo;
    }

    public void setId_costo(int id_costo) {
        this.id_costo = id_costo;
    }

    public int getId_condVta() {
        return id_condVta;
    }

    public void setId_condVta(int id_condVta) {
        this.id_condVta = id_condVta;
    }

    public float getMonto() {
        return monto;
    }

    public void setMonto(float monto) {
        this.monto = monto;
    }
    
    
    
}
