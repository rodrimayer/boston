-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 21-09-2019 a las 22:06:44
-- Versión del servidor: 5.6.12-log
-- Versión de PHP: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `db_boston`
--
CREATE DATABASE IF NOT EXISTS `db_boston` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `db_boston`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulo`
--

CREATE TABLE IF NOT EXISTS `articulo` (
  `codigo` varchar(10) NOT NULL,
  `id_tipoProd` int(11) NOT NULL,
  `id_tipoProd2` int(11) NOT NULL,
  `precio_costo` float NOT NULL,
  `precio_vta` float NOT NULL,
  `desc_articulo` varchar(30) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `articulo`
--

INSERT INTO `articulo` (`codigo`, `id_tipoProd`, `id_tipoProd2`, `precio_costo`, `precio_vta`, `desc_articulo`) VALUES
('1/2bo', 1, 7, 125, 125, '1/2 Pizza Boston'),
('1/2es', 1, 7, 115, 115, '1/2 Pizza Especial'),
('1/2fu', 1, 7, 110, 110, '1/2 Pizza Fugazetta'),
('1/2mu', 1, 7, 100, 100, '1/2 Pizza Muzzarella'),
('A', 2, 5, 50, 100, 'Amber'),
('A150', 2, 21, 50, 80, 'Agua 1.5'),
('A5', 2, 21, 20, 50, 'Agua 500'),
('AB', 2, 4, 50, 170, 'Andes 1L'),
('AP', 3, 17, 100, 300, 'Promo Abusa + Amber'),
('BB', 2, 4, 50, 150, 'Brahma 1l'),
('BUB', 2, 4, 50, 150, 'Budweiser 1l'),
('CC', 2, 4, 50, 100, 'Corona  355'),
('CG', 2, 4, 80, 200, 'CORONA 710'),
('DF', 4, 11, 0, 30, 'Descuento Futbol'),
('DM', 4, 11, 15, 15, 'Descuento Mujer'),
('DP', 1, 18, 100, 200, 'Docena Pollo'),
('EP', 3, 10, 0, 200, 'Empanada + Pinta Amber'),
('FER', 2, 2, 80, 150, 'Fernet 1l'),
('FERN', 2, 2, 50, 100, 'Fernet 500'),
('G', 2, 5, 50, 100, 'Golden'),
('GAN', 2, 3, 100, 150, 'GANCIA 1L'),
('GANC', 2, 3, 80, 100, 'Gancia 500'),
('H', 2, 5, 50, 100, 'Honey'),
('HA', 3, 14, 100, 150, 'Happy Amber'),
('HAB', 1, 6, 100, 220, 'McDorian Abusa'),
('HAH', 3, 10, 100, 150, 'Happy Amber Honey'),
('HB', 1, 9, 100, 180, 'Hot Dog Brooklyn'),
('HCL', 1, 6, 70, 180, 'McDorian Clasica'),
('HCR', 1, 6, 80, 200, 'McDorian Criminal'),
('HG', 3, 14, 100, 150, 'Happy Golden'),
('HGA', 3, 14, 100, 150, 'Happy Golden Amber'),
('HGH', 3, 14, 100, 150, 'Happy Golden Honey'),
('HH', 3, 14, 100, 150, 'Happy Honey'),
('HM', 1, 9, 100, 180, 'Hot Dog Manhattan'),
('HS', 3, 14, 100, 150, 'Happy Stout'),
('HSA', 3, 14, 100, 150, 'Happy Stout Amber'),
('HSG', 3, 14, 100, 150, 'Happy Stout Golden'),
('IGU', 2, 4, 80, 150, 'IGUANA'),
('LQ', 2, 4, 40, 70, 'Quilmes Lata'),
('M125', 2, 20, 40, 100, 'Mirinda 1.25'),
('M150', 2, 20, 50, 120, 'Mirinda 1.50'),
('M250', 2, 20, 10, 30, 'Mirinda 250'),
('M500', 2, 20, 30, 60, 'Mirinda 500'),
('P', 2, 5, 50, 100, 'Porter'),
('P125', 2, 20, 40, 100, 'Pepsi 1.25'),
('P150', 2, 20, 40, 120, 'Pepsi 1.50'),
('P2', 3, 16, 160, 200, 'Patag 2x1'),
('P24', 2, 4, 80, 160, 'Patagonia 24/7'),
('P250', 2, 20, 15, 30, 'Pepsi 250'),
('P500', 2, 20, 20, 60, 'Pepsi 500'),
('PAB', 3, 24, 220, 200, 'Promo Abusa'),
('PB', 1, 7, 100, 250, 'Pizza Boston'),
('PBA', 1, 8, 100, 160, 'Patatas Bacon'),
('PCA', 3, 17, 130, 250, 'Promo Criminal + Amber'),
('PCG', 3, 17, 130, 250, 'Promo Criminal + Golden'),
('PCH', 3, 17, 130, 250, 'Promo Criminal + Honey'),
('PCP', 3, 10, 150, 300, 'Promo Criminal + 2 Pintas'),
('PCS', 3, 17, 200, 270, 'Promo Criminal + Stout'),
('PE', 1, 7, 100, 230, 'Pizza Especial'),
('PF', 1, 7, 100, 220, 'Pizza Fugazzeta'),
('PK', 2, 4, 80, 180, 'Patagonia Kune'),
('PM', 1, 7, 100, 200, 'Pizza Muzzarela'),
('pmccl', 1, 15, 100, 180, 'Promo Mc Criminal'),
('PMCL', 1, 15, 100, 150, 'Promo Mc Clasica'),
('PMZ', 1, 8, 100, 160, 'PATATAS C/ MUZZA'),
('PT125', 2, 20, 50, 100, 'Paso Toros 1.25'),
('PT150', 2, 20, 50, 120, 'Paso Toros 1.50'),
('PT250', 2, 20, 10, 30, 'Paso Toros 250'),
('PT500', 2, 20, 30, 60, 'Paso Toros 500'),
('Q', 2, 4, 70, 150, 'QUILMES 1L'),
('S', 2, 5, 50, 100, 'Stout'),
('S125', 2, 20, 30, 100, 'Seven 1.25'),
('S150', 2, 20, 40, 120, 'Seven 1.50'),
('S250', 2, 20, 10, 30, 'Seven 250'),
('S500', 2, 20, 20, 60, 'Seven 500'),
('STE', 2, 4, 100, 180, 'Stella Artois');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `barril`
--

CREATE TABLE IF NOT EXISTS `barril` (
  `id_barril` int(11) NOT NULL AUTO_INCREMENT,
  `desc_barril` varchar(20) NOT NULL,
  `cantidad_total` float NOT NULL,
  `cantidad_consumida` float NOT NULL,
  `posicion` int(11) NOT NULL,
  PRIMARY KEY (`id_barril`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Volcado de datos para la tabla `barril`
--

INSERT INTO `barril` (`id_barril`, `desc_barril`, `cantidad_total`, `cantidad_consumida`, `posicion`) VALUES
(16, 'Amber', 11, 11, 1),
(17, 'Golden', 28, 25.5, 2),
(18, 'Stout', 20, 12, 3),
(19, 'Amber', 28, 4, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `boleta`
--

CREATE TABLE IF NOT EXISTS `boleta` (
  `id_boleta` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_boleta` date NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `mesa` int(11) NOT NULL,
  `id_mozo` int(11) NOT NULL,
  `total` float NOT NULL,
  `cerrada` int(11) NOT NULL,
  `pagada` int(11) NOT NULL,
  PRIMARY KEY (`id_boleta`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=131 ;

--
-- Volcado de datos para la tabla `boleta`
--

INSERT INTO `boleta` (`id_boleta`, `fecha_boleta`, `id_cliente`, `mesa`, `id_mozo`, `total`, `cerrada`, `pagada`) VALUES
(1, '2019-09-16', 1, 0, 11, 200, 1, 1),
(2, '2019-09-16', 1, 1, 11, 200, 1, 1),
(3, '2019-09-16', 1, 0, 11, 300, 1, 1),
(4, '2019-09-16', 1, 0, 11, 100, 1, 1),
(5, '2019-09-16', 1, 0, 11, 300, 1, 1),
(6, '2019-09-16', 1, 1, 11, 400, 1, 1),
(9, '2019-09-17', 1, 2, 2222, 200, 1, 1),
(11, '2019-09-17', 1, 3, 2222, 450, 1, 1),
(12, '2019-09-17', 1, 3, 2222, 200, 1, 1),
(13, '2019-09-17', 1, 6, 2222, 480, 1, 1),
(14, '2019-09-17', 1, 11, 2222, 0, 1, 1),
(15, '2019-09-17', 1, 11, 2222, 320, 1, 1),
(16, '2019-09-17', 1, 15, 2222, 260, 1, 1),
(17, '2019-09-17', 1, 0, 11111111, 180, 1, 1),
(18, '2019-09-17', 1, 5, 2222, 200, 1, 1),
(19, '2019-09-17', 1, 3, 2222, 520, 1, 1),
(20, '2019-09-17', 1, 12, 2222, 220, 1, 1),
(21, '2019-09-17', 1, 16, 2222, 780, 1, 1),
(22, '2019-09-17', 1, 11, 2222, 980, 1, 1),
(23, '2019-09-17', 1, 1, 2222, 620, 1, 1),
(24, '2019-09-17', 1, 5, 11, 260, 1, 1),
(25, '2019-09-17', 1, 6, 2222, 760, 1, 1),
(26, '2019-09-17', 1, 12, 2222, 480, 1, 1),
(27, '2019-09-17', 1, 0, 2222, 360, 1, 1),
(28, '2019-09-17', 1, 3, 2222, 640, 1, 1),
(29, '2019-09-17', 1, 0, 2222, 220, 1, 0),
(30, '2019-09-17', 1, 20, 2222, 660, 1, 1),
(31, '2019-09-17', 1, 15, 11, 510, 1, 1),
(32, '2019-09-17', 1, 0, 11111111, 100, 1, 1),
(33, '2019-09-17', 1, 5, 2222, 850, 1, 1),
(34, '2019-09-18', 1, 11, 2222, 400, 1, 1),
(35, '2019-09-18', 1, 14, 2222, 1400, 1, 1),
(36, '2019-09-18', 1, 0, 11111111, 1160, 1, 1),
(37, '2019-09-18', 1, 1, 2222, 380, 1, 1),
(38, '2019-09-18', 1, 5, 11111111, 250, 1, 1),
(39, '2019-09-18', 1, 11, 1111, 315, 1, 1),
(40, '2019-09-18', 1, 14, 1111, 350, 1, 1),
(41, '2019-09-18', 1, 5, 1111, 460, 1, 1),
(42, '2019-09-18', 1, 5, 1111, 410, 1, 1),
(43, '2019-09-18', 1, 0, 11111111, 300, 1, 1),
(44, '2019-09-18', 1, 0, 11111111, 370, 1, 1),
(45, '2019-09-18', 1, 0, 11111111, 160, 1, 1),
(46, '2019-09-18', 1, 0, 11111111, 150, 1, 1),
(47, '2019-09-18', 1, 10, 1111, 360, 1, 1),
(48, '2019-09-18', 1, 0, 11111111, 350, 1, 1),
(49, '2019-09-18', 1, 0, 11111111, 150, 1, 1),
(50, '2019-09-18', 1, 5, 1111, 450, 1, 1),
(51, '2019-09-18', 1, 3, 1111, 450, 1, 1),
(52, '2019-09-18', 1, 14, 1111, 520, 1, 1),
(53, '2019-09-18', 1, 5, 1111, 760, 1, 1),
(54, '2019-09-18', 1, 20, 1111, 790, 1, 1),
(55, '2019-09-18', 1, 24, 1111, 930, 1, 1),
(56, '2019-09-18', 1, 12, 1111, 365, 1, 1),
(57, '2019-09-18', 1, 15, 1111, 290, 1, 1),
(58, '2019-09-18', 1, 14, 1111, 600, 1, 1),
(59, '2019-09-18', 1, 10, 1111, 180, 1, 1),
(60, '2019-09-19', 1, 0, 1111, 120, 1, 1),
(61, '2019-09-19', 1, 20, 1111, 510, 1, 1),
(62, '2019-09-19', 1, 0, 11111111, 150, 1, 1),
(64, '2019-09-19', 1, 16, 1111, 480, 1, 1),
(65, '2019-09-19', 1, 14, 1111, 1440, 1, 1),
(66, '2019-09-19', 1, 1, 1111, 300, 1, 1),
(67, '2019-09-19', 1, 0, 11111111, 150, 1, 1),
(68, '2019-09-19', 1, 10, 1111, 590, 1, 1),
(69, '2019-09-19', 1, 0, 11111111, 440, 1, 1),
(70, '2019-09-19', 1, 0, 11111111, 150, 1, 1),
(71, '2019-09-19', 1, 15, 1111, 830, 1, 1),
(72, '2019-09-19', 1, 12, 1111, 1170, 1, 1),
(73, '2019-09-19', 1, 8, 11, 510, 1, 1),
(74, '2019-09-19', 1, 14, 1111, 830, 1, 1),
(75, '2019-09-19', 1, 16, 1111, 840, 1, 1),
(76, '2019-09-19', 1, 13, 1111, 400, 1, 1),
(77, '2019-09-19', 1, 0, 11111111, 180, 1, 1),
(78, '2019-09-19', 1, 10, 1111, 350, 1, 1),
(79, '2019-09-19', 1, 13, 1111, 2240, 1, 1),
(80, '2019-09-19', 1, 7, 1111, 630, 1, 1),
(81, '2019-09-19', 1, 2, 1111, 700, 1, 1),
(82, '2019-09-20', 1, 8, 1111, 520, 1, 1),
(83, '2019-09-20', 1, 3, 1111, 620, 1, 1),
(84, '2019-09-20', 1, 11, 1111, 450, 1, 1),
(85, '2019-09-20', 1, 18, 1111, 595, 1, 1),
(86, '2019-09-20', 1, 15, 11, 150, 1, 1),
(87, '2019-09-20', 1, 2, 11, 350, 1, 1),
(88, '2019-09-20', 1, 7, 11, 300, 1, 1),
(89, '2019-09-20', 1, 17, 1111, 630, 1, 1),
(90, '2019-09-20', 1, 10, 1111, 550, 1, 1),
(91, '2019-09-20', 1, 3, 1111, 250, 1, 1),
(92, '2019-09-20', 1, 0, 11111111, 150, 1, 1),
(93, '2019-09-20', 1, 0, 11111111, 100, 1, 1),
(94, '2019-09-20', 1, 0, 11111111, 100, 1, 1),
(95, '2019-09-20', 1, 2, 11, 200, 1, 1),
(96, '2019-09-20', 1, 3, 11, 150, 1, 1),
(97, '2019-09-20', 1, 0, 11111111, 150, 1, 1),
(98, '2019-09-20', 1, 5, 2222, 800, 1, 1),
(99, '2019-09-20', 1, 0, 11111111, 200, 1, 1),
(100, '2019-09-20', 1, 4, 1, 480, 1, 1),
(101, '2019-09-20', 1, 3, 1, 1150, 1, 1),
(102, '2019-09-20', 1, 14, 2222, 340, 1, 1),
(103, '2019-09-20', 1, 11, 2222, 1540, 1, 1),
(104, '2019-09-20', 1, 0, 11111111, 200, 1, 1),
(105, '2019-09-20', 1, 6, 1, 260, 1, 1),
(106, '2019-09-20', 1, 0, 11111111, 400, 1, 1),
(107, '2019-09-20', 1, 15, 2222, 550, 1, 1),
(108, '2019-09-20', 1, 2, 1, 550, 1, 1),
(110, '2019-09-20', 1, 1, 1, 960, 1, 1),
(111, '2019-09-20', 1, 4, 2222, 1110, 1, 1),
(112, '2019-09-20', 1, 5, 2222, 1130, 1, 1),
(113, '2019-09-20', 1, 13, 2222, 1420, 1, 1),
(114, '2019-09-20', 1, 10, 2222, 150, 1, 1),
(115, '2019-09-20', 1, 2, 1, 840, 1, 1),
(117, '2019-09-21', 1, 1, 1, 1000, 1, 1),
(118, '2019-09-21', 1, 11, 2222, 300, 1, 1),
(119, '2019-09-21', 1, 3, 2222, 725, 1, 1),
(120, '2019-09-21', 1, 5, 1, 500, 1, 1),
(121, '2019-09-21', 1, 6, 1, 520, 1, 1),
(122, '2019-09-21', 1, 0, 11111111, 300, 1, 1),
(123, '2019-09-21', 1, 14, 2222, 400, 1, 1),
(124, '2019-09-21', 1, 0, 11111111, 210, 1, 1),
(125, '2019-09-21', 1, 0, 11111111, 475, 1, 1),
(126, '2019-09-21', 1, 15, 2222, 600, 1, 1),
(127, '2019-09-21', 1, 0, 2222, 360, 1, 1),
(128, '2019-09-21', 1, 12, 2222, 580, 1, 1),
(129, '2019-09-21', 1, 0, 11111111, 70, 1, 1),
(130, '2019-09-21', 1, 0, 2222, 230, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
  `id_cliente` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_cliente` varchar(30) NOT NULL,
  `apellido_cliente` varchar(30) NOT NULL,
  `direccion_cliente` varchar(30) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `mail` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_cliente`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id_cliente`, `nombre_cliente`, `apellido_cliente`, `direccion_cliente`, `telefono`, `mail`) VALUES
(1, 'Consumidor', 'Final', '-', '-', '-');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cond_vta`
--

CREATE TABLE IF NOT EXISTS `cond_vta` (
  `id_condVta` int(11) NOT NULL AUTO_INCREMENT,
  `desc_condVta` varchar(30) NOT NULL,
  PRIMARY KEY (`id_condVta`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `cond_vta`
--

INSERT INTO `cond_vta` (`id_condVta`, `desc_condVta`) VALUES
(1, 'CONTADO'),
(2, 'CREDITO'),
(3, 'DEBITO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracion`
--

CREATE TABLE IF NOT EXISTS `configuracion` (
  `id_configuracion` int(11) NOT NULL AUTO_INCREMENT,
  `desc_configuracion` varchar(25) NOT NULL,
  `valor_configuracion` float NOT NULL,
  PRIMARY KEY (`id_configuracion`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `configuracion`
--

INSERT INTO `configuracion` (`id_configuracion`, `desc_configuracion`, `valor_configuracion`) VALUES
(1, 'Happy Hour', 80),
(4, 'Impresion mesa', 1),
(7, 'Impresion rapida', 1),
(8, 'Impresion caja', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `costo`
--

CREATE TABLE IF NOT EXISTS `costo` (
  `id_costo` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_costo` date NOT NULL,
  `id_tipoCosto` int(11) NOT NULL,
  `desc_costo` varchar(20) NOT NULL,
  `valor` float NOT NULL,
  `cerrado` int(11) NOT NULL,
  PRIMARY KEY (`id_costo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Volcado de datos para la tabla `costo`
--

INSERT INTO `costo` (`id_costo`, `fecha_costo`, `id_tipoCosto`, `desc_costo`, `valor`, `cerrado`) VALUES
(1, '2019-09-16', 6, 'Comodin', 300, 1),
(2, '2019-09-16', 6, 'Chino', 600, 1),
(3, '2019-09-17', 17, 'VERDURA', 315, 1),
(4, '2019-09-17', 18, 'CARNE PARA PANCHOS', 280, 1),
(6, '2019-09-17', 19, 'PAN DE HAMBURGUESAS', 300, 1),
(7, '2019-09-17', 6, 'OBRERO', 220, 1),
(8, '2019-09-17', 6, 'ROTONDA', 1600, 1),
(9, '2019-09-17', 9, 'PAPEL MANTECA', 180, 1),
(10, '2019-09-17', 5, 'PEPSI', 600, 1),
(14, '2019-09-18', 6, 'ROTONDA', 3370, 1),
(15, '2019-09-18', 6, 'OBRERO', 440, 1),
(16, '2019-09-18', 9, 'PAPEL MANTECA', 90, 1),
(17, '2019-09-18', 5, 'PEPSI 500CC X 12 U', 390, 1),
(18, '2019-09-18', 18, 'CARNE HAMBUR', 780, 1),
(19, '2019-09-18', 17, 'VERDURAS', 210, 1),
(20, '2019-09-18', 19, 'PAN', 300, 1),
(21, '2019-09-18', 5, 'CERVEZA STELLA 1L X6', 600, 1),
(22, '2019-09-18', 6, 'VEA', 93, 1),
(23, '2019-09-18', 9, 'MESAS MUNI IMP', 50, 1),
(24, '2019-09-19', 17, 'Verduleria', 225, 1),
(25, '2019-09-19', 18, 'Carniceria', 780, 1),
(26, '2019-09-19', 20, 'Mesas', 80, 1),
(27, '2019-09-20', 6, 'Rotonda', 2190, 1),
(28, '2019-09-20', 9, 'Salta Plast', 410, 1),
(29, '2019-09-20', 6, 'Obrero', 635, 1),
(30, '2019-09-20', 9, 'Papel Manteca', 150, 1),
(31, '2019-09-20', 5, 'Pico', 350, 1),
(32, '2019-09-20', 19, 'Pan', 150, 1),
(33, '2019-09-20', 18, 'carne hambur', 1380, 1),
(34, '2019-09-20', 19, 'pan hambur', 350, 1),
(35, '2019-09-20', 17, 'verduras', 560, 1),
(36, '2019-09-20', 9, 'torta', 300, 1),
(37, '2019-09-20', 6, 'rotonda', 2840, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_mesa`
--

CREATE TABLE IF NOT EXISTS `detalle_mesa` (
  `id_detalleMesa` int(11) NOT NULL AUTO_INCREMENT,
  `id_boleta` int(11) NOT NULL,
  `codigo` varchar(11) NOT NULL,
  `cantidad` float NOT NULL,
  `precio` float NOT NULL,
  PRIMARY KEY (`id_detalleMesa`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=319 ;

--
-- Volcado de datos para la tabla `detalle_mesa`
--

INSERT INTO `detalle_mesa` (`id_detalleMesa`, `id_boleta`, `codigo`, `cantidad`, `precio`) VALUES
(1, 1, 'a', 2, 100),
(2, 2, 'A', 2, 100),
(3, 3, 'HCR', 1, 200),
(4, 3, 'a', 1, 100),
(5, 4, 's', 1, 100),
(6, 5, 'AP', 1, 300),
(7, 6, 'hcr', 2, 200),
(10, 9, 'a', 2, 100),
(12, 11, 'a', 2, 100),
(13, 11, 'pb', 1, 250),
(14, 12, 'a', 2, 100),
(15, 13, 'PBA', 1, 160),
(16, 13, 'HCR', 1, 200),
(17, 13, 'P150', 1, 120),
(18, 15, 'PM', 1, 200),
(19, 15, 'P150', 1, 120),
(20, 16, 'HCR', 1, 200),
(21, 16, 'P500', 1, 60),
(22, 17, 'HCL', 1, 180),
(23, 19, 'P150', 1, 120),
(24, 18, 'G', 1, 100),
(25, 18, 'S', 1, 100),
(26, 19, 'HAB', 1, 220),
(27, 19, 'HCL', 1, 180),
(28, 20, 'PBA', 1, 160),
(29, 20, 'P500', 1, 60),
(30, 21, 'HAB', 3, 220),
(31, 21, 'P150', 1, 120),
(32, 22, 'HAB', 2, 220),
(33, 22, 'HAB', 1, 220),
(34, 23, 'A', 2, 100),
(35, 23, 'G', 1, 100),
(36, 23, 'PBA', 2, 160),
(37, 24, 'g', 1, 100),
(38, 24, 'PBA', 1, 160),
(39, 25, 'a', 1, 100),
(40, 25, 'g', 1, 100),
(41, 25, 'HCR', 2, 200),
(42, 25, 'PBA', 1, 160),
(43, 22, 'M150', 1, 120),
(44, 22, 'a', 1, 100),
(45, 22, 'g', 1, 100),
(46, 26, 'A', 3, 100),
(47, 27, 'HCL', 2, 180),
(48, 26, 'HCL', 1, 180),
(49, 28, 'A', 4, 100),
(50, 29, 'HAB', 1, 220),
(51, 30, 'HAB', 1, 220),
(52, 30, 'A', 1, 100),
(53, 30, 'AB', 2, 170),
(54, 31, 'PB', 1, 250),
(55, 31, 'S500', 1, 60),
(56, 31, 'G', 2, 100),
(57, 32, 'A', 1, 100),
(58, 33, 'A', 4, 100),
(59, 33, 'S', 1, 100),
(60, 33, 'PB', 1, 250),
(61, 34, 'A', 4, 100),
(62, 35, '1/2es', 1, 230),
(63, 35, '1/2bo', 1, 250),
(64, 35, 'STE', 4, 180),
(65, 35, 'PM', 1, 200),
(66, 36, '1/2bo', 1, 125),
(67, 36, '1/2es', 1, 115),
(68, 36, 'PM', 1, 200),
(69, 36, 'STE', 4, 180),
(70, 28, '1/2bo', 1, 125),
(71, 28, '1/2es', 1, 115),
(72, 37, 'A', 1, 100),
(73, 37, 'HM', 1, 180),
(74, 33, 'G', 1, 100),
(75, 37, 'G', 1, 100),
(76, 38, 'A', 1, 100),
(77, 38, 'PMCL', 1, 150),
(78, 39, 'A', 1, 100),
(79, 39, 'G', 1, 100),
(80, 39, '1/2es', 1, 115),
(81, 40, 'PE', 1, 230),
(82, 40, 'P150', 1, 120),
(83, 41, 'PK', 1, 180),
(84, 41, 'HCL', 1, 180),
(85, 41, 'M125', 1, 100),
(86, 42, 'PMCL', 1, 150),
(87, 42, 'PMZ', 1, 160),
(88, 42, 'M125', 1, 100),
(89, 43, 'PMCL', 2, 150),
(90, 44, 'PMCL', 1, 150),
(91, 44, 'HAB', 1, 220),
(92, 45, 'PBA', 1, 160),
(93, 46, 'PMCL', 1, 150),
(94, 47, 'PMZ', 1, 160),
(95, 47, 'A', 1, 100),
(96, 47, 'G', 1, 100),
(97, 48, 'HCR', 1, 200),
(98, 48, 'PMCL', 1, 150),
(99, 49, 'PMCL', 1, 150),
(100, 50, 'P150', 1, 120),
(101, 50, 'PMCL', 1, 150),
(102, 50, 'HM', 1, 180),
(103, 51, 'HCR', 1, 200),
(104, 51, 'PMCL', 1, 150),
(105, 51, 'M125', 1, 100),
(106, 52, 'PBA', 1, 160),
(107, 52, 'PMCL', 1, 150),
(108, 52, 'P500', 1, 60),
(109, 52, 'Q', 1, 150),
(110, 53, 'PMCL', 2, 150),
(111, 53, 'PBA', 1, 160),
(112, 53, 'BB', 1, 150),
(113, 53, 'Q', 1, 150),
(114, 54, 'G', 2, 100),
(115, 54, 'HCR', 1, 200),
(116, 54, 'HAB', 1, 220),
(117, 54, 'AB', 1, 170),
(118, 55, 'HAB', 1, 220),
(119, 55, 'HCR', 1, 200),
(120, 55, 'M500', 1, 60),
(121, 56, 'PMCL', 1, 150),
(122, 56, '1/2es', 1, 115),
(123, 56, 'M125', 1, 100),
(124, 55, 'GAN', 3, 150),
(125, 57, '1/2fu', 1, 110),
(126, 57, 'STE', 1, 180),
(127, 58, 'G', 2, 100),
(128, 59, 'STE', 1, 180),
(129, 60, 'P150', 1, 120),
(130, 58, 'A', 4, 100),
(131, 61, 'PMCL', 1, 150),
(132, 61, 'HCR', 1, 200),
(133, 61, 'A', 1, 100),
(134, 61, 'P500', 1, 60),
(135, 62, 'ha', 1, 150),
(139, 64, 'P150', 1, 120),
(140, 64, 'pmccl', 1, 180),
(141, 64, 'HCL', 1, 180),
(142, 65, 'HSA', 2, 150),
(143, 65, 'HGA', 2, 150),
(145, 65, 'HA', 2, 150),
(146, 65, 'HCL', 1, 180),
(147, 65, 'pmccl', 2, 180),
(148, 66, 'HSA', 1, 150),
(149, 67, 'HA', 1, 150),
(150, 68, 'PE', 1, 230),
(151, 68, 'HB', 1, 180),
(152, 68, 'M500', 1, 60),
(153, 68, 'P150', 1, 120),
(154, 66, 'HSG', 1, 150),
(155, 69, 'HAB', 2, 220),
(156, 70, 'HGA', 1, 150),
(157, 71, 'PM', 2, 200),
(158, 71, 'PMZ', 1, 160),
(159, 71, 'P150', 1, 120),
(160, 71, 'Q', 1, 150),
(161, 72, 'HG', 3, 150),
(162, 73, 'HGA', 1, 150),
(163, 73, 'HCR', 1, 200),
(164, 73, 'PBA', 1, 160),
(165, 74, 'HAB', 2, 220),
(166, 74, 'HGA', 1, 150),
(167, 74, 'pmccl', 1, 180),
(168, 74, 'P500', 1, 60),
(169, 72, 'pmccl', 2, 180),
(170, 72, 'HCL', 2, 180),
(171, 75, 'pmccl', 3, 180),
(172, 75, 'HGA', 1, 150),
(173, 76, 'PB', 1, 250),
(174, 76, 'HS', 1, 150),
(175, 75, 'HSG', 1, 150),
(176, 77, 'pmccl', 1, 180),
(177, 78, 'Q', 1, 150),
(178, 78, 'PM', 1, 200),
(179, 79, 'HAB', 3, 220),
(180, 79, 'HG', 1, 150),
(181, 79, 'pmccl', 3, 180),
(182, 79, 'PMCL', 1, 150),
(183, 79, 'PM', 1, 200),
(184, 79, 'P150', 1, 120),
(185, 79, 'M150', 1, 120),
(186, 79, 'A5', 1, 50),
(187, 79, 'Q', 1, 150),
(188, 80, 'HCL', 1, 180),
(189, 80, 'pmccl', 1, 180),
(190, 80, 'P150', 1, 120),
(191, 81, 'HCL', 1, 180),
(192, 81, 'HAB', 1, 220),
(193, 81, 'HGA', 2, 150),
(194, 80, 'HSG', 1, 150),
(195, 82, '1/2mu', 1, 100),
(196, 82, 'HA', 1, 150),
(197, 82, 'P150', 1, 120),
(198, 83, 'PB', 2, 250),
(199, 83, 'P150', 1, 120),
(200, 79, 'G', 1, 100),
(201, 84, 'HA', 3, 150),
(202, 85, 'HS', 1, 150),
(203, 85, 'HCL', 1, 180),
(204, 85, '1/2es', 1, 115),
(205, 85, 'Q', 1, 150),
(206, 86, 'HGA', 1, 150),
(207, 82, 'HGA', 1, 150),
(208, 87, 'PB', 1, 250),
(209, 87, 'S125', 1, 100),
(210, 88, 'HGA', 2, 150),
(211, 89, 'P150', 1, 120),
(212, 89, 'HSA', 1, 150),
(213, 89, 'HCL', 1, 180),
(214, 89, 'pmccl', 1, 180),
(215, 90, 'HA', 2, 150),
(216, 91, 'HGA', 1, 150),
(217, 92, 'HG', 1, 150),
(218, 91, 'FERN', 1, 100),
(219, 93, 'G', 1, 100),
(220, 90, 'HGA', 1, 150),
(221, 94, 'A', 1, 100),
(222, 95, 'CG', 1, 200),
(223, 96, 'HG', 1, 150),
(224, 90, 'A', 1, 100),
(225, 97, 'HG', 1, 150),
(226, 98, 'hg', 2, 150),
(227, 98, 'P150', 1, 120),
(228, 98, 'pab', 1, 200),
(231, 99, 'CG', 1, 200),
(233, 100, 'P150', 1, 120),
(234, 101, 'PAB', 2, 200),
(235, 101, 'G', 4, 100),
(236, 101, 'hg', 1, 150),
(237, 102, 'HCL', 1, 180),
(238, 102, 'G', 1, 100),
(239, 102, 'S500', 1, 60),
(240, 103, 'HCR', 5, 200),
(241, 103, 'HS', 2, 150),
(242, 104, 'PAB', 1, 200),
(243, 101, 'HCR', 1, 200),
(244, 98, 'HCL', 1, 180),
(245, 105, 'PBA', 1, 160),
(246, 105, 'M125', 1, 100),
(247, 100, 'HCL', 2, 180),
(248, 106, 'PAB', 2, 200),
(249, 103, 'p150', 2, 120),
(250, 107, 'hg', 1, 150),
(251, 108, 'PM', 1, 200),
(252, 108, '1/2mu', 1, 100),
(253, 108, 'S500', 2, 60),
(254, 108, 'P500', 1, 60),
(255, 108, 'lq', 1, 70),
(257, 110, 'HCL', 3, 180),
(258, 110, 'HCR', 1, 200),
(259, 110, 'P150', 1, 120),
(260, 110, 'G', 1, 100),
(261, 111, 'PM', 1, 200),
(262, 111, 'PBA', 1, 160),
(263, 111, 'A', 3, 100),
(264, 111, 'S', 3, 100),
(265, 112, 'PM', 1, 200),
(266, 112, 'M150', 1, 120),
(267, 112, 'Q', 3, 150),
(268, 112, 'PBA', 1, 160),
(271, 113, 'G', 1, 100),
(272, 113, 'A', 1, 100),
(273, 113, 'S', 3, 100),
(274, 113, 'PAB', 2, 200),
(275, 113, 'HCR', 1, 200),
(276, 114, 'Q', 1, 150),
(277, 115, 'HCL', 4, 180),
(278, 115, 'P150', 1, 120),
(279, 107, 'A', 1, 100),
(280, 107, 'G', 3, 100),
(282, 112, 'HCR', 1, 200),
(283, 117, 'HCL', 4, 180),
(284, 117, 'PBA', 1, 160),
(285, 117, 'P150', 1, 120),
(286, 118, 'G', 1, 100),
(287, 118, 'S', 1, 100),
(289, 119, 'Q', 1, 150),
(290, 119, 'STE', 2, 180),
(291, 118, 'A', 1, 100),
(292, 119, '1/2es', 1, 115),
(293, 119, '1/2mu', 1, 100),
(294, 120, 'A', 2, 100),
(295, 120, 'G', 3, 100),
(296, 111, 'Q', 1, 150),
(297, 121, 'PAB', 2, 200),
(298, 121, 'P150', 1, 120),
(299, 122, 'A', 1, 100),
(300, 122, 'G', 2, 100),
(301, 123, 'A', 2, 100),
(302, 123, 'G', 2, 100),
(303, 124, 'M500', 1, 60),
(304, 124, 'Q', 1, 150),
(305, 125, 'PB', 1, 250),
(306, 125, '1/2es', 1, 115),
(307, 125, '1/2fu', 1, 110),
(309, 127, 'PBA', 1, 160),
(310, 127, 'G', 2, 100),
(311, 128, 'HB', 1, 180),
(312, 128, 'G', 4, 100),
(313, 113, 'Q', 1, 150),
(314, 113, 'AB', 1, 170),
(315, 126, 'G', 4, 100),
(316, 126, 'A', 2, 100),
(317, 129, 'LQ', 1, 70),
(318, 130, 'PE', 1, 230);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equiv_prom`
--

CREATE TABLE IF NOT EXISTS `equiv_prom` (
  `id_equiv` int(11) NOT NULL AUTO_INCREMENT,
  `codigo_promocion` varchar(10) NOT NULL,
  `codigo_articulo` varchar(10) NOT NULL,
  `cantidad_articulo` int(11) NOT NULL,
  PRIMARY KEY (`id_equiv`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Volcado de datos para la tabla `equiv_prom`
--

INSERT INTO `equiv_prom` (`id_equiv`, `codigo_promocion`, `codigo_articulo`, `cantidad_articulo`) VALUES
(1, 'PCA', 'hcr', 1),
(2, 'PCA', 'a', 1),
(3, 'PCG', 'hcr', 1),
(4, 'PCG', 'g', 1),
(5, 'PCH', 'hcr', 1),
(6, 'PCH', 'h', 1),
(12, 'HG', 'g', 2),
(13, 'HA', 'a', 2),
(14, 'HH', 'h', 2),
(15, 'HGH', 'g', 1),
(16, 'HGH', 'h', 1),
(17, 'HGA', 'g', 1),
(18, 'HGA', 'a', 1),
(19, 'P2', 'pk', 2),
(20, 'EP', 'hcr', 1),
(21, 'EP', 'a', 1),
(22, 'AP', 'HAB', 1),
(23, 'AP', 'A', 1),
(24, 'HS', 's', 2),
(25, 'HSG', 's', 1),
(26, 'HSG', 'g', 1),
(27, 'HSA', 's', 1),
(28, 'HSA', 'a', 1),
(29, 'PCS', 'HCR', 1),
(30, 'PCS', 'S', 1),
(31, 'PAB', 'hab', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturacion_boleta`
--

CREATE TABLE IF NOT EXISTS `facturacion_boleta` (
  `id_factBoleta` int(11) NOT NULL AUTO_INCREMENT,
  `id_boleta` int(11) NOT NULL,
  `id_condVta` int(11) NOT NULL,
  `monto` float NOT NULL,
  PRIMARY KEY (`id_factBoleta`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=126 ;

--
-- Volcado de datos para la tabla `facturacion_boleta`
--

INSERT INTO `facturacion_boleta` (`id_factBoleta`, `id_boleta`, `id_condVta`, `monto`) VALUES
(1, 1, 1, 200),
(2, 2, 1, 200),
(3, 3, 1, 300),
(4, 4, 1, 100),
(5, 5, 1, 300),
(6, 6, 1, 400),
(7, 9, 1, 200),
(8, 11, 1, 450),
(9, 12, 1, 200),
(10, 14, 1, 0),
(11, 15, 1, 320),
(12, 13, 1, 480),
(13, 17, 1, 180),
(14, 16, 1, 260),
(15, 20, 1, 220),
(16, 19, 1, 520),
(17, 18, 1, 200),
(18, 27, 1, 360),
(19, 22, 1, 980),
(20, 21, 1, 780),
(21, 23, 1, 620),
(22, 25, 1, 760),
(23, 24, 1, 260),
(24, 32, 1, 100),
(25, 26, 1, 480),
(26, 31, 1, 510),
(27, 35, 1, 1400),
(28, 36, 1, 1160),
(29, 33, 1, 850),
(30, 30, 1, 660),
(31, 37, 1, 380),
(32, 28, 1, 640),
(33, 34, 1, 400),
(34, 38, 1, 250),
(35, 39, 1, 315),
(36, 41, 1, 460),
(37, 40, 1, 350),
(38, 42, 1, 410),
(39, 43, 1, 300),
(40, 44, 1, 370),
(41, 45, 1, 160),
(42, 46, 1, 150),
(43, 48, 1, 350),
(44, 49, 1, 150),
(45, 50, 1, 450),
(46, 51, 1, 450),
(47, 47, 1, 360),
(48, 52, 1, 520),
(49, 54, 1, 790),
(50, 57, 1, 290),
(51, 56, 1, 365),
(52, 53, 1, 760),
(53, 60, 1, 120),
(54, 59, 1, 180),
(55, 55, 1, 930),
(56, 61, 1, 510),
(57, 58, 1, 600),
(58, 62, 1, 150),
(59, 64, 1, 480),
(60, 67, 1, 150),
(61, 68, 1, 590),
(62, 69, 1, 440),
(63, 70, 1, 150),
(64, 65, 1, 1440),
(65, 66, 1, 300),
(66, 71, 1, 830),
(67, 73, 1, 510),
(68, 77, 1, 180),
(69, 76, 1, 400),
(70, 74, 1, 830),
(71, 72, 1, 1170),
(72, 78, 1, 350),
(73, 81, 1, 700),
(74, 79, 1, 2240),
(75, 83, 1, 620),
(76, 80, 1, 630),
(77, 85, 1, 595),
(78, 87, 1, 350),
(79, 82, 1, 520),
(80, 75, 1, 840),
(81, 84, 1, 450),
(82, 89, 1, 630),
(83, 92, 1, 150),
(84, 91, 1, 250),
(85, 93, 1, 100),
(86, 94, 1, 100),
(87, 88, 1, 300),
(88, 86, 1, 150),
(89, 96, 1, 150),
(90, 90, 1, 550),
(91, 97, 1, 150),
(92, 95, 1, 200),
(93, 99, 1, 200),
(94, 104, 1, 200),
(95, 98, 1, 800),
(96, 100, 1, 480),
(97, 102, 1, 340),
(98, 106, 1, 400),
(100, 105, 1, 260),
(101, 108, 1, 550),
(102, 110, 1, 960),
(103, 101, 1, 1150),
(104, 103, 1, 1540),
(106, 115, 1, 840),
(107, 112, 1, 1130),
(108, 111, 1, 1110),
(109, 121, 1, 520),
(110, 122, 1, 300),
(111, 117, 1, 1000),
(112, 107, 1, 550),
(113, 124, 1, 210),
(114, 118, 1, 300),
(115, 125, 1, 475),
(116, 127, 1, 360),
(117, 128, 1, 580),
(118, 120, 1, 500),
(119, 119, 1, 725),
(120, 129, 1, 70),
(121, 113, 1, 1420),
(122, 126, 1, 600),
(123, 130, 1, 230),
(124, 123, 1, 400),
(125, 114, 1, 150);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturacion_costo`
--

CREATE TABLE IF NOT EXISTS `facturacion_costo` (
  `id_factCosto` int(11) NOT NULL AUTO_INCREMENT,
  `id_costo` int(11) NOT NULL,
  `id_condVta` int(11) NOT NULL,
  `monto` float NOT NULL,
  PRIMARY KEY (`id_factCosto`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Volcado de datos para la tabla `facturacion_costo`
--

INSERT INTO `facturacion_costo` (`id_factCosto`, `id_costo`, `id_condVta`, `monto`) VALUES
(1, 1, 1, 300),
(2, 2, 1, 600),
(3, 3, 1, 315),
(4, 4, 1, 280),
(5, 4, 1, 300),
(6, 4, 1, 300),
(7, 5, 1, 200),
(8, 6, 1, 300),
(9, 7, 1, 220),
(10, 8, 1, 1600),
(11, 9, 1, 180),
(12, 10, 1, 600),
(13, 11, 1, 300),
(14, 11, 1, 500),
(15, 12, 1, 500),
(16, 13, 1, 540),
(17, 14, 1, 3370),
(18, 15, 1, 440),
(19, 16, 1, 90),
(20, 17, 1, 390),
(21, 18, 1, 780),
(22, 19, 1, 210),
(23, 20, 1, 300),
(24, 21, 1, 600),
(25, 22, 1, 93),
(26, 23, 1, 50),
(27, 24, 1, 225),
(28, 25, 1, 780),
(29, 26, 1, 80),
(30, 27, 1, 2190),
(31, 28, 1, 410),
(32, 29, 1, 635),
(33, 30, 1, 150),
(34, 31, 1, 350),
(35, 32, 1, 150),
(36, 33, 1, 1380),
(37, 34, 1, 350),
(38, 35, 1, 560),
(39, 36, 1, 300),
(40, 37, 1, 2840);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mesa`
--

CREATE TABLE IF NOT EXISTS `mesa` (
  `numero_mesa` int(11) NOT NULL,
  `id_mozo` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  PRIMARY KEY (`numero_mesa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `privilegio`
--

CREATE TABLE IF NOT EXISTS `privilegio` (
  `id_privilegio` int(11) NOT NULL AUTO_INCREMENT,
  `desc_privilegio` varchar(20) NOT NULL,
  PRIMARY KEY (`id_privilegio`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `privilegio`
--

INSERT INTO `privilegio` (`id_privilegio`, `desc_privilegio`) VALUES
(1, 'Administrador'),
(2, 'Mozo'),
(3, 'Comprador');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_costo`
--

CREATE TABLE IF NOT EXISTS `tipo_costo` (
  `id_tipoCosto` int(11) NOT NULL AUTO_INCREMENT,
  `desc_tipoCosto` varchar(20) NOT NULL,
  PRIMARY KEY (`id_tipoCosto`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Volcado de datos para la tabla `tipo_costo`
--

INSERT INTO `tipo_costo` (`id_tipoCosto`, `desc_tipoCosto`) VALUES
(1, 'Alquiler'),
(2, 'Mantenimiento'),
(3, 'Servicios'),
(4, 'Comida'),
(5, 'Bebida'),
(6, 'Super'),
(7, 'Barriles'),
(8, 'Nafta'),
(9, 'Otros'),
(10, 'Sueldo'),
(11, 'Publicidad'),
(15, 'Pruebas'),
(16, 'No'),
(17, 'Verduras'),
(18, 'Carniceria'),
(19, 'Panaderia'),
(20, 'Municipal');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_prod`
--

CREATE TABLE IF NOT EXISTS `tipo_prod` (
  `id_tipoProd1` int(11) NOT NULL AUTO_INCREMENT,
  `desc_tipoProd1` varchar(30) NOT NULL,
  PRIMARY KEY (`id_tipoProd1`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `tipo_prod`
--

INSERT INTO `tipo_prod` (`id_tipoProd1`, `desc_tipoProd1`) VALUES
(1, 'Comida'),
(2, 'Bebida'),
(3, 'Promocion'),
(4, 'Descuento');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_prod2`
--

CREATE TABLE IF NOT EXISTS `tipo_prod2` (
  `id_tipoProd2` int(11) NOT NULL AUTO_INCREMENT,
  `desc_tipoProd2` varchar(30) NOT NULL,
  `id_tipoProd1` int(11) NOT NULL,
  PRIMARY KEY (`id_tipoProd2`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Volcado de datos para la tabla `tipo_prod2`
--

INSERT INTO `tipo_prod2` (`id_tipoProd2`, `desc_tipoProd2`, `id_tipoProd1`) VALUES
(1, 'Vodka', 2),
(2, 'Fernet', 2),
(3, 'Gancia', 2),
(4, 'Cerveza Industrial', 2),
(5, 'Cerveza Artesanal', 2),
(6, 'Hamburguesa', 1),
(7, 'Pizza', 1),
(8, 'Papas Fritas', 1),
(9, 'Hot Dogs', 1),
(10, 'Promocion', 3),
(11, 'Descuento', 4),
(14, 'Happy Hour', 3),
(15, 'Hamburguesa', 3),
(16, 'Cerveza 2x1', 3),
(17, 'Comida + Bebida', 3),
(18, 'Empanadas', 1),
(19, 'Whisky', 2),
(20, 'Gaseosa', 2),
(21, 'Agua', 2),
(22, '', 0),
(23, 'Stella ARtois', 2),
(24, 'Comida', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `dni` int(11) NOT NULL,
  `cuenta_user` varchar(20) NOT NULL,
  `contraseña_user` varchar(20) NOT NULL,
  `nombre_user` varchar(20) NOT NULL,
  `apellido_user` varchar(20) NOT NULL,
  `direccion_user` varchar(30) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `id_privilegio` int(11) NOT NULL,
  `habilitado` int(11) NOT NULL,
  PRIMARY KEY (`dni`),
  KEY `direccion_user` (`direccion_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`dni`, `cuenta_user`, `contraseña_user`, `nombre_user`, `apellido_user`, `direccion_user`, `telefono`, `id_privilegio`, `habilitado`) VALUES
(1, 'jazmin', 'jazmin', 'Jazmin', '-', '1', '1', 2, 1),
(11, 'QWER', 'QPEMR', 'Lucas', '-', 'aa', 'ssdf', 2, 1),
(1111, 'belen', 'belen', 'Belen', '-', '-', '-', 2, 1),
(2222, 'Cami', 'Cami', 'Cami', '-', '-', '-', 2, 1),
(11111111, 'admin', 'admin', 'Boston', 'Beer & Co', 'Sixto Ovejero', '1111111', 1, 1),
(37305672, 'rmmayer', 'rodri0104', 'Rodrigo', 'Mayer', 'Maipu 76', '3886525523', 1, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
