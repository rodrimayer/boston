-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 15-06-2020 a las 20:58:49
-- Versión del servidor: 5.7.21
-- Versión de PHP: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_boston`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulo`
--

DROP TABLE IF EXISTS `articulo`;
CREATE TABLE IF NOT EXISTS `articulo` (
  `codigo` varchar(10) NOT NULL,
  `id_tipoProd` int(11) NOT NULL,
  `id_tipoProd2` int(11) NOT NULL,
  `precio_costo` float NOT NULL,
  `precio_vta` float NOT NULL,
  `desc_articulo` varchar(30) NOT NULL,
  `baja` int(11) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `articulo`
--

INSERT INTO `articulo` (`codigo`, `id_tipoProd`, `id_tipoProd2`, `precio_costo`, `precio_vta`, `desc_articulo`, `baja`) VALUES
('A', 2, 5, 50, 100, 'Amber', 0),
('DF', 4, 11, 0, 30, 'Descuento Futbol', 0),
('DM', 4, 11, 15, 15, 'Descuento Mujer', 0),
('DP', 1, 18, 100, 200, 'Docena Pollo', 1),
('EP', 3, 17, 10, 200, 'Empanada + Pinta Amber', 0),
('G', 2, 5, 50, 100, 'Golden', 0),
('H', 2, 5, 50, 120, 'Honey', 0),
('HA', 3, 14, 100, 150, 'Happy Amber', 0),
('HAB', 1, 6, 100, 220, 'McDorian Abusa', 0),
('HAH', 3, 14, 100, 150, 'Happy Amber Honey', 0),
('HCL', 1, 6, 70, 180, 'McDorian Clasica', 0),
('HCR', 1, 6, 80, 200, 'McDorian Criminal', 0),
('HG', 3, 14, 100, 150, 'Happy Golden', 0),
('HGA', 3, 14, 100, 150, 'Happy Golden Amber', 0),
('HGH', 3, 14, 100, 150, 'Happy Golden Honey', 0),
('HH', 3, 14, 100, 150, 'Happy Honey', 0),
('I', 2, 5, 50, 100, 'Ipa', 0),
('P2', 3, 16, 160, 200, 'Patag 2x1', 0),
('P24', 2, 4, 80, 160, 'Patagonia 24/7', 0),
('PC', 3, 17, 100, 300, 'Pinta + hamb', 0),
('PCA', 3, 17, 130, 250, 'Promo Criminal + Amber', 0),
('PCG', 3, 17, 130, 250, 'Promo Criminal + Golden', 0),
('PCH', 3, 17, 130, 250, 'Promo Criminal + Honey', 0),
('PCP', 3, 17, 150, 300, 'Promo Criminal + 2 Pintas', 0),
('PF', 1, 7, 100, 230, 'Pizza Fugazzeta', 0),
('PK', 2, 4, 80, 180, 'Patagonia Kune', 0),
('S', 2, 5, 50, 100, 'Stout', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `barril`
--

DROP TABLE IF EXISTS `barril`;
CREATE TABLE IF NOT EXISTS `barril` (
  `id_barril` int(11) NOT NULL AUTO_INCREMENT,
  `desc_barril` varchar(20) NOT NULL,
  `cantidad_total` float NOT NULL,
  `cantidad_consumida` float NOT NULL,
  `posicion` int(11) NOT NULL,
  PRIMARY KEY (`id_barril`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `barril`
--

INSERT INTO `barril` (`id_barril`, `desc_barril`, `cantidad_total`, `cantidad_consumida`, `posicion`) VALUES
(1, 'Amber', 30, 30, 1),
(2, 'Golden', 30, 30, 2),
(3, 'Honey', 30, 30, 3),
(4, 'Porter', 30, 30, 2),
(5, 'Stout', 30, 30, 2),
(6, 'Amber', 30, 30, 1),
(7, 'Honey', 30, 30, 3),
(8, 'Amber', 28, 28, 1),
(9, 'Golden', 20, 0.5, 1),
(10, 'Stout', 50, 50, 3),
(11, 'Amber', 50, 10.5, 2),
(12, 'Golden', 40, 40, 4),
(13, 'Honey', 40, 6.5, 4),
(14, 'Ipa', 30, 2, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `boleta`
--

DROP TABLE IF EXISTS `boleta`;
CREATE TABLE IF NOT EXISTS `boleta` (
  `id_boleta` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_boleta` date NOT NULL,
  `hora_apertura` time NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `mesa` int(11) NOT NULL,
  `id_mozo` int(11) NOT NULL,
  `total` float NOT NULL,
  `cerrada` int(11) NOT NULL,
  `pagada` int(11) NOT NULL,
  `fecha_cierre` date DEFAULT NULL,
  `hora_cierre` time NOT NULL,
  PRIMARY KEY (`id_boleta`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `boleta`
--

INSERT INTO `boleta` (`id_boleta`, `fecha_boleta`, `hora_apertura`, `id_cliente`, `mesa`, `id_mozo`, `total`, `cerrada`, `pagada`, `fecha_cierre`, `hora_cierre`) VALUES
(1, '2019-09-13', '00:00:00', 1, 0, 37305672, 450, 1, 1, '2019-09-13', '00:00:00'),
(2, '2019-09-13', '00:00:00', 1, 1, 37305672, 1000, 1, 1, '2019-09-13', '00:00:00'),
(3, '2019-09-13', '00:00:00', 1, 1, 37305672, 520, 1, 1, '2019-09-13', '00:00:00'),
(4, '2019-09-14', '00:00:00', 1, 0, 11111111, 350, 1, 1, '2019-09-14', '00:00:00'),
(5, '2019-09-17', '00:00:00', 1, 0, 37305672, 200, 1, 1, '2019-09-17', '00:00:00'),
(6, '2019-09-17', '00:00:00', 1, 0, 37305672, 300, 1, 1, '2019-09-17', '00:00:00'),
(7, '2019-09-17', '00:00:00', 1, 0, 37305672, 200, 1, 1, '2019-09-17', '00:00:00'),
(8, '2019-09-17', '00:00:00', 1, 0, 37305672, 200, 1, 1, '2019-09-17', '00:00:00'),
(9, '2019-09-17', '00:00:00', 1, 1, 37305672, 500, 1, 1, '2019-09-17', '00:00:00'),
(10, '2019-09-17', '00:00:00', 1, 0, 37305672, 1100, 1, 1, '2019-09-17', '00:00:00'),
(11, '2019-09-17', '00:00:00', 1, 1, 37305672, 500, 1, 1, '2019-09-17', '00:00:00'),
(12, '2019-09-17', '00:00:00', 1, 1, 37305672, 900, 1, 1, '2019-09-17', '00:00:00'),
(13, '2019-09-17', '00:00:00', 1, 2, 37305672, 200, 1, 1, '2019-09-17', '00:00:00'),
(14, '2019-09-17', '00:00:00', 1, 4, 37305672, 380, 1, 1, '2019-09-17', '00:00:00'),
(15, '2019-09-17', '00:00:00', 1, 3, 37305672, 200, 1, 1, '2019-09-17', '00:00:00'),
(16, '2019-09-17', '00:00:00', 1, 5, 37305672, 300, 1, 1, '2019-09-17', '00:00:00'),
(17, '2019-09-17', '00:00:00', 0, 1, 37305672, 200, 1, 1, '2019-09-17', '00:00:00'),
(18, '2019-09-17', '00:00:00', 1, 0, 11111111, 740, 1, 1, '2019-09-17', '00:00:00'),
(19, '2019-09-17', '00:00:00', 1, 0, 11111111, 1060, 1, 1, '2019-09-17', '00:00:00'),
(20, '2019-09-20', '00:00:00', 1, 0, 37305672, 200, 1, 1, '2019-09-20', '00:00:00'),
(21, '2019-09-20', '00:00:00', 1, 0, 37305672, 200, 1, 1, '2019-09-20', '00:00:00'),
(22, '2019-09-21', '00:00:00', 1, 1, 11111111, 420, 1, 1, '2019-09-21', '00:00:00'),
(23, '2019-09-25', '23:19:24', 1, 0, 11111111, 1020, 2, 1, '2019-09-25', '23:19:24'),
(24, '2019-09-26', '00:25:11', 1, 0, 11111111, 870, 3, 1, '2019-09-26', '00:25:11'),
(25, '2019-09-27', '13:36:14', 1, 0, 37305672, 810, 3, 1, '2019-09-27', '13:36:14'),
(26, '2019-10-09', '20:18:18', 1, 0, 11111111, 620, 0, 1, '2019-10-09', '20:18:18'),
(27, '2019-10-09', '20:19:43', 1, 1, 11111111, 550, 0, 1, '2019-10-09', '20:20:05'),
(28, '2019-10-10', '19:14:52', 1, 0, 11111111, 350, 0, 1, '2019-10-10', '19:14:52'),
(29, '2019-10-10', '19:15:56', 1, 1, 11111111, 300, 0, 1, '2019-10-10', '19:17:16'),
(30, '2019-10-10', '19:20:12', 1, 0, 11111111, 300, 0, 1, '2019-10-10', '19:20:12'),
(31, '2020-02-27', '00:11:24', 1, 10, 11111111, 500, 0, 1, '2020-02-27', '00:19:44'),
(32, '2020-02-27', '00:21:44', 1, 10, 11111111, 630, 0, 1, '2020-04-18', '02:48:17'),
(33, '2020-04-18', '02:46:42', 1, 1, 37305672, 940, 0, 1, '2020-04-18', '02:47:16'),
(34, '2020-06-09', '17:02:40', 1, 0, 11111111, 320, 0, 1, '2020-06-09', '17:02:40'),
(35, '2020-06-09', '17:24:22', 1, 0, 37305672, 200, 0, 1, '2020-06-09', '17:24:22'),
(36, '2020-06-09', '17:24:58', 1, 0, 11111111, 200, 0, 1, '2020-06-09', '17:24:58'),
(37, '2020-06-10', '17:59:48', 1, 2, 11111111, 1040, 0, 1, '2020-06-10', '19:30:25'),
(38, '2020-06-10', '19:31:00', 1, 1, 11111111, 220, 0, 1, '2020-06-10', '19:31:15'),
(39, '2020-06-10', '19:44:26', 1, 1, 11111111, 500, 0, 1, '2020-06-10', '20:04:46'),
(40, '2020-06-10', '20:05:29', 1, 0, 11111111, 200, 0, 1, '2020-06-10', '20:05:29'),
(41, '2020-06-10', '20:07:40', 1, 0, 11111111, 200, 0, 1, '2020-06-10', '20:07:40'),
(42, '2020-06-10', '20:12:21', 1, 0, 11111111, 200, 0, 1, '2020-06-10', '20:12:21'),
(43, '2020-06-10', '20:08:05', 1, 0, 11111111, 180, 0, 1, '2020-06-10', '20:08:05'),
(44, '2020-06-10', '20:08:53', 1, 0, 37305672, 100, 0, 1, '2020-06-10', '20:08:53'),
(45, '2020-06-10', '20:22:27', 1, 0, 11111111, 380, 0, 1, '2020-06-10', '20:22:27'),
(46, '2020-06-10', '21:17:28', 1, 0, 11111111, 200, 0, 1, '2020-06-10', '21:17:28'),
(47, '2020-06-10', '21:49:11', 1, 1, 11111111, 600, 0, 1, '2020-06-10', '23:43:41'),
(48, '2020-06-10', '23:44:11', 1, 2, 11111111, 500, 0, 1, '2020-06-10', '23:52:41'),
(49, '2020-06-10', '23:53:14', 1, 2, 11111111, 200, 0, 0, '2020-06-10', '00:00:00'),
(50, '2020-06-11', '00:00:43', 1, 0, 11111111, 200, 0, 1, '2020-06-11', '00:00:43'),
(51, '2020-06-11', '00:02:50', 1, 5, 11111111, 100, 0, 0, '2020-06-11', '00:00:00'),
(52, '2020-06-11', '00:20:16', 1, 10, 11111111, 200, 0, 0, '2020-06-11', '00:00:00'),
(53, '2020-06-11', '00:26:31', 1, 0, 11111111, 200, 0, 1, '2020-06-11', '00:26:31'),
(54, '2020-06-11', '00:39:17', 1, 0, 11111111, 100, 0, 1, '2020-06-11', '00:39:17'),
(55, '2020-06-11', '10:30:55', 1, 4, 11111111, 600, 0, 0, '2020-06-11', '00:00:00'),
(56, '2020-06-11', '15:38:41', 1, 0, 11111111, 300, 0, 1, '2020-06-11', '15:38:41'),
(57, '2020-06-14', '16:33:34', 1, 6, 11111111, 300, 0, 0, '2020-06-14', '00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cierre_caja`
--

DROP TABLE IF EXISTS `cierre_caja`;
CREATE TABLE IF NOT EXISTS `cierre_caja` (
  `id_cierreCaja` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_cierre` date NOT NULL,
  `hora_cierre` time NOT NULL,
  `venta_contado` float NOT NULL,
  `venta_credito` float NOT NULL,
  `venta_debito` float NOT NULL,
  `costo_contado` float NOT NULL,
  `costo_credito` float NOT NULL,
  `costo_debito` float NOT NULL,
  PRIMARY KEY (`id_cierreCaja`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cierre_caja`
--

INSERT INTO `cierre_caja` (`id_cierreCaja`, `fecha_cierre`, `hora_cierre`, `venta_contado`, `venta_credito`, `venta_debito`, `costo_contado`, `costo_credito`, `costo_debito`) VALUES
(1, '2019-09-25', '23:12:39', 820, 0, 0, 0, 0, 0),
(2, '2019-09-25', '23:23:22', 1020, 0, 0, 900, 0, 0),
(3, '2019-09-27', '13:37:07', 1680, 0, 0, 100, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

DROP TABLE IF EXISTS `cliente`;
CREATE TABLE IF NOT EXISTS `cliente` (
  `id_cliente` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_cliente` varchar(30) NOT NULL,
  `apellido_cliente` varchar(30) NOT NULL,
  `direccion_cliente` varchar(30) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `mail` varchar(30) DEFAULT NULL,
  `id_cta_cte` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_cliente`)
) ENGINE=InnoDB AUTO_INCREMENT=37305628 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id_cliente`, `nombre_cliente`, `apellido_cliente`, `direccion_cliente`, `telefono`, `mail`, `id_cta_cte`) VALUES
(1, 'Consumidor', 'Final', 'Alla', '-', '-', 6),
(37305627, 'Juan Ignacio', 'Rodriguez', 'Buenos Aires', '3886', 'cani.r6@gmail.com', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cond_vta`
--

DROP TABLE IF EXISTS `cond_vta`;
CREATE TABLE IF NOT EXISTS `cond_vta` (
  `id_condVta` int(11) NOT NULL AUTO_INCREMENT,
  `desc_condVta` varchar(30) NOT NULL,
  PRIMARY KEY (`id_condVta`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cond_vta`
--

INSERT INTO `cond_vta` (`id_condVta`, `desc_condVta`) VALUES
(1, 'CONTADO'),
(2, 'CREDITO'),
(3, 'DEBITO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracion`
--

DROP TABLE IF EXISTS `configuracion`;
CREATE TABLE IF NOT EXISTS `configuracion` (
  `id_configuracion` int(11) NOT NULL AUTO_INCREMENT,
  `desc_configuracion` varchar(25) NOT NULL,
  `valor_configuracion` float NOT NULL,
  PRIMARY KEY (`id_configuracion`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `configuracion`
--

INSERT INTO `configuracion` (`id_configuracion`, `desc_configuracion`, `valor_configuracion`) VALUES
(1, 'Happy Hour', 80),
(4, 'Impresion', 0),
(7, 'Impresion rapida', 1),
(8, 'Impresion mesa', 0),
(9, 'Impresion caja', 0),
(10, 'Caños', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `costo`
--

DROP TABLE IF EXISTS `costo`;
CREATE TABLE IF NOT EXISTS `costo` (
  `id_costo` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_costo` date NOT NULL,
  `hora_costo` time NOT NULL,
  `id_tipoCosto` int(11) NOT NULL,
  `desc_costo` varchar(20) NOT NULL,
  `valor` float NOT NULL,
  `cerrado` int(11) NOT NULL,
  PRIMARY KEY (`id_costo`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `costo`
--

INSERT INTO `costo` (`id_costo`, `fecha_costo`, `hora_costo`, `id_tipoCosto`, `desc_costo`, `valor`, `cerrado`) VALUES
(1, '2019-09-25', '23:22:33', 10, 'Cami', 400, 2),
(2, '2019-09-25', '23:23:12', 8, 'Viaje Jujuy', 500, 2),
(3, '2019-09-27', '13:36:36', 10, 'Rodri', 100, 3),
(4, '2019-10-10', '19:20:49', 4, 'Vea', 1500, 0),
(5, '2020-04-18', '02:43:37', 10, 'Moza', 500, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cta_cte`
--

DROP TABLE IF EXISTS `cta_cte`;
CREATE TABLE IF NOT EXISTS `cta_cte` (
  `id_cta_cte` int(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` int(11) NOT NULL,
  `total` float NOT NULL,
  `habilitada` int(11) NOT NULL,
  PRIMARY KEY (`id_cta_cte`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cta_cte`
--

INSERT INTO `cta_cte` (`id_cta_cte`, `id_cliente`, `total`, `habilitada`) VALUES
(5, 37305627, 0, 1),
(6, 1, 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_cta_cte`
--

DROP TABLE IF EXISTS `detalle_cta_cte`;
CREATE TABLE IF NOT EXISTS `detalle_cta_cte` (
  `id_detalle` int(11) NOT NULL AUTO_INCREMENT,
  `id_cta_cte` int(11) NOT NULL,
  `id_boleta` int(11) NOT NULL,
  `monto` float NOT NULL,
  `saldo` float NOT NULL,
  PRIMARY KEY (`id_detalle`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_mesa`
--

DROP TABLE IF EXISTS `detalle_mesa`;
CREATE TABLE IF NOT EXISTS `detalle_mesa` (
  `id_detalleMesa` int(11) NOT NULL AUTO_INCREMENT,
  `id_boleta` int(11) NOT NULL,
  `codigo` varchar(11) NOT NULL,
  `cantidad` float NOT NULL,
  `precio` float NOT NULL,
  PRIMARY KEY (`id_detalleMesa`)
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `detalle_mesa`
--

INSERT INTO `detalle_mesa` (`id_detalleMesa`, `id_boleta`, `codigo`, `cantidad`, `precio`) VALUES
(1, 1, 'hcr', 1, 200),
(2, 1, 'PCA', 1, 250),
(3, 2, 'hcr', 1, 200),
(4, 2, 'PCH', 2, 250),
(5, 2, 'HH', 2, 150),
(6, 3, 'hcr', 1, 200),
(7, 3, 'hab', 1, 220),
(8, 3, 'A', 1, 100),
(9, 4, 'hcr', 1, 200),
(10, 4, 'HA', 1, 150),
(11, 5, 'hcr', 1, 200),
(12, 6, 'hcr', 1, 200),
(13, 6, 'a', 1, 100),
(14, 7, 'hcr', 1, 200),
(15, 8, 'hcr', 1, 200),
(16, 9, 'hcr', 1, 200),
(17, 9, 'a', 1, 100),
(18, 9, 'h', 1, 100),
(19, 9, 'S', 1, 100),
(20, 10, 'hcr', 4, 200),
(21, 10, 'h', 3, 100),
(22, 11, 'A', 1, 100),
(23, 11, 'HCR', 1, 200),
(24, 11, 'S', 2, 100),
(25, 12, 'hcr', 2, 200),
(26, 13, 'hcr', 1, 200),
(27, 14, 'hcr', 1, 200),
(28, 15, 'hcr', 1, 200),
(29, 16, 'hcr', 1, 200),
(30, 12, 'a', 5, 100),
(31, 16, 'h', 1, 100),
(32, 14, 'PK', 1, 180),
(33, 17, 'hcr', 1, 200),
(34, 18, 'HAB', 2, 220),
(35, 18, 's', 3, 100),
(36, 19, 'hcr', 2, 200),
(37, 19, 'HAB', 3, 220),
(38, 20, 'hcr', 1, 200),
(39, 21, 'hcr', 1, 200),
(40, 22, 'hcr', 1, 200),
(41, 22, 'hab', 1, 220),
(42, 23, 'hcl', 1, 180),
(43, 23, 'hcr', 1, 200),
(44, 23, 'hab', 2, 220),
(45, 23, 'A', 2, 100),
(46, 24, 'hcr', 1, 200),
(47, 24, 'hab', 1, 220),
(48, 24, 'a', 2, 100),
(49, 24, 'PCH', 1, 250),
(50, 25, 'hcr', 1, 200),
(51, 25, 'hcl', 2, 180),
(52, 25, 'PCH', 1, 250),
(53, 26, 'hcr', 1, 200),
(54, 26, 'hab', 1, 220),
(55, 26, 'DP', 1, 200),
(56, 27, 'hcr', 1, 200),
(57, 27, 'a', 1, 100),
(58, 27, 's', 1, 100),
(59, 27, 'HA', 1, 150),
(60, 28, 'hcr', 1, 200),
(61, 28, 'HA', 1, 150),
(62, 29, 'a', 3, 100),
(63, 30, 'pc', 1, 300),
(64, 31, 'i', 1, 100),
(65, 31, 'h', 2, 100),
(66, 31, 'hcr', 1, 200),
(67, 32, 'hcr', 1, 200),
(68, 32, 'a', 2, 100),
(69, 33, 'h', 7, 120),
(70, 33, 'a', 1, 100),
(71, 32, 'PF', 1, 230),
(72, 34, 'h', 1, 120),
(73, 34, 'a', 2, 100),
(74, 35, 'hcr', 1, 200),
(75, 36, 'hcr', 1, 200),
(76, 37, 'hcr', 3, 200),
(77, 37, 'a', 2, 100),
(78, 37, 'h', 1, 120),
(79, 37, 'h', 1, 120),
(80, 38, 'a', 1, 100),
(81, 38, 'h', 1, 120),
(82, 39, 'hcr', 2, 200),
(83, 39, 'a', 1, 100),
(84, 40, 'hcr', 1, 200),
(85, 41, 'a', 1, 100),
(86, 41, 'g', 1, 100),
(87, 42, 'HCR', 1, 200),
(88, 43, 'HCL', 1, 180),
(89, 44, 'A', 1, 100),
(90, 45, 'hcr', 1, 200),
(91, 45, 'hcl', 1, 180),
(92, 46, 'hcr', 1, 200),
(93, 47, 'hcr', 2, 200),
(94, 47, 'a', 2, 100),
(95, 48, 'a', 5, 100),
(96, 49, 'hcr', 1, 200),
(97, 50, 'hcr', 1, 200),
(99, 51, 'I', 1, 100),
(100, 52, 'hcr', 1, 200),
(101, 53, 'hcr', 1, 200),
(102, 54, 'a', 1, 100),
(103, 55, 'hcr', 1, 200),
(104, 55, 'hab', 1, 220),
(105, 55, 'hcl', 1, 180),
(106, 56, 'hcr', 1, 200),
(107, 56, 'i', 1, 100),
(108, 57, 'A', 2, 100),
(109, 57, 'i', 1, 100);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equiv_prom`
--

DROP TABLE IF EXISTS `equiv_prom`;
CREATE TABLE IF NOT EXISTS `equiv_prom` (
  `id_equiv` int(11) NOT NULL AUTO_INCREMENT,
  `codigo_promocion` varchar(10) NOT NULL,
  `codigo_articulo` varchar(10) NOT NULL,
  `cantidad_articulo` int(11) NOT NULL,
  PRIMARY KEY (`id_equiv`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `equiv_prom`
--

INSERT INTO `equiv_prom` (`id_equiv`, `codigo_promocion`, `codigo_articulo`, `cantidad_articulo`) VALUES
(1, 'PCA', 'hcr', 1),
(2, 'PCA', 'a', 1),
(3, 'PCG', 'hcr', 1),
(4, 'PCG', 'g', 1),
(5, 'PCH', 'hcr', 1),
(6, 'PCH', 'h', 1),
(12, 'HG', 'g', 2),
(13, 'HA', 'a', 2),
(14, 'HH', 'h', 2),
(15, 'HGH', 'g', 1),
(16, 'HGH', 'h', 1),
(17, 'HGA', 'g', 1),
(18, 'HGA', 'a', 1),
(19, 'P2', 'pk', 2),
(20, 'EP', 'hcr', 1),
(21, 'EP', 'a', 1),
(22, 'PC', 'a', 1),
(23, 'PC', 'hcl', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturacion_boleta`
--

DROP TABLE IF EXISTS `facturacion_boleta`;
CREATE TABLE IF NOT EXISTS `facturacion_boleta` (
  `id_factBoleta` int(11) NOT NULL AUTO_INCREMENT,
  `id_boleta` int(11) NOT NULL,
  `id_condVta` int(11) NOT NULL,
  `monto` float NOT NULL,
  PRIMARY KEY (`id_factBoleta`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `facturacion_boleta`
--

INSERT INTO `facturacion_boleta` (`id_factBoleta`, `id_boleta`, `id_condVta`, `monto`) VALUES
(1, 1, 1, 450),
(2, 2, 1, 1000),
(3, 3, 1, 520),
(4, 4, 1, 350),
(5, 5, 1, 200),
(6, 6, 1, 300),
(7, 7, 1, 200),
(8, 8, 1, 200),
(9, 9, 1, 500),
(10, 10, 1, 1100),
(11, 11, 1, 500),
(12, 13, 1, 200),
(13, 15, 1, 200),
(14, 12, 1, 900),
(15, 16, 1, 300),
(16, 14, 1, 380),
(17, 17, 1, 200),
(18, 18, 1, 740),
(19, 19, 1, 1060),
(20, 20, 1, 200),
(21, 21, 1, 200),
(22, 22, 1, 420),
(23, 23, 1, 1020),
(24, 24, 1, 870),
(25, 25, 1, 810),
(26, 26, 1, 620),
(27, 27, 1, 550),
(28, 28, 1, 350),
(29, 29, 1, 200),
(30, 29, 3, 100),
(31, 30, 1, 300),
(32, 31, 1, 500),
(33, 33, 1, 500),
(34, 33, 3, 440),
(35, 32, 1, 630),
(36, 34, 1, 320),
(37, 35, 1, 200),
(38, 36, 1, 200),
(39, 37, 1, 1040),
(40, 38, 1, 220),
(41, 39, 1, 500),
(42, 40, 1, 200),
(43, 41, 1, 200),
(44, 42, 1, 200),
(45, 43, 1, 180),
(46, 44, 1, 100),
(47, 45, 1, 380),
(48, 46, 1, 200),
(49, 47, 1, 600),
(50, 48, 1, 500),
(51, 50, 1, 200),
(52, 53, 1, 200),
(53, 54, 1, 100),
(54, 56, 1, 300);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturacion_costo`
--

DROP TABLE IF EXISTS `facturacion_costo`;
CREATE TABLE IF NOT EXISTS `facturacion_costo` (
  `id_factCosto` int(11) NOT NULL AUTO_INCREMENT,
  `id_costo` int(11) NOT NULL,
  `id_condVta` int(11) NOT NULL,
  `monto` float NOT NULL,
  PRIMARY KEY (`id_factCosto`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `facturacion_costo`
--

INSERT INTO `facturacion_costo` (`id_factCosto`, `id_costo`, `id_condVta`, `monto`) VALUES
(1, 1, 1, 400),
(2, 2, 1, 500),
(3, 3, 1, 100),
(4, 4, 1, 1500),
(5, 5, 1, 500);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mesa`
--

DROP TABLE IF EXISTS `mesa`;
CREATE TABLE IF NOT EXISTS `mesa` (
  `numero_mesa` int(11) NOT NULL,
  `id_mozo` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  PRIMARY KEY (`numero_mesa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `privilegio`
--

DROP TABLE IF EXISTS `privilegio`;
CREATE TABLE IF NOT EXISTS `privilegio` (
  `id_privilegio` int(11) NOT NULL AUTO_INCREMENT,
  `desc_privilegio` varchar(20) NOT NULL,
  PRIMARY KEY (`id_privilegio`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `privilegio`
--

INSERT INTO `privilegio` (`id_privilegio`, `desc_privilegio`) VALUES
(1, 'Administrador'),
(2, 'Mozo'),
(3, 'Comprador');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_costo`
--

DROP TABLE IF EXISTS `tipo_costo`;
CREATE TABLE IF NOT EXISTS `tipo_costo` (
  `id_tipoCosto` int(11) NOT NULL AUTO_INCREMENT,
  `desc_tipoCosto` varchar(20) NOT NULL,
  PRIMARY KEY (`id_tipoCosto`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_costo`
--

INSERT INTO `tipo_costo` (`id_tipoCosto`, `desc_tipoCosto`) VALUES
(1, 'Alquiler'),
(2, 'Mantenimiento'),
(3, 'Servicios'),
(4, 'Comida'),
(5, 'Bebida'),
(6, 'Comodin'),
(7, 'Barriles'),
(8, 'Nafta'),
(9, 'Otros'),
(10, 'Sueldo'),
(11, 'Publicidad'),
(15, 'Pruebas'),
(16, 'No');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_prod`
--

DROP TABLE IF EXISTS `tipo_prod`;
CREATE TABLE IF NOT EXISTS `tipo_prod` (
  `id_tipoProd1` int(11) NOT NULL AUTO_INCREMENT,
  `desc_tipoProd1` varchar(30) NOT NULL,
  PRIMARY KEY (`id_tipoProd1`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_prod`
--

INSERT INTO `tipo_prod` (`id_tipoProd1`, `desc_tipoProd1`) VALUES
(1, 'Comida'),
(2, 'Bebida'),
(3, 'Promocion'),
(4, 'Descuento');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_prod2`
--

DROP TABLE IF EXISTS `tipo_prod2`;
CREATE TABLE IF NOT EXISTS `tipo_prod2` (
  `id_tipoProd2` int(11) NOT NULL AUTO_INCREMENT,
  `desc_tipoProd2` varchar(30) NOT NULL,
  `id_tipoProd1` int(11) NOT NULL,
  PRIMARY KEY (`id_tipoProd2`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_prod2`
--

INSERT INTO `tipo_prod2` (`id_tipoProd2`, `desc_tipoProd2`, `id_tipoProd1`) VALUES
(1, 'Vodka', 2),
(2, 'Fernet', 2),
(3, 'Gancia', 2),
(4, 'Cerveza Industrial', 2),
(5, 'Cerveza Artesanal', 2),
(6, 'Hamburguesa', 1),
(7, 'Pizza', 1),
(8, 'Papas Fritas', 1),
(9, 'Hot Dogs', 1),
(10, 'Promocion', 3),
(11, 'Descuento', 4),
(14, 'Happy Hour', 3),
(15, 'Hamburguesa', 3),
(16, 'Cerveza 2x1', 3),
(17, 'Comida + Bebida', 3),
(18, 'Empanadas', 1),
(19, 'Whisky', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `dni` int(11) NOT NULL,
  `cuenta_user` varchar(20) NOT NULL,
  `contraseña_user` varchar(20) NOT NULL,
  `nombre_user` varchar(20) NOT NULL,
  `apellido_user` varchar(20) NOT NULL,
  `direccion_user` varchar(30) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `id_privilegio` int(11) NOT NULL,
  `habilitado` int(11) NOT NULL,
  `root` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`dni`),
  KEY `direccion_user` (`direccion_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`dni`, `cuenta_user`, `contraseña_user`, `nombre_user`, `apellido_user`, `direccion_user`, `telefono`, `id_privilegio`, `habilitado`, `root`) VALUES
(11111111, 'admin', 'admin', 'Boston', 'Beer & Co', 'Sixto Ovejero', '1111111', 1, 1, 0),
(37305672, 'rmmayer', 'rodri0104', 'Rodrigo', 'Mayer', 'Maipu 76', '3886525523', 1, 1, 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
