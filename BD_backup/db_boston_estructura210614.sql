-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 14-06-2021 a las 16:53:10
-- Versión del servidor: 5.7.21
-- Versión de PHP: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_boston`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulo`
--

DROP TABLE IF EXISTS `articulo`;
CREATE TABLE IF NOT EXISTS `articulo` (
  `codigo` varchar(10) NOT NULL,
  `id_tipoProd` int(11) NOT NULL,
  `id_tipoProd2` int(11) NOT NULL,
  `precio_costo` float NOT NULL,
  `precio_vta` float NOT NULL,
  `desc_articulo` varchar(30) NOT NULL,
  `baja` int(11) DEFAULT '0',
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `barril`
--

DROP TABLE IF EXISTS `barril`;
CREATE TABLE IF NOT EXISTS `barril` (
  `id_barril` int(11) NOT NULL AUTO_INCREMENT,
  `desc_barril` varchar(20) NOT NULL,
  `cantidad_total` float NOT NULL,
  `cantidad_consumida` float NOT NULL,
  `posicion` int(11) NOT NULL,
  PRIMARY KEY (`id_barril`)
) ENGINE=InnoDB AUTO_INCREMENT=243 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `boleta`
--

DROP TABLE IF EXISTS `boleta`;
CREATE TABLE IF NOT EXISTS `boleta` (
  `id_boleta` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_boleta` date NOT NULL,
  `hora_apertura` time NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `mesa` int(11) NOT NULL DEFAULT '0',
  `id_mozo` int(11) NOT NULL,
  `total` float NOT NULL,
  `cerrada` int(11) NOT NULL,
  `pagada` int(11) NOT NULL,
  `fecha_cierre` date DEFAULT NULL,
  `hora_cierre` time NOT NULL,
  `direccion_delivery` varchar(50) DEFAULT NULL,
  `pago_delivery` float DEFAULT NULL,
  `id_tipoVta` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_boleta`)
) ENGINE=InnoDB AUTO_INCREMENT=14950 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cierre_caja`
--

DROP TABLE IF EXISTS `cierre_caja`;
CREATE TABLE IF NOT EXISTS `cierre_caja` (
  `id_cierreCaja` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_cierre` date NOT NULL,
  `hora_cierre` time NOT NULL,
  `venta_contado` float NOT NULL,
  `venta_credito` float NOT NULL,
  `venta_debito` float NOT NULL,
  `costo_contado` float NOT NULL,
  `costo_credito` float NOT NULL,
  `costo_debito` float NOT NULL,
  PRIMARY KEY (`id_cierreCaja`)
) ENGINE=MyISAM AUTO_INCREMENT=418 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

DROP TABLE IF EXISTS `cliente`;
CREATE TABLE IF NOT EXISTS `cliente` (
  `id_cliente` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_cliente` varchar(30) NOT NULL,
  `apellido_cliente` varchar(30) NOT NULL,
  `direccion_cliente` varchar(30) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `mail` varchar(30) DEFAULT NULL,
  `id_cta_cte` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_cliente`)
) ENGINE=InnoDB AUTO_INCREMENT=37305628 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cond_vta`
--

DROP TABLE IF EXISTS `cond_vta`;
CREATE TABLE IF NOT EXISTS `cond_vta` (
  `id_condVta` int(11) NOT NULL AUTO_INCREMENT,
  `desc_condVta` varchar(30) NOT NULL,
  PRIMARY KEY (`id_condVta`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracion`
--

DROP TABLE IF EXISTS `configuracion`;
CREATE TABLE IF NOT EXISTS `configuracion` (
  `id_configuracion` int(11) NOT NULL AUTO_INCREMENT,
  `desc_configuracion` varchar(25) NOT NULL,
  `valor_configuracion` float NOT NULL,
  PRIMARY KEY (`id_configuracion`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `costo`
--

DROP TABLE IF EXISTS `costo`;
CREATE TABLE IF NOT EXISTS `costo` (
  `id_costo` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_costo` date NOT NULL,
  `hora_costo` time NOT NULL,
  `id_tipoCosto` int(11) NOT NULL,
  `desc_costo` varchar(20) NOT NULL,
  `valor` float NOT NULL,
  `cerrado` int(11) NOT NULL,
  PRIMARY KEY (`id_costo`)
) ENGINE=InnoDB AUTO_INCREMENT=1228 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cta_cte`
--

DROP TABLE IF EXISTS `cta_cte`;
CREATE TABLE IF NOT EXISTS `cta_cte` (
  `id_cta_cte` int(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` int(11) NOT NULL,
  `habilitada` int(11) NOT NULL,
  PRIMARY KEY (`id_cta_cte`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_cta_cte`
--

DROP TABLE IF EXISTS `detalle_cta_cte`;
CREATE TABLE IF NOT EXISTS `detalle_cta_cte` (
  `id_detalle` int(11) NOT NULL AUTO_INCREMENT,
  `id_cta_cte` int(11) NOT NULL,
  `id_boleta` int(11) NOT NULL,
  `saldo` float NOT NULL,
  PRIMARY KEY (`id_detalle`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_mesa`
--

DROP TABLE IF EXISTS `detalle_mesa`;
CREATE TABLE IF NOT EXISTS `detalle_mesa` (
  `id_detalleMesa` int(11) NOT NULL AUTO_INCREMENT,
  `id_boleta` int(11) NOT NULL,
  `codigo` varchar(11) NOT NULL,
  `cantidad` float NOT NULL,
  `precio` float NOT NULL,
  PRIMARY KEY (`id_detalleMesa`)
) ENGINE=InnoDB AUTO_INCREMENT=42436 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equiv_prom`
--

DROP TABLE IF EXISTS `equiv_prom`;
CREATE TABLE IF NOT EXISTS `equiv_prom` (
  `id_equiv` int(11) NOT NULL AUTO_INCREMENT,
  `codigo_promocion` varchar(10) NOT NULL,
  `codigo_articulo` varchar(10) NOT NULL,
  `cantidad_articulo` int(11) NOT NULL,
  PRIMARY KEY (`id_equiv`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturacion_boleta`
--

DROP TABLE IF EXISTS `facturacion_boleta`;
CREATE TABLE IF NOT EXISTS `facturacion_boleta` (
  `id_factBoleta` int(11) NOT NULL AUTO_INCREMENT,
  `id_boleta` int(11) NOT NULL,
  `id_condVta` int(11) NOT NULL,
  `monto` float NOT NULL,
  PRIMARY KEY (`id_factBoleta`)
) ENGINE=InnoDB AUTO_INCREMENT=14929 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturacion_costo`
--

DROP TABLE IF EXISTS `facturacion_costo`;
CREATE TABLE IF NOT EXISTS `facturacion_costo` (
  `id_factCosto` int(11) NOT NULL AUTO_INCREMENT,
  `id_costo` int(11) NOT NULL,
  `id_condVta` int(11) NOT NULL,
  `monto` float NOT NULL,
  PRIMARY KEY (`id_factCosto`)
) ENGINE=InnoDB AUTO_INCREMENT=1235 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturacion_cta_cte`
--

DROP TABLE IF EXISTS `facturacion_cta_cte`;
CREATE TABLE IF NOT EXISTS `facturacion_cta_cte` (
  `id_factCtaCte` int(11) NOT NULL AUTO_INCREMENT,
  `id_cta_cte` int(11) NOT NULL,
  `id_condVta` int(11) NOT NULL,
  `monto` float NOT NULL,
  `fecha` datetime NOT NULL,
  PRIMARY KEY (`id_factCtaCte`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mesa`
--

DROP TABLE IF EXISTS `mesa`;
CREATE TABLE IF NOT EXISTS `mesa` (
  `numero_mesa` int(11) NOT NULL,
  `id_mozo` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  PRIMARY KEY (`numero_mesa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `privilegio`
--

DROP TABLE IF EXISTS `privilegio`;
CREATE TABLE IF NOT EXISTS `privilegio` (
  `id_privilegio` int(11) NOT NULL AUTO_INCREMENT,
  `desc_privilegio` varchar(20) NOT NULL,
  PRIMARY KEY (`id_privilegio`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_costo`
--

DROP TABLE IF EXISTS `tipo_costo`;
CREATE TABLE IF NOT EXISTS `tipo_costo` (
  `id_tipoCosto` int(11) NOT NULL AUTO_INCREMENT,
  `desc_tipoCosto` varchar(20) NOT NULL,
  PRIMARY KEY (`id_tipoCosto`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_prod`
--

DROP TABLE IF EXISTS `tipo_prod`;
CREATE TABLE IF NOT EXISTS `tipo_prod` (
  `id_tipoProd1` int(11) NOT NULL AUTO_INCREMENT,
  `desc_tipoProd1` varchar(30) NOT NULL,
  `imagen_tipoProd1` longtext NOT NULL,
  PRIMARY KEY (`id_tipoProd1`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_prod2`
--

DROP TABLE IF EXISTS `tipo_prod2`;
CREATE TABLE IF NOT EXISTS `tipo_prod2` (
  `id_tipoProd2` int(11) NOT NULL AUTO_INCREMENT,
  `desc_tipoProd2` varchar(30) NOT NULL,
  `id_tipoProd1` int(11) NOT NULL,
  PRIMARY KEY (`id_tipoProd2`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_vta`
--

DROP TABLE IF EXISTS `tipo_vta`;
CREATE TABLE IF NOT EXISTS `tipo_vta` (
  `id_tipoVta` int(11) NOT NULL AUTO_INCREMENT,
  `desc_tipoVta` varchar(11) NOT NULL,
  PRIMARY KEY (`id_tipoVta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `dni` int(11) NOT NULL,
  `cuenta_user` varchar(20) DEFAULT NULL,
  `pass_user` varchar(20) DEFAULT NULL,
  `nombre_user` varchar(20) NOT NULL,
  `apellido_user` varchar(20) DEFAULT NULL,
  `direccion_user` varchar(30) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  `id_privilegio` int(11) NOT NULL,
  `habilitado` int(11) NOT NULL,
  `root` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`dni`),
  KEY `direccion_user` (`direccion_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
