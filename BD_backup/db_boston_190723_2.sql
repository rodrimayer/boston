-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 23-07-2019 a las 21:42:25
-- Versión del servidor: 5.5.24-log
-- Versión de PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `db_boston`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulo`
--

CREATE TABLE IF NOT EXISTS `articulo` (
  `codigo` varchar(10) NOT NULL,
  `id_tipoProd` int(11) NOT NULL,
  `id_tipoProd2` int(11) NOT NULL,
  `precio_costo` float NOT NULL,
  `precio_vta` float NOT NULL,
  `desc_articulo` varchar(30) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `barril`
--

CREATE TABLE IF NOT EXISTS `barril` (
  `id_barril` int(11) NOT NULL AUTO_INCREMENT,
  `desc_barril` varchar(20) NOT NULL,
  `cantidad_total` float NOT NULL,
  PRIMARY KEY (`id_barril`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `boleta`
--

CREATE TABLE IF NOT EXISTS `boleta` (
  `id_boleta` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_boleta` date NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_mozo` int(11) NOT NULL,
  `total` float NOT NULL,
  `cerrada` int(11) NOT NULL,
  PRIMARY KEY (`id_boleta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
  `id_cliente` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_cliente` varchar(30) NOT NULL,
  `apellido_cliente` varchar(30) NOT NULL,
  `direccion_cliente` varchar(30) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `mail` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_cliente`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37305673 ;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id_cliente`, `nombre_cliente`, `apellido_cliente`, `direccion_cliente`, `telefono`, `mail`) VALUES
(1111111, 'Consumidor', 'Final', '-', '-', '-'),
(37305672, 'Rodrigo', 'Mayer', 'Maipu 76', '3886525523', 'rodrimmayer@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cond_vta`
--

CREATE TABLE IF NOT EXISTS `cond_vta` (
  `id_condVta` int(11) NOT NULL AUTO_INCREMENT,
  `desc_condVta` varchar(30) NOT NULL,
  `permite_cuotas` int(11) NOT NULL,
  PRIMARY KEY (`id_condVta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_mesa`
--

CREATE TABLE IF NOT EXISTS `detalle_mesa` (
  `id_detalleMesa` int(11) NOT NULL AUTO_INCREMENT,
  `id_mesa` int(11) NOT NULL,
  `id_boleta` int(11) NOT NULL,
  `codigo` int(11) NOT NULL,
  `cantidad` float NOT NULL,
  `precio` float NOT NULL,
  PRIMARY KEY (`id_detalleMesa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturacion_boleta`
--

CREATE TABLE IF NOT EXISTS `facturacion_boleta` (
  `id_factBoleta` int(11) NOT NULL AUTO_INCREMENT,
  `id_boleta` int(11) NOT NULL,
  `id_condVta` int(11) NOT NULL,
  `monto` float NOT NULL,
  `cuota` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_factBoleta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mesa`
--

CREATE TABLE IF NOT EXISTS `mesa` (
  `numero_mesa` int(11) NOT NULL,
  `id_mozo` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  PRIMARY KEY (`numero_mesa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `privilegio`
--

CREATE TABLE IF NOT EXISTS `privilegio` (
  `id_privilegio` int(11) NOT NULL AUTO_INCREMENT,
  `desc_privilegio` varchar(20) NOT NULL,
  PRIMARY KEY (`id_privilegio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_prod`
--

CREATE TABLE IF NOT EXISTS `tipo_prod` (
  `id_tipoProd1` int(11) NOT NULL AUTO_INCREMENT,
  `desc_tipoProd1` varchar(30) NOT NULL,
  PRIMARY KEY (`id_tipoProd1`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `tipo_prod`
--

INSERT INTO `tipo_prod` (`id_tipoProd1`, `desc_tipoProd1`) VALUES
(1, 'Comida'),
(2, 'Bebida');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_prod2`
--

CREATE TABLE IF NOT EXISTS `tipo_prod2` (
  `id_tipoProd2` int(11) NOT NULL AUTO_INCREMENT,
  `desc_tipoProd2` varchar(30) NOT NULL,
  `id_tipoProd1` int(11) NOT NULL,
  PRIMARY KEY (`id_tipoProd2`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `tipo_prod2`
--

INSERT INTO `tipo_prod2` (`id_tipoProd2`, `desc_tipoProd2`, `id_tipoProd1`) VALUES
(1, 'Vodka', 2),
(2, 'Fernet', 2),
(3, 'Gancia', 2),
(4, 'Cerveza Industrial', 2),
(5, 'Cerveza Artesanal', 2),
(6, 'Hamburguesa', 1),
(7, 'Pizza', 1),
(8, 'Papas Fritas', 1),
(9, 'Hot Dogs', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `dni` int(11) NOT NULL,
  `contraseña` varchar(20) NOT NULL,
  `nombre_user` varchar(20) NOT NULL,
  `apellido_user` varchar(20) NOT NULL,
  `direccion_user` varchar(30) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `id_privilegio` int(11) NOT NULL,
  PRIMARY KEY (`dni`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
