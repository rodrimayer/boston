-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 15-08-2019 a las 15:49:32
-- Versión del servidor: 5.5.24-log
-- Versión de PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `db_boston`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulo`
--

CREATE TABLE IF NOT EXISTS `articulo` (
  `codigo` varchar(10) NOT NULL,
  `id_tipoProd` int(11) NOT NULL,
  `id_tipoProd2` int(11) NOT NULL,
  `precio_costo` float NOT NULL,
  `precio_vta` float NOT NULL,
  `desc_articulo` varchar(30) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `articulo`
--

INSERT INTO `articulo` (`codigo`, `id_tipoProd`, `id_tipoProd2`, `precio_costo`, `precio_vta`, `desc_articulo`) VALUES
('A', 2, 5, 50, 100, 'Amber'),
('G', 2, 5, 50, 100, 'Golden'),
('H', 2, 5, 50, 100, 'Honey'),
('HA', 2, 5, 100, 150, 'Happy Amber'),
('HAH', 2, 5, 100, 150, 'Happy Amber Honey'),
('HG', 2, 5, 100, 150, 'Happy Golden'),
('HGA', 2, 5, 100, 150, 'Happy Golden Amber'),
('HGH', 2, 5, 100, 150, 'Happy Golden Honey'),
('HH', 2, 5, 100, 150, 'Happy Honey'),
('PF', 1, 7, 100, 230, 'Pizza Fugazzeta');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `barril`
--

CREATE TABLE IF NOT EXISTS `barril` (
  `id_barril` int(11) NOT NULL AUTO_INCREMENT,
  `desc_barril` varchar(20) NOT NULL,
  `cantidad_total` float NOT NULL,
  `cantidad_consumida` float NOT NULL,
  `posicion` int(11) NOT NULL,
  PRIMARY KEY (`id_barril`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Volcado de datos para la tabla `barril`
--

INSERT INTO `barril` (`id_barril`, `desc_barril`, `cantidad_total`, `cantidad_consumida`, `posicion`) VALUES
(2, 'Amber', 30, 30, 1),
(3, 'Golden', 30, 30, 2),
(4, 'Honey', 30, 30, 3),
(5, 'Golden', 50, 50, 2),
(6, 'Honey', 100, 100, 3),
(7, 'Honey', 30, 22, 3),
(8, 'Golden', 30, 30, 2),
(9, 'Amber', 30, 30, 1),
(10, 'Golden', 30, 21, 2),
(11, 'Golden', 30, 30, 1),
(12, 'Amber', 30, 23.5, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `boleta`
--

CREATE TABLE IF NOT EXISTS `boleta` (
  `id_boleta` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_boleta` date NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `mesa` int(11) NOT NULL,
  `id_mozo` int(11) NOT NULL,
  `total` float NOT NULL,
  `cerrada` int(11) NOT NULL,
  `pagada` int(11) NOT NULL,
  PRIMARY KEY (`id_boleta`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `boleta`
--

INSERT INTO `boleta` (`id_boleta`, `fecha_boleta`, `id_cliente`, `mesa`, `id_mozo`, `total`, `cerrada`, `pagada`) VALUES
(1, '2019-08-15', 1, 1, 37305672, 2130, 0, 1),
(2, '2019-08-15', 1, 0, 11111111, 300, 0, 1),
(3, '2019-08-15', 1, 0, 11111111, 300, 0, 1),
(4, '2019-08-15', 1, 1, 37305672, 200, 0, 1),
(5, '2019-08-15', 1, 0, 11111111, 600, 0, 1),
(6, '2019-08-15', 1, 1, 37305672, 600, 0, 1),
(7, '2019-08-15', 1, 0, 11111111, 600, 0, 1),
(8, '2019-08-15', 1, 0, 11111111, 300, 0, 1),
(9, '2019-08-15', 1, 1, 37305672, 660, 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
  `id_cliente` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_cliente` varchar(30) NOT NULL,
  `apellido_cliente` varchar(30) NOT NULL,
  `direccion_cliente` varchar(30) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `mail` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_cliente`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37305673 ;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id_cliente`, `nombre_cliente`, `apellido_cliente`, `direccion_cliente`, `telefono`, `mail`) VALUES
(1, 'Consumidor', 'Final', '-', '-', '-'),
(37305672, 'Rodrigo', 'Mayer', 'Maipu 76', '3886525523', 'rodrimmayer@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cond_vta`
--

CREATE TABLE IF NOT EXISTS `cond_vta` (
  `id_condVta` int(11) NOT NULL AUTO_INCREMENT,
  `desc_condVta` varchar(30) NOT NULL,
  PRIMARY KEY (`id_condVta`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `cond_vta`
--

INSERT INTO `cond_vta` (`id_condVta`, `desc_condVta`) VALUES
(1, 'CONTADO'),
(2, 'CREDITO'),
(3, 'DEBITO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracion`
--

CREATE TABLE IF NOT EXISTS `configuracion` (
  `id_configuracion` int(11) NOT NULL AUTO_INCREMENT,
  `desc_configuracion` varchar(25) NOT NULL,
  `valor_configuracion` float NOT NULL,
  PRIMARY KEY (`id_configuracion`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `configuracion`
--

INSERT INTO `configuracion` (`id_configuracion`, `desc_configuracion`, `valor_configuracion`) VALUES
(1, 'Happy Hour', 80);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `costo`
--

CREATE TABLE IF NOT EXISTS `costo` (
  `id_costo` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_costo` date NOT NULL,
  `id_tipoCosto` int(11) NOT NULL,
  `desc_costo` varchar(20) NOT NULL,
  `valor` float NOT NULL,
  `cerrado` int(11) NOT NULL,
  PRIMARY KEY (`id_costo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `costo`
--

INSERT INTO `costo` (`id_costo`, `fecha_costo`, `id_tipoCosto`, `desc_costo`, `valor`, `cerrado`) VALUES
(1, '2019-08-15', 1, 'Local', 20000, 0),
(2, '2019-08-15', 10, 'Cani', 400, 0),
(3, '2019-08-15', 10, 'Belen', 400, 0),
(4, '2019-08-15', 10, 'Camila', 400, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_mesa`
--

CREATE TABLE IF NOT EXISTS `detalle_mesa` (
  `id_detalleMesa` int(11) NOT NULL AUTO_INCREMENT,
  `id_boleta` int(11) NOT NULL,
  `codigo` varchar(11) NOT NULL,
  `cantidad` float NOT NULL,
  `precio` float NOT NULL,
  PRIMARY KEY (`id_detalleMesa`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Volcado de datos para la tabla `detalle_mesa`
--

INSERT INTO `detalle_mesa` (`id_detalleMesa`, `id_boleta`, `codigo`, `cantidad`, `precio`) VALUES
(1, 1, 'a', 2, 100),
(2, 1, 'h', 8, 100),
(3, 1, 'g', 9, 100),
(4, 1, 'pf', 1, 230),
(5, 2, 'g', 1, 100),
(6, 2, 'h', 1, 100),
(7, 2, 'a', 1, 100),
(8, 3, 'g', 1, 100),
(9, 3, 'h', 1, 100),
(10, 3, 'a', 1, 100),
(11, 3, 'g', 1, 100),
(12, 3, 'h', 1, 100),
(13, 3, 'a', 1, 100),
(14, 4, 'g', 2, 100),
(15, 4, 'h', 2, 100),
(16, 5, 'h', 1, 100),
(17, 5, 'a', 5, 100),
(18, 4, 'g', -2, 100),
(19, 6, 'a', 2, 100),
(20, 6, 'g', 2, 100),
(21, 6, 'h', 2, 100),
(22, 7, 'a', 2, 100),
(23, 7, 'g', 2, 100),
(24, 7, 'h', 2, 100),
(25, 8, 'g', 1, 100),
(26, 8, 'a', 1, 100),
(27, 8, 'h', 1, 100),
(28, 9, 'g', 1, 100),
(29, 9, 'a', 1, 100),
(30, 9, 'pf', 2, 230);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturacion_boleta`
--

CREATE TABLE IF NOT EXISTS `facturacion_boleta` (
  `id_factBoleta` int(11) NOT NULL AUTO_INCREMENT,
  `id_boleta` int(11) NOT NULL,
  `id_condVta` int(11) NOT NULL,
  `monto` float NOT NULL,
  PRIMARY KEY (`id_factBoleta`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `facturacion_boleta`
--

INSERT INTO `facturacion_boleta` (`id_factBoleta`, `id_boleta`, `id_condVta`, `monto`) VALUES
(1, 2, 1, 300),
(2, 3, 1, 300),
(3, 1, 1, 2130),
(4, 5, 1, 600),
(5, 4, 1, 200),
(6, 6, 1, 600),
(7, 7, 1, 600),
(8, 8, 1, 300),
(9, 9, 1, 300),
(10, 9, 3, 360);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturacion_costo`
--

CREATE TABLE IF NOT EXISTS `facturacion_costo` (
  `id_factCosto` int(11) NOT NULL AUTO_INCREMENT,
  `id_costo` int(11) NOT NULL,
  `id_condVta` int(11) NOT NULL,
  `monto` float NOT NULL,
  PRIMARY KEY (`id_factCosto`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `facturacion_costo`
--

INSERT INTO `facturacion_costo` (`id_factCosto`, `id_costo`, `id_condVta`, `monto`) VALUES
(1, 1, 1, 20000),
(2, 2, 1, 200),
(3, 2, 3, 200),
(4, 3, 1, 400),
(5, 4, 1, 400);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mesa`
--

CREATE TABLE IF NOT EXISTS `mesa` (
  `numero_mesa` int(11) NOT NULL,
  `id_mozo` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  PRIMARY KEY (`numero_mesa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `privilegio`
--

CREATE TABLE IF NOT EXISTS `privilegio` (
  `id_privilegio` int(11) NOT NULL AUTO_INCREMENT,
  `desc_privilegio` varchar(20) NOT NULL,
  PRIMARY KEY (`id_privilegio`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `privilegio`
--

INSERT INTO `privilegio` (`id_privilegio`, `desc_privilegio`) VALUES
(1, 'administrador'),
(2, 'mozo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_costo`
--

CREATE TABLE IF NOT EXISTS `tipo_costo` (
  `id_tipoCosto` int(11) NOT NULL AUTO_INCREMENT,
  `desc_tipoCosto` varchar(20) NOT NULL,
  PRIMARY KEY (`id_tipoCosto`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Volcado de datos para la tabla `tipo_costo`
--

INSERT INTO `tipo_costo` (`id_tipoCosto`, `desc_tipoCosto`) VALUES
(1, 'Alquiler'),
(2, 'Mantenimiento'),
(3, 'Servicios'),
(4, 'Comida'),
(5, 'Bebida'),
(6, 'Super'),
(7, 'Barriles'),
(8, 'Nafta'),
(9, 'Otros'),
(10, 'Sueldos'),
(11, 'Publicidad'),
(15, 'Prueba');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_prod`
--

CREATE TABLE IF NOT EXISTS `tipo_prod` (
  `id_tipoProd1` int(11) NOT NULL AUTO_INCREMENT,
  `desc_tipoProd1` varchar(30) NOT NULL,
  PRIMARY KEY (`id_tipoProd1`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `tipo_prod`
--

INSERT INTO `tipo_prod` (`id_tipoProd1`, `desc_tipoProd1`) VALUES
(1, 'Comida'),
(2, 'Bebida');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_prod2`
--

CREATE TABLE IF NOT EXISTS `tipo_prod2` (
  `id_tipoProd2` int(11) NOT NULL AUTO_INCREMENT,
  `desc_tipoProd2` varchar(30) NOT NULL,
  `id_tipoProd1` int(11) NOT NULL,
  PRIMARY KEY (`id_tipoProd2`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `tipo_prod2`
--

INSERT INTO `tipo_prod2` (`id_tipoProd2`, `desc_tipoProd2`, `id_tipoProd1`) VALUES
(1, 'Vodka', 2),
(2, 'Fernet', 2),
(3, 'Gancia', 2),
(4, 'Cerveza Industrial', 2),
(5, 'Cerveza Artesanal', 2),
(6, 'Hamburguesa', 1),
(7, 'Pizza', 1),
(8, 'Papas Fritas', 1),
(9, 'Hot Dogs', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `dni` int(11) NOT NULL,
  `cuenta_user` varchar(20) NOT NULL,
  `contraseña_user` varchar(20) NOT NULL,
  `nombre_user` varchar(20) NOT NULL,
  `apellido_user` varchar(20) NOT NULL,
  `direccion_user` varchar(30) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `id_privilegio` int(11) NOT NULL,
  PRIMARY KEY (`dni`),
  KEY `direccion_user` (`direccion_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`dni`, `cuenta_user`, `contraseña_user`, `nombre_user`, `apellido_user`, `direccion_user`, `telefono`, `id_privilegio`) VALUES
(11111111, 'admin', 'admin', 'Boston', 'Beer & Co', 'Sixto Ovejero', '1111111', 1),
(37305672, 'rodrimayer', 'rodri0104', 'Rodrigo', 'Mayer', 'Maipu 76', '3886525523', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
