-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 06-09-2019 a las 21:36:30
-- Versión del servidor: 5.5.24-log
-- Versión de PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `db_boston`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulo`
--

CREATE TABLE IF NOT EXISTS `articulo` (
  `codigo` varchar(10) NOT NULL,
  `id_tipoProd` int(11) NOT NULL,
  `id_tipoProd2` int(11) NOT NULL,
  `precio_costo` float NOT NULL,
  `precio_vta` float NOT NULL,
  `desc_articulo` varchar(30) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `articulo`
--

INSERT INTO `articulo` (`codigo`, `id_tipoProd`, `id_tipoProd2`, `precio_costo`, `precio_vta`, `desc_articulo`) VALUES
('A', 2, 5, 50, 100, 'Amber'),
('DP', 4, 11, 15, 15, 'Descuento Mujer'),
('G', 2, 5, 50, 100, 'Golden'),
('H', 2, 5, 50, 100, 'Honey'),
('HA', 3, 10, 100, 150, 'Happy Amber'),
('HAB', 1, 6, 100, 220, 'Hamburguesa Abusa'),
('HAH', 2, 5, 100, 150, 'Happy Amber Honey'),
('HC', 1, 6, 80, 200, 'Hamburguesa Criminal'),
('HCL', 1, 6, 70, 180, 'Hamburguesa Clasica'),
('HG', 3, 10, 100, 150, 'Happy Golden'),
('HGA', 2, 5, 100, 150, 'Happy Golden Amber'),
('HGH', 2, 5, 100, 150, 'Happy Golden Honey'),
('HH', 2, 5, 100, 150, 'Happy Honey'),
('P24', 2, 4, 80, 160, 'Patagonia 24/7'),
('PCA', 3, 10, 130, 250, 'Criminal + Amber'),
('PCR', 3, 10, 100, 180, 'Promo Criminal'),
('PF', 1, 7, 100, 230, 'Pizza Fugazzeta'),
('PK', 2, 4, 80, 180, 'Patagonia Kune');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `barril`
--

CREATE TABLE IF NOT EXISTS `barril` (
  `id_barril` int(11) NOT NULL AUTO_INCREMENT,
  `desc_barril` varchar(20) NOT NULL,
  `cantidad_total` float NOT NULL,
  `cantidad_consumida` float NOT NULL,
  `posicion` int(11) NOT NULL,
  PRIMARY KEY (`id_barril`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `barril`
--

INSERT INTO `barril` (`id_barril`, `desc_barril`, `cantidad_total`, `cantidad_consumida`, `posicion`) VALUES
(1, 'Amber', 30, 7, 1),
(2, 'Golden', 30, 3, 2),
(3, 'Honey', 30, 2, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `boleta`
--

CREATE TABLE IF NOT EXISTS `boleta` (
  `id_boleta` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_boleta` date NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `mesa` int(11) NOT NULL,
  `id_mozo` int(11) NOT NULL,
  `total` float NOT NULL,
  `cerrada` int(11) NOT NULL,
  `pagada` int(11) NOT NULL,
  PRIMARY KEY (`id_boleta`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Volcado de datos para la tabla `boleta`
--

INSERT INTO `boleta` (`id_boleta`, `fecha_boleta`, `id_cliente`, `mesa`, `id_mozo`, `total`, `cerrada`, `pagada`) VALUES
(1, '2019-08-15', 1, 0, 11111111, 400, 0, 1),
(2, '2019-08-15', 1, 1, 11111111, 660, 0, 1),
(3, '2019-08-15', 1, 2, 37305672, 900, 0, 1),
(4, '2019-08-22', 1, 0, 11111111, 100, 0, 1),
(5, '2019-08-26', 1, 0, 11111111, 360, 0, 1),
(6, '2019-08-26', 37305672, 10, 11111111, 880, 0, 1),
(7, '2019-08-27', 1, 0, 11111111, 250, 0, 1),
(8, '2019-08-28', 1, 1, 37305672, 1450, 0, 1),
(9, '2019-08-28', 1, 0, 11111111, 357, 0, 1),
(10, '2019-08-28', 1, 0, 11111111, 150, 0, 1),
(11, '2019-08-29', 1, 0, 11111111, 180, 0, 1),
(12, '2019-08-30', 1, 10, 37305672, 580, 0, 1),
(13, '2019-09-03', 1, 0, 11111111, 200, 0, 1),
(14, '2019-09-03', 1, 0, 11111111, 200, 0, 1),
(15, '2019-09-03', 1, 0, 11111111, 200, 0, 1),
(16, '2019-09-03', 1, 5, 37305672, 200, 0, 1),
(17, '2019-09-03', 1, 0, 37305672, 380, 0, 0),
(18, '2019-09-03', 1, 0, 37305672, 200, 0, 1),
(19, '2019-09-03', 1, 0, 11111111, 300, 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
  `id_cliente` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_cliente` varchar(30) NOT NULL,
  `apellido_cliente` varchar(30) NOT NULL,
  `direccion_cliente` varchar(30) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `mail` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_cliente`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37305673 ;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id_cliente`, `nombre_cliente`, `apellido_cliente`, `direccion_cliente`, `telefono`, `mail`) VALUES
(1, 'Consumidor', 'Final', '-', '-', '-'),
(123213, 'sadsadsad', 'asdsad', 'asdsad', 'asdsad', 'asdsad'),
(37305672, 'Rodrigo', 'Mayer', 'Maipu 76', '3886525523', 'rodrimmayer@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cond_vta`
--

CREATE TABLE IF NOT EXISTS `cond_vta` (
  `id_condVta` int(11) NOT NULL AUTO_INCREMENT,
  `desc_condVta` varchar(30) NOT NULL,
  PRIMARY KEY (`id_condVta`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `cond_vta`
--

INSERT INTO `cond_vta` (`id_condVta`, `desc_condVta`) VALUES
(1, 'CONTADO'),
(2, 'CREDITO'),
(3, 'DEBITO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracion`
--

CREATE TABLE IF NOT EXISTS `configuracion` (
  `id_configuracion` int(11) NOT NULL AUTO_INCREMENT,
  `desc_configuracion` varchar(25) NOT NULL,
  `valor_configuracion` float NOT NULL,
  PRIMARY KEY (`id_configuracion`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `configuracion`
--

INSERT INTO `configuracion` (`id_configuracion`, `desc_configuracion`, `valor_configuracion`) VALUES
(1, 'Happy Hour', 80),
(2, 'Happy Hour x2', 0),
(3, 'Cuenta Corriente', 0),
(4, 'Impresion', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `costo`
--

CREATE TABLE IF NOT EXISTS `costo` (
  `id_costo` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_costo` date NOT NULL,
  `id_tipoCosto` int(11) NOT NULL,
  `desc_costo` varchar(20) NOT NULL,
  `valor` float NOT NULL,
  `cerrado` int(11) NOT NULL,
  PRIMARY KEY (`id_costo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Volcado de datos para la tabla `costo`
--

INSERT INTO `costo` (`id_costo`, `fecha_costo`, `id_tipoCosto`, `desc_costo`, `valor`, `cerrado`) VALUES
(1, '2019-08-15', 3, 'Internet', 1100, 0),
(2, '2019-08-15', 3, 'Luz', 4000, 0),
(3, '2019-08-15', 3, 'Gas', 800, 0),
(4, '2019-08-15', 10, 'Cani', 400, 0),
(5, '2019-08-15', 10, 'Cami', 500, 0),
(6, '2019-08-15', 1, 'Local', 20000, 0),
(7, '2019-08-15', 4, 'Comodin', 900, 0),
(8, '2019-08-15', 5, 'Purma', 4500, 0),
(9, '2019-08-15', 8, 'Viaje Jujuy', 2300, 0),
(10, '2019-08-22', 3, 'prueba', 800, 0),
(11, '2019-08-22', 6, 'Vea', 3500, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_mesa`
--

CREATE TABLE IF NOT EXISTS `detalle_mesa` (
  `id_detalleMesa` int(11) NOT NULL AUTO_INCREMENT,
  `id_boleta` int(11) NOT NULL,
  `codigo` varchar(11) NOT NULL,
  `cantidad` float NOT NULL,
  `precio` float NOT NULL,
  PRIMARY KEY (`id_detalleMesa`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=39 ;

--
-- Volcado de datos para la tabla `detalle_mesa`
--

INSERT INTO `detalle_mesa` (`id_detalleMesa`, `id_boleta`, `codigo`, `cantidad`, `precio`) VALUES
(1, 1, 'H', 1, 100),
(2, 1, 'G', 1, 100),
(3, 1, 'A', 2, 100),
(4, 2, 'G', 1, 100),
(5, 2, 'PF', 2, 230),
(6, 2, 'H', 1, 100),
(7, 3, 'hc', 1, 200),
(8, 3, 'hcl', 1, 180),
(9, 3, 'hab', 1, 220),
(10, 3, 'a', 1, 100),
(11, 3, 'g', 1, 100),
(12, 3, 'h', 1, 100),
(13, 4, 'a', 1, 100),
(14, 5, 'pk', 2, 180),
(15, 6, 'hc', 1, 200),
(16, 6, 'hab', 1, 220),
(17, 6, 'g', 1, 100),
(18, 6, 'pk', 2, 180),
(19, 8, 'HC', 1, 200),
(20, 8, 'PCA', 5, 250),
(21, 9, 'hc', 1, 200),
(22, 9, 'hab', 1, 220),
(23, 9, 'DP', 1, -63),
(24, 10, 'HA', 1, 150),
(25, 11, 'PCR', 1, 180),
(26, 12, 'PCR', 1, 180),
(27, 12, 'A', 1, 100),
(28, 12, 'g', 2, 100),
(29, 12, 'h', 1, 100),
(30, 13, 'hc', 1, 200),
(31, 14, 'hc', 1, 200),
(32, 15, 'hc', 1, 200),
(33, 16, 'hc', 1, 200),
(34, 17, 'hc', 1, 200),
(35, 17, 'PCR', 1, 180),
(36, 18, 'hc', 1, 200),
(37, 19, 'a', 1, 100),
(38, 19, 'hc', 1, 200);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equiv_prom`
--

CREATE TABLE IF NOT EXISTS `equiv_prom` (
  `id_equiv` int(11) NOT NULL AUTO_INCREMENT,
  `codigo_promocion` varchar(10) NOT NULL,
  `codigo_articulo` varchar(10) NOT NULL,
  `cantidad_articulo` int(11) NOT NULL,
  PRIMARY KEY (`id_equiv`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `equiv_prom`
--

INSERT INTO `equiv_prom` (`id_equiv`, `codigo_promocion`, `codigo_articulo`, `cantidad_articulo`) VALUES
(1, 'PCA', 'hc', 1),
(2, 'PCA', 'a', 1),
(3, 'HG', 'g', 2),
(4, 'HA', 'a', 2),
(5, 'PCR', 'hc', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturacion_boleta`
--

CREATE TABLE IF NOT EXISTS `facturacion_boleta` (
  `id_factBoleta` int(11) NOT NULL AUTO_INCREMENT,
  `id_boleta` int(11) NOT NULL,
  `id_condVta` int(11) NOT NULL,
  `monto` float NOT NULL,
  PRIMARY KEY (`id_factBoleta`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Volcado de datos para la tabla `facturacion_boleta`
--

INSERT INTO `facturacion_boleta` (`id_factBoleta`, `id_boleta`, `id_condVta`, `monto`) VALUES
(1, 1, 1, 400),
(2, 2, 1, 660),
(3, 3, 1, 450),
(4, 3, 2, 450),
(6, 5, 1, 360),
(7, 6, 1, 880),
(8, 7, 1, 250),
(9, 8, 1, 1450),
(10, 9, 1, 357),
(11, 10, 1, 150),
(12, 11, 1, 180),
(13, 13, 1, 200),
(14, 14, 1, 200),
(15, 15, 1, 200),
(16, 12, 1, 580),
(17, 16, 1, 200),
(18, 18, 1, 200),
(19, 19, 1, 300);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturacion_costo`
--

CREATE TABLE IF NOT EXISTS `facturacion_costo` (
  `id_factCosto` int(11) NOT NULL AUTO_INCREMENT,
  `id_costo` int(11) NOT NULL,
  `id_condVta` int(11) NOT NULL,
  `monto` float NOT NULL,
  PRIMARY KEY (`id_factCosto`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Volcado de datos para la tabla `facturacion_costo`
--

INSERT INTO `facturacion_costo` (`id_factCosto`, `id_costo`, `id_condVta`, `monto`) VALUES
(1, 1, 1, 1100),
(2, 2, 1, 4000),
(3, 3, 1, 800),
(4, 4, 1, 400),
(5, 5, 1, 500),
(6, 6, 1, 20000),
(7, 7, 1, 900),
(8, 8, 1, 4500),
(9, 9, 1, 2300),
(10, 10, 2, 400),
(11, 10, 1, 400),
(12, 11, 3, 3500);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mesa`
--

CREATE TABLE IF NOT EXISTS `mesa` (
  `numero_mesa` int(11) NOT NULL,
  `id_mozo` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  PRIMARY KEY (`numero_mesa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `privilegio`
--

CREATE TABLE IF NOT EXISTS `privilegio` (
  `id_privilegio` int(11) NOT NULL AUTO_INCREMENT,
  `desc_privilegio` varchar(20) NOT NULL,
  PRIMARY KEY (`id_privilegio`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `privilegio`
--

INSERT INTO `privilegio` (`id_privilegio`, `desc_privilegio`) VALUES
(1, 'Administrador'),
(2, 'Mozo'),
(3, 'Comprador');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `promociones_vendidas`
--

CREATE TABLE IF NOT EXISTS `promociones_vendidas` (
  `id_promVendidas` int(11) NOT NULL AUTO_INCREMENT,
  `codigo_promocion` varchar(10) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `cerrada` int(11) NOT NULL,
  PRIMARY KEY (`id_promVendidas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_costo`
--

CREATE TABLE IF NOT EXISTS `tipo_costo` (
  `id_tipoCosto` int(11) NOT NULL AUTO_INCREMENT,
  `desc_tipoCosto` varchar(20) NOT NULL,
  PRIMARY KEY (`id_tipoCosto`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Volcado de datos para la tabla `tipo_costo`
--

INSERT INTO `tipo_costo` (`id_tipoCosto`, `desc_tipoCosto`) VALUES
(1, 'Alquiler'),
(2, 'Mantenimiento'),
(3, 'Servicios'),
(4, 'Comida'),
(5, 'Bebida'),
(6, 'Super'),
(7, 'Barriles'),
(8, 'Nafta'),
(9, 'Otros'),
(10, 'Sueldos'),
(11, 'Publicidad'),
(15, 'Prueba');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_prod`
--

CREATE TABLE IF NOT EXISTS `tipo_prod` (
  `id_tipoProd1` int(11) NOT NULL AUTO_INCREMENT,
  `desc_tipoProd1` varchar(30) NOT NULL,
  PRIMARY KEY (`id_tipoProd1`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `tipo_prod`
--

INSERT INTO `tipo_prod` (`id_tipoProd1`, `desc_tipoProd1`) VALUES
(1, 'Comida'),
(2, 'Bebida'),
(3, 'Promocion'),
(4, 'Descuento');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_prod2`
--

CREATE TABLE IF NOT EXISTS `tipo_prod2` (
  `id_tipoProd2` int(11) NOT NULL AUTO_INCREMENT,
  `desc_tipoProd2` varchar(30) NOT NULL,
  `id_tipoProd1` int(11) NOT NULL,
  PRIMARY KEY (`id_tipoProd2`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Volcado de datos para la tabla `tipo_prod2`
--

INSERT INTO `tipo_prod2` (`id_tipoProd2`, `desc_tipoProd2`, `id_tipoProd1`) VALUES
(1, 'Vodka', 2),
(2, 'Fernet', 2),
(3, 'Gancia', 2),
(4, 'Cerveza Industrial', 2),
(5, 'Cerveza Artesanal', 2),
(6, 'Hamburguesa', 1),
(7, 'Pizza', 1),
(8, 'Papas Fritas', 1),
(9, 'Hot Dogs', 1),
(10, 'Promocion', 3),
(11, 'Descuento', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `dni` int(11) NOT NULL,
  `cuenta_user` varchar(20) NOT NULL,
  `contraseña_user` varchar(20) NOT NULL,
  `nombre_user` varchar(20) NOT NULL,
  `apellido_user` varchar(20) NOT NULL,
  `direccion_user` varchar(30) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `id_privilegio` int(11) NOT NULL,
  `habilitado` int(11) NOT NULL,
  PRIMARY KEY (`dni`),
  KEY `direccion_user` (`direccion_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`dni`, `cuenta_user`, `contraseña_user`, `nombre_user`, `apellido_user`, `direccion_user`, `telefono`, `id_privilegio`, `habilitado`) VALUES
(3213213, 'gege', 'gege', 'gegege', 'gegege', 'gege', 'gege', 3, 1),
(11111111, 'admin', 'admin', 'Boston', 'Beer & Co', 'Sixto Ovejero', '1111111', 1, 1),
(12345676, 'prueba', 'prueba', 'Prueba', 'Prueba', 'Prueba', '123145', 2, 0),
(37305672, 'rodrimayer', 'rodri0104', 'Rodrigo', 'Mayer', 'Maipu 76', '3886525523', 1, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
