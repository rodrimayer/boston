-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 08-08-2019 a las 15:19:08
-- Versión del servidor: 5.5.24-log
-- Versión de PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `db_boston`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulo`
--

CREATE TABLE IF NOT EXISTS `articulo` (
  `codigo` varchar(10) NOT NULL,
  `id_tipoProd` int(11) NOT NULL,
  `id_tipoProd2` int(11) NOT NULL,
  `precio_costo` float NOT NULL,
  `precio_vta` float NOT NULL,
  `desc_articulo` varchar(30) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `articulo`
--

INSERT INTO `articulo` (`codigo`, `id_tipoProd`, `id_tipoProd2`, `precio_costo`, `precio_vta`, `desc_articulo`) VALUES
('A', 2, 5, 50, 100, 'Amber'),
('G', 2, 5, 50, 100, 'Golden'),
('H', 2, 5, 50, 100, 'Honey'),
('PF', 1, 7, 100, 230, 'Pizza Fugazzeta');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `barril`
--

CREATE TABLE IF NOT EXISTS `barril` (
  `id_barril` int(11) NOT NULL AUTO_INCREMENT,
  `desc_barril` varchar(20) NOT NULL,
  `cantidad_total` float NOT NULL,
  `cantidad_consumida` float NOT NULL,
  `posicion` int(11) NOT NULL,
  PRIMARY KEY (`id_barril`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `barril`
--

INSERT INTO `barril` (`id_barril`, `desc_barril`, `cantidad_total`, `cantidad_consumida`, `posicion`) VALUES
(1, 'Amber', 30, 30, 1),
(2, 'Golden', 30, 30, 2),
(3, 'Honey', 30, 5, 3),
(4, 'Amber', 30, 30, 2),
(5, 'Amber', 30, 3.5, 1),
(6, 'Golden', 30, 30, 2),
(7, 'Golden', 30, 10, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `boleta`
--

CREATE TABLE IF NOT EXISTS `boleta` (
  `id_boleta` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_boleta` date NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_mozo` int(11) NOT NULL,
  `total` float NOT NULL,
  `cerrada` int(11) NOT NULL,
  PRIMARY KEY (`id_boleta`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `boleta`
--

INSERT INTO `boleta` (`id_boleta`, `fecha_boleta`, `id_cliente`, `id_mozo`, `total`, `cerrada`) VALUES
(1, '2019-08-08', 1, 37305672, 5000, 0),
(2, '2019-08-08', 1, 37305672, 200, 0),
(3, '2019-08-08', 1, 37305672, 500, 0),
(4, '2019-08-08', 1, 37305672, 100, 0),
(5, '2019-08-08', 1, 37305672, 2000, 0),
(6, '2019-08-08', 1, 37305672, 500, 0),
(7, '2019-08-08', 1, 37305672, 500, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
  `id_cliente` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_cliente` varchar(30) NOT NULL,
  `apellido_cliente` varchar(30) NOT NULL,
  `direccion_cliente` varchar(30) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `mail` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_cliente`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37305673 ;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id_cliente`, `nombre_cliente`, `apellido_cliente`, `direccion_cliente`, `telefono`, `mail`) VALUES
(1, 'Consumidor', 'Final', '-', '-', '-'),
(37305672, 'Rodrigo', 'Mayer', 'Maipu 76', '3886525523', 'rodrimmayer@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cond_vta`
--

CREATE TABLE IF NOT EXISTS `cond_vta` (
  `id_condVta` int(11) NOT NULL AUTO_INCREMENT,
  `desc_condVta` varchar(30) NOT NULL,
  PRIMARY KEY (`id_condVta`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `cond_vta`
--

INSERT INTO `cond_vta` (`id_condVta`, `desc_condVta`) VALUES
(1, 'DEBITO'),
(2, 'CREDITO'),
(3, 'CONTADO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracion`
--

CREATE TABLE IF NOT EXISTS `configuracion` (
  `id_configuracion` int(11) NOT NULL AUTO_INCREMENT,
  `desc_configuracion` varchar(25) NOT NULL,
  `valor_configuracion` float NOT NULL,
  PRIMARY KEY (`id_configuracion`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `configuracion`
--

INSERT INTO `configuracion` (`id_configuracion`, `desc_configuracion`, `valor_configuracion`) VALUES
(1, 'Happy Hour', 80);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_mesa`
--

CREATE TABLE IF NOT EXISTS `detalle_mesa` (
  `id_detalleMesa` int(11) NOT NULL AUTO_INCREMENT,
  `id_mesa` int(11) NOT NULL,
  `id_boleta` int(11) NOT NULL,
  `codigo` varchar(11) NOT NULL,
  `cantidad` float NOT NULL,
  `precio` float NOT NULL,
  PRIMARY KEY (`id_detalleMesa`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `detalle_mesa`
--

INSERT INTO `detalle_mesa` (`id_detalleMesa`, `id_mesa`, `id_boleta`, `codigo`, `cantidad`, `precio`) VALUES
(1, 0, 1, 'g', 50, 100),
(2, 0, 2, 'a', 2, 100),
(3, 0, 3, 'h', 5, 100),
(4, 0, 4, 'g', 1, 100),
(5, 0, 5, 'g', 20, 100),
(6, 0, 6, 'h', 5, 100),
(7, 0, 7, 'a', 5, 100);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturacion_boleta`
--

CREATE TABLE IF NOT EXISTS `facturacion_boleta` (
  `id_factBoleta` int(11) NOT NULL AUTO_INCREMENT,
  `id_boleta` int(11) NOT NULL,
  `id_condVta` int(11) NOT NULL,
  `monto` float NOT NULL,
  PRIMARY KEY (`id_factBoleta`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `facturacion_boleta`
--

INSERT INTO `facturacion_boleta` (`id_factBoleta`, `id_boleta`, `id_condVta`, `monto`) VALUES
(1, 3, 3, 500),
(2, 4, 1, 100),
(3, 5, 1, 2000),
(4, 6, 3, 500),
(5, 7, 1, 500);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mesa`
--

CREATE TABLE IF NOT EXISTS `mesa` (
  `numero_mesa` int(11) NOT NULL,
  `id_mozo` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  PRIMARY KEY (`numero_mesa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `privilegio`
--

CREATE TABLE IF NOT EXISTS `privilegio` (
  `id_privilegio` int(11) NOT NULL AUTO_INCREMENT,
  `desc_privilegio` varchar(20) NOT NULL,
  PRIMARY KEY (`id_privilegio`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `privilegio`
--

INSERT INTO `privilegio` (`id_privilegio`, `desc_privilegio`) VALUES
(1, 'administrador'),
(2, 'vendedor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_prod`
--

CREATE TABLE IF NOT EXISTS `tipo_prod` (
  `id_tipoProd1` int(11) NOT NULL AUTO_INCREMENT,
  `desc_tipoProd1` varchar(30) NOT NULL,
  PRIMARY KEY (`id_tipoProd1`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `tipo_prod`
--

INSERT INTO `tipo_prod` (`id_tipoProd1`, `desc_tipoProd1`) VALUES
(1, 'Comida'),
(2, 'Bebida');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_prod2`
--

CREATE TABLE IF NOT EXISTS `tipo_prod2` (
  `id_tipoProd2` int(11) NOT NULL AUTO_INCREMENT,
  `desc_tipoProd2` varchar(30) NOT NULL,
  `id_tipoProd1` int(11) NOT NULL,
  PRIMARY KEY (`id_tipoProd2`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `tipo_prod2`
--

INSERT INTO `tipo_prod2` (`id_tipoProd2`, `desc_tipoProd2`, `id_tipoProd1`) VALUES
(1, 'Vodka', 2),
(2, 'Fernet', 2),
(3, 'Gancia', 2),
(4, 'Cerveza Industrial', 2),
(5, 'Cerveza Artesanal', 2),
(6, 'Hamburguesa', 1),
(7, 'Pizza', 1),
(8, 'Papas Fritas', 1),
(9, 'Hot Dogs', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `dni` int(11) NOT NULL,
  `cuenta_user` varchar(20) NOT NULL,
  `contraseña_user` varchar(20) NOT NULL,
  `nombre_user` varchar(20) NOT NULL,
  `apellido_user` varchar(20) NOT NULL,
  `direccion_user` varchar(30) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `id_privilegio` int(11) NOT NULL,
  PRIMARY KEY (`dni`),
  KEY `direccion_user` (`direccion_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`dni`, `cuenta_user`, `contraseña_user`, `nombre_user`, `apellido_user`, `direccion_user`, `telefono`, `id_privilegio`) VALUES
(37305672, 'rodrimayer', 'rodri0104', 'Rodrigo', 'Mayer', 'Maipu 76', '3886525523', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
