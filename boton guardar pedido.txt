if (btnGuardarCambios.getText().equals("GUARDAR CAMBIOS")) {
            System.out.println("ENTRO POR AQUIII");
            if (mf.existeMesa(Integer.parseInt(txtMesa.getText())) && Integer.parseInt(txtMesa.getText()) != boleta.getMesa()) {
                JOptionPane.showMessageDialog(null, "LA MESA YA SE ENCUENTRA OCUPADA");
            } else {
                int opcion = JOptionPane.showConfirmDialog(null, "ESTA SEGURO QUE DESEA GUARDAR ESTA BOLETA?");
                if (opcion == 0) {

                    if (t == 1) {//si se acaba de abrir esta mesa
                        String busquedaCliente = this.comboBoxCliente.getEditor().getItem().toString();
                        System.out.println("CLIENTE A BUSCAR: " + busquedaCliente);
                        cli = mcli.obtenerCliente(busquedaCliente);
                        //System.out.println("CLIENTE COMPRADOR: "+cli.getNombre_cliente()+" "+cli.getApellido_cliente());

                        String busquedaMozo = this.comboBoxMozo.getEditor().getItem().toString();
                        user = mu.traerUsuarioString(busquedaMozo);

                        boleta.setCliente(cli);
                        boleta.setFecha_boleta(mfechas.obtenerFechaActual());
                        boleta.setHora_apertura(mfechas.obtenerHoraActual());
                        boleta.setVendedor(user);
                        boleta.setTotal(totalTicket);
                        boleta.setMesa(Integer.parseInt(txtMesa.getText()));
                        boleta.setHora_cierre("00:00:00");

                        mf.insertarBoleta(boleta, 2);
                        boleta.setId_boleta(mf.obtenerID_boleta());
                    } else {
                        if (Integer.parseInt(txtMesa.getText()) != boleta.getMesa()) {
                            mf.actualizarMesa(boleta.getId_boleta(), Integer.parseInt(txtMesa.getText()));
                            System.out.println("SON DISTINTAS");
                        } else {
                            System.out.println("SON IGUALES");
                        }
                    }

                    for (int i = 0; i < this.tablaVentaRapida.getRowCount(); i++) {
                        int id_a_modificar = Integer.parseInt(tablaVentaRapida.getValueAt(i, 0).toString());
                        Detalle_boleta detalleNuevo = new Detalle_boleta();

                        int cantidad = Integer.parseInt(tablaVentaRapida.getValueAt(i, 3).toString());
                        float precio = 0;
                        if (a.getId_tipoProd1() != 4) {
                            precio = Float.parseFloat(this.tablaVentaRapida.getValueAt(i, 4).toString());
                        } else {
                            precio = Float.parseFloat(this.tablaVentaRapida.getValueAt(i, 5).toString());
                            System.out.println("el descuento es de: " + precio);
                        }

                        String codigo = tablaVentaRapida.getValueAt(i, 1).toString();

                        detalleNuevo.setId_boleta(boleta.getId_boleta());
                        detalleNuevo.setPrecio(precio);
                        detalleNuevo.setCantidad(cantidad);
                        detalleNuevo.setId_detalleboleta(id_a_modificar);
                        detalleNuevo.setCodigo(codigo);

                        if (id_a_modificar == 0) {
                            System.out.println("SE DEBE A�ADIR ESTE ARTICULO");
                            mf.insertarDetallesBoleta(detalleNuevo, dB);

                        } else {
                            System.out.println("ESTE ARTICULO SE DEBE ACTUALIZAR");
                            mf.actualizarDetallesBoleta(detalleNuevo, dB);
                        }
                    }

                    if (t == 2) {
                        float acumulado = 0;

                        for (int k = 0; k < this.tablaVentaRapida.getRowCount(); k++) {
                            acumulado = acumulado + Float.parseFloat(this.tablaVentaRapida.getValueAt(k, 5).toString());
                        }

                        mf.actualizarTotalBoleta(boleta.getId_boleta(), acumulado);

                        for (int j = 0; j < dmmo.getRowCount(); j++) {
                            if (Integer.parseInt(dmmo.getValueAt(j, 0).toString()) == boleta.getId_boleta()) {
                                dmmo.setValueAt(acumulado, j, 3);
                                dmmo.setValueAt(txtMesa.getText(), j, 1);
                            }
                        }
                    }

                    mt.traerMesasOcupadas((Inicio.ModeloTablaMesasOcupadas) dmmo);
                    ma.actualizarBarriles(pintasReemplazo, dB);
                    pintasReemplazo.clear();
                    JOptionPane.showMessageDialog(null, "MESA GUARDADA CON EXITO");
                    this.dispose();
                }
            }
        }