SELECT t1.id_tipoProd1, t1.desc_tipoProd1, SUM( d.cantidad * d.precio ) 
FROM Detalle_mesa AS d
JOIN articulo AS a ON a.codigo = d.codigo
JOIN Tipo_Prod AS t1 ON a.id_tipoProd = t1.id_tipoProd1
JOIN Boleta AS b ON b.id_boleta = d.id_boleta
WHERE b.cerrada =0
AND b.pagada =1
GROUP BY t1.id_tipoProd1, t1.desc_tipoProd1


SELECT t2.id_tipoProd2, t2.desc_tipoProd2, SUM( d.cantidad * d.precio ) 
FROM Detalle_mesa AS d
JOIN articulo AS a ON a.codigo = d.codigo
JOIN Tipo_Prod2 AS t2 ON a.id_tipoProd2 = t2.id_tipoProd2
JOIN Boleta AS b ON b.id_boleta = d.id_boleta
WHERE b.cerrada =0
AND b.pagada =1
GROUP BY t2.id_tipoProd2, t2.desc_tipoProd2


SELECT t.id_tipoCosto, t.desc_tipoCosto, c.desc_costo, SUM( c.valor ) 
FROM Costo AS c
JOIN Tipo_Costo AS t ON t.id_tipoCosto = c.id_tipoCosto
WHERE cerrado =0
GROUP BY t.id_tipoCosto, t.desc_tipoCosto, c.desc_costo

SELECT fb.id_condVta, c.desc_condVta, SUM(fb.monto)
FROM Facturacion_boleta as fb
JOIN Cond_Vta as c
on fb.id_condVta = c.id_condVta
group by fb.id_condVta, c.desc_condVta

SELECT fc.id_condVta, c.desc_condVta, SUM(fc.monto)
FROM Facturacion_costo as fc
JOIN Cond_Vta as c
on fc.id_condVta = c.id_condVta
group by fc.id_condVta, c.desc_condVta